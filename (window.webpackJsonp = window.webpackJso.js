(window.webpackJsonp = window.webpackJsonp || []).push([[22], {
    "7qQQ": function(t, e, o) {
        "use strict";
        o.r(e);
        o("fbCW"),
        o("QWBl");
        var a = o("vDqi")
          , s = o.n(a)
          , r = o("EVdn")
          , n = o.n(r)
          , l = (o("2B1R"),
        o("sMBO"),
        {
            name: "tags-picker",
            props: {
                type: {
                    type: String,
                    required: !0
                },
                value: {
                    type: Array,
                    default: function() {
                        return []
                    }
                },
                tags: {
                    type: Array,
                    default: function() {
                        return []
                    }
                },
                autoLoadTags: {
                    type: Boolean,
                    default: !1
                }
            },
            watch: {
                selectedTags: function(t) {
                    this.$emit("input", t)
                },
                value: function(t) {
                    this.selectedTags = t
                }
            },
            data: function() {
                return {
                    selectedTags: [],
                    allTags: []
                }
            },
            mounted: function() {
                var t = this;
                this.selectedTags = this.value,
                this.allTags = this.tags.map((function(t) {
                    return t.name
                }
                )),
                this.autoLoadTags && s.a.get(this.router("api.tag.index", {
                    type: this.type
                })).then((function(e) {
                    t.allTags = e.data.map((function(t) {
                        return t.name
                    }
                    ))
                }
                ))
            },
            methods: {
                addTag: function(t) {
                    var e = this;
                    s.a.post(this.router("api.tag.store"), {
                        name: t,
                        type: this.type
                    }).then((function(o) {
                        e.allTags.push(t),
                        e.selectedTags.push(t)
                    }
                    ), (function(t) {
                        t.response && 422 === t.response.status ? window.SwalTimer.fire({
                            icon: "error",
                            title: t.response.data.name[0]
                        }) : window.SwalTimer.fire({
                            icon: "error",
                            title: "Add tag failed",
                            text: "Please try again."
                        })
                    }
                    ))
                }
            }
        })
          , i = o("KHd+")
          , d = Object(i.a)(l, (function() {
            var t = this
              , e = t.$createElement;
            return (t._self._c || e)("multiselect", {
                attrs: {
                    multiple: !0,
                    placeholder: "Enter Tag",
                    "select-label": "",
                    "deselect-label": "Click to remove",
                    "tag-placeholder": "Add as new tag",
                    options: t.allTags,
                    searchable: !0,
                    "internal-search": !0,
                    "options-limit": 300,
                    "max-height": 600,
                    "show-no-options": !1,
                    taggable: !0
                },
                on: {
                    tag: t.addTag
                },
                model: {
                    value: t.selectedTags,
                    callback: function(e) {
                        t.selectedTags = e
                    },
                    expression: "selectedTags"
                }
            })
        }
        ), [], !1, null, "165c918e", null).exports
          , c = (o("piMb"),
        {
            name: "MultiVideoPicker",
            props: {
                value: {
                    type: Array,
                    default: function() {
                        return []
                    }
                }
            },
            data: function() {
                return {
                    selectedVideos: [],
                    loadingVideos: !1,
                    videos: []
                }
            },
            watch: {
                selectedVideos: function(t) {
                    if (null !== this.value) {
                        var e = t.map((function(t) {
                            return t.id
                        }
                        ))
                          , o = JSON.parse(JSON.stringify(this.value));
                        e.length === o.length && e.sort().every((function(t, e) {
                            return t === o.sort()[e]
                        }
                        )) || this.$emit("input", e)
                    } else
                        this.$emit("input", [])
                },
                value: function(t) {
                    this.loadVideoFromIds(t)
                }
            },
            mounted: function() {
                null !== this.value ? this.value && this.loadVideoFromIds(this.value) : this.$emit("input", [])
            },
            methods: {
                searchVideo: function(t) {
                    var e = this;
                    t && (this.loadingVideos = !0,
                    s.a.get(this.router("api.video.index", {
                        search: t,
                        email_html: 0,
                        scope: this.constants.GLOBAL.SCOPE_ALL,
                        page: 1
                    })).then((function(t) {
                        e.loadingVideos = !1,
                        e.videos = t.data.data
                    }
                    )))
                },
                loadVideoFromIds: function(t) {
                    var e = this;
                    if (0 === t.length)
                        return this.videos = [],
                        void (this.selectedVideos = []);
                    s.a.get(this.router("api.video.index", {
                        ids: t,
                        email_html: 0,
                        scope: this.constants.GLOBAL.SCOPE_ALL,
                        page: 1
                    })).then((function(t) {
                        e.videos = t.data.data,
                        e.selectedVideos = t.data.data
                    }
                    ))
                }
            }
        })
          , m = Object(i.a)(c, (function() {
            var t = this
              , e = t.$createElement
              , o = t._self._c || e;
            return o("multiselect", {
                attrs: {
                    multiple: !0,
                    placeholder: "Search Video",
                    "track-by": "id",
                    label: "title",
                    "select-label": "",
                    options: t.videos,
                    "deselect-label": "Click to remove",
                    searchable: !0,
                    "internal-search": !1,
                    "options-limit": 300,
                    "max-height": 600,
                    "show-no-options": !1,
                    loading: t.loadingVideos
                },
                on: {
                    "search-change": t.searchVideo
                },
                scopedSlots: t._u([{
                    key: "singleLabel",
                    fn: function(e) {
                        return [o("span", {
                            staticClass: "option__desc"
                        }, [o("span", {
                            staticClass: "option__title"
                        }, [t._v(t._s(e.option.title))])])]
                    }
                }, {
                    key: "option",
                    fn: function(e) {
                        return [o("div", {
                            attrs: {
                                flex: "",
                                row: "",
                                layout: "start center"
                            }
                        }, [o("div", {
                            staticClass: "text-center m-r-lg",
                            staticStyle: {
                                width: "100px"
                            }
                        }, [o("img", {
                            staticClass: "option__image",
                            staticStyle: {
                                "max-height": "100px",
                                "max-width": "100px"
                            },
                            attrs: {
                                src: e.option.preview_path
                            }
                        })]), t._v(" "), o("span", {
                            staticClass: "option__desc"
                        }, [o("span", {
                            staticClass: "option__title"
                        }, [t._v(t._s(e.option.title))])])])]
                    }
                }]),
                model: {
                    value: t.selectedVideos,
                    callback: function(e) {
                        t.selectedVideos = e
                    },
                    expression: "selectedVideos"
                }
            })
        }
        ), [], !1, null, "1ff52671", null).exports
          , u = (o("TeQF"),
        o("yXV3"),
        o("Y+p1"))
          , p = o.n(u)
          , _ = {
            name: "cta-picker",
            props: {
                value: {
                    type: Array,
                    default: function() {
                        return []
                    }
                }
            },
            data: function() {
                return {
                    selectedCtas: [],
                    allCtas: []
                }
            },
            watch: {
                selectedCtas: function(t) {
                    var e = t.map((function(t) {
                        return t.id
                    }
                    ));
                    p()(e, this.value) || this.$emit("input", e)
                },
                value: function(t) {
                    this.allCtas.length > 0 && this.updateSelectedCtas(t)
                }
            },
            mounted: function() {
                var t = this;
                s.a.get(this.router("api.call_to_action.index")).then((function(e) {
                    t.allCtas = e.data,
                    t.updateSelectedCtas(t.value)
                }
                ))
            },
            methods: {
                updateSelectedCtas: function(t) {
                    var e = t.map((function(t) {
                        return parseInt(t)
                    }
                    ));
                    this.selectedCtas = this.allCtas.filter((function(t) {
                        return e.indexOf(parseInt(t.id)) >= 0
                    }
                    ))
                }
            }
        }
          , f = Object(i.a)(_, (function() {
            var t = this
              , e = t.$createElement;
            return (t._self._c || e)("multiselect", {
                attrs: {
                    multiple: !0,
                    label: "name",
                    "track-by": "id",
                    placeholder: "Enter CTA Name",
                    "select-label": "",
                    "deselect-label": "Click to remove",
                    options: t.allCtas,
                    searchable: !0,
                    "internal-search": !0,
                    "options-limit": 300,
                    "max-height": 600,
                    "show-no-options": !1
                },
                model: {
                    value: t.selectedCtas,
                    callback: function(e) {
                        t.selectedCtas = e
                    },
                    expression: "selectedCtas"
                }
            })
        }
        ), [], !1, null, "7ffc66f2", null).exports
          , v = (o("ma9I"),
        o("x0AG"),
        o("oVuX"),
        o("pDQq"),
        o("FZtP"),
        o("2OPs"))
          , h = o.n(v)
          , g = o("LvDl")
          , w = o.n(g)
          , C = {
            data: function() {
                return {
                    nodes: [],
                    treeNode: null,
                    diagram: null
                }
            },
            methods: {
                initTreeNode: function() {
                    var t = w.a.find(this.nodes, {
                        previous_node_id: null
                    });
                    t ? (this.buildTreeNode(t, this.nodes),
                    this.treeNode = t) : this.treeNode = null
                },
                buildTreeNode: function(t, e) {
                    var o = this;
                    t.children = w.a.filter(e, {
                        previous_node_id: t.id
                    }),
                    t.children.forEach((function(t) {
                        o.buildTreeNode(t, e)
                    }
                    ))
                },
                initBuilder: function() {
                    var t = this;
                    this.diagram && this.diagram.clean(),
                    window.workflow_node_click = function(e, o) {
                        t.$refs.nodeEditor.showEditNode(w.a.find(t.nodes, {
                            id: parseInt(o.params.id)
                        }))
                    }
                    ,
                    window.workflow_add_node = function(e, o) {
                        o.params.id ? t.$refs.nodeEditor.showAddNode(parseInt(o.params.id), o.params.cond ? o.params.cond : null) : t.$refs.nodeEditor.showAddNode(null)
                    }
                    ;
                    var e = "start=>start: Workflow Triggered\n";
                    if (this.treeNode) {
                        var o = function e(o) {
                            var a, s = "node_".concat(o.id), r = t.maps.App_Models_WorkflowNode.actionMap[o.action];
                            switch (o.action) {
                            case t.constants.App_Models_WorkflowNode.ACTION_ADD_TAG:
                            case t.constants.App_Models_WorkflowNode.ACTION_REMOVE_TAG:
                            case t.constants.App_Models_WorkflowNode.ACTION_SEND_CAMPAIGN:
                            case t.constants.App_Models_WorkflowNode.ACTION_DELAY:
                            case t.constants.App_Models_WorkflowNode.ACTION_CREATE_TASK:
                                a = "operation";
                                break;
                            case t.constants.App_Models_WorkflowNode.ACTION_CONDITION:
                                a = "condition"
                            }
                            var n = ["".concat(s, "(id=").concat(o.id, ")=>").concat(a, ": ").concat(r, "|").concat(o.action, ":$workflow_node_click")]
                              , l = [];
                            if (o.action === t.constants.App_Models_WorkflowNode.ACTION_CONDITION) {
                                o.children || (o.children = []);
                                for (var i = function(t) {
                                    var a;
                                    if (o.data[t] && (a = w.a.find(o.children, {
                                        id: o.data[t]
                                    })),
                                    a) {
                                        var r = e(a);
                                        return r.links.push("".concat(s, "(").concat(t, ")->").concat(r.nodeName)),
                                        r
                                    }
                                    return {
                                        symbols: ["add_".concat(s, "_").concat(t, "(id=").concat(o.id, ",cond=").concat(t, ")=>end: Add ").concat(t, " branch|add_node:$workflow_add_node")],
                                        links: ["".concat(s, "(").concat(t, ")->add_").concat(s, "_").concat(t)]
                                    }
                                }, d = 0, c = ["yes", "no"]; d < c.length; d++) {
                                    var m = i(c[d]);
                                    n = n.concat(m.symbols),
                                    l = l.concat(m.links)
                                }
                            } else
                                o.children && o.children.length > 0 ? o.children.forEach((function(t) {
                                    var o = e(t);
                                    n = n.concat(o.symbols),
                                    l.push("".concat(s, "->").concat(o.nodeName)),
                                    l = l.concat(o.links)
                                }
                                )) : (n.push("add_".concat(s, "(id=").concat(o.id, ")=>end: Add new action|add_node:$workflow_add_node")),
                                l.push("".concat(s, "->add_").concat(s)));
                            return {
                                nodeName: s,
                                symbols: n,
                                links: l,
                                node: o
                            }
                        }(this.treeNode);
                        e += o.symbols.join("\n") + "\n",
                        e += "start->".concat(o.nodeName, "\n"),
                        e += o.links.join("\n")
                    } else
                        e += "add_first_node=>end: Add Node|add_node:$workflow_add_node\n",
                        e += "start->add_first_node\n";
                    this.diagram = h.a.parse(e),
                    this.diagram.drawSVG(this.$refs.builder, {
                        "line-length": 20,
                        "line-color": "#555",
                        "font-color": "#555",
                        "element-color": "#555"
                    })
                },
                onNodeDeleted: function(t) {
                    var e = w.a.findIndex(this.nodes, {
                        id: t
                    });
                    this.nodes.splice(e, 1),
                    this.initTreeNode(),
                    this.initBuilder()
                },
                onNodeAdded: function(t, e) {
                    if (this.nodes.push(t),
                    t.previous_node_id && e) {
                        var o = w.a.find(this.nodes, {
                            id: t.previous_node_id
                        });
                        o && o.action === this.constants.App_Models_WorkflowNode.ACTION_CONDITION && (o.data[e] = t.id)
                    }
                    this.initTreeNode(),
                    this.initBuilder()
                },
                onNodeUpdated: function(t) {
                    var e = w.a.findIndex(this.nodes, {
                        id: t.id
                    });
                    e >= 0 && this.nodes.splice(e, 1, t),
                    this.initTreeNode(),
                    this.initBuilder()
                }
            }
        }
          , k = {
            name: "contacts",
            props: {
                workflow: {
                    required: !0
                }
            },
            data: function() {
                return {
                    contacts: {
                        data: [],
                        meta: {}
                    },
                    loadingContacts: !1
                }
            },
            mounted: function() {
                this.loadContacts()
            },
            methods: {
                loadContacts: function() {
                    var t = this
                      , e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 1;
                    this.loadingContacts = !0,
                    s.a.get(this.router("api.workflow.contacts", {
                        workflow: this.workflow.id,
                        page: e
                    })).then((function(e) {
                        t.contacts = e.data,
                        t.loadingContacts = !1
                    }
                    ))
                },
                stop: function(t) {
                    var e = this;
                    window.SwalConfirm.fire({
                        icon: "warning",
                        title: "Are you sure?",
                        text: "Workflow for this contact will be stopped.",
                        confirmButtonText: "Yes, stop it!"
                    }).then((function(o) {
                        o.value && s.a.post(e.router("api.workflow.contact.stop", {
                            workflow: e.workflow.id,
                            contact: t.id
                        })).then((function() {
                            e.loadContacts(e.contacts.meta.current_page),
                            window.SwalTimer.fire("Workflow for this contact has been stopped", "", "success")
                        }
                        ))
                    }
                    ))
                }
            }
        }
          , b = Object(i.a)(k, (function() {
            var t = this
              , e = t.$createElement
              , o = t._self._c || e;
            return o("div", {
                staticClass: "portlet light bordered"
            }, [t._m(0), t._v(" "), o("div", {
                staticClass: "portlet-body"
            }, [t.loadingContacts ? o("div", {
                staticClass: "text-center"
            }, [o("i", {
                staticClass: "fal fa-spin fa-spinner"
            }), t._v(" Loading")]) : t._e(), t._v(" "), t.loadingContacts || 0 !== t.contacts.data.length ? t._e() : o("div", [t._v("There is no contact triggered this workflow yet.")]), t._v(" "), !t.loadingContacts && t.contacts.data.length > 0 ? o("div", {
                staticClass: "row mt-comments"
            }, t._l(t.contacts.data, (function(e) {
                return o("div", {
                    staticClass: "col-md-4"
                }, [o("div", {
                    staticClass: "mt-comment"
                }, [o("div", {
                    staticClass: "mt-comment-img"
                }, [o("img", {
                    attrs: {
                        src: e.contact.avatar_url
                    }
                })]), t._v(" "), o("div", {
                    staticClass: "mt-comment-body"
                }, [o("div", {
                    staticClass: "mt-comment-info"
                }, [o("span", {
                    staticClass: "mt-comment-author"
                }, [t._v(t._s(e.contact.display_name))])]), t._v(" "), o("div", {
                    staticClass: "mt-comment-text"
                }, [o("div", {
                    staticClass: "m-b-xs"
                }, [t._v("Triggered at " + t._s(t._f("relative")(e.created_at)))]), t._v(" "), o("div", {
                    staticClass: "m-b-xs"
                }, [e.status === t.constants.App_Models_ContactWorkflow.STATUS_STOPPED ? o("span", {
                    staticClass: "badge badge-danger"
                }, [t._v("STOPPED")]) : t._e(), t._v(" "), e.status === t.constants.App_Models_ContactWorkflow.STATUS_RUNNING ? o("button", {
                    staticClass: "btn btn-xs red",
                    attrs: {
                        type: "button"
                    },
                    on: {
                        click: function(o) {
                            return t.stop(e.contact)
                        }
                    }
                }, [t._v("Stop Workflow")]) : t._e()])])])])])
            }
            )), 0) : t._e(), t._v(" "), o("div", {
                staticClass: "row"
            }, [t.contacts.meta.last_page > 1 ? o("div", {
                staticClass: "pull-right"
            }, [o("paginator", {
                attrs: {
                    pager: t.contacts.meta,
                    func: t.loadContacts
                }
            })], 1) : t._e()])])])
        }
        ), [function() {
            var t = this.$createElement
              , e = this._self._c || t;
            return e("div", {
                staticClass: "portlet-title"
            }, [e("div", {
                staticClass: "caption font-dubb"
            }, [e("span", {
                staticClass: "caption-subject bold uppercase"
            }, [this._v("Contacts")])])])
        }
        ], !1, null, "5dacf458", null).exports
          , F = o("oCYn")
          , y = o("en44")
          , A = {
            name: "campaign-picker",
            props: {
                value: {
                    default: null
                }
            },
            data: function() {
                return {
                    loading: !1,
                    selectedCampaign: null,
                    campaigns: []
                }
            },
            watch: {
                selectedCampaign: function(t) {
                    t && t.id === this.value || this.$emit("input", t ? t.id : null)
                },
                value: function(t) {
                    this.loadCampaign(t)
                }
            },
            mounted: function() {
                this.searchCampaigns("")
            },
            methods: {
                label: function(t) {
                    return "".concat(t.title, " [").concat(this.maps.App_Models_Campaign.typesMap[t.type], "]")
                },
                searchCampaigns: function(t) {
                    var e = this;
                    s.a.get(this.router("api.campaign.index", {
                        is_workflow: 1,
                        search: t
                    })).then((function(t) {
                        e.campaigns = t.data.data
                    }
                    ))
                },
                loadCampaign: function(t) {
                    var e = this;
                    if (!t)
                        return this.campaigns = [],
                        void (this.selectedCampaign = null);
                    s.a.get(this.router("api.campaign.index", {
                        is_workflow: 1,
                        ids: [t]
                    })).then((function(t) {
                        e.campaigns = t.data.data,
                        e.campaigns.length > 0 ? e.selectedCampaign = e.campaigns[0] : e.selectedCampaign = null
                    }
                    ))
                }
            }
        }
          , N = {
            name: "node-editor",
            components: {
                CampaignPicker: Object(i.a)(A, (function() {
                    var t = this
                      , e = t.$createElement;
                    return (t._self._c || e)("multiselect", {
                        attrs: {
                            placeholder: "Search Campaign",
                            "track-by": "id",
                            "custom-label": t.label,
                            "select-label": "",
                            options: t.campaigns,
                            searchable: !0,
                            "internal-search": !1,
                            "options-limit": 300,
                            "max-height": 600,
                            "show-no-options": !1,
                            loading: t.loading
                        },
                        on: {
                            "search-change": t.searchCampaigns
                        },
                        model: {
                            value: t.selectedCampaign,
                            callback: function(e) {
                                t.selectedCampaign = e
                            },
                            expression: "selectedCampaign"
                        }
                    })
                }
                ), [], !1, null, "b88ba8a2", null).exports,
                TeamMemberPicker: y.a,
                MultiVideoPicker: m,
                TagsPicker: d,
                CtaPicker: f
            },
            props: {
                workflow: {
                    required: !0
                }
            },
            data: function() {
                return {
                    selectedAssignee: null,
                    nodeForm: new SparkForm({
                        id: "",
                        action: "",
                        data: {},
                        previous_node_id: null
                    })
                }
            },
            watch: {
                "nodeForm.action": function(t) {
                    switch (t) {
                    case this.constants.App_Models_WorkflowNode.ACTION_DELAY:
                        this.nodeForm.data = {
                            delay_value: "",
                            delay_unit: this.constants.App_Models_WorkflowNode.DELAY_UNIT_MINUTES
                        };
                        break;
                    case this.constants.App_Models_WorkflowNode.ACTION_SEND_CAMPAIGN:
                        this.nodeForm.data = {
                            campaign_id: ""
                        };
                        break;
                    case this.constants.App_Models_WorkflowNode.ACTION_ADD_TAG:
                    case this.constants.App_Models_WorkflowNode.ACTION_REMOVE_TAG:
                        this.nodeForm.data = {
                            tags: []
                        };
                        break;
                    case this.constants.App_Models_WorkflowNode.ACTION_CONDITION:
                        this.nodeForm.data = {
                            type: ""
                        };
                        break;
                    case this.constants.App_Models_WorkflowNode.ACTION_CREATE_TASK:
                        this.nodeForm.data = {
                            title: "",
                            description: "",
                            assignee_id: null,
                            due_value: "",
                            due_unit: this.constants.App_Models_WorkflowNode.DELAY_UNIT_MINUTES
                        }
                    }
                }
            },
            methods: {
                resetNodeForm: function() {
                    this.nodeForm = new SparkForm({
                        id: "",
                        action: "",
                        data: {},
                        previous_node_id: null
                    })
                },
                showAddNode: function() {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null
                      , e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null;
                    this.resetNodeForm(),
                    this.nodeForm.previous_node_id = t,
                    this.nodeForm.yes_no_branch = e,
                    n()(this.$refs.nodeModal).modal()
                },
                showEditNode: function(t) {
                    var e = this;
                    this.resetNodeForm(),
                    this.nodeForm.id = t.id,
                    this.nodeForm.action = t.action,
                    this.nodeForm.previous_node_id = t.previous_node_id,
                    F.default.nextTick((function() {
                        e.nodeForm.data = JSON.parse(JSON.stringify(t.data))
                    }
                    )),
                    n()(this.$refs.nodeModal).modal()
                },
                submitNodeForm: function() {
                    var t = this;
                    this.nodeForm.action === this.constants.App_Models_WorkflowNode.ACTION_CREATE_TASK && this.selectedAssignee && (this.nodeForm.data.assignee_id = this.selectedAssignee.id);
                    var e = this.router("api.workflow.node.store", {
                        workflow: this.workflow.id
                    })
                      , o = "post";
                    this.nodeForm.id && (e = this.router("api.workflow.node.update", {
                        workflow: this.workflow.id,
                        node: this.nodeForm.id
                    }),
                    o = "put"),
                    Spark[o](e, this.nodeForm).then((function(e) {
                        n()(t.$refs.nodeModal).modal("hide"),
                        t.nodeForm.id ? t.$emit("node-updated", e) : t.$emit("node-added", e, t.nodeForm.yes_no_branch)
                    }
                    ))
                },
                deleteNode: function(t) {
                    var e = this;
                    window.SwalConfirm.fire({
                        icon: "warning",
                        title: "Are you sure?",
                        text: "You will not be able to recover this node!",
                        confirmButtonText: "Yes, delete it!"
                    }).then((function(o) {
                        o.value && s.a.delete(e.router("api.workflow.node.destroy", {
                            workflow: e.workflow.id,
                            node: t
                        })).then((function() {
                            n()(e.$refs.nodeModal).modal("hide"),
                            window.SwalTimer.fire({
                                icon: "success",
                                title: "Deleted"
                            }),
                            e.$emit("node-deleted", t)
                        }
                        ))
                    }
                    ))
                },
                resetConditionData: function() {
                    if (this.nodeForm.action === this.constants.App_Models_WorkflowNode.ACTION_CONDITION)
                        switch (this.nodeForm.data.type) {
                        case this.constants.App_Models_WorkflowNode.CONDITION_TYPE_VIDEO_WATCHED:
                            this.nodeForm.data.video_ids = [];
                            break;
                        case this.constants.App_Models_WorkflowNode.CONDITION_TYPE_CTA_CLICKED:
                            this.nodeForm.data.cta_ids = []
                        }
                }
            }
        }
          , T = {
            name: "create-workflow",
            components: {
                Contacts: b,
                CtaPicker: f,
                TagsPicker: d,
                MultiVideoPicker: m,
                NodeEditor: Object(i.a)(N, (function() {
                    var t = this
                      , e = t.$createElement
                      , o = t._self._c || e;
                    return o("div", {
                        ref: "nodeModal",
                        staticClass: "modal fade"
                    }, [o("div", {
                        staticClass: "modal-dialog"
                    }, [o("div", {
                        staticClass: "modal-content"
                    }, [o("div", {
                        staticClass: "modal-header"
                    }, [t._m(0), t._v(" "), o("h4", {
                        staticClass: "modal-title"
                    }, [t._v(t._s(t.nodeForm.id ? "Edit" : "Create") + " Action")])]), t._v(" "), o("div", {
                        staticClass: "modal-body"
                    }, [o("form", {
                        staticClass: "form-horizontal",
                        attrs: {
                            role: "form"
                        }
                    }, [o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("action")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Action")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("select", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: t.nodeForm.action,
                            expression: "nodeForm.action"
                        }],
                        staticClass: "form-control",
                        attrs: {
                            disabled: !!t.nodeForm.id
                        },
                        on: {
                            change: function(e) {
                                var o = Array.prototype.filter.call(e.target.options, (function(t) {
                                    return t.selected
                                }
                                )).map((function(t) {
                                    return "_value"in t ? t._value : t.value
                                }
                                ));
                                t.$set(t.nodeForm, "action", e.target.multiple ? o : o[0])
                            }
                        }
                    }, [o("option", {
                        attrs: {
                            value: ""
                        }
                    }, [t._v("Select Action")]), t._v(" "), t._l(t.maps.App_Models_WorkflowNode.actionMap, (function(e, a) {
                        return o("option", {
                            domProps: {
                                value: a
                            }
                        }, [t._v(t._s(e))])
                    }
                    ))], 2), t._v(" "), t.nodeForm.errors.has("action") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("action")))]) : t._e()])]), t._v(" "), t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_SEND_CAMPAIGN ? o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.campaign_id")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Campaign")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("campaign-picker", {
                        model: {
                            value: t.nodeForm.data.campaign_id,
                            callback: function(e) {
                                t.$set(t.nodeForm.data, "campaign_id", e)
                            },
                            expression: "nodeForm.data.campaign_id"
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.campaign_id") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.campaign_id")))]) : t._e()], 1)]) : t._e(), t._v(" "), t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_DELAY ? o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.delay_value")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Delay")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("input", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: t.nodeForm.data.delay_value,
                            expression: "nodeForm.data.delay_value"
                        }],
                        staticClass: "form-control",
                        attrs: {
                            type: "text",
                            maxlength: "3"
                        },
                        domProps: {
                            value: t.nodeForm.data.delay_value
                        },
                        on: {
                            input: function(e) {
                                e.target.composing || t.$set(t.nodeForm.data, "delay_value", e.target.value)
                            }
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.delay_value") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.delay_value")))]) : t._e()])]) : t._e(), t._v(" "), t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_DELAY ? o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.delay_unit")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("select", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: t.nodeForm.data.delay_unit,
                            expression: "nodeForm.data.delay_unit"
                        }],
                        staticClass: "form-control",
                        on: {
                            change: function(e) {
                                var o = Array.prototype.filter.call(e.target.options, (function(t) {
                                    return t.selected
                                }
                                )).map((function(t) {
                                    return "_value"in t ? t._value : t.value
                                }
                                ));
                                t.$set(t.nodeForm.data, "delay_unit", e.target.multiple ? o : o[0])
                            }
                        }
                    }, [o("option", {
                        attrs: {
                            value: ""
                        }
                    }, [t._v("Select Unit")]), t._v(" "), t._l(t.maps.App_Models_WorkflowNode.delayUnitMap, (function(e, a) {
                        return o("option", {
                            domProps: {
                                value: a
                            }
                        }, [t._v(t._s(e))])
                    }
                    ))], 2), t._v(" "), t.nodeForm.errors.has("data.delay_unit") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.delay_unit")))]) : t._e()])]) : t._e(), t._v(" "), t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_ADD_TAG || t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_REMOVE_TAG ? o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.tags")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Tags")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("tags-picker", {
                        attrs: {
                            type: t.constants.App_Models_Tag.TYPE_CONTACT,
                            "auto-load-tags": !0
                        },
                        model: {
                            value: t.nodeForm.data.tags,
                            callback: function(e) {
                                t.$set(t.nodeForm.data, "tags", e)
                            },
                            expression: "nodeForm.data.tags"
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.tags") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.tags")))]) : t._e()], 1)]) : t._e(), t._v(" "), t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_CONDITION ? o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.type")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Condition")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("select", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: t.nodeForm.data.type,
                            expression: "nodeForm.data.type"
                        }],
                        staticClass: "form-control",
                        on: {
                            change: [function(e) {
                                var o = Array.prototype.filter.call(e.target.options, (function(t) {
                                    return t.selected
                                }
                                )).map((function(t) {
                                    return "_value"in t ? t._value : t.value
                                }
                                ));
                                t.$set(t.nodeForm.data, "type", e.target.multiple ? o : o[0])
                            }
                            , t.resetConditionData]
                        }
                    }, [o("option", {
                        attrs: {
                            value: ""
                        }
                    }, [t._v("Select Condition")]), t._v(" "), t._l(t.maps.App_Models_WorkflowNode.conditionTypeMap, (function(e, a) {
                        return o("option", {
                            domProps: {
                                value: a
                            }
                        }, [t._v(t._s(e))])
                    }
                    ))], 2), t._v(" "), t.nodeForm.errors.has("data.type") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.type")))]) : t._e()])]) : t._e(), t._v(" "), t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_CONDITION && t.nodeForm.data.type === t.constants.App_Models_WorkflowNode.CONDITION_TYPE_VIDEO_WATCHED ? o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.video_ids")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Videos")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("multi-video-picker", {
                        model: {
                            value: t.nodeForm.data.video_ids,
                            callback: function(e) {
                                t.$set(t.nodeForm.data, "video_ids", e)
                            },
                            expression: "nodeForm.data.video_ids"
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.video_ids") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.video_ids")))]) : t._e()], 1)]) : t._e(), t._v(" "), t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_CONDITION && t.nodeForm.data.type === t.constants.App_Models_WorkflowNode.CONDITION_TYPE_CTA_CLICKED ? o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.cta_ids")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Call to Actions")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("cta-picker", {
                        model: {
                            value: t.nodeForm.data.cta_ids,
                            callback: function(e) {
                                t.$set(t.nodeForm.data, "cta_ids", e)
                            },
                            expression: "nodeForm.data.cta_ids"
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.cta_ids") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.cta_ids")))]) : t._e()], 1)]) : t._e(), t._v(" "), t.nodeForm.action === t.constants.App_Models_WorkflowNode.ACTION_CREATE_TASK ? [o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.title")
                        }
                    }, [t._m(1), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("input", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: t.nodeForm.data.title,
                            expression: "nodeForm.data.title"
                        }],
                        staticClass: "form-control",
                        attrs: {
                            type: "text"
                        },
                        domProps: {
                            value: t.nodeForm.data.title
                        },
                        on: {
                            input: function(e) {
                                e.target.composing || t.$set(t.nodeForm.data, "title", e.target.value)
                            }
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.title") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.title")))]) : t._e()])]), t._v(" "), o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.description")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Description")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("textarea", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: t.nodeForm.data.description,
                            expression: "nodeForm.data.description"
                        }],
                        staticClass: "form-control",
                        domProps: {
                            value: t.nodeForm.data.description
                        },
                        on: {
                            input: function(e) {
                                e.target.composing || t.$set(t.nodeForm.data, "description", e.target.value)
                            }
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.description") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.description")))]) : t._e()])]), t._v(" "), o("div", {
                        staticClass: "form-group",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.assignee_id")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Assignee")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("team-member-picker", {
                        attrs: {
                            placeholder: "Type name or email to search"
                        },
                        model: {
                            value: t.selectedAssignee,
                            callback: function(e) {
                                t.selectedAssignee = e
                            },
                            expression: "selectedAssignee"
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.assignee_id") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.taskForm.errors.get("data.assignee_id")))]) : t._e()], 1)]), t._v(" "), o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.due_value")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }, [t._v("Due After")]), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("input", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: t.nodeForm.data.due_value,
                            expression: "nodeForm.data.due_value"
                        }],
                        staticClass: "form-control",
                        attrs: {
                            type: "text",
                            maxlength: "3"
                        },
                        domProps: {
                            value: t.nodeForm.data.due_value
                        },
                        on: {
                            input: function(e) {
                                e.target.composing || t.$set(t.nodeForm.data, "due_value", e.target.value)
                            }
                        }
                    }), t._v(" "), t.nodeForm.errors.has("data.due_value") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.due_value")))]) : t._e()])]), t._v(" "), o("div", {
                        staticClass: "form-group form-md-line-input",
                        class: {
                            "has-error": t.nodeForm.errors.has("data.due_unit")
                        }
                    }, [o("label", {
                        staticClass: "control-label col-lg-3"
                    }), t._v(" "), o("div", {
                        staticClass: "col-lg-6"
                    }, [o("select", {
                        directives: [{
                            name: "model",
                            rawName: "v-model",
                            value: t.nodeForm.data.due_unit,
                            expression: "nodeForm.data.due_unit"
                        }],
                        staticClass: "form-control",
                        on: {
                            change: function(e) {
                                var o = Array.prototype.filter.call(e.target.options, (function(t) {
                                    return t.selected
                                }
                                )).map((function(t) {
                                    return "_value"in t ? t._value : t.value
                                }
                                ));
                                t.$set(t.nodeForm.data, "due_unit", e.target.multiple ? o : o[0])
                            }
                        }
                    }, [o("option", {
                        attrs: {
                            value: ""
                        }
                    }, [t._v("Select Unit")]), t._v(" "), t._l(t.maps.App_Models_WorkflowNode.delayUnitMap, (function(e, a) {
                        return o("option", {
                            domProps: {
                                value: a
                            }
                        }, [t._v(t._s(e))])
                    }
                    ))], 2), t._v(" "), t.nodeForm.errors.has("data.due_unit") ? o("span", {
                        staticClass: "help-block"
                    }, [t._v(t._s(t.nodeForm.errors.get("data.due_unit")))]) : t._e()])])] : t._e()], 2)]), t._v(" "), o("div", {
                        staticClass: "modal-footer"
                    }, [t.nodeForm.id ? o("button", {
                        staticClass: "btn red pull-left",
                        attrs: {
                            type: "button"
                        },
                        on: {
                            click: function(e) {
                                return t.deleteNode(t.nodeForm.id)
                            }
                        }
                    }, [t._v("Delete")]) : t._e(), t._v(" "), o("button", {
                        staticClass: "btn btn-default",
                        attrs: {
                            type: "button",
                            "data-dismiss": "modal"
                        }
                    }, [t._v("Close")]), t._v(" "), o("button", {
                        staticClass: "btn blue",
                        attrs: {
                            type: "button",
                            disabled: t.nodeForm.busy
                        },
                        on: {
                            click: function(e) {
                                return t.submitNodeForm()
                            }
                        }
                    }, [t._v("Submit")])])])])])
                }
                ), [function() {
                    var t = this.$createElement
                      , e = this._self._c || t;
                    return e("button", {
                        staticClass: "close",
                        attrs: {
                            type: "button",
                            "data-dismiss": "modal"
                        }
                    }, [e("span", {
                        attrs: {
                            "aria-hidden": "true"
                        }
                    }, [this._v("×")])])
                }
                , function() {
                    var t = this.$createElement
                      , e = this._self._c || t;
                    return e("label", {
                        staticClass: "control-label col-lg-3"
                    }, [this._v("Title "), e("span", {
                        staticClass: "font-red"
                    }, [this._v("*")])])
                }
                ], !1, null, "7789426a", null).exports
            },
            mixins: [C],
            data: function() {
                return {
                    currentStep: 1,
                    maxStep: 3,
                    loading: !1,
                    workflow: null,
                    workflowForm: new SparkForm({
                        name: "",
                        enabled: !1,
                        one_time_only: !0,
                        trigger: {
                            type: "",
                            operator: "",
                            value: null
                        }
                    }),
                    agree_tos: !0
                }
            },
            methods: {
                onMounted: function() {
                    var t = this;
                    Spark.state.user.current_team_id ? (this.$route.params.slug ? this.loadWorkflow(this.$route.params.slug) : (this.workflow = null,
                    this.nodes = [],
                    this.resetWorkflowForm()),
                    n()(this.$el).find(".form-wizard").bootstrapWizard({
                        onTabShow: function(e, o, a) {
                            t.currentStep = a + 1,
                            2 === t.currentStep && setTimeout((function() {
                                t.initBuilder()
                            }
                            ), 100)
                        },
                        onTabClick: function(e, o, a, s) {
                            if (!t.workflow)
                                return !1;
                            t.$router.push({
                                name: "dashboard.workflow.edit",
                                params: {
                                    slug: t.$route.params.slug,
                                    step: s + 1
                                }
                            })
                        }
                    }),
                    n()(this.$el).find(".form-wizard .steps li").eq(0).find('a[data-toggle="tab"]').tab("show")) : window.SwalTimer.fire({
                        icon: "error",
                        title: "Please create a team first"
                    }).then((function() {
                        location.href = t.router("web.subdomain")
                    }
                    ))
                },
                loadWorkflow: function(t) {
                    var e = this;
                    this.loading = !0,
                    s.a.get(this.router("api.workflow.show", {
                        workflow: t
                    })).then((function(t) {
                        e.workflow = t.data,
                        ["name", "enabled", "one_time_only", "trigger"].forEach((function(o) {
                            e.workflowForm[o] = t.data[o]
                        }
                        )),
                        t.data.trigger || (e.workflowForm.trigger = {
                            type: "",
                            operator: "",
                            value: null
                        }),
                        e.nodes = t.data.nodes,
                        e.initTreeNode(),
                        e.currentStep = e.$route.params.step || 1,
                        n()(e.$el).find(".form-wizard .steps li").eq(e.currentStep - 1).find('a[data-toggle="tab"]').tab("show"),
                        e.loading = !1
                    }
                    ))
                },
                resetWorkflowForm: function() {
                    this.workflowForm = new SparkForm({
                        name: "",
                        enabled: !1,
                        one_time_only: !0,
                        trigger: {
                            type: "",
                            operator: "",
                            value: null
                        }
                    })
                },
                save: function(t, e) {
                    var o = this;
                    if (3 !== this.currentStep || this.agree_tos) {
                        var a = this.router("api.workflow.store")
                          , s = "post";
                        this.workflow && (a = this.router("api.workflow.update", {
                            workflow: this.workflow.id
                        }),
                        s = "put"),
                        Spark[s](a, this.workflowForm).then((function(a) {
                            if (o.workflow || e || o.loadWorkflow(a.id),
                            t)
                                return n()(o.$el).find(".form-wizard .steps li").eq(o.currentStep).find('a[data-toggle="tab"]').tab("show"),
                                void o.$router.push({
                                    name: "dashboard.workflow.edit",
                                    params: {
                                        slug: a.id,
                                        step: o.currentStep
                                    }
                                });
                            e ? o.$router.push({
                                name: "dashboard.workflow.index"
                            }) : window.SwalTimer.fire({
                                icon: "success",
                                title: "Success"
                            })
                        }
                        ))
                    } else
                        window.SwalTimer.fire({
                            icon: "error",
                            title: "Please agree to the TOS"
                        })
                }
            }
        }
          , x = (o("EYye"),
        Object(i.a)(T, (function() {
            var t = this
              , e = t.$createElement
              , o = t._self._c || e;
            return o("feature-locker", {
                attrs: {
                    permission: t.constants.GLOBAL.PERMISSION_AUTOMATION
                },
                on: {
                    mounted: function(e) {
                        return t.onMounted()
                    }
                }
            }, [o("div", {
                staticClass: "page-bar",
                staticStyle: {
                    border: "0"
                }
            }, [o("ul", {
                staticClass: "page-breadcrumb"
            }, [o("li", [o("router-link", {
                attrs: {
                    to: {
                        name: "dashboard.workflow.index"
                    }
                }
            }, [t._v("Automation")])], 1), t._v(" "), o("li", [o("i", {
                staticClass: "fa fa-chevron-right"
            }), t._v(" "), o("span", [t._v(t._s(t.workflow ? "Edit" : "Create") + " Workflow")])])])]), t._v(" "), o("div", {
                staticClass: "portlet light bordered"
            }, [o("div", {
                staticClass: "portlet-title"
            }, [o("div", {
                staticClass: "caption font-dubb"
            }, [o("span", {
                staticClass: "caption-subject bold uppercase"
            }, [t._v(t._s(t.workflow ? "Edit" : "Create") + " Workflow")])])]), t._v(" "), o("div", {
                staticClass: "portlet-body"
            }, [t.loading ? o("div", {
                staticClass: "text-center p-t-lg p-b-lg"
            }, [o("i", {
                staticClass: "fal fa-spinner fa-spin fa-4x"
            })]) : t._e(), t._v(" "), o("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: !t.loading,
                    expression: "!loading"
                }],
                staticClass: "form-wizard"
            }, [o("div", {
                staticClass: "form-body"
            }, [o("ul", {
                staticClass: "nav nav-pills nav-justified steps"
            }, [o("li", {
                staticClass: "active"
            }, [o("a", {
                staticClass: "uppercase step",
                attrs: {
                    href: "#tab_workflow_configuration",
                    "data-toggle": "tab",
                    "aria-expanded": "false"
                }
            }, [o("span", {
                staticClass: "number"
            }, [t._v(" 1 ")]), t._v(" "), o("span", {
                staticClass: "desc"
            }, [t._v("Setup")])])]), t._v(" "), o("li", [o("a", {
                staticClass: "uppercase step",
                attrs: {
                    href: "#tab_workflow_builder",
                    "data-toggle": "tab",
                    "aria-expanded": "false"
                }
            }, [o("span", {
                staticClass: "number"
            }, [t._v(" 2 ")]), t._v(" "), o("span", {
                staticClass: "desc"
            }, [t._v("Build")])])]), t._v(" "), o("li", [o("a", {
                staticClass: "uppercase step",
                attrs: {
                    href: "#tab_workflow_confirm",
                    "data-toggle": "tab",
                    "aria-expanded": "false"
                }
            }, [o("span", {
                staticClass: "number"
            }, [t._v(" 3 ")]), t._v(" "), o("span", {
                staticClass: "desc"
            }, [t._v("Launch")])])])]), t._v(" "), o("div", {
                staticClass: "tab-content"
            }, [o("div", {
                staticClass: "tab-pane p-b-lg active",
                attrs: {
                    id: "tab_workflow_configuration"
                }
            }, [o("form", {
                staticClass: "form-horizontal",
                attrs: {
                    role: "form"
                }
            }, [o("div", {
                staticClass: "form-group form-md-line-input",
                class: {
                    "has-error": t.workflowForm.errors.has("name")
                }
            }, [o("label", {
                staticClass: "control-label col-lg-3"
            }, [t._v("\n                    Workflow Name\n                    "), o("span", {
                staticClass: "hint--bottom hint--large hint--rounded",
                attrs: {
                    "aria-label": "Create a unique title for your workflow for easy reference."
                }
            }, [o("i", {
                staticClass: "fad fa-info-circle"
            })])]), t._v(" "), o("div", {
                staticClass: "col-lg-6"
            }, [o("input", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.workflowForm.name,
                    expression: "workflowForm.name"
                }],
                staticClass: "form-control",
                attrs: {
                    type: "text"
                },
                domProps: {
                    value: t.workflowForm.name
                },
                on: {
                    input: function(e) {
                        e.target.composing || t.$set(t.workflowForm, "name", e.target.value)
                    }
                }
            }), t._v(" "), t.workflowForm.errors.has("name") ? o("span", {
                staticClass: "help-block"
            }, [t._v(t._s(t.workflowForm.errors.get("name")))]) : t._e()])]), t._v(" "), o("div", {
                staticClass: "form-group form-md-line-input",
                class: {
                    "has-error": t.workflowForm.errors.has("trigger.type")
                }
            }, [o("label", {
                staticClass: "control-label col-lg-3"
            }, [t._v("\n                    Trigger\n                    "), o("span", {
                staticClass: "hint--bottom hint--large hint--rounded",
                attrs: {
                    "aria-label": "Specify one or more tags that activate this workflow"
                }
            }, [o("i", {
                staticClass: "fad fa-info-circle"
            })])]), t._v(" "), o("div", {
                staticClass: "col-lg-6"
            }, [o("select", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.workflowForm.trigger.type,
                    expression: "workflowForm.trigger.type"
                }],
                staticClass: "form-control",
                on: {
                    change: function(e) {
                        var o = Array.prototype.filter.call(e.target.options, (function(t) {
                            return t.selected
                        }
                        )).map((function(t) {
                            return "_value"in t ? t._value : t.value
                        }
                        ));
                        t.$set(t.workflowForm.trigger, "type", e.target.multiple ? o : o[0])
                    }
                }
            }, [o("option", {
                attrs: {
                    value: ""
                }
            }, [t._v("Select Type")]), t._v(" "), t._l(t.maps.App_Models_Workflow.triggerTypeMap, (function(e, a) {
                return o("option", {
                    domProps: {
                        value: a
                    }
                }, [t._v(t._s(e))])
            }
            ))], 2), t._v(" "), t.workflowForm.errors.has("trigger.type") ? o("span", {
                staticClass: "help-block"
            }, [t._v(t._s(t.workflowForm.errors.get("trigger.type")))]) : t._e()])]), t._v(" "), t.workflowForm.trigger.type ? o("div", {
                staticClass: "form-group form-md-line-input",
                class: {
                    "has-error": t.workflowForm.errors.has("trigger.operator")
                }
            }, [o("label", {
                staticClass: "control-label col-lg-3"
            }), t._v(" "), o("div", {
                staticClass: "col-lg-6"
            }, [o("select", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.workflowForm.trigger.operator,
                    expression: "workflowForm.trigger.operator"
                }],
                staticClass: "form-control",
                on: {
                    change: function(e) {
                        var o = Array.prototype.filter.call(e.target.options, (function(t) {
                            return t.selected
                        }
                        )).map((function(t) {
                            return "_value"in t ? t._value : t.value
                        }
                        ));
                        t.$set(t.workflowForm.trigger, "operator", e.target.multiple ? o : o[0])
                    }
                }
            }, [o("option", {
                attrs: {
                    value: ""
                }
            }, [t._v("Select Condition")]), t._v(" "), t._l(t.maps.App_Models_Workflow.triggerTypeOperatorMap[t.workflowForm.trigger.type], (function(e) {
                return o("option", {
                    domProps: {
                        value: e
                    }
                }, [t._v(t._s(t.maps.App_Models_Workflow.triggerOperatorMap[e]))])
            }
            ))], 2), t._v(" "), t.workflowForm.errors.has("trigger.operator") ? o("span", {
                staticClass: "help-block"
            }, [t._v(t._s(t.workflowForm.errors.get("trigger.operator")))]) : t._e()])]) : t._e(), t._v(" "), t.workflowForm.trigger.type === t.constants.App_Models_Workflow.TRIGGER_TYPE_TAG_ADDED ? o("div", {
                staticClass: "form-group form-md-line-input",
                class: {
                    "has-error": t.workflowForm.errors.has("trigger.value")
                }
            }, [o("label", {
                staticClass: "control-label col-lg-3"
            }), t._v(" "), o("div", {
                staticClass: "col-lg-6"
            }, [o("tags-picker", {
                attrs: {
                    type: t.constants.App_Models_Tag.TYPE_CONTACT,
                    "auto-load-tags": !0
                },
                model: {
                    value: t.workflowForm.trigger.value,
                    callback: function(e) {
                        t.$set(t.workflowForm.trigger, "value", e)
                    },
                    expression: "workflowForm.trigger.value"
                }
            }), t._v(" "), t.workflowForm.errors.has("trigger.value") ? o("span", {
                staticClass: "help-block"
            }, [t._v(t._s(t.workflowForm.errors.get("trigger.value")))]) : o("span", {
                staticClass: "help-block"
            }, [t._v("Tag must be added to a contact after the workflow is turned on in order to trigger the workflow")])], 1)]) : t._e(), t._v(" "), t.workflowForm.trigger.type === t.constants.App_Models_Workflow.TRIGGER_TYPE_VIDEO_WATCHED ? o("div", {
                staticClass: "form-group form-md-line-input",
                class: {
                    "has-error": t.workflowForm.errors.has("trigger.value")
                }
            }, [o("label", {
                staticClass: "control-label col-lg-3"
            }), t._v(" "), o("div", {
                staticClass: "col-lg-6"
            }, [o("multi-video-picker", {
                model: {
                    value: t.workflowForm.trigger.value,
                    callback: function(e) {
                        t.$set(t.workflowForm.trigger, "value", e)
                    },
                    expression: "workflowForm.trigger.value"
                }
            }), t._v(" "), t.workflowForm.errors.has("trigger.value") ? o("span", {
                staticClass: "help-block"
            }, [t._v(t._s(t.workflowForm.errors.get("trigger.value")))]) : t._e()], 1)]) : t._e(), t._v(" "), t.workflowForm.trigger.type === t.constants.App_Models_Workflow.TRIGGER_TYPE_CTA_CLICKED ? o("div", {
                staticClass: "form-group form-md-line-input",
                class: {
                    "has-error": t.workflowForm.errors.has("trigger.value")
                }
            }, [o("label", {
                staticClass: "control-label col-lg-3"
            }), t._v(" "), o("div", {
                staticClass: "col-lg-6"
            }, [o("cta-picker", {
                model: {
                    value: t.workflowForm.trigger.value,
                    callback: function(e) {
                        t.$set(t.workflowForm.trigger, "value", e)
                    },
                    expression: "workflowForm.trigger.value"
                }
            }), t._v(" "), t.workflowForm.errors.has("trigger.value") ? o("span", {
                staticClass: "help-block"
            }, [t._v(t._s(t.workflowForm.errors.get("trigger.value")))]) : t._e()], 1)]) : t._e()])]), t._v(" "), o("div", {
                staticClass: "tab-pane",
                attrs: {
                    id: "tab_workflow_builder"
                }
            }, [o("div", {
                ref: "builder",
                staticClass: "text-center"
            })]), t._v(" "), o("div", {
                staticClass: "tab-pane",
                attrs: {
                    id: "tab_workflow_confirm"
                }
            }, [o("form", {
                staticClass: "form-horizontal",
                attrs: {
                    role: "form"
                }
            }, [o("div", {
                staticClass: "form-group"
            }, [o("div", {
                staticClass: "col-lg-6 col-lg-offset-3",
                attrs: {
                    flex: "",
                    row: "",
                    layout: "start center"
                }
            }, [o("div", {
                staticClass: "m-l-lg m-r-lg"
            }, [o("input", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.workflowForm.one_time_only,
                    expression: "workflowForm.one_time_only"
                }],
                staticClass: "tgl tgl-ios",
                attrs: {
                    id: "toggle-one-time-only-workflow",
                    type: "checkbox"
                },
                domProps: {
                    checked: Array.isArray(t.workflowForm.one_time_only) ? t._i(t.workflowForm.one_time_only, null) > -1 : t.workflowForm.one_time_only
                },
                on: {
                    change: function(e) {
                        var o = t.workflowForm.one_time_only
                          , a = e.target
                          , s = !!a.checked;
                        if (Array.isArray(o)) {
                            var r = t._i(o, null);
                            a.checked ? r < 0 && t.$set(t.workflowForm, "one_time_only", o.concat([null])) : r > -1 && t.$set(t.workflowForm, "one_time_only", o.slice(0, r).concat(o.slice(r + 1)))
                        } else
                            t.$set(t.workflowForm, "one_time_only", s)
                    }
                }
            }), t._v(" "), o("label", {
                staticClass: "tgl-btn",
                attrs: {
                    for: "toggle-one-time-only-workflow"
                }
            })]), t._v(" "), o("div", [t._v("\n                      Only trigger contacts once\n                      "), o("span", {
                staticClass: "hint--bottom hint--large hint--rounded",
                attrs: {
                    "aria-label": "Switch on to activate contacts only one time per workflow. This will ensure that your contacts don’t receive more than one email."
                }
            }, [o("i", {
                staticClass: "fad fa-info-circle"
            })])])])]), t._v(" "), o("div", {
                staticClass: "form-group"
            }, [o("div", {
                staticClass: "col-lg-6 col-lg-offset-3",
                attrs: {
                    flex: "",
                    row: "",
                    layout: "start center"
                }
            }, [o("div", {
                staticClass: "m-l-lg m-r-lg"
            }, [o("input", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.workflowForm.enabled,
                    expression: "workflowForm.enabled"
                }],
                staticClass: "tgl tgl-ios",
                attrs: {
                    id: "toggle-enable-workflow",
                    type: "checkbox"
                },
                domProps: {
                    checked: Array.isArray(t.workflowForm.enabled) ? t._i(t.workflowForm.enabled, null) > -1 : t.workflowForm.enabled
                },
                on: {
                    change: function(e) {
                        var o = t.workflowForm.enabled
                          , a = e.target
                          , s = !!a.checked;
                        if (Array.isArray(o)) {
                            var r = t._i(o, null);
                            a.checked ? r < 0 && t.$set(t.workflowForm, "enabled", o.concat([null])) : r > -1 && t.$set(t.workflowForm, "enabled", o.slice(0, r).concat(o.slice(r + 1)))
                        } else
                            t.$set(t.workflowForm, "enabled", s)
                    }
                }
            }), t._v(" "), o("label", {
                staticClass: "tgl-btn",
                attrs: {
                    for: "toggle-enable-workflow"
                }
            })]), t._v(" "), o("div", [t._v("\n                      Turn this workflow on\n                      "), o("span", {
                staticClass: "hint--bottom hint--large hint--rounded",
                attrs: {
                    "aria-label": "Switch on to activate this workflow. You can always enable this workflow at a later date from the list menu or by editing the workflow."
                }
            }, [o("i", {
                staticClass: "fad fa-info-circle"
            })])])])]), t._v(" "), o("div", {
                staticClass: "form-group"
            }, [o("div", {
                staticClass: "col-md-offset-3 col-md-6"
            }, [o("div", {
                staticClass: "md-checkbox m-l-lg"
            }, [o("input", {
                directives: [{
                    name: "model",
                    rawName: "v-model",
                    value: t.agree_tos,
                    expression: "agree_tos"
                }],
                staticClass: "md-check",
                attrs: {
                    type: "checkbox",
                    id: "workflow_agree_tos"
                },
                domProps: {
                    checked: Array.isArray(t.agree_tos) ? t._i(t.agree_tos, null) > -1 : t.agree_tos
                },
                on: {
                    change: function(e) {
                        var o = t.agree_tos
                          , a = e.target
                          , s = !!a.checked;
                        if (Array.isArray(o)) {
                            var r = t._i(o, null);
                            a.checked ? r < 0 && (t.agree_tos = o.concat([null])) : r > -1 && (t.agree_tos = o.slice(0, r).concat(o.slice(r + 1)))
                        } else
                            t.agree_tos = s
                    }
                }
            }), t._v(" "), o("label", {
                attrs: {
                    for: "workflow_agree_tos"
                }
            }, [o("span", {
                staticClass: "inc"
            }), t._v(" "), o("span", {
                staticClass: "check"
            }), t._v(" "), o("span", {
                staticClass: "box"
            }), t._v(" I agree to the NO SPAM policy and understand my account can be deleted if I violate the "), o("a", {
                attrs: {
                    href: t.router("web.terms"),
                    target: "_blank"
                }
            }, [t._v("terms of service ")])])])])])])])])]), t._v(" "), o("div", {
                staticClass: "form-actions m-t-lg"
            }, [o("div", {
                staticClass: "row"
            }, [o("div", {
                staticClass: "col-md-offset-3 col-md-6 text-center"
            }, [o("button", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.currentStep < t.maxStep,
                    expression: "currentStep < maxStep"
                }],
                staticClass: "btn purple m-l-lg",
                attrs: {
                    type: "button",
                    disabled: t.workflowForm.busy
                },
                on: {
                    click: function(e) {
                        return t.save(!0, !1)
                    }
                }
            }, [t._v("Save and Continue")]), t._v(" "), o("button", {
                staticClass: "btn grey m-l-lg",
                attrs: {
                    type: "button",
                    disabled: t.workflowForm.busy
                },
                on: {
                    click: function(e) {
                        return t.save(!1, !0)
                    }
                }
            }, [t._v("Save For Later")]), t._v(" "), t.currentStep === t.maxStep ? o("button", {
                staticClass: "btn blue m-l-lg",
                attrs: {
                    type: "button",
                    disabled: t.workflowForm.busy
                },
                on: {
                    click: function(e) {
                        return t.save(!1, !1)
                    }
                }
            }, [t._v("Save")]) : t._e()])])])])])]), t._v(" "), t.workflow ? o("contacts", {
                attrs: {
                    workflow: t.workflow
                }
            }) : t._e(), t._v(" "), t.workflow ? o("node-editor", {
                ref: "nodeEditor",
                attrs: {
                    workflow: t.workflow
                },
                on: {
                    "node-deleted": t.onNodeDeleted,
                    "node-added": t.onNodeAdded,
                    "node-updated": t.onNodeUpdated
                }
            }) : t._e()], 1)
        }
        ), [], !1, null, "b9d53a3c", null));
        e.default = x.exports
    },
    EYye: function(t, e, o) {
        "use strict";
        var a = o("R//a");
        o.n(a).a
    },
    "R//a": function(t, e, o) {},
    en44: function(t, e, o) {
        "use strict";
        var a = {
            name: "team-member-picker",
            props: {
                value: {}
            },
            data: function() {
                return {
                    selectedTeamMember: null
                }
            },
            computed: {
                teamMembers: function() {
                    return this.$store.state.TeamMembers.data
                }
            },
            mounted: function() {
                this.$store.dispatch("TeamMembers/loadData", {}),
                this.selectedTeamMember = this.value
            },
            watch: {
                value: function(t) {
                    this.selectedTeamMember = t
                },
                selectedTeamMember: function(t) {
                    (t && !this.value || !t && this.value || t && this.value && t.id !== this.value.id) && this.$emit("input", t)
                }
            }
        }
          , s = o("KHd+")
          , r = Object(s.a)(a, (function() {
            var t = this
              , e = t.$createElement;
            return (t._self._c || e)("multiselect", {
                attrs: {
                    label: "name",
                    "track-by": "id",
                    placeholder: "Enter name",
                    "select-label": "",
                    "deselect-label": "Click to remove",
                    options: t.teamMembers,
                    searchable: !0,
                    "internal-search": !0,
                    "options-limit": 300,
                    "max-height": 600,
                    "show-no-options": !1
                },
                model: {
                    value: t.selectedTeamMember,
                    callback: function(e) {
                        t.selectedTeamMember = e
                    },
                    expression: "selectedTeamMember"
                }
            })
        }
        ), [], !1, null, "70c57110", null);
        e.a = r.exports
    }
}]);
//# sourceMappingURL=22.b64c0c69.js.map
