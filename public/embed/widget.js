(function () {
    var videoWidget = document.getElementById('fd-widget');
    var videoIframe = document.getElementById('fd-widget-iframe');
    var videoClose = document.getElementById('fd-widget-close');

    // small screen
    if (window.screen && window.screen.width <= 540) {
      if (videoWidget.getAttribute('data-mobile') === 'false') {
        return;
      }
      videoWidget.style.width = '40vw';
    } else {
      videoWidget.style.width = '25vw';
    }
    videoIframe.setAttribute('src', videoIframe.getAttribute('url'));

    videoWidget.style.position = 'fixed';
    switch (videoWidget.getAttribute('data-position')) {
      case 'rightBottom':
        videoWidget.style.right = '20px';
        videoWidget.style.bottom = '20px';
        break;
      case 'leftBottom':
        videoWidget.style.left = '20px';
        videoWidget.style.bottom = '20px';
        break;
      case 'rightTop':
        videoWidget.style.right = '20px';
        videoWidget.style.top = '20px';
        break;
      case 'leftTop':
        videoWidget.style.left = '20px';
        videoWidget.style.top = '20px';
        break;
    }

    videoWidget.style.zIndex = '9999999';
    videoWidget.style.display = 'block';
    videoIframe.style.width = '100%';
    videoClose.style.position = 'absolute';
    videoClose.style.right = '0';
    videoClose.style.top = '0';
    videoClose.style.color = '#ccc';
    videoClose.style.background = 'rgba(0,0,0,0.5)';
    videoClose.style.zIndex = '9999999';
    videoClose.style.padding = '0 5px';
    videoClose.style.cursor = 'pointer';
    videoClose.style.fontSize = '20px';
    videoClose.style.lineHeight = '20px';

    if (videoIframe.getAttribute('ratio')) {
      var height = videoWidget.offsetWidth / videoIframe.getAttribute('ratio');
      videoWidget.style.height = height + 'px';
      videoIframe.style.height = height + 'px';
    }
    videoClose.onclick = function () {
      videoWidget.parentNode.removeChild(videoWidget);
    };
})();
