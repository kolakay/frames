
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        landingPage: {
            name: '',
            video_id: '',
            is_active:true,
            template_id: ''
        },
        landingPages: [],
        landingPageEdit: {
            id: '',
            name: '',
            is_active: true,
        },
        videos: [],
        templates: [],
        tags: [],
        url : {
            edit: '',
            clone : '',
            create : '',
            update : '',
            delete : '',
            preview: ''
        }
    },
    mounted() {
        this.landingPages = JSON.parse($("#landing-pages").val());
        this.videos = JSON.parse($("#videos").val());
        this.templates = JSON.parse($("#templates").val());
        this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.delete = $("#delete-url").val();
        this.url.clone = $("#clone-url").val();
        this.url.edit = $("#edit-url").val() + '/';
        this.url.preview = $("#preview-url").val() + '/';

    },
    methods: {
        addlandingPage() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in this.landingPage) {
                let value = this.landingPage[key];
                formData.append(key, value);
            }
            this.isLoading = true;

                axios.post(this.url.create, formData)
                    .then((response) => {
                        $('#add-landing-page').modal('hide');
                        this.isLoading = false;

                        this.$notify({
                            title: 'Success',
                            message: response.data.message,
                            type: 'success'
                        });

                        this.landingPages.push(Object.assign({}, response.data.landingPage, {}));
                    })
                    .catch((error) => {
                        this.isLoading = false;
                        if(error.response){
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        }else{
                            this.$notify.error({
                                title: 'Error',
                                message: 'oops! Unable to complete request.'
                            });
                        }

                    });
        },
        selectLandingPage(index){
            this.landingPageEdit = Object.assign({}, this.landingPages[index]);
        },
        updateLandingPage(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in this.landingPageEdit) {
                let value = this.landingPageEdit[key];
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.update, formData)
                .then((response) => {
                    $('#edit-landing-page').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    var editedLandingPage = response.data.landingPage;
                    this.landingPages = this.landingPages.map((page) => {
                        if (page.id === editedLandingPage.id) {
                            page = Object.assign({}, editedLandingPage);
                        }
                        return page;
                    });

                    // $('.data-table-custom').DataTable().destroy();
                    // $('.data-table-custom').DataTable();
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        deleteLandingPage(index){
            let landingPage = Object.assign({}, this.landingPages[index]);
            landingPage._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this landing page? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: landingPage})
                        .then(response => {
                            this.isLoading = false;
                            this.landingPages.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        },
        cloneLandingPage(index){
            let page = Object.assign({}, this.landingPages[index]);
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in page) {
                let value = page[key];
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.clone, formData)
                .then((response) => {
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    this.landingPages.push(Object.assign({}, response.data.landingPage, {}));
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        }
    }
})
