if (window.Vue) {
	new Vue({
		el: '#app',
		data : {
			url : {
				create : "",
				edit: "",
				getDeal: ""
			},

			deal : {},
			deals : [],

			contacts: [],
			companies: [],

			deals_display: 'board',

			stages: [],
			listedStages : {
				qualified_to_buy: [],
				presentation_scheduled: [],
				appointment_scheduled: [],
				decision_maker_bought_in: [],
				contract_sent: [],
				closed_won: [],
				closed_lost: []
			},
		},

		mounted() {
			this.deals = JSON.parse($("#deals").val());
			this.stages = JSON.parse($("#stages").val());
			this.contacts = JSON.parse($("#dealContacts").val());
			this.companies = JSON.parse($("#dealCompanies").val());
			this.url.create = $("#createURL").val();
			this.url.edit = $("#editURL").val();
			this.url.getDeal = $("#getDealsURL").val();
			this.url.delete = $("#deleteURL").val();
			this.listStages();
		},
		components: {
			vuedraggable
		},
		methods : {
			log (evt, newStage) {
				if (evt.hasOwnProperty("added")){
					this.findDeal(evt.added.element.id)
					this.deal.stage = newStage
					this.store(this.url.edit)
				}
			},

			//function that filters the deals by stages in to specified arrays
			listStages() {
				for (let stage in this.stages) {
					this.listedStages[stage] = this.deals.filter((deal) => deal["stage"] == stage)
				}
			},

			setStage(stage) {
				let deal = { "stage" : ""};
				deal.stage = stage;
				this.deal = deal;
			},

			changeList() {
				this.getDeals();
				this.deals_display = 'list'
			},

			changeBoard() {
				this.getDeals();
				this.deals_display = 'board'
			},

			store(url) {
				formData = new FormData();

				for ( let key in this.deal ) {
					let value = this.deal[key];
					formData.append(key, value);
				}

				formData.append('_token', $('input[name=_token]').val());

				axios.post(url, formData).then((response) => {
					this.$notify({
						title: 'Success',
						message: response.data.message,
						type: 'success'
					});
					this.getDeals();
				})
				.catch( (error) => {
					this.$notify.error({
						title: 'Error',
						message: error.response.data.message
					});
				});
			},

			findDeal(id) {
				for ( let deal in this.deals ){
					if (this.deals[deal].id == id) {
						this.deal = this.deals[deal];
					}
				}
			},

			getDeals() {
				axios.get(this.url.getDeal).then((response) => {
					this.deals = response.data.deals;
					this.listStages();
				})
				.catch( (error) => {
					this.$notify.error({
						title: 'Error',
						message: error.response.data.message
					});
				});
			},

			deleteDeal(id) {
				this.findDeal(id);
				formData = new FormData();

				for ( let key in this.deal ) {
					let value = this.deal[key];
					formData.append(key, value);
				}

				formData.append('_token', $('input[name=_token]').val());

				const customAlert = swal({
					title: 'Warning',
					text: `Are you sure you want to delete this Deal? This action cannot be undone.`,
					icon: 'warning',
					closeOnClickOutside: false,
					buttons: {
						cancel: {
							text: "cancel",
							visible: true,
							className: "",
							closeModal: true,
						},
						confirm: {
							text: "Confirm",
							value: 'delete',
							visible: true,
							className: "btn-danger",
						}
					}
				});

				customAlert.then(value => {
					if (value == 'delete') {
						this.isLoading = true;
						this.remove(formData);
					}
				});
			},

			remove(data) {
				axios.post(this.url.delete, data).then((response) => {
					this.$notify({
						title: 'Success',
						message: response.data.message,
						type: 'success'
					});
					this.getDeals()
				})
				.catch( (error) => {
					this.$notify.error({
						title: 'Error',
						message: error.response.data.message
					});
				});
			},

		}
	});
}
