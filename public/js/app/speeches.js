
new Vue({
    el: "#app",
    data : {
        isLoading : false,
        speech: {
            gender: '1',
            title: '',
            text: '',
            language: 'en-US',
        },
        speeches: [
            {
                gender: '1',
                title: '',
                text: '',
                language: 'en-US',
            }
        ],
        url : {
            convert : '',
            download : '',
            delete : ''
        }
    },
    mounted() {
        this.speeches = JSON.parse($("#speeches").val());

        this.url.create = $("#convert-url").val();
        this.url.download = $("#download-url").val() + '/';
        this.url.delete = $("#delete-url").val();
    },
    methods: {
        convertText() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for ( var key in this.speech ) {
                let value = this.speech[key];
                formData.append(key, value);
            }
            this.isLoading = true;

                axios.post(this.url.create, formData)
                    .then((response) => {
                        this.isLoading = false;

                        this.$notify({
                            title: 'Success',
                            message: response.data.message,
                            type: 'success'
                        });

                        this.speeches.push(Object.assign({}, response.data.speech, {}));
                    })
                    .catch((error) => {
                        this.isLoading = false;
                        if(error.response){
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        }else{
                            this.$notify.error({
                                title: 'Error',
                                message: 'oops! Unable to complete request.'
                            });
                        }

                    });
        },
        downloadSpeech(index){
            this.speech = Object.assign({}, this.speeches[index]);
        },
        deleteSpeech(index){
            let speech = Object.assign({}, this.speeches[index]);
            speech._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this speech? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: speech})
                        .then(response => {
                            this.isLoading = false;
                            this.speeches.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        }
    }
})
