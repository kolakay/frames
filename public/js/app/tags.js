
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        tag: {
            name: '',
        },
        tags: [
            {
                name: '',
                id: ''
            }
        ],
        tagEdit: {
            id: '',
            name: '',
        },
        url : {
            create : '',
            update : '',
            delete : ''
        }
    },
    mounted() {
        this.tags = JSON.parse($("#tags").val());

        this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.delete = $("#delete-url").val();
    },
    methods: {
        addTag() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('tag_name', this.tag.name);
            this.isLoading = true;

                axios.post(this.url.create, formData)
                    .then((response) => {
                        $('#add-tag').modal('hide');
                        this.isLoading = false;

                        this.$notify({
                            title: 'Success',
                            message: response.data.message,
                            type: 'success'
                        });

                        this.tags.push(Object.assign({}, response.data.tag, {}));
                    })
                    .catch((error) => {
                        this.isLoading = false;
                        if(error.response){
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        }else{
                            this.$notify.error({
                                title: 'Error',
                                message: 'oops! Unable to complete request.'
                            });
                        }

                    });
        },
        selectTag(index){
            this.tagEdit = Object.assign({}, this.tags[index]);
        },
        updateTag(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('tag_name', this.tagEdit.name);
            formData.append('tag_id', this.tagEdit.id);
            this.isLoading = true;

            axios.post(this.url.update, formData)
                .then((response) => {
                    $('#edit-tag').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    var tag = response.data.tag;
                    this.tags = this.tags.map((tag) => {
                        if (tag.id === tag.id) {
                            tag = Object.assign({}, tag);
                        }
                        return tag;
                    });

                    // $('.data-table-custom').DataTable().destroy();
                    // $('.data-table-custom').DataTable();
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        deleteTag(index){
            let tag = Object.assign({}, this.tags[index]);
            tag._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this Tag? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: tag})
                        .then(response => {
                            this.isLoading = false;
                            this.tags.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        }
    }
})
