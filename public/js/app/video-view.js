
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        video: {

        },
        playlist: [],
        ctas : [],
        url : {
            create : '',
            update : {},
            delete : '',
            view: '',
            video_name : '',
            video_thumbnail: '',
            video_trim:'',
            design: '',
            register: ''
        },
        videoplayer : {},
        videoDetails: {},
        intervalId: '',
        videoViewRegistered: false
    },
    mounted() {
        this.video = JSON.parse($("#video").val());
        let videoDetails = JSON.parse($("#video-details").val());
        this.playlist = (videoDetails)? videoDetails.curated_list : [];
        this.ctas = (videoDetails)? videoDetails.curated_ctas : [];
        this.videoDetails = videoDetails;
        this.url.register = $("#log-url").val();
        this.videoPlayerSetup();
        let vueInstance = this;
        this.intervalId = setInterval(() => {
            if(this.videoViewRegistered == false){
                if(this.videoplayer.currentTime() >= 3){
                    this.getVideoView();
                }
            }
        }, 1000);

    },
    methods: {

        videoPlayerSetup(){
            let vueInstance = this;
            let auotPlayIsSet = (this.videoDetails)? this.videoDetails.auto_play : false;
            this.videoplayer = videojs("video-player", {
                autoplay: auotPlayIsSet
            });
            
            // $("#trackSRT").attr("src", this.video.vtt_file);
            this.videoplayer.src({
                type: 'video/mp4',
                src: this.video.video_path
            });
            this.videoplayer.addRemoteTextTrack({
                kind: 'captions', 
                src: this.video.vtt_file
            });
            // $("#trackSRT").src = this.video.vtt_file;
            
            // this.videoplayer.textTracks([{ 
            //     kind: 'captions', 
            //     // label: 'English', 
            //     // srclang: 'en', 
            //     src: 
            // }]);
            this.videoplayer.poster(this.video.video_thumbnail_path);

            this.videoplayer.controlBar.progressControl.seekBar.on('mouseup', function(event) {
                var seekBarEl = this.el();
                var seekBarRect = videojs.dom.getBoundingClientRect(seekBarEl);
                var seekBarPoint = videojs.dom.getPointerPosition(seekBarEl, event).x;
                var duration = vueInstance.videoplayer.duration();
                var seekBarClickedTime = videojs.formatTime(seekBarPoint * duration, duration);
                // console.log('Seekbar clicked time: ', seekBarClickedTime);
            });


        },
        selectVideo(video){
            console.log(video.vtt_file);
            this.videoplayer.remoteTextTracks();
            this.videoplayer.remoteTextTrackEls();
            this.videoplayer.src({
                type: 'video/mp4',
                src: video.video_path
            });
            this.videoplayer.addRemoteTextTrack({
                kind: 'captions', 
                src: video.vtt_file
            });
            
            // this.videoplayer.textTracks([{ 
            //     kind: 'captions', 
            //     // label: 'English', 
            //     // srclang: 'en', 
            //     src: video.vtt_file
            // }]);
        },
        getCTAClick(index){
            let cta = this.ctas[index];
            console.log(cta);
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('cta_id', cta.id);
            formData.append('contact_id', 0);
            formData.append('type', 'cta');
            formData.append('video_id', this.video.id);
            axios.post(this.url.register, formData)
                .then((response) => {

                })
                .catch((error) => {


                });
        },
        getReactionClick(reaction){

            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('contact_id', 0);
            formData.append('type', 'reaction');
            formData.append('reaction', reaction);
            formData.append('video_id', this.video.id);
            axios.post(this.url.register, formData)
                .then((response) => {

                })
                .catch((error) => {


                });
        },
        getVideoView(){

            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('contact_id', 0);
            formData.append('type', 'video_view');
            formData.append('video_id', this.video.id);
            axios.post(this.url.register, formData)
                .then((response) => {
                    this.videoViewRegistered = true;
                    clearInterval(this.intervalId);
                })
                .catch((error) => {


                });

        }

    }
})
