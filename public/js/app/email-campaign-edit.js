
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        tags: [],
        videos: [],
        emailCampaign: {
            id: '',
            title: '',
            provider_id: '',
            video_id: '',
            from_name: '',
            subject: '',
            video_header: '',
            template_id: '0',
            email_body: '',
            schedule_is_set: false,
            schedule_date: '',
            emails: [],
            inclusion_list: [],
            exclusion_list: []
        },

        url : {
            save : '',
            publish : '',
            test: '',
            campaigns: '',
            edit_template: '',
            preview_template: ''
        },
        custom_embed: '',
        embed: '',
        email_copy: '',
        contacts: [],
        emailIntegrations: [],
        emailTemplates:[],
        step: 1,
    },
    async mounted() {
        let vueInstance = this;
        let campaign = JSON.parse($("#campaign").val());
        this.videos = JSON.parse($("#videos").val());
        this.tags = JSON.parse($("#tags").val());
        this.contacts = JSON.parse($("#contacts").val());
        this.emailTemplates = JSON.parse($("#emailTemplates").val());
        this.emailIntegrations = JSON.parse($("#emailIntegrations").val());

        this.url.save = $("#save-url").val();
        this.url.campaigns = $("#campaign-list-url").val();

        this.url.publish = $("#publish-url").val();
        this.url.test = $("#test-email-url").val();
        this.url.edit_template = $("#edit-template-url").val() + '/';
        this.url.preview_template = $("#preview-template-url").val() + '/';

        this.emailCampaign = (campaign)? campaign : this.emailCampaign;

        var tinymceEditor = await tinymce.init({
            selector: '#simple-editor'
        });
        console.log(this.emailCampaign.schedule_date);
        tinymceEditor[0].setContent(this.emailCampaign.email_body);

        $(".user-timepicker").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d H:i K",
            disableMobile: true,
            defaultDate: this.emailCampaign.schedule_date
        });
        $("#contactEmails").val(this.emailCampaign.emails).trigger('change');
        $("#inclusionList").val(this.emailCampaign.inclusion_list).trigger('change');
        $("#exclusionList").val(this.emailCampaign.exclusion_list).trigger('change');
        $("#providerId").val(this.emailCampaign.provider_id).trigger('change');




    },
    methods: {
        setStep(event, number){
            event.preventDefault();
            this.step = number;
        },
        nextStep(){
            // console.log($("#user-timepicker").val());
            this.saveDraft();
            if(this.step < 4){
                this.step += 1;
            }
        },
        publish(){
            if(this.step == 4){
                this.saveDraft(true);
            }
        },
        selectVideo(id){
            // console.log(id);
            this.emailCampaign.video_id = id;
        },
        saveDraft(publish = false){
            this.emailCampaign.provider_id = $("#providerId").val();
            let emailBody = tinymce.activeEditor.getContent();
            this.emailCampaign.email_body = emailBody;
            this.emailCampaign.emails = JSON.stringify($("#contactEmails").val());
            this.emailCampaign.inclusion_list = JSON.stringify($("#inclusionList").val());
            this.emailCampaign.exclusion_list = JSON.stringify($("#exclusionList").val());
            this.emailCampaign.schedule_date = $("#user-timepicker").val();
            const formData = new FormData();
            for ( var key in this.emailCampaign ) {
                let value = this.emailCampaign[key];
                formData.append(key, value);
            }
            formData.append('is_save_and_publish', publish);
            formData.append('_token', $('input[name=_token]').val());


            axios.post(this.url.save, formData)
            .then((response) => {
                // this.isLoading = false;
                this.emailCampaign.id = response.data.campaign.id;
                if(publish == true){
                    window.location = this.url.campaigns;
                }
                this.$notify({
                    title: 'Success',
                    message: response.data.message,
                    type: 'success'
                });
            })
            .catch((error) => {
                // this.isLoading = false;

                if(error.response){
                    this.$notify.error({
                        title: 'Error',
                        message: error.response.data.message
                    });
                }else{
                    this.$notify.error({
                        title: 'Error',
                        message: 'oops! Unable to complete request.'
                    });
                }

            });
        },

    }
})
