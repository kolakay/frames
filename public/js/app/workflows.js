
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        
        workflows: [
            {
                name: '',
                id: ''
            }
        ],
        url : {
            edit : '',
            create : '',
            delete : ''
        }
    },
    mounted() {
        this.workflows = JSON.parse($("#workflows").val());

        this.url.create = $("#create-url").val();
        this.url.edit = $("#edit-url").val() + '/';
        this.url.delete = $("#delete-url").val();
    },
    methods: {
        
        deleteWorkflow(index){
            let workflow = Object.assign({}, this.workflows[index]);
            workflow._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this Workflow? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: workflow})
                        .then(response => {
                            this.isLoading = false;
                            this.workflows.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        }
    }
})
