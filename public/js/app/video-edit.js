
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        ctas: [],
        tags: [],
        videos: [],
        video: {

        },
        selectedEditVideo:'',
        videoEditOption: {
            video_id: '',
            position: 'intro',
            selectedEditVideo: ''
        },
        editNameOption: false,
        videoEditName: '',
        video_details: {
            video_id: '',
            description: '',
            tags: "",
            ctas: [],
            playlist: "",

            auto_play: false,
            add_exit_intent_modal: false,
            add_emoji_response_buttons: false,
            add_facebook_comment_widget: false,
            add_messaging: false,
            share_with_team: false,
            disable_tracking: false,
            allow_video_download: false,
            auto_delete: false,
            auto_delete_date: '',
            visibility: 'public',
            has_password: false,
            password: '',
        },
        video_design: {
            display_logo: false,
            player_color: '#DD0F20',
            default_player_color: false,
            player_background_image: '',
            player_background_image_src: '',
            player_bg_url: '',
            default_player_background_image: false,

            cta_color: '#DD0F20',
            default_cta_color: false,
            cta_background_image: '',
            cta_background_image_src: '',
            cta_bg_url: '',
            default_cta_background_image: false
        },
        video_edit: {
            start_trim: 0,
            end_trim: 0,
            video_thumbnail : '',
            thumbnail_nail: ''
        },
        video_share: {
            disable_gif_animation: false,
            disable_url_text_encoding: false,
            disable_email_open_tracking: false,
            personalization_text: '',
            contact_id: '',

            embed_type:"responsive",
            embed_custom_width: 2880,
            embed_custom_height: 1800,
            embed_disable_cta_overlay: false,
            embed_autoplay: true,
            embed_sound_off: true,

            widget_position: 'rightBottom',
            widget_autoplay: false,
            widget_sound_off: false,
            widget_show_cta:true,
            widget_hide_in_mobile: true,

            direct_email_provider_id: '',
            direct_email_contact_id: '',
            direct_email_personalization_text: '',
            direct_email_subject: '',
            direct_email_template_id: 'null',
            direct_email_body: ''
        },

        url : {
            create : '',
            update : {},
            delete : '',
            view: '',
            video_name : '',
            video_thumbnail: '',
            video_trim:'',
            design: '',
            embed: '',
            widgetScript: '',
            video_share: '',
            video_send_mail:'',
            edit_template: '',
            preview_template: '',
            tracker: '',
            chart_url: '',
            srt:'',
            transcribe: '',
            srt_download: '',
            concatenate: ''
        },
        quill: {},
        videoplayer : {},
        CTABGQuery: '',
        playerBGQuery: '',
        CTABGQueryType: 'all',
        playerBGQueryType: 'all',
        pixabayAPIKey: '',
        pixabayPlayerPage : 1,
        pixabayCTAPage : 1,
        pixabayPlayerImages: [],
        pixabayCTAImages: [],
        currentImageIndex: -1,
        currentCTAImageIndex: -1,
        playerUploadType: 'upload',
        CTAUploadType: 'upload',
        widget_value : '',
        custom_embed: '',
        embed: '',
        email_copy: '',
        contacts: [],
        emailIntegrations: [],
        emailTemplates:[],
        graph: {},
        generalStats: {},
        activityLogs: [],
        videoViewTime: {
            id : "today",
            name: "Today"
        },
        generalStatsTime: {
            id : "today",
            name: "Today"
        },
        bars: [],
        videoChart: {},
        times: [
            {
                id : "all_time",
                name: "All Time"
            },
            {
                id : "today",
                name: "Today"
            },
            {
                id : "yesterday",
                name: "Yesterday"
            },
            {
                id : "this_week",
                name: "This Week"
            },
            {
                id : "last_week",
                name: "Last Week"
            },
            {
                id : "this_month",
                name: "This Month"
            },
            {
                id : "last_month",
                name: "Last Month"
            },
            {
                id : "last_30_days",
                name: "Last 30 Days"
            }
        ],
        activityTypes : [
            {
                id :"cta",
                name : "CTA Clicks"
            },
            {
                id: "video_view",
                name: "Watch Rates"
            },
            {
                id: "email_open",
                name: "Email Opens"
            },
            {
                id:"email_click",
                name: "Email Clicks"
            },
            {
                id: "reaction",
                name: "Reaction Clicks"
            },
            {
                id: "call_rate",
                name: "Call Rates"
            },
            {
                id: "page_view",
                name: "Page View"
            }
        ],
        activityFilterArray : [],
        transcriptionDetails: {
            user_id: '',
            video_id: '',
            srt_file: '',
            vtt_file: '',
            status: 'failed'
        }
    },
    mounted() {
        let vueInstance = this;
        this.ctas = JSON.parse($("#ctas").val());
        this.videos = JSON.parse($("#videos").val());
        this.video = JSON.parse($("#video").val());
        this.tags = JSON.parse($("#tags").val());
        this.contacts = JSON.parse($("#contacts").val());
        this.graph = JSON.parse($("#graph-data").val());
        this.generalStats = JSON.parse($("#general-stat-data").val());
        this.activityLogs = JSON.parse($("#analytics-log-data").val());

        this.emailTemplates = JSON.parse($("#emailTemplates").val());
        this.emailIntegrations = JSON.parse($("#emailIntegrations").val());
        // this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.view = $("#view-url").val();
        this.url.video_name = $("#video-name").val();
        this.url.video_thumbnail = $("#video-thumbnail").val();
        this.url.video_trim = $("#video-trim").val();
        this.pixabayAPIKey = $("#pixabay-key").val();
        this.url.design = $("#design-url").val();
        this.url.embed = $("#widget-url").val();
        this.url.widgetScript = $("#widget-asset-url").val();
        this.url.video_share = $("#video-share-url").val();
        this.url.video_send_mail = $("#video-send-mail-url").val();
        this.url.edit_template = $("#edit-template-url").val() + '/';
        this.url.preview_template = $("#preview-template-url").val() + '/';
        this.url.tracker = $("#tracker-url").val();
        this.url.chart_url = $("#chart-url").val();
        this.url.transcribe = $("#transcribe-url").val();
        this.url.concatenate = $("#concatenate-url").val();

        this.url.srt = $("#srt-url").val();
        this.url.srt_download = $("#srt-download-url").val() + '/';


        this.videoEditName = this.video.name;

        let userTranscription = $("#transcriptionDetails").val();
        this.transcriptionDetails = (userTranscription != '[]')? JSON.parse(userTranscription): this.transcriptionDetails;
        let details =  $("#video-details").val();
        this.video_details = (details)? JSON.parse(details) : this.video_details;
        let design =  $("#video-design").val();
        this.video_design = (design)? JSON.parse(design) : this.video_design;
        let videoShare =  $("#video-share").val();
        this.video_share = (videoShare)? JSON.parse(videoShare) : this.video_share;

        this.video_details.video_id = this.video.id;
        $(".delete_datepicker").datepicker({
            autoclose:!0,
            // rtl:i,
            // setDate: ,
            templates:{
                leftArrow:'<i class="simple-icon-arrow-left"></i>',
                rightArrow:'<i class="simple-icon-arrow-right"></i>'
            },
            // dateFormat: 'yyyy-mm-dd'
        });
        $(".delete_datepicker").datepicker("setDate", this.video_details.parsed_date);

        $(".delete_datepicker").on('change', function(){
            vueInstance.updateVideoDetails();
        })
        this.videoPlayerSetup();
        this.updateWidget();
        this.updateEmbed();
        this.initEmailCopy();
        this.initAnalyticsChat(this.graph.videoViews);
        this.initGeneralStat();


        $("#player-color").colorpicker();
        $("#cta-color").colorpicker();
        $("#vplayer_color").val(this.video_design.player_color);
        $("#vcta_color").val(this.video_design.cta_color);
        $("#vplayer_color").on('change', function(){
            vueInstance.video_design.player_color = $(this).val();
            vueInstance.updateVideoDesign(null, 'player');
        })
        $("#vcta_color").on('change', function(){
            vueInstance.video_design.cta_color = $(this).val();
            vueInstance.updateVideoDesign(null, 'cta');
        });

        $('#video_ctas').on('select2:select select2:unselect', function (e) {
            vueInstance.updateVideoDetails();
        });

        $('#video_playlist').on('select2:select select2:unselect', function (e) {
            vueInstance.updateVideoDetails();
        });
        $('#video_tags').on('select2:select select2:unselect', function (e) {
            vueInstance.updateVideoDetails();
        });

        tinymce.init({
            selector: '#simple-editor'
        });





    },
    methods: {
        setName(){
            this.editNameOption = true;
        },
        exitEditName(){
            this.editNameOption = false;
        },
        videoPlayerSetup(){
            let vueInstance = this;
            this.videoplayer = videojs("video-player");
            this.videoplayer.src({
                type: 'video/mp4',
                src: this.video.video_path
            });
            this.videoplayer.poster(this.video.video_thumbnail_path);

            this.videoplayer.controlBar.progressControl.seekBar.on('mouseup', function(event) {
                var seekBarEl = this.el();
                var seekBarRect = videojs.dom.getBoundingClientRect(seekBarEl);
                var seekBarPoint = videojs.dom.getPointerPosition(seekBarEl, event).x;
                var duration = vueInstance.videoplayer.duration();
                var seekBarClickedTime = videojs.formatTime(seekBarPoint * duration, duration);
                // console.log('Seekbar clicked time: ', seekBarClickedTime);
            });
        },
        setTrim(position){
            if(position == 'start'){
                let currentTime = this.videoplayer.currentTime();
                this.video_edit.start_trim = currentTime.toFixed(1);
                this.video_edit.end_trim = 0;
                this.videoplayer.paused();

            }else{
                let currentTime = this.videoplayer.currentTime();
                if(this.video_edit.start_trim < currentTime){
                    this.video_edit.end_trim = currentTime.toFixed(1);
                    this.videoplayer.paused();
                }else{
                    this.$notify.error({
                        title: 'Error',
                        message: 'Unable to set start frame further than end frame'
                    });
                }
            }
        },
        trimVideo(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('start_time', this.video_edit.start_trim);
            formData.append('end_time', this.video_edit.end_trim);

            formData.append('video_id', this.video.id);
            this.isLoading = true;
            axios.post(this.url.video_trim, formData)
                .then((response) => {
                    this.isLoading = false;
                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    location.reload(true);
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        setThumbnailTimeFrame(){
            let currentTime = this.videoplayer.currentTime();
            this.video_edit.thumbnail_nail = currentTime.toFixed(1);
            this.videoplayer.paused();
        },
        getVideoThumbnail(file = null){
            const formData = new FormData();
            if(file){
                formData.append('image', file);
            }
            formData.append('_token', $('input[name=_token]').val());
            formData.append('frame_time', this.video_edit.thumbnail_nail);
            formData.append('video_id', this.video.id);
            this.isLoading = true;
            axios.post(this.url.video_thumbnail, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            })
                .then((response) => {
                    this.isLoading = false;
                    this.video.thumbnail = response.data.thumbnail;
                    this.video.thumbnail_path = response.data.thumbnail_path;
                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        saveVideoName(){

            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('name', this.videoEditName);
            formData.append('id', this.video.id);
            this.isLoading = true;

            axios.post(this.url.video_name, formData)
                .then((response) => {
                    this.isLoading = false;
                    this.video.name = this.videoEditName;
                    this.editNameOption = false;
                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                })
                .catch((error) => {
                    this.isLoading = false;
                    this.videoEditName = this.video.name;
                    this.editNameOption = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        setPlayerBGQuery(query){
            this.playerBGQueryType = query;
        },
        setCTABGQuery(query){
            this.CTABGQueryType = query;
        },
        updateVideoDetails(){
            this.video_details.auto_delete_date = $("#autoDeleteDate").val();
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            this.video_details.ctas = JSON.stringify($("#video_ctas").val());
            this.video_details.playlist = JSON.stringify($("#video_playlist").val());
            this.video_details.tags = JSON.stringify($("#video_tags").val());
            for ( var key in this.video_details ) {
                let value = this.video_details[key];
                formData.append(key, value);
            }
            // this.isLoading = true;

            axios.post(this.url.update, formData)
                .then((response) => {
                    // this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                })
                .catch((error) => {
                    // this.isLoading = false;

                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        changeThumbnailImage(){
            this.video.thumbnail = '';
        },
        copyVideoURL(){


            var videoLink = this.url.view;

            const el = document.createElement('textarea');
            el.value =  videoLink;
            el.setAttribute('readonly', '');
            el.style.position = 'absolute';
            el.style.left = '-9999px';
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            if(document.execCommand("copy")) {
                this.$notify({
                    title: 'Success',
                    message: 'Video link copied',
                    type: 'success'
                });
                document.body.removeChild(el);

            }else{
                this.$notify.error({
                    title: 'Error',
                    message: 'Unable to copy video link'
                });
                document.body.removeChild(el);

            }
        },
        getPixabayImagesForPlayer(){
            let query = this.playerBGQuery;
            if(query != ''){
                this.getPixabayImages(query, this.pixabayPlayerPage, 'player');
            }
        },
        getPixabayImagesForCTA(){
            let query = this.CTABGQuery;
            if(query != ''){
                this.getPixabayImages(query, this.pixabayCTAPage, 'cta');
            }
        },
        getPixabayImages(query, page, setup){
            var type = (setup == 'cta')? this.CTABGQueryType: this.playerBGQueryType;
            var URL = "https://pixabay.com/api/?key="+
                this.pixabayAPIKey+"&q="+
                encodeURIComponent(query)+"&page="+page+"&image_type="+type;
            this.isLoading = true;
            axios.get(URL)
                .then((response) => {
                    this.isLoading = false;
                    if(response.data.hits.length > 0){
                        if(setup == 'cta'){
                            for(let key in response.data.hits){
                                this.pixabayCTAImages.push(response.data.hits[key])
                            }
                        }else{
                            for(let key in response.data.hits){
                                this.pixabayPlayerImages.push(response.data.hits[key])
                            }
                        }

                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! No media matches your query'
                        });
                    }

                })
                .catch((error) => {
                    this.isLoading = false;
                    this.$notify.error({
                        title: 'Error',
                        message: 'oops! Unable to complete request.'
                    });

                });

        },
        setImageAsPlayerBackground(index){
            this.currentImageIndex = index;
            let selectedImage = this.pixabayPlayerImages[index];
            this.video_design.player_bg_url = selectedImage.largeImageURL;
            this.updateVideoDesign(null, 'player');
        },
        loadMorePlayerBackgroundImages(){
            let query = this.playerBGQuery;
            this.pixabayPlayerPage += 1;
            if(query != ''){
                this.getPixabayImages(query, this.pixabayPlayerPage, 'player');
            }
        },
        setImageAsCTABackground(index){
            this.currentCTAImageIndex = index;
            let selectedImage = this.pixabayCTAImages[index];
            this.video_design.cta_bg_url = selectedImage.largeImageURL;
            this.updateVideoDesign(null, 'cta');
        },
        loadMoreCTABackgroundImages(){
            let query = this.CTABGQuery;
            this.pixabayCTAPage += 1;
            if(query != ''){
                this.getPixabayImages(query, this.pixabayCTAPage, 'cta');
            }
        },
        clearBackgroundSearch(type){
            if(type == 'player'){
                this.playerBGQuery = '';
                this.pixabayPlayerPage = 1;
                this.pixabayPlayerImages = [];
            }else{
                this.CTABGQuery = '';
                this.pixabayCTAPage = 1;
                this.pixabayCTAImages = [];
            }
        },
        uploadImage(type){
            let isPlayer = (type == 'cta')? false : true;
            if(type == 'thumbnail'){
                $("#thumbnail").click();
            }else{
                if(isPlayer){
                    $("#player-background").click();
                }else{
                    $("#cta-background").click();
                }
            }

        },
        uploadSrt(event){
            event.preventDefault();
            $("#srtUpload").click();
        },
        readSRTFile(){
            if (window.File && window.FileList && window.FileReader) {
                var files = event.target.files; //FileList object
                var file = files[0];
                this.getSRtFile(file);

            } else {
                this.$notify.error({
                    title: 'Error',
                    message: 'Unable to complete request. Please update your browser'
                });
            }
        },
        
        autoTranscribe(){
            let formData = new FormData();
           
            formData.append('video_id', this.video.id);
            formData.append('_token', $('input[name=_token]').val());
            this.isLoading = true;
            this.transcriptionDetails.status = 'pending';
            axios.post(this.url.transcribe, formData)
            .then((response) => {
                // this.video_design = response.data.design;
                this.isLoading = false;
                this.$notify({
                    title: 'Success',
                    message: 'Transcription request added to queue.',
                    type: 'success'
                });
            })
            .catch( (error) => {
                this.transcriptionDetails.status = 'failed';
                this.$notify.error({
                    title: 'Error',
                    message: 'oops! Unable to complete request.'
                });

                this.isLoading = false;
            });
        },
        getSRtFile(file){
            let formData = new FormData();
            if(file){
                formData.append('srt_file', file);
            }
            formData.append('video_id', this.video.id);
            formData.append('_token', $('input[name=_token]').val());
            this.isLoading = true;

            axios.post(this.url.srt, formData,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            })
            .then((response) => {
                // this.video_design = response.data.design;
                this.isLoading = false;
                this.$notify({
                    title: 'Success',
                    message: 'SRT uploaded',
                    type: 'success'
                });
            })
            .catch( (error) => {

                this.$notify.error({
                    title: 'Error',
                    message: 'oops! Unable to complete request.'
                });

                this.isLoading = false;
            });
        },
        readThumbnailImage(){
            if (window.File && window.FileList && window.FileReader) {
                var files = event.target.files; //FileList object
                var file = files[0];
                this.getVideoThumbnail(file);

            } else {
                this.$notify.error({
                    title: 'Error',
                    message: 'Unable to complete request. Please update your browser'
                });
            }
        },

        readImage(type){
            let isPlayer = (type == 'cta')? false : true;
            if (window.File && window.FileList && window.FileReader) {
                var files = event.target.files; //FileList object
                var file = files[0];
                this.updateVideoDesign(file, type);

            } else {
                this.$notify.error({
                    title: 'Error',
                    message: 'Unable to complete request. Please update your browser'
                });
            }
        },
        updateVideoDesign(file = null, type){
            let formData = new FormData();
            if(file){
                formData.append('image', file);
            }
            formData.append('type', type);
            formData.append('video_id', this.video.id);
            formData.append('_token', $('input[name=_token]').val());
            for ( var key in this.video_design ) {
                let value = this.video_design[key];
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.design, formData,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            })
            .then((response) => {
                this.video_design = response.data.design;
                this.isLoading = false;
                this.$notify({
                    title: 'Success',
                    message: 'Video design updated',
                    type: 'success'
                });
            })
            .catch( (error) => {

                this.$notify.error({
                    title: 'Error',
                    message: 'oops! Unable to complete request.'
                });

                this.isLoading = false;
            });
        },
        changeImage(type){
            let isPlayer = (type == 'cta')? false : true;
            if(isPlayer){
                this.video_design.player_background_image = '';
            }else{
                this.video_design.cta_background_image = '';
            }
        },
        updatePrivacy(){
            this.updateVideoDetails();
        },
        updateWidget(){
            this.widget_value =
            `<div id="fd-widget" style="display: none;" data-position="${this.video_share.widget_position}" data-mobile="${this.video_share.widget_hide_in_mobile}"><iframe id="fd-widget-iframe" url="${this.url.embed}?widget=1&no_cta=${this.video_share.widget_show_cta? 1: 0}&autoplay=${this.video_share.widget_autoplay? 1: 0}&muted=${this.video_share.widget_sound_off? 1: 0}" frameborder="0" ratio="1.890" allow="autoplay; encrypted-media" allowfullscreen scrolling="no"></iframe><div id="fd-widget-close">&times;</div><script src="${this.url.widgetScript}"></script></div>`
        },
        updateEmbed(){
            this.updateCustomEmbed();
            this.updateDefaultEmbed();
        },
        updateCustomEmbed(){
            this.custom_embed = `<iframe width="${this.video_share.embed_custom_width}" height="${this.video_share.embed_custom_height}" src="${this.url.embed}?width=${this.video_share.embed_custom_width}&height=${this.video_share.embed_custom_height}&autoplay=${this.video_share.embed_autoplay? 1: 0}&no_cta=${this.video_share.embed_disable_cta_overlay? 1: 0}&muted=${this.video_share.embed_sound_off? 1: 0}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>`;
        },
        updateDefaultEmbed(){
            this.embed = `<div style="position: relative; height: 0; padding-bottom: 62.50%;"><iframe style="position: absolute; width: 100%; height: 100%; left: 0;" allow="autoplay; encrypted-media" src="${this.url.embed}?width=auto&height=auto&autoplay=${this.video_share.embed_autoplay? 1: 0}&no_cta=${this.video_share.embed_disable_cta_overlay? 1: 0}&muted=${this.video_share.embed_sound_off? 1: 0}" width="auto" height="auto" frameborder="0" allowfullscreen></iframe></div>`;
        },
        initEmailCopy(){
            let imageSrc = (this.video_share.disable_gif_animation)? this.video.video_thumbnail_path : this.video.video_gif_path;
            this.setupEmailCopy(imageSrc);
        },
        copyGifForEmail(){
            let imageSrc = this.video.video_gif_path;
            this.setupEmailCopy(imageSrc);
            this.copyForEmail();
        },
        copyThumbnailForEmail(){
            let imageSrc = this.video.video_thumbnail_path;
            this.setupEmailCopy(imageSrc);
            this.copyForEmail();
        },
        copyImageAndLink(){
            document.addEventListener("copy", this.copyHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyHandler);
        },
        copyForEmail(){
            this.initEmailCopy();
            document.addEventListener("copy", this.copyHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyHandler);
        },
        copyHandler(e){
            e.clipboardData.setData("text/html", this.email_copy);
            e.clipboardData.setData("text/plain", this.video.thumbnail_video_url);
            e.preventDefault();
        },
        setGifForEmail(){
            let imageSrc = this.video.video_gif_path;
            this.setupEmailCopy(imageSrc);
        },
        setupEmailCopy(imageSrc){
            let contactField = (this.video_share.contact_id != '')? `contact=${this.video_share.contact_id}` : '';
            let hashField = (this.video_share.contact_id != '')? `image_hash=${this.video_share.contact_id}` : '';

            let tracker = `<img src="${this.url.tracker}?${hashField}" style="display: none; width: 0px; height: 0px;">`;
            let emailTracker = (this.video_share.disable_email_open_tracking)? '' : tracker;

            this.email_copy = `<div style="font-family: Open Sans, Arial, sans-serif;">
                <table style="text-align: center; border: none; padding: 10px; background-color: transparent;">
                    <tbody>
                        <tr>
                            <td>
                                <div style="font-size: 24px; background-color: rgb(0, 0, 0); color: rgb(255, 255, 255); margin: 0px auto; width: 600px;">${this.video_share.personalization_text}</div>
                                <a target="_blank" href="${this.video.video_url}?message=${this.video_share.personalization_text}&${contactField}&from_email=1"><img width="600" height="375" src="${imageSrc}" style="max-height: 400px; max-width: 600px;" alt="Video For Sales"></a>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 20px;">
                                <a target="_blank" href="${this.video.video_url}?message=${this.video_share.personalization_text}&${contactField}&from_email=1" style="color: rgb(0, 113, 188); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: middle; padding: 6px 12px; border-radius: 4px; border: 1px solid rgb(0, 113, 188); text-decoration-line: none; margin-left: auto; margin-right: auto;">Watch Video</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="color: rgb(51, 51, 51); line-height: 24px; font-size: 13px; padding-top: 10px;">
                                <img src="#" style="width: 13px; height: 13px; margin-right: 5px; vertical-align: middle;">
                                Video powered by <a target="_blank" style="color: rgb(51, 51, 51);" href="#">Face Drip</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                ${emailTracker}
            </div>`;
        },

        copyThumbnail(){
            document.addEventListener("copy", this.copyThumbnailHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyThumbnailHandler);
        },
        copyThumbnailHandler(e){
            let imageSrc = this.video.video_thumbnail_path;
            e.clipboardData.setData("text/html", `<img width="600" height="375" src="${imageSrc}" style="max-height: 400px; max-width: 600px;" alt="Video For Sales">`);
            e.clipboardData.setData("text/plain", this.video.thumbnail_video_url);
            e.preventDefault();
        },

        copyGif(){
            document.addEventListener("copy", this.copyGifHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyGifHandler);
        },
        copyGifHandler(e){
            let imageSrc = this.video.video_gif_path;
            e.clipboardData.setData("text/html", `<img width="600" height="375" src="${imageSrc}" style="max-height: 400px; max-width: 600px;" alt="Video For Sales">`);
            e.clipboardData.setData("text/plain", this.video.thumbnail_video_url);
            e.preventDefault();
        },

        copyHtml(){
            this.initEmailCopy();
            document.addEventListener("copy", this.copyHtmlHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyHtmlHandler);
        },
        copyHtmlHandler(e){
            e.clipboardData.setData("text/plain", this.email_copy);
            e.preventDefault();
        },

        copyURL(){
            this.initEmailCopy();
            document.addEventListener("copy", this.copyURLHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyURLHandler);
        },
        copyURLHandler(e){
            e.clipboardData.setData("text/plain", this.video.thumbnail_video_url);
            e.preventDefault();
        },
        copyWidget(){
            document.addEventListener("copy", this.copyWidgetHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyWidgetHandler);
        },
        copyWidgetHandler(e){
            e.clipboardData.setData("text/plain", this.widget_value);
            e.preventDefault();
        },

        copyEmbed(){
            document.addEventListener("copy", this.copyEmbedHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyEmbedHandler);
        },
        copyEmbedHandler(e){
            e.clipboardData.setData("text/plain", this.embed);
            e.preventDefault();
        },

        copyCustomEmbed(){
            document.addEventListener("copy", this.copyCustomEmbedHandler);
            if(document.execCommand("copy")) {
                this.copyMessage();
            }
            document.removeEventListener("copy", this.copyCustomEmbedHandler);
        },
        copyCustomEmbedHandler(e){
            e.clipboardData.setData("text/plain", this.custom_embed);
            e.preventDefault();
        },

        copyMessage(){
            this.$notify({
                title: 'Success',
                message: "Copy Successful",
                type: 'success'
            });
        },

        storeVideoShare(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('video_id', this.video.id);
            this.video_share.widget_position = $("#widget-position").val();
            formData.append('video_share', JSON.stringify(this.video_share));

            axios.post(this.url.video_share, formData)
                .then((response) => {
                    // this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                })
                .catch((error) => {
                    // this.isLoading = false;

                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },

        sendMail(){
            const formData = new FormData();
            this.setGifForEmail();
            let emailBody = tinymce.activeEditor.getContent();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('video_id', this.video.id);
            formData.append('provider_id', $("#providerId").val());
            formData.append('contact_id', $("#contactId").val());
            formData.append('subject', this.video_share.direct_email_subject);
            formData.append('email_copy', this.email_copy);
            formData.append('email_body', emailBody);
            formData.append('personalization_text', this.video_share.direct_email_personalization_text);
            formData.append('template_id', $("#templateId").val());

            axios.post(this.url.video_send_mail, formData)
                .then((response) => {
                    // this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                })
                .catch((error) => {
                    // this.isLoading = false;

                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });

        },

        initAnalyticsChat(graphStats){
            let videoLabels = [];
            let videoViews = [];
            for(key in graphStats){
                videoLabels.push(graphStats[key].time);
                videoViews.push(graphStats[key].sumOfViews);
            }
            if (document.getElementById("AnalyticsChat")) {
                var P = document.getElementById("AnalyticsChat").getContext("2d");
                var e = getComputedStyle(document.body)
                  , r = e.getPropertyValue("--theme-color-2").trim()
                  , f = e.getPropertyValue("--foreground-color").trim()
                  , p = e.getPropertyValue("--theme-color-2-10").trim()
                  , g = (e.getPropertyValue("--theme-color-6-10").trim(),
                e.getPropertyValue("--primary-color").trim())
                  , b = e.getPropertyValue("--separator-color").trim();

                var config = {
                    backgroundColor: "#FFF",
                    titleFontColor: g,
                    borderColor: b,
                    borderWidth: .5,
                    bodyFontColor: g,
                    bodySpacing: 10,
                    xPadding: 15,
                    yPadding: 15,
                    cornerRadius: .15,
                    displayColors: !1
                };
                this.videoChart = new Chart(P,{
                    type: "line",
                    options: {
                        plugins: {
                            datalabels: {
                                display: !1
                            }
                        },
                        responsive: !0,
                        maintainAspectRatio: !1,
                        scales: {
                            yAxes: [{
                                gridLines: {
                                    display: !0,
                                    lineWidth: 1,
                                    color: "rgba(0,0,0,0.1)",
                                    drawBorder: !1
                                },
                                ticks: {
                                    beginAtZero: !0,
                                    stepSize: 10,
                                    min: 0,
                                    max: 100,
                                    padding: 0
                                }
                            }],
                            xAxes: [{
                                gridLines: {
                                    display: !1
                                }
                            }]
                        },
                        legend: {
                            display: !1
                        },
                        tooltips: config
                    },
                    data: {
                        labels: videoLabels,
                        datasets: [{
                            label: "Video Views",
                            data: videoViews,
                            borderColor: r,
                            pointBackgroundColor: f,
                            pointBorderColor: r,
                            pointHoverBackgroundColor: r,
                            pointHoverBorderColor: f,
                            pointRadius: 4,
                            pointBorderWidth: 2,
                            pointHoverRadius: 5,
                            fill: !0,
                            borderWidth: 2,
                            backgroundColor: p
                        }]
                    }
                })
            }
        },
        initGeneralStat(){
            let vueInstance = this;
            $('#emailClicks').attr('aria-valuenow', this.generalStats.emailClicks);
            $('#ctaClicks').attr('aria-valuenow', this.generalStats.ctaClicks);
            $('#watchRates').attr('aria-valuenow', this.generalStats.watchRates);
            $('#reactionClicks').attr('aria-valuenow', this.generalStats.reactionClicks);
            $('#callRates').attr('aria-valuenow', this.generalStats.callRates);
            $('#emailOpens').attr('aria-valuenow', this.generalStats.emailOpens);

            $(".general-progress-bar").each((function() {
                var e = $(this).attr("aria-valuenow")
                  , a = $(this).data("color") || t
                  , n = $(this).data("trailColor") || "#d7d7d7"
                  , o = $(this).attr("aria-valuemax") || 100
                  , i = $(this).data("showPercent");
                var progressBars = new ProgressBar.Circle(this,{
                        color: a,
                        duration: 20,
                        easing: "easeInOut",
                        strokeWidth: 4,
                        trailColor: n,
                        trailWidth: 4,
                        text: {
                            autoStyleContainer: !1
                        },
                        step: function(t, a) {
                            i ? a.setText(Math.round(100 * a.value()) + "%") : a.setText(e + "/" + o)
                        }
                    });
                vueInstance.bars.push(progressBars);
                progressBars.animate(e / o);
            }
            ))
        },
        selectVideoTime(e, index){
            e.preventDefault();
            this.videoViewTime = this.times[index];
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('filter', this.videoViewTime.id);
            formData.append('type', 'videoViews');

            formData.append('video_id', this.video.id);
            this.isLoading = true;
            axios.post(this.url.chart_url, formData)
            .then((response) => {
                this.isLoading = false;
                this.graph = response.data.graph;
                this.videoChart.destroy();
                this.initAnalyticsChat(this.graph.videoViews);

            })
            .catch((error) => {
                this.isLoading = false;
                if(error.response){
                    this.$notify.error({
                        title: 'Error',
                        message: error.response.data.message
                    });
                }else{
                    this.$notify.error({
                        title: 'Error',
                        message: 'oops! Unable to complete request.'
                    });
                }

            });
        },
        selectGeneralStatsTime(e, index){
            e.preventDefault();
            this.generalStatsTime = this.times[index];
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('filter', this.generalStatsTime.id);
            formData.append('type', 'general');
            formData.append('video_id', this.video.id);
            this.isLoading = true;
            axios.post(this.url.chart_url, formData)
            .then((response) => {
                this.isLoading = false;
                this.generalStats = response.data.generalStats;
                for(key in this.bars){
                    this.bars[key].destroy();
                }
                this.bars = [];
                this.initGeneralStat();

            })
            .catch((error) => {
                this.isLoading = false;
                if(error.response){
                    this.$notify.error({
                        title: 'Error',
                        message: error.response.data.message
                    });
                }else{
                    this.$notify.error({
                        title: 'Error',
                        message: 'oops! Unable to complete request.'
                    });
                }

            });
        },
        selectActivityType(e, index){
            e.preventDefault();
            let type = this.activityTypes[index];
            if(this.activityFilterArray.includes(type.id)){
                this.activityFilterArray = this.activityFilterArray.filter(function(item) {
                    return item !== type.id
                })
            }else{
                this.activityFilterArray.push(type.id);
            }
        },
        selectVideo(id){
            this.videoEditOption.selectedEditVideo = id;
            this.videoEditOption.video_id = this.video.id;
        },
        editVideo(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('video_id', this.video.id);
            formData.append('input_video', this.videoEditOption.selectedEditVideo);
            formData.append('position', this.videoEditOption.position);

            axios.post(this.url.concatenate, formData)
                .then((response) => {
                    // this.isLoading = false;
                    this.video = response.data.video;
                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });
                })
                .catch((error) => {
                    // this.isLoading = false;

                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        }
    }
})
