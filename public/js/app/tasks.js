if (window.Vue) {
	new Vue({
		el: '#app',
		data : {
			url : {
				create : "",
				edit: "",
				getTasks: ""
			},
			filterBy: "all",
			task : {},
			tasks : [],
			radios : {
				pending : "",
				completed : "",
				all : ""
			},
			contactsList: [],
			dealsList: [],
			videosList: []
		},

		mounted() {
			this.tasks = JSON.parse($("#tasks").val());
			this.contactsList = JSON.parse($("#tasksContacts").val());
			this.dealsList = JSON.parse($("#tasksDeals").val());
			this.videosList = JSON.parse($("#tasksVideos").val());
			this.url.create = $("#tasksCreateURL").val();
			this.url.edit = $("#tasksEditURL").val();
			this.url.getTasks = $("#getTasksURL").val();
			this.url.delete = $("#taskDeleteURL").val();
		},

		computed: {
		    filterTasks() {
		    	if (this.filterBy == 'pending') {
		    		return this.tasks.filter(task => task.status == "pending" )
		    	} else if (this.filterBy == 'completed') {
		    		return this.tasks.filter(task => task.status == "completed" )
		    	} else {
		    		return this.tasks.sort( (a,b) => a.id > b.id )
		    	}
			}
		},

		methods : {
			findTask(id) {
				for ( let task in this.tasks ) {
					if (this.tasks[task].id == id){
						this.task = this.tasks[task]
					}
				}
			},
			store(url) {
				formData = new FormData();

				for ( let key in this.task ) {
					let value = this.task[key];
					formData.append(key, value);
				}

				formData.append('_token', $('input[name=_token]').val());

				axios.post(url, formData).then((response) => {
					this.getTasks()
					this.$notify({
						title: 'Success',
						message: response.data.message,
						type: 'success'
					});
				})
				.catch( (error) => {
					this.$notify.error({
						title: 'Error',
						message: error.response.data.message
					});
				});
			},

			getTasks() {
				axios.get(this.url.getTasks).then((response) => {
					this.tasks = response.data.tasks;
				})
				.catch( (error) => {
					this.$notify.error({
						title: 'Error',
						message: error.response.data.message
					});
				});
			},

			deleteTask(id) {
				this.findTask(id);
				formData = new FormData();

				for ( let key in this.task ) {
					let value = this.task[key];
					formData.append(key, value);
				}

				formData.append('_token', $('input[name=_token]').val());

				const customAlert = swal({
					title: 'Warning',
					text: `Are you sure you want to delete this task? This action cannot be undone.`,
					icon: 'warning',
					closeOnClickOutside: false,
					buttons: {
						cancel: {
							text: "cancel",
							visible: true,
							className: "",
							closeModal: true,
						},
						confirm: {
							text: "Confirm",
							value: 'delete',
							visible: true,
							className: "btn-danger",
						}
					}
				});

				customAlert.then(value => {
					if (value == 'delete') {
						this.isLoading = true;
						this.remove(formData);
					}
				});
			},

			remove(data) {
				axios.post(this.url.delete, data).then((response) => {
					this.getTasks()
					this.$notify({
						title: 'Success',
						message: response.data.message,
						type: 'success'
					});
				})
				.catch( (error) => {
					this.$notify.error({
						title: 'Error',
						message: error.response.data.message
					});
				});
			},
		}
	});
}
