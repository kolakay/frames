if (window.Vue) {
    const vueApp = new Vue({
        el: '#app',
        data : {
            isLoading: false,
            filter: '',
            videos : [],
            videoView: 'grid',
            tags: [],
            filterTag: 'all',
            url : {
                edit : ``,
                view: ``,
                delete: ``,
                replace: ``
            },

        },
        mounted(){
            this.videos = JSON.parse($("#videos").val());
            this.tags = JSON.parse($("#tags").val());
            this.url.edit = $("#edit-video-url").val() + '/';
            this.url.view = $("#view-video-url").val() + '/';
            this.url.delete = $("#delete-video-url").val();
            this.url.replace = $("#replace-video-url").val() + '/';

        },
        computed: {
            getVideos() {

                var result = this.videos.filter((video) => {
                    if(this.filterTag == 'all'){
                        return video.name.toLowerCase().includes(this.filter.toLowerCase());
                    }else{
                            return video.name.toLowerCase().includes(this.filter.toLowerCase())
                        &&  video.tags.includes(this.filterTag.toString());

                    }
                });


                return result;
            }
        },
        methods : {
            createAsset(){
                // console.log(this.apartment)
                const formData = new FormData();

                for ( var key in this.video ) {
                    let value = this.video[key];
                    formData.append(key, value);
                }

                formData.append('_token', $('input[name=_token]').val());
                this.isLoading = true;
                axios.post(this.url.create, formData)
                    .then((response) => {
                        // console.log(response);
                        window.location = response.data.url;
                    })
                    .catch( (error) => {

                        if(error.response.data.errors){
                            const values = Object.values(error.response.data.errors);
                            for (let index = 0; index < values.length; index++) {
                                for (let j = 0; j < values[index].length; j++) {
                                    const element = values[index][j];
                                    this.formErrors.push(element);
                                }

                            }
                        }

                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                        this.isLoading = false;
                    });
            },

            deleteVideo(index){
                let video = Object.assign({}, this.videos[index]);
                video._token = $('input[name=_token]').val();
                const customAlert = swal({
                    title: 'Warning',
                    text: `Are you sure you want to delete this video? All features related to this video will be deleted. This action cannot be undone.`,
                    icon: 'warning',
                    closeOnClickOutside: false,
                    buttons: {
                        cancel: {
                            text: "cancel",
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Confirm",
                            value: 'delete',
                            visible: true,
                            className: "btn-danger",
                        }
                    }
                });

                customAlert.then(value => {
                    if (value == 'delete') {
                        this.isLoading = true;
                        axios.delete(this.url.delete, {data: video})
                            .then(response => {
                                this.isLoading = false;
                                this.videos.splice(index, 1);
                                this.$notify({
                                    title: 'Success',
                                    message: response.data.message,
                                    type: 'success'
                                });

                            }).catch(error => {
                                if (error.response) {
                                    this.isLoading = false;
                                    this.$notify.error({
                                        title: 'Error',
                                        message: error.response.data.message
                                    });
                                }else{
                                    this.$notify.error({
                                        title: 'Error',
                                        message: 'oops! Unable to complete request.'
                                    });
                                }
                            });

                    }
                });
            },

            copyVideoURL(video, hasThumbnail = false){

                if(hasThumbnail == true){
                    var videoLink = video.thumbnail_video_url;
                }else{
                    var videoLink = video.video_url;
                }

                const el = document.createElement('textarea');
                el.value =  videoLink;
                el.setAttribute('readonly', '');
                el.style.position = 'absolute';
                el.style.left = '-9999px';
                document.body.appendChild(el);
                el.select();
                document.execCommand('copy');
                if(document.execCommand("copy")) {
                    this.$notify({
                        title: 'Success',
                        message: 'Video link copied',
                        type: 'success'
                    });
                    document.body.removeChild(el);

                }else{
                    this.$notify.error({
                        title: 'Error',
                        message: 'Unable to copy video link'
                    });
                    document.body.removeChild(el);

                }
            },
            getImage(video){
                if(video.asset_images){
                    console.log(video.asset_images);
                    return video.asset_images[0].src
                }else{
                    console.log(video.asset_images);
                    return this.defaultImage;
                }
            }

        }
    });
}
