
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        cta: {
            name: '',
            type : 'email',
            button_text : '',
            is_default : false,
            description: '',

            email: '',
            email_button_text: '',
            email_custom_button_text: '',

            url: '',
            url_tab: 'blank',
            url_button_text:'',
            url_custom_button_text: '',

            phone_action : 'sms',
            phone: '',
            phone_button_text:'',
            phone_custom_button_text: '',

            chat_platform : 'messenger',
            chat_username: '',
            chat_button_text:'',
            chat_custom_button_text: '',

            calendar_platform : 'calendly',
            calendar_username: '',
            calendar_id: '',
            calendar_hash: '',
            calendar_button_text: '',
            calendar_custom_button_text: '',

            form : '',
            form_button_text: '',
            form_custom_button_text: '',

            iframe_title: '',
            iframe_source: '',
            iframe_height: '',
            iframe_scroll: false,
            iframe_button_text: '',
            iframe_custom_button_text: '',

            document_title : '',
            document_button_text: '',
            document_custom_button_text: '',
            document_upload : '',
            document_file: '',

            product_titles: '',
            product_button_text: '',
            product_custom_button_text: '',

            web_page_title: '',
            web_page_url: '',
            web_page_height: '',
            web_page_scroll: false,
            web_page_button_text: '',
            web_page_custom_button_text:''

        },
        forms: [],
        ctas: [
            {
                name: '',
                id: ''
            }
        ],
        CTAEdit: {
            id: '',
            name: '',
        },
        url : {
            create : '',
            update : '',
            delete : '',
            upload: ''
        }
    },
    mounted() {
        this.CTAEdit = Object.assign({}, this.cta);
        this.ctas = JSON.parse($("#ctas").val());
        this.forms = JSON.parse($("#forms").val());

        this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.delete = $("#delete-url").val();
        this.url.upload = $("#upload-url").val();
    },
    methods: {
        addCTA() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            // formData.append('cta_name', this.cta.name);
            for (let key in this.cta) {
                let value = this.cta[key];
                // if (key == 'access_levels'){
                //     value = JSON.stringify(value);
                // }
                formData.append(key, value);
            }
            this.isLoading = true;

                axios.post(this.url.create, formData)
                    .then((response) => {
                        $('#add-cta').modal('hide');
                        this.isLoading = false;

                        this.$notify({
                            title: 'Success',
                            message: response.data.message,
                            type: 'success'
                        });

                        this.ctas.push(Object.assign({}, response.data.cta, {}));
                    })
                    .catch((error) => {
                        this.isLoading = false;
                        if(error.response){
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        }else{
                            this.$notify.error({
                                title: 'Error',
                                message: 'oops! Unable to complete request.'
                            });
                        }

                    });
        },
        selectCTA(index){
            this.CTAEdit = Object.assign({}, this.ctas[index]);
        },
        readFile(){
            if (window.File && window.FileList && window.FileReader) {
                var files = event.target.files; //FileList object
                let formData = new FormData();

                var file = files[0];
                formData.append('document', file);
                this.storeDocument(formData, this.cta);
            }else {
                this.$notify.error({
                    title: 'Error',
                    message: 'Unable to complete request. Please update your browser'
                });
            }

        },
        updateFile(){
            if (window.File && window.FileList && window.FileReader) {
                var files = event.target.files; //FileList object
                let formData = new FormData();

                var file = files[0];
                formData.append('document', file);
                this.storeDocument(formData, this.CTAEdit);
            }else {
                this.$notify.error({
                    title: 'Error',
                    message: 'Unable to complete request. Please update your browser'
                });
            }
        },
        storeDocument(formData, currentCTA){
            formData.append('_token', $('input[name=_token]').val());
                formData.append('prev_document', currentCTA.document_file);

                this.isLoading = true;

                axios.post(this.url.upload, formData,{
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                }).then((response) => {
                    console.log(response.data.document_file)
                    currentCTA.document_file = response.data.document_file;
                    this.isLoading = false;
                })
                .catch( (error) => {

                    this.$notify.error({
                        title: 'Error',
                        message: 'Unable to complete request. Encountered an error'
                    });

                    this.isLoading = false;
                });
        },
        updateCTA(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in this.CTAEdit) {
                let value = this.CTAEdit[key];
                // if (key == 'access_levels'){
                //     value = JSON.stringify(value);
                // }
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.update, formData)
                .then((response) => {
                    $('#edit-cta').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    var editedCTA = response.data.cta;
                    this.ctas = this.ctas.map((cta) => {
                        if (cta.id === editedCTA.id) {
                            cta = Object.assign({}, editedCTA);
                        }
                        return cta;
                    });

                    // $('.data-table-custom').DataTable().destroy();
                    // $('.data-table-custom').DataTable();
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        deleteCTA(index){
            let cta = Object.assign({}, this.ctas[index]);
            cta._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this CTA? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: cta})
                        .then(response => {
                            this.isLoading = false;
                            this.ctas.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        }
    }
})
