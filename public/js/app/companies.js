
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        company: {
            name: '',
        },
        companies: [
            {
                name: '',
                id: ''
            }
        ],
        companyEdit: {
            id: '',
            name: '',
        },
        url : {
            create : '',
            update : '',
            delete : ''
        }
    },
    mounted() {
        this.companies = JSON.parse($("#companies").val());

        this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.delete = $("#delete-url").val();
    },
    methods: {
        addCompany() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('company_name', this.company.name);
            this.isLoading = true;

                axios.post(this.url.create, formData)
                    .then((response) => {
                        $('#add-company').modal('hide');
                        this.isLoading = false;

                        this.$notify({
                            title: 'Success',
                            message: response.data.message,
                            type: 'success'
                        });

                        this.companies.push(Object.assign({}, response.data.company, {}));
                    })
                    .catch((error) => {
                        this.isLoading = false;
                        if(error.response){
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        }else{
                            this.$notify.error({
                                title: 'Error',
                                message: 'oops! Unable to complete request.'
                            });
                        }

                    });
        },
        selectCompany(index){
            this.companyEdit = Object.assign({}, this.companies[index]);
        },
        updateCompany(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            formData.append('company_name', this.companyEdit.name);
            formData.append('company_id', this.companyEdit.id);
            this.isLoading = true;

            axios.post(this.url.update, formData)
                .then((response) => {
                    $('#edit-company').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    var company = response.data.company;
                    this.companies = this.companies.map((company) => {
                        if (company.id === company.id) {
                            company = Object.assign({}, company);
                        }
                        return company;
                    });

                    // $('.data-table-custom').DataTable().destroy();
                    // $('.data-table-custom').DataTable();
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        deleteCompany(index){
            let company = Object.assign({}, this.companies[index]);
            company._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this Company? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: company})
                        .then(response => {
                            this.isLoading = false;
                            this.companies.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        }
    }
})
