
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        contact: {
            email: '',
            first_name: '',
            last_name: '',
            company: '',
            title: '',
            phone: '',
            mobile: '',
            tags: [],
            notes: '',
            source: '',
            unsubscribed: false,
            user_id: ''
        },
        contacts: [
            {
                id: '',
                email: '',
                first_name: '',
                last_name: '',
                company: '',
                title: '',
                phone: '',
                mobile: '',
                tags: [],
                notes: '',
                source: '',
                unsubscribed: false,
                user_id: ''
            }
        ],
        contactEdit: {
            id: '',
            email: '',
            first_name: '',
            last_name: '',
            company: '',
            title: '',
            phone: '',
            mobile: '',
            tags: [],
            notes: '',
            source: '',
            unsubscribed: false,
            user_id: ''
        },
        companies: [],
        tags: [],
        url : {
            create : '',
            update : '',
            delete : ''
        }
    },
    mounted() {
        this.contacts = JSON.parse($("#contacts").val());
        this.companies = JSON.parse($("#companies").val());
        this.tags = JSON.parse($("#tags").val());

        this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.delete = $("#delete-url").val();
    },
    methods: {
        addContact() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            this.contact.tags = $("#contact-tags").val();
            for (let key in this.contact) {
                let value = this.contact[key];
                if (key == 'tags'){
                    value = JSON.stringify(value);
                }
                formData.append(key, value);
            }
            this.isLoading = true;

                axios.post(this.url.create, formData)
                    .then((response) => {
                        $('#add-contact').modal('hide');
                        this.isLoading = false;

                        this.$notify({
                            title: 'Success',
                            message: response.data.message,
                            type: 'success'
                        });

                        this.contacts.push(Object.assign({}, response.data.contact, {}));
                    })
                    .catch((error) => {
                        this.isLoading = false;
                        if(error.response){
                            this.$notify.error({
                                title: 'Error',
                                message: error.response.data.message
                            });
                        }else{
                            this.$notify.error({
                                title: 'Error',
                                message: 'oops! Unable to complete request.'
                            });
                        }

                    });
        },
        selectContact(index){
            this.contactEdit = Object.assign({}, this.contacts[index]);
            var tags = JSON.parse(this.contactEdit.tags);
            $(document).find("#contactEdit-tags").val(tags);
            $('#contactEdit-tags').trigger('change');
        },
        updateContact(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            this.contactEdit.tags = $("#contactEdit-tags").val();
            for (let key in this.contactEdit) {
                let value = this.contactEdit[key];
                if (key == 'tags'){
                    value = JSON.stringify(value);
                }
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.update, formData)
                .then((response) => {
                    $('#edit-contact').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    var editedContact = response.data.contact;
                    this.contacts = this.contacts.map((contact) => {
                        if (contact.id === editedContact.id) {
                            contact = Object.assign({}, editedContact);
                        }
                        return contact;
                    });

                    // $('.data-table-custom').DataTable().destroy();
                    // $('.data-table-custom').DataTable();
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        deleteContact(index){
            let contact = Object.assign({}, this.contacts[index]);
            contact._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this Contact? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: contact})
                        .then(response => {
                            this.isLoading = false;
                            this.contacts.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        }
    }
})
