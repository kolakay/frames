
var vueApp = new Vue({
    el: "#app",
    data : {
        isLoading : false,
        tags: [],
        videos: [],
        nodes:[],
        diagram:{},
        workflow: {
            id: '',
            name: '',
            trigger: '',
            includes: 'all',
            elements: [],
            build: '',
            trigger_once: true,
            is_active: true,
            terms_accepted: true
        },
        newNode: {
            node_id: '',
            type: '',
            normal_branch : "",
            yes_branch : "",
            no_branch : "",
            parent : "",
            condition_type: '',

            action: '',
            condition: '',
            add_tags: [],
            remove_tags: [],
            element_id: '',
            task_title: '',
            task_description: '',
            task_assignee: '',
            task_due_after: '',
            task_unit: '',
            delay: '',
            unit: '',
            campaign_id: ''
        },
        nodeEdit: {
            node_id: '',
            type: '',
            normal_branch : "",
            yes_branch : "",
            no_branch : "",
            parent : "",
            condition_type: '',

            action: '',
            condition: '',
            add_tags: [],
            remove_tags: [],
            element_id: '',
            task_title: '',
            task_description: '',
            task_assignee: '',
            task_due_after: '',
            task_unit: '',
            delay: '',
            unit: '',
            campaign_id: ''
        },
        elements: [],

        url : {
            save : '',
            workflows: '',
            saveWorkflow: '',
            saveNode: '',
            updateNode: '',
        },
        ctas: [],
        contacts: [],
        emailIntegrations: [],
        emailTemplates:[],
        step: 1,
        campaigns: []
    },
    async mounted() {
        let vueInstance = this;
        this.videos = JSON.parse($("#dbvideos").val());
        this.tags = JSON.parse($("#dbtags").val());
        this.ctas = JSON.parse($("#dbctas").val());
        this.contacts = JSON.parse($("#contacts").val());
        this.campaigns = JSON.parse($("#e-campaigns").val());
        let workflow = JSON.parse($("#dbworkflow").val());
        let nodes = JSON.parse($("#nodes").val());

        // this.emailTemplates = JSON.parse($("#emailTemplates").val());
        // this.emailIntegrations = JSON.parse($("#emailIntegrations").val());

        this.url.save = $("#save-url").val();
        this.url.workflows = $("#list-url").val();

        this.url.saveWorkflow = $("#save-workflow-url").val();
        this.url.saveNode = $("#save-node-url").val();
        this.url.updateNode = $("#update-node-url").val();

        if(workflow){
            this.workflow =  workflow;
            let elements = Object.values(this.workflow.elements);

            if(this.workflow.trigger == 'tag_added'){
                $("#triggerTags").val(elements).trigger('change');
            }else if (this.workflow.trigger == 'cta_clicked') {
                $("#ctas").val(elements).trigger('change');
            } else if (this.workflow.trigger == 'video_watched') {
                $("#videos").val(elements).trigger('change');
            }        
        }
        this.nodes = (nodes.length > 0 )? nodes : this.nodes;
        

    },
    updated(){
        if(this.step == 2){
            this.initFlowSVG();
            // this.$nextTick(() => {
                
            // })
        }
    },
    methods: {
        setStep(event, number){
            event.preventDefault();
            this.step = number;
            // if(this.step == 2){
            //     // this.step += 1;
            //     this.initFlowSVG();
            // }
        },
        nextStep(){
            // console.log($("#user-timepicker").val());
            if(this.step == 1){
                this.saveWorkFlowDetails();
            }
            
            if(this.step < 3){
                this.step += 1;
                
            }
        },
        saveWorkFlowDetails(exist = false){
            const formData = new FormData();
            if(this.workflow.trigger == 'tag_added'){
                this.workflow.elements = JSON.stringify($("#triggerTags").val());
            }else if (this.workflow.trigger == 'cta_clicked') {
                this.workflow.elements = JSON.stringify($("#ctas").val());
            } else if (this.workflow.trigger == 'video_watched') {
                this.workflow.elements = JSON.stringify($("#videos").val());
            }
            
            for ( var key in this.workflow ) {
                let value = this.workflow[key];
                formData.append(key, value);
            }
            formData.append('_token', $('input[name=_token]').val());
            axios.post(this.url.saveWorkflow, formData)
            .then((response) => {

                this.workflow.id = response.data.workflow.id;
                if(exist){
                    window.location = this.url.workflows;
                }
                
                this.$notify({
                    title: 'Success',
                    message: response.data.message,
                    type: 'success'
                });
            })
            .catch((error) => {

                if(error.response){
                    this.$notify.error({
                        title: 'Error',
                        message: error.response.data.message
                    });
                }else{
                    this.$notify.error({
                        title: 'Error',
                        message: 'oops! Unable to complete request.'
                    });
                }

            });

        },
        storeNode(){
            const formData = new FormData();
            this.newNode.add_tags = JSON.stringify($("#addTags").val());
            this.newNode.remove_tags = JSON.stringify($("#removeTags").val());
            
            for ( var key in this.newNode ) {
                let value = this.newNode[key];
                formData.append(key, value);
            }
            formData.append('workflow_id', this.workflow.id);
            formData.append('_token', $('input[name=_token]').val());
            axios.post(this.url.saveNode, formData)
            .then((response) => {

                this.nodes = response.data.nodes;
                $("#add-process").modal("hide");
                this.initFlowSVG();
                
                this.$notify({
                    title: 'Success',
                    message: response.data.message,
                    type: 'success'
                });
            })
            .catch((error) => {

                if(error.response){
                    this.$notify.error({
                        title: 'Error',
                        message: error.response.data.message
                    });
                }else{
                    this.$notify.error({
                        title: 'Error',
                        message: 'oops! Unable to complete request.'
                    });
                }

            });
        },
        updateNode(){
            const formData = new FormData();
            this.nodeEdit.add_tags = JSON.stringify($("#editAddTags").val());
            this.nodeEdit.remove_tags = JSON.stringify($("#editRemoveTags").val());
            
            for ( var key in this.nodeEdit ) {
                let value = this.nodeEdit[key];
                formData.append(key, value);
            }
            formData.append('workflow_id', this.workflow.id);
            formData.append('_token', $('input[name=_token]').val());
            axios.post(this.url.updateNode, formData)
            .then((response) => {

                this.nodes = response.data.nodes;
                $("#edit-process").modal("hide");
                this.initFlowSVG();
                
                this.$notify({
                    title: 'Success',
                    message: response.data.message,
                    type: 'success'
                });
            })
            .catch((error) => {

                if(error.response){
                    this.$notify.error({
                        title: 'Error',
                        message: error.response.data.message
                    });
                }else{
                    this.$notify.error({
                        title: 'Error',
                        message: 'oops! Unable to complete request.'
                    });
                }

            });
        },
        publish(){
            if(this.step == 3){
                // this.initFlowSVG();
                this.saveDraft(true);
            }
        },
        selectVideo(id){
            // console.log(id);
            this.emailCampaign.video_id = id;
        },
        saveDraft(){
            // this.emailCampaign.provider_id = $("#providerId").val();
            // let emailBody = tinymce.activeEditor.getContent();
            // this.emailCampaign.email_body = emailBody;
            // this.emailCampaign.emails = JSON.stringify($("#contactEmails").val());
            // this.emailCampaign.inclusion_list = JSON.stringify($("#inclusionList").val());
            // this.emailCampaign.exclusion_list = JSON.stringify($("#exclusionList").val());
            // this.emailCampaign.schedule_date = $("#user-timepicker").val();
            // const formData = new FormData();
            // for ( var key in this.emailCampaign ) {
            //     let value = this.emailCampaign[key];
            //     formData.append(key, value);
            // }
            // formData.append('is_save_and_publish', publish);
            // formData.append('_token', $('input[name=_token]').val());


            // axios.post(this.url.save, formData)
            // .then((response) => {
            //     // this.isLoading = false;
            //     this.emailCampaign.id = response.data.campaign.id;
            //     if(publish == true){
            //         window.location = this.url.campaigns;
            //     }
            //     this.$notify({
            //         title: 'Success',
            //         message: response.data.message,
            //         type: 'success'
            //     });
            // })
            // .catch((error) => {
            //     // this.isLoading = false;

            //     if(error.response){
            //         this.$notify.error({
            //             title: 'Error',
            //             message: error.response.data.message
            //         });
            //     }else{
            //         this.$notify.error({
            //             title: 'Error',
            //             message: 'oops! Unable to complete request.'
            //         });
            //     }

            // });
        },
        initFlowSVG(){
            let code = '';
            // this.nodes = [
            //     {
            //         node_id : "node1",
            //         type : "operation",
            //         normal : "node2",
            //         left : "",
            //         no : "",
            //         parent : ""
            //     },
            //     {
            //         node_id : "node2",
            //         type : "condition",
            //         normal : "",
            //         parent : "node1",
            //         yes : "node3",
            //         no : "node4",

            //     },
            //     {
            //         node_id : "node3",
            //         type : "operation",
            //         normal : "",
            //         parent : "node2",
            //         yes : "",
            //         no : "",
                    
            //     },
            //     {
            //         node_id : "node4",
            //         type : "condition",
            //         normal : "",
            //         parent : "node2",
            //         yes : "node5",
            //         no : "node6",
                    
            //     },
            //     {
            //         node_id : "node5",
            //         type : "operation",
            //         normal : "",
            //         parent : "node4",
            //         yes : "",
            //         no : "",
                    
            //     },
            //     {
            //         node_id : "node6",
            //         type : "operation",
            //         normal : "",
            //         parent : "node4",
            //         yes : "",
            //         no : "",
                    
            //     }
            // ];
            let treeCode = 'start=>start: Workflow Triggered\n';// + 'e=>end\n';
            if(this.nodes.length > 0){
                for(key in this.nodes){
                    value = this.nodes[key];
                    let text = value.node_text;
                    treeCode += `${value.node_id}=>${value.type}: ${text}:>javascript:editNode(${value.node_id})` + '\n';
                    if(value.type != "operation"){
                        if(value.yes_branch == ""){
                            treeCode += `${value.node_id}_add_yes=>end: Add yes node:>javascript:addNode(${value.node_id}, 'yes')` + '\n';
                        }
                        if(value.no_branch == ""){
                            treeCode += `${value.node_id}_add_no=>end: Add no node:>javascript:addNode(${value.node_id}, 'no')` + '\n';
                        }
    
                    }else if (value.type == "operation"){
                        if(value.normal_branch == ""){
                            treeCode += `${value.node_id}_add_node=>end: Add new action:>javascript:addNode(${value.node_id}, 'normal')` + '\n';
                        }
                    }
                }
            }else{
                treeCode += 'node_0=>end: Add new action:>javascript:addNode(node_0, "normal")\n';
            }
            console.log(this.nodes);
           
            
            let tree = "start";
            if(this.nodes.length > 0){
                tree += this.getNodeBranch(tree, this.nodes[0].node_id);
            }else{
                tree += "->node_0";
            }

            let fullCode = treeCode + "\n" + tree;
            // if(this.diagram.length > 0){
            //     this.diagram.clean();
            // }
            let diagram = this.drawFlowChart(fullCode);
            diagram.clean();
            diagram = this.drawFlowChart(fullCode);

        },
        getNodeBranch(currentTree, currentNodeId){
            currentTree = "";
            let currentNode = {};
            for(var i=0; i< this.nodes.length; i++) {
              if(this.nodes[i].node_id === currentNodeId){
                currentNode = this.nodes[i];
              }
            }
            currentTree += "->" + currentNode.node_id;
            if(currentNode.type != "operation"){
              if(currentNode.yes_branch != ""){
                currentTree += '\n'+ `${currentNode.node_id}(yes)` + this.getNodeBranch(currentTree, currentNode.yes_branch)
              }
              else{
                currentTree += '\n'+ `${currentNode.node_id}(yes)` + "->" + currentNode.node_id + '_add_yes';
              }
              if(currentNode.no_branch != ""){
                currentTree += '\n'+ `${currentNode.node_id}(no)` + this.getNodeBranch(currentTree, currentNode.no_branch)
              }
              else{
                currentTree += '\n'+ `${currentNode.node_id}(no)` + "->" + currentNode.node_id + '_add_no';
              }
                //$tree += " cond()"
            }else if (currentNode.type == "operation"){
                if(currentNode.normal_branch != ""){
                    currentTree += this.getNodeBranch(currentTree, currentNode.normal_branch)
                }
                else{
                  currentTree += "->" + currentNode.node_id + '_add_node';
                }
            }
          
            return currentTree;
        },
        drawFlowChart(code){
            $("#canvas").html('');
            var diagram = flowchart.parse(code); 
            diagram.drawSVG('canvas',{
              "line-length": 50,
              "line-color": "#555",
              "font-color": "#555",
              "element-color": "#555"
            });
          
            return diagram;
        },
        addNewNode(node, type = 'normal'){
            let nodeId = $(node).attr('id');
            let nodeData = nodeId.split("_"); 

            this.newNode.parent = nodeId;
            this.newNode.condition_type = type;
            $("#add-process").modal("show");
        },
        editNode(node){
            let nodeId = $(node).attr('id');
            let currentNode = {};
            for(var i=0; i< this.nodes.length; i++) {
              if(this.nodes[i].node_id === nodeId){
                currentNode = this.nodes[i];
              }
            }
            if(currentNode){
                this.nodeEdit = Object.assign({}, currentNode);
                $("#edit-process").modal("show");
            }
        }
          

    }
})
window.VUE_APP = vueApp;