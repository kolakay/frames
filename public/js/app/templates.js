
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        template: {
            name: ''
        },
        templates: [
            {
                id: '',
                name: ''
            }
        ],
        defaultTemplates: [
            {
                id: '',
                name: ''
            }
        ],
        templateEdit: {
            id: '',
            name: '',
        },
        url : {
            create : '',
            clone : '',
            update : '',
            delete : '',
            edit: '',
            preview: ''
        }
    },
    mounted() {
        this.templates = JSON.parse($("#templates").val());
        this.defaultTemplates = JSON.parse($("#defaultTemplates").val());

        this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.delete = $("#delete-url").val();
        this.url.clone = $("#clone-url").val();
        this.url.edit = $("#edit-url").val() + '/';
        this.url.preview = $("#preview-url").val() + '/';
    },
    methods: {
        addTemplate() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in this.template) {
                let value = this.template[key];

                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.create, formData)
                .then((response) => {
                    $('#add-template').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    window.location = response.data.url;

                    this.templates.push(Object.assign({}, response.data.template, {}));
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        selectTemplate(index){
            this.templateEdit = Object.assign({}, this.templates[index]);

        },
        updateTemplate(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in this.templateEdit) {
                let value = this.templateEdit[key];
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.update, formData)
                .then((response) => {
                    $('#edit-template').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    var editedtemplate = response.data.template;
                    this.templates = this.templates.map((template) => {
                        if (template.id === editedtemplate.id) {
                            template = Object.assign({}, editedtemplate);
                        }
                        return template;
                    });

                    // $('.data-table-custom').DataTable().destroy();
                    // $('.data-table-custom').DataTable();
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        deleteTemplate(index){
            let template = Object.assign({}, this.templates[index]);
            template._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this template? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: template})
                        .then(response => {
                            this.isLoading = false;
                            this.templates.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        },
        cloneTemplate(index){
            let template = Object.assign({}, this.templates[index]);
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in template) {
                let value = template[key];
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.clone, formData)
                .then((response) => {
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    this.templates.push(Object.assign({}, response.data.template, {}));
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        }
    }
})
