
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        forms: [],
        form: {
            name: '',
            title : '',
            button_text : 'SUBMIT',
            description: '',
            custom_button_text: '',
            fields : [
                {
                    name: 'Email',
                    type: 'email',
                    is_set: true,
                    is_required: true,

                },
                {
                    name: 'Company',
                    type: 'text',
                    is_set: false,
                    is_required: false,

                },
                {
                    name: 'First Name',
                    type: 'text',
                    is_set: false,
                    is_required: false,

                },
                {
                    name: 'Last Name',
                    type: 'text',
                    is_set: false,
                    is_required: false,

                },
                {
                    name: 'Mobile Phone',
                    type: 'text',
                    is_set: false,
                    is_required: false,

                },{
                    name: 'Phone',
                    type: 'text',
                    is_set: false,
                    is_required: false,

                }

            ],
            complete_action: 'url',
            message: '',
            url: '',
            tags: [],

        },
        tags: [
            {
                name: '',
                id: ''
            }
        ],
        formEdit: {
            id: '',
            name: '',
        },
        url : {
            create : '',
            update : '',
            delete : ''
        }
    },
    mounted() {
        this.formEdit = Object.assign({}, this.form);
        this.forms = JSON.parse($("#forms").val());
        this.tags = JSON.parse($("#tags").val());

        this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.delete = $("#delete-url").val();
    },
    methods: {
        addForm() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in this.form) {
                let value = this.form[key];
                if (key == 'fields' || key == 'tags'){
                    value = JSON.stringify(value);
                }
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.create, formData)
                .then((response) => {
                    $('#add-form').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    this.forms.push(Object.assign({}, response.data.form, {}));
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        selectForm(index){
            this.formEdit = Object.assign({}, this.forms[index]);
        },
        updateForm(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            for (let key in this.formEdit) {
                let value = this.formEdit[key];
                if (key == 'fields' || key == 'tags'){
                    value = JSON.stringify(value);
                }
                formData.append(key, value);
            }
            this.isLoading = true;

            axios.post(this.url.update, formData)
                .then((response) => {
                    $('#edit-form').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    var editedForm = response.data.form;
                    this.forms = this.forms.map((form) => {
                        if (form.id === editedForm.id) {
                            form = Object.assign({}, editedForm);
                        }
                        return form;
                    });

                    // $('.data-table-custom').DataTable().destroy();
                    // $('.data-table-custom').DataTable();
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        deleteForm(index){
            let form = Object.assign({}, this.forms[index]);
            form._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this Form? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: form})
                        .then(response => {
                            this.isLoading = false;
                            this.forms.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        }
    }
})
