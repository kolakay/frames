
new Vue({
    el: "#app",
    isLoading : false,
    data : {
        configuration: {
            type: 'email',
            service: '',
            email: '',
            limit_per_day: 1000,
            cc: '',
            bcc: '',
            mandrill_username:'',
            mandrill_password:'',
            sendgrid_api_key: '',
            smtp_host: '',
            smtp_port: '',
            smtp_username: '',
            smtp_password:'',
            smtp_encryption: 'none',
            is_active: false,
            ses_key: '',
            ses_secret: '',
            ses_region:''
        },
        configurations: [
            {
                type: '',
                service: '',
                email: '',
                limit_per_day: '',
                cc: '',
                bcc: '',
                mandrill_username:'',
                mandrill_password:'',
                sendgrid_api_key: '',
                smtp_host: '',
                smtp_port: '',
                smtp_username: '',
                smtp_password:'',
                smtp_encryption: 'none',
                is_active: false,
                ses_key: '',
                ses_secret: '',
                ses_region:''
            }
        ],
        configurationEdit: {
            type: '',
            service: '',
            email: '',
            limit_per_day: '',
            cc: '',
            bcc: '',
            mandrill_username:'',
            mandrill_password:'',
            sendgrid_api_key: '',
            smtp_host: '',
            smtp_port: '',
            smtp_username: '',
            smtp_password:'',
            smtp_encryption: 'none',
            is_active: false,
            ses_key: '',
            ses_secret: '',
            ses_region:''
        },
        url : {
            create : '',
            update : '',
            delete : ''
        }
    },
    mounted() {
        this.configurations = JSON.parse($("#configurations").val());

        this.url.create = $("#create-url").val();
        this.url.update = $("#update-url").val();
        this.url.delete = $("#delete-url").val();
    },
    methods: {
        addConfiguration() {
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            this.isLoading = true;
            for (let key in this.configuration) {
                let value = this.configuration[key];
                formData.append(key, value);
            }
            axios.post(this.url.create, formData)
                .then((response) => {
                    $('#add-configuration').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    this.configurations.push(Object.assign({}, response.data.configuration, {}));
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        selectConfiguration(index){
            this.configurationEdit = Object.assign({}, this.configurations[index]);
        },
        updateConfiguration(){
            const formData = new FormData();
            formData.append('_token', $('input[name=_token]').val());
            this.isLoading = true;
            for (let key in this.configurationEdit) {
                let value = this.configurationEdit[key];
                formData.append(key, value);
            }

            axios.post(this.url.update, formData)
                .then((response) => {
                    $('#edit-configuration').modal('hide');
                    this.isLoading = false;

                    this.$notify({
                        title: 'Success',
                        message: response.data.message,
                        type: 'success'
                    });

                    var editConfiguration = response.data.configuration;
                    this.configurations = this.configurations.map((configuration) => {
                        if (editConfiguration.id === configuration.id) {
                            configuration = Object.assign({}, editConfiguration);
                        }
                        return configuration;
                    });

                    // $('.data-table-custom').DataTable().destroy();
                    // $('.data-table-custom').DataTable();
                })
                .catch((error) => {
                    this.isLoading = false;
                    if(error.response){
                        this.$notify.error({
                            title: 'Error',
                            message: error.response.data.message
                        });
                    }else{
                        this.$notify.error({
                            title: 'Error',
                            message: 'oops! Unable to complete request.'
                        });
                    }

                });
        },
        deleteConfiguration(index){
            let configuration = Object.assign({}, this.configurations[index]);
            configuration._token = $('input[name=_token]').val();

            const customAlert = swal({
                title: 'Warning',
                text: `Are you sure you want to delete this Configuration? This action cannot be undone.`,
                icon: 'warning',
                closeOnClickOutside: false,
                buttons: {
                    cancel: {
                        text: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "Confirm",
                        value: 'delete',
                        visible: true,
                        className: "btn-danger",
                    }
                }
            });

            customAlert.then(value => {
                if (value == 'delete') {
                    this.isLoading = true;
                    axios.delete(this.url.delete, {data: configuration})
                        .then(response => {
                            this.isLoading = false;
                            this.configurations.splice(index, 1);
                            this.$notify({
                                title: 'Success',
                                message: response.data.message,
                                type: 'success'
                            });

                        }).catch(error => {
                            if (error.response) {
                                this.isLoading = false;
                                this.$notify.error({
                                    title: 'Error',
                                    message: error.response.data.message
                                });
                            }else{
                                this.$notify.error({
                                    title: 'Error',
                                    message: 'oops! Unable to complete request.'
                                });
                            }
                        });

                }
            });
        }
    }
})
