1
00:00:00,000 --> 00:00:04,620
In sales cold calling and emailing doesn't work like it used to. Now you

2
00:00:04,620 --> 00:00:09,150
have to stand out from the crowd. Capturing and sharing videos is one of

3
00:00:09,090 --> 00:00:12,570
the most valuable things that you can do for sales by sharing sales

4
00:00:12,540 --> 00:00:16,380
videos. You can build trust, streamline your back and forth communication

5
00:00:16,350 --> 00:00:21,150
and eventually build longlasting, mutually beneficial relationships, check

6
00:00:21,090 --> 00:00:25,080
out this sample video. Hi, my name is Shannon. And I just wanted to see if you

7
00:00:25,020 --> 00:00:28,530
needed some help with your business. We have a great value proposition,

8
00:00:28,500 --> 00:00:32,130
and if you click on the buttons below, you can learn more or schedule. A

9
00:00:32,070 --> 00:00:33,750
time to talk. Thanks so much.