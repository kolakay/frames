@extends('Layouts.auth')
@section('header')
    <title>Reset Password</title>

@endsection
@section('content')
    <div class="form-side col-12">
        <a href="#"><span class="logo-single"></span></a>
        <h6 class="mb-4">Reset Password</h6>
        @include('Includes.messages')
        <form method="POST" action="{{route('auth.update-password')}}">
            @csrf
            <input type="hidden" name="token" value="{{ $user->token }}" />
            <label class="form-group has-float-label mb-4">
                <input class="form-control" type="password" name="password" placeholder="">
                <span>Password</span>
            </label>
            <label class="form-group has-float-label mb-4">
                <input class="form-control" type="password" name="re-type_password">
                <span>Confirm Password</span>
            </label>
            {{-- <div class="d-flex justify-content-between align-items-center"><a href="{{ route('auth.forgot-password') }}">Forget password?</a> --}}
                <button class="btn btn-primary btn-lg btn-shadow" type="submit">Update Password</button>
            </div>
        </form>
    </div>
@endsection
