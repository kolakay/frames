@extends('Layouts.auth')
@section('header')
    <title>Forgot Password</title>
@endsection
@section('content')
<div class="form-side col-12">
    <a href="#">
        <span class="logo-single"></span>
    </a>
    <h6 class="mb-4">Forgot Password</h6>
    @include('Includes.messages')
    <form method="POST" action="{{ route('auth.recover-password') }}">
        @csrf
        <label class="form-group has-float-label mb-4">
            <input class="form-control" name="email"> <span>E-mail</span></label>
        <div class="d-flex justify-content-end align-items-center">
            <button class="btn btn-primary btn-lg btn-shadow" type="submit">RESET</button>
        </div>
    </form>
</div>
@endsection
