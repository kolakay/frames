@extends('Layouts.auth')
@section('header')
    <title>Login</title>
@endsection
@section('content')
    <div class="form-side col-12">
        <a href="#"><span class="logo-single"></span></a>
        <h6 class="mb-4">Login</h6>
        @include('Includes.messages')
        <form method="POST" action="{{route('auth.login.account')}}">
            @csrf
            <label class="form-group has-float-label mb-4">
                <input class="form-control" name="email"> <span>E-mail</span></label>
            <label class="form-group has-float-label mb-4">
                <input class="form-control" type="password" name="password" placeholder=""> <span>Password</span></label>
            <div class="d-flex justify-content-between align-items-center"><a href="{{ route('auth.forgot-password') }}">Forget password?</a>
                <button class="btn btn-primary btn-lg btn-shadow" type="submit">LOGIN</button>
            </div>
        </form>
    </div>
@endsection
