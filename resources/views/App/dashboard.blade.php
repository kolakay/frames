@extends('Layouts.master')
@section('header')
<title>Dashboard</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main>
        <style>
            .progressbar-text {
                color : #FFF !important;
            }
            .stat-count{
                font-size: 34px;
                color: #FFF;
            }
            .br-0{
                border-radius: 0px !important;
            }
            .caption{
                font-size: 16px;
                font-weight: 800;
            }
            .menu-btn{
                font-size: 20px;
                text-align: center!important;
            }
            #messagesContainer {
                overflow-y: scroll; 
            }
        </style>
        <div class="container-fluid" id="app">
            <div class="row">
                <div class="col-12">
                    <h1>Dashboard</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-md-12">
                    <div class="card br-0">
                        <div class="card-body" id="messagesContainer" style="max-height: 200px;overflow-y: hidden;">
                            <h5 class="card-title mb-1">Messages</h5>
                            <div class="scroll dashboard-logs ps ps--active-y">
                                <table class="table table-sm table-borderless">
                                    <tbody>
                                        <tr>
                                            <td><span class="log-indicator border-theme-1 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New user registiration</span></td>
                                            <td class="text-right"><span class="text-muted">14:12</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-theme-2 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New sale: Soufflé</span></td>
                                            <td class="text-right"><span class="text-muted">13:20</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-danger align-middle"></span></td>
                                            <td><span class="font-weight-medium">14 products added</span></td>
                                            <td class="text-right"><span class="text-muted">12:55</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-theme-2 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New sale: Napoleonshat</span></td>
                                            <td class="text-right"><span class="text-muted">12:44</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-theme-2 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New sale: Cremeschnitte</span></td>
                                            <td class="text-right"><span class="text-muted">12:30</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-theme-2 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New sale: Soufflé</span></td>
                                            <td class="text-right"><span class="text-muted">10:40</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-danger align-middle"></span></td>
                                            <td><span class="font-weight-medium">2 categories added</span></td>
                                            <td class="text-right"><span class="text-muted">10:20</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-theme-2 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New sale: Chocolate Cake</span></td>
                                            <td class="text-right"><span class="text-muted">09:28</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-theme-2 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New sale: Bebinca</span></td>
                                            <td class="text-right"><span class="text-muted">09:25</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-theme-2 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New sale: Bebinca</span></td>
                                            <td class="text-right"><span class="text-muted">09:20</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="log-indicator border-theme-2 align-middle"></span></td>
                                            <td><span class="font-weight-medium">New sale: Chocolate Cake</span></td>
                                            <td class="text-right"><span class="text-muted">08:20</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                </div>
                                <div class="ps__rail-y" style="top: 0px; height: 270px; right: 0px;">
                                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 232px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-4">
                    <div class="row sortable">
                        <div class="col-xl-3 col-lg-6 mb-4">
                            <div class="card">

                                <div class="card-body bg-primary text-right pr-2">
                                    <p class="col-12 mb-2 stat-count">{{ $videoCount }}</p>
                                    <h2 class="mb-0 text-white col-12 mb-2">Videos</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 mb-4">
                            <div class="card">

                                <div class="card-body pr-2 bg-info text-right">
                                    <p class="col-12 mb-2 stat-count">0</p>
                                    <h2 class="mb-0 text-white col-12 mb-2">Total Video Views</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 mb-4">
                            <div class="card">

                                <div class="card-body pr-2 bg-danger text-right">
                                    <p class="col-12 mb-2 stat-count">{{ $ctaCount }}</p>
                                    <h2 class="mb-0 text-white col-12 mb-2">Calls To Action</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 mb-4">
                            <div class="card">

                                <div class="card-body pr-2 bg-warning text-right">
                                    <p class="col-12 mb-2 stat-count">0</p>
                                    <h2 class="mb-0 text-white col-12 mb-2">Team Members</h2>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt-4">
                    <div class="row">
                        <div class="col-9 card p-4 br-0">
                            @csrf
                            @include('Includes.Video.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="videos">{{ json_encode($videos) }}</textarea>
        <textarea style="display:none" id="tags">{{ json_encode($tags) }}</textarea>

        <textarea style="display:none" id="view-video-url">{{ route('user.videos.view') }}</textarea>
        <textarea style="display:none" id="edit-video-url">{{  route('user.videos.edit') }}</textarea>
        <textarea style="display:none" id="delete-video-url">{{  route('user.videos.delete') }}</textarea>
        <textarea style="display:none" id="replace-video-url">{{  route('user.videos.replace') }}</textarea>
    </main>
@endsection
@section('scripts')
<script src="{{ asset('js/vendor/progressbar.min.js') }}" style="opacity: 1;"></script>
<script src="{{ asset('js/app/dashboard.js') }}" style="opacity: 1;"></script>
@endsection
