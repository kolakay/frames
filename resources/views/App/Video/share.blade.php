<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <style>
        .myvideo {
            height: 100vh;
            width: 100vw;
        }
    </style>
    {{-- <div> --}}
        <video controls  class="myvideo border-primary d-flex" style="object-fit: fill;">
            <source src="{{ $video->video_path }}" type="video/mp4">
        </video>
    {{-- </div> --}}
</body>
</html>
