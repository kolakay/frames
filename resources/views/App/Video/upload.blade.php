@extends('Layouts.master')
@section('header')
<title>Upload Video</title>
@endsection

@section('content')
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Upload</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="card col-8 mx-auto">
                    <div class="card-header pl-0 pr-0">
                        <ul class="nav nav-tabs upload-options card-header-tabs ml-0 mr-0" role="tablist">
                            <li class="nav-item w-25 text-center">
                                <a class="nav-link active" id="first-tab_"
                                    data-toggle="tab" href="#computer"
                                    role="tab" aria-controls="first"
                                    aria-selected="true">Upload From Computer</a>
                            </li>
                            <li class="nav-item w-25 text-center">
                                <a class="nav-link" id="second-tab_"
                                    data-toggle="tab" href="#record"
                                    role="tab" aria-controls="second"
                                    aria-selected="false">Record with Camera</a>
                            </li>
                            <li class="nav-item w-25 text-center">
                                <a class="nav-link" id="third-tab_"
                                    data-toggle="tab" href="#youtube"
                                    role="tab" aria-controls="youtube"
                                    aria-selected="false">Add From Youtube</a>
                            </li>
                        </ul>
                        <hr>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade active show"
                                id="computer" role="tabpanel"
                                aria-labelledby="first-tab_">
                                {{-- <div class="card no-shadow col-12"> --}}
                                    <div class="card-body">
                                        <form action="/file-upload">
                                            @csrf
                                            <div class="dropzone dz-clickable">
                                                <div class="dz-default dz-message">

                                                    <span><i class="iconsminds-laptop---phone"></i> <br />Click or Drag File to Upload Video</span>
                                                </div>
                                            </div>
                                            <div class="progress" style="height:13px;">
                                                <div class="progress-bar progress-bar-primary" role="progressbar" data-dz-uploadprogress>
                                                    <span class="progress-text"></span>
                                                </div>
                                            </div>
                                            <div class="col-12 text-center mt-2" id="submit-video" style="display:none">
                                                <button
                                                    type="submit"
                                                    class="btn btn-primary"
                                                    name="videoDropzoneSubmit"
                                                    id="videoDropzoneSubmit">Save</button>
                                            </div>
                                        </form>

                                    </div>
                                {{-- </div> --}}
                            </div>
                            <div class="tab-pane fade"
                                id="record" role="tabpanel"
                                aria-labelledby="second-tab_">
                                <h6 class="mb-4">Wedding Cake with Flowers Macarons and Blueberries</h6>
                                <button type="button" class="btn btn-sm btn-outline-primary">Edit</button>
                            </div>
                            <div class="tab-pane fade"
                                id="youtube" role="tabpanel"
                                aria-labelledby="third-tab_">
                                <h6 class="mb-4">Wedding Cake with Flowers Macarons and Blueberries</h6>
                                <button type="button" class="btn btn-sm btn-outline-primary">Edit</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </main>
@endsection
