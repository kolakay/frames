@extends('Layouts.master')
@section('header')
<title>Videos</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Tags</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                            <div class="col-8 mx-auto mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <button type="button" data-toggle="modal"
                                            data-target="#add-tag" class="btn btn-secondary default mb-4">Add Tag</button>
                                        <table class="data-table data-table-custom table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr v-for="(tag, index) in tags">
                                                        <td>@{{ tag.name }}</td>
                                                        <td>
                                                            <button type="button"
                                                                @click="selectTag(index)"
                                                                data-toggle="modal"
                                                                data-target="#edit-tag"
                                                                class="btn btn-primary btn-sm default">
                                                                    <i class="simple-icon-pencil"></i>
                                                            </button>
                                                            <button type="button"
                                                            @click="deleteTag(index)"
                                                                class="btn btn-danger btn-sm default">
                                                                    <i class="simple-icon-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="tags" cols="30" rows="10">{{ json_encode($tags) }}</textarea>

        <textarea style="display:none" id="create-url" cols="30" rows="10">{{ route('user.videos.tags.create') }}</textarea>
        <textarea style="display:none" id="update-url" cols="30" rows="10">{{ route('user.videos.tags.update') }}</textarea>
        <textarea style="display:none" id="delete-url" cols="30" rows="10">{{ route('user.videos.tags.delete') }}</textarea>

        <div class="modal fade" id="add-tag" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">New Tag</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" id="tag-name" v-model="tag.name">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="addTag">Add Tag</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="edit-tag" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">Edit Tag</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" id="tag-name" v-model="tagEdit.name">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="updateTag">Update Tag</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
{{-- <script>
    $(".data-table-custom").DataTable({
        sDom: '<"row view-filter"<"col-sm-12"<"float-right"l><"float-left"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        columns: [{
            data: "Name"
        }, {
            data: "Action"
        }],
        drawCallback: function() {
            $($(".dataTables_wrapper .pagination li:first-of-type")).find("a").addClass("prev"),
            $($(".dataTables_wrapper .pagination li:last-of-type")).find("a").addClass("next"),
            $(".dataTables_wrapper .pagination").addClass("pagination-sm")
        },
        language: {
            paginate: {
                previous: "<i class='simple-icon-arrow-left'></i>",
                next: "<i class='simple-icon-arrow-right'></i>"
            },
            search: "_INPUT_",
            searchPlaceholder: "Search...",
            lengthMenu: "Items Per Page _MENU_"
        }
    });

</script> --}}
<script src="{{ asset('js/app/tags.js') }}"></script>

@endsection
