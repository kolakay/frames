@extends('Layouts.master')
@section('header')
<title>Edit Video</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('styles')

    <style>
         .video-container:hover,  .active-video-container{
            border:1px solid blue;
        }
        .video-container{
            cursor:pointer
        }
        .general-progress-bar{
            margin: auto;
        }
        .general-progress-bar{
            width:54px;height:54px
        }

        .icon-title{
            cursor: pointer;
            columns: red;
        }
        .imageActive{
            display: flex;
            border: 2px solid red;
        }
        .clickable_link{
            cursor: pointer !important;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- <link href="{{ asset('css/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('js/tinymce/css/skin.min.css') }}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.3/video-js.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.2.0/css/bootstrap-colorpicker.css" integrity="sha256-VPUdnjgz+19KRUsqdLlc/0gH8ePQ1dY66XajIAlVRqk=" crossorigin="anonymous" />
@endsection
@section('content')
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Edit Video</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>

                <div class="col-10 mx-auto" id="app">
                    @csrf
                    <div class="mb-2 row">
                        <div class="col-12 row">
                            <div class="col-12 d-flex flex-row">
                                <video height="150" controls  class="border-primary d-flex" style="object-fit: fill;">
                                    <source src="{{ $video->video_path }}" type="video/mp4">
                                        <track kind="subtitles" src="{{ $video->vtt_file }}" default>
                                </video>
                                <div class="d-flex flex-grow-1 pl-4">
                                    <div class="card-body p-0 d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                        <div class="min-width-zero col-8">
                                            <div v-if="editNameOption == 'false' || editNameOption == false">
                                                <a href="#" @click="setName">
                                                    <p class="list-item-heading mb-1 truncate">@{{ video.name }}
                                                        <span><i class="simple-icon-pencil"></i></span>
                                                    </p>
                                                </a>
                                                <p class="mb-2 text-muted text-small">{{ time_elapsed_string($video->created_at) }}</p>

                                            </div>
                                            <div class="input-group mt-2 mb-2" v-if="editNameOption == 'true' || editNameOption == true">
                                                <input
                                                    type="text" class="form-control"
                                                    v-model="videoEditName"
                                                    placeholder="Video URL" aria-label="Recipient's username"
                                                    aria-describedby="basic-addon2">
                                                <div class="input-group-append">
                                                    <button class="btn btn-outline-secondary"
                                                        data-toggle="tooltip"
                                                        @click="saveVideoName()"
                                                        data-original-title="Copy video URL"
                                                        type="button"><i class="iconsminds-save"></i>
                                                    </button>

                                                    <button class="btn btn-outline-danger straight-btn"
                                                    @click="exitEditName"
                                                    data-toggle="tooltip"
                                                    data-original-title="Close box"
                                                    type="button">
                                                        <i class="iconsminds-close"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                                                <ol class="breadcrumb p-0">

                                                    <li class="breadcrumb-item">
                                                        @{{ video.email_opens}}
                                                        <i class="simple-icon-envelope-open"
                                                        data-toggle="tooltip"
                                                        data-original-title="Email Opens"></i>
                                                    </li>
                                                    <li class="breadcrumb-item">
                                                        @{{ video.email_clicks}}
                                                        <i class="simple-icon-mouse"
                                                        data-toggle="tooltip"
                                                        data-original-title="Email Clicks"></i>
                                                    </li>
                                                    <li class="breadcrumb-item">
                                                        @{{ video.page_views}}
                                                        <i class="simple-icon-eye"
                                                            data-toggle="tooltip"
                                                            data-original-title="Page Views"></i>
                                                    </li>
                                                    <li class="breadcrumb-item">
                                                        @{{ video.video_views}}
                                                        <i class="simple-icon-camrecorder"
                                                            data-toggle="tooltip"
                                                            data-original-title="Video Views"></i>
                                                    </li>
                                                    <li class="breadcrumb-item" aria-current="page">
                                                        @{{ video.cta_clicks}}
                                                        <i class="simple-icon-speech"
                                                            data-toggle="tooltip"
                                                            data-original-title="CTA Clicks"></i>
                                                    </li>
                                                </ol>
                                            </nav>
                                            <div class="form-group">
                                                <button data-toggle="modal" data-target="#share"type="button" class="btn btn-info br-0 straight-btn w-100" style="">Share & Send</button>
                                                @include('Includes.Video.share')
                                                <div class="input-group mt-2">
                                                    <input
                                                        type="text" class="form-control"
                                                        v-model="url.view"
                                                        disabled

                                                        placeholder="Video URL" aria-label="Recipient's username"
                                                        aria-describedby="basic-addon2">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary"
                                                            data-toggle="tooltip"
                                                            @click="copyVideoURL()"
                                                            data-original-title="Copy video URL"
                                                            type="button"><i class="iconsminds-file-copy"></i>
                                                        </button>

                                                        <a class="btn btn-outline-secondary straight-btn"
                                                            :href="url.view" target="_blank">
                                                            <i class="iconsminds-right-3"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-12"></div>

                    </div>
                    <ul class="nav nav-tabs separator-tabs ml-0 mb-5" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="first-tab"
                                data-toggle="tab" href="#first"
                                role="tab" aria-controls="first" aria-selected="false">Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="second-tab"
                                data-toggle="tab" href="#second"
                                role="tab" aria-controls="second" aria-selected="false">Analytics</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="third-tab"
                                data-toggle="tab" href="#third"
                                role="tab" aria-controls="third" aria-selected="true">Editing</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" id="fourth-tab"
                                data-toggle="tab" href="#fourth"
                                role="tab" aria-controls="fourth" aria-selected="false">Design</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="fifth-tab"
                                data-toggle="tab" href="#fifth"
                                role="tab" aria-controls="fifth" aria-selected="false">Privacy</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="sixth-tab"
                                data-toggle="tab" href="#sixth"
                                role="tab" aria-controls="sixth" aria-selected="true">Video Replies</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active show" id="first" role="tabpanel" aria-labelledby="first-tab">
                            @include('Includes.Video.edit-details')
                        </div>
                        <div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
                            @include('Includes.Video.analytics')
                        </div>
                        <div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">
                            @include('Includes.Video.edit-video')
                        </div>

                        <div class="tab-pane fade" id="fourth" role="tabpanel" aria-labelledby="fourth-tab">
                            @include('Includes.Video.design')
                        </div>
                        <div class="tab-pane fade" id="fifth" role="tabpanel" aria-labelledby="fifth-tab">
                            @include('Includes.Video.privacy')
                        </div>
                        <div class="tab-pane fade" id="sixth" role="tabpanel" aria-labelledby="sixth-tab">
                            @include('Includes.Video.replies')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="tags" cols="30" rows="10">{{ json_encode($tags) }}</textarea>
        <textarea style="display:none" id="videos" cols="30" rows="10">{{ json_encode($videos) }}</textarea>
        <textarea style="display:none" id="ctas" cols="30" rows="10">{{ json_encode($ctas) }}</textarea>
        <textarea style="display:none" id="video" cols="30" rows="10">{{ json_encode($video) }}</textarea>


        {{-- <textarea style="display:none" id="create-url" cols="30" rows="10">{{ route('user.videos.tags.create') }}</textarea> --}}
        <textarea style="display:none" id="update-url" cols="30" rows="10">{{ route('user.videos.details.update') }}</textarea>
        <textarea style="display:none" id="view-url" cols="30" rows="10">{{ route('user.videos.view', $video->uuid) }}</textarea>
        <textarea style="display:none" id="video-name" cols="30" rows="10">{{ route('user.videos.edit.name') }}</textarea>
        <textarea id="video-details" style="display:none">{{ ($video->videoDetails)? json_encode($video->videoDetails): '' }}</textarea>
        <textarea id="video-design" style="display:none">{{ ($video->videoDesign)? json_encode($video->videoDesign): '' }}</textarea>
        <textarea id="video-share" style="display:none">{{ ($video->videoShare)? json_encode($video->videoShare): '' }}</textarea>
        <textarea style="display:none" id="video-thumbnail" cols="30" rows="10">{{ route('user.videos.edit.thumbnail') }}</textarea>
        <textarea style="display:none" id="video-trim" cols="30" rows="10">{{ route('user.videos.edit.trim') }}</textarea>
        <textarea style="display:none" id="pixabay-key" cols="30" rows="10">{{ getPixabayAPIKey() }}</textarea>
        <textarea style="display:none" id="design-url" cols="30" rows="10">{{ route('user.videos.edit.design') }}</textarea>
        <textarea style="display:none" id="widget-url" cols="30" rows="10">{{ route('user.videos.embed.widget', $video->uuid) }}</textarea>
        <textarea style="display:none" id="widget-asset-url" cols="30" rows="10">{{ asset('embed/widget.js')}}</textarea>
        <textarea style="display:none" id="video-share-url" cols="30" rows="10">{{ route('user.videos.edit.share') }}</textarea>
        <textarea style="display:none" id="video-send-mail-url" cols="30" rows="10">{{ route('user.videos.edit.send.mail') }}</textarea>
        <textarea style="display:none" id="contacts" cols="30" rows="10">{{ json_encode($contacts) }}</textarea>
        <textarea style="display:none" id="emailTemplates" cols="30" rows="10">{{ json_encode($emailTemplates) }}</textarea>
        <textarea style="display:none" id="emailIntegrations" cols="30" rows="10">{{ json_encode($emailIntegrations) }}</textarea>
        <textarea style="display:none" id="edit-template-url" cols="30" rows="10">{{ route('user.builder') }}</textarea>
        <textarea style="display:none" id="preview-template-url" cols="30" rows="10">{{ route('user.builder.preview') }}</textarea>
        <textarea style="display:none" id="tracker-url" cols="30" rows="10">{{ route('user.videos.log', $video->uuid) }}</textarea>
        <textarea style="display:none" id="graph-data" cols="30" rows="10">{{ json_encode($graph) }}</textarea>
        <textarea style="display:none" id="chart-url" cols="30" rows="10">{{ route('user.videos.chart.data') }}</textarea>
        <textarea style="display:none" id="general-stat-data" cols="30" rows="10">{{ json_encode($generalStats) }}</textarea>
        <textarea style="display:none" id="analytics-log-data" cols="30" rows="10">{{ json_encode($activityLogs) }}</textarea>
        <textarea style="display:none" id="srt-url" cols="30" rows="10">{{ route('user.videos.edit.srt') }}</textarea>
        <textarea style="display:none" id="transcribe-url" cols="30" rows="10">{{ route('user.videos.edit.transcribe') }}</textarea>
        <textarea style="display:none" id="transcriptionDetails" cols="30" rows="10">{{ json_encode($transcriptionDetails) }}</textarea>
        <textarea style="display:none" id="srt-download-url" cols="30" rows="10">{{ route('user.videos.download.caption') }}</textarea>
        <textarea style="display:none" id="concatenate-url" cols="30" rows="10">{{ route('user.videos.edit.concatenate') }}</textarea>

    </main>
@endsection
@section('scripts')
<script src="{{ asset('js/vendor/Chart.bundle.min.js') }}" style="opacity: 1;"></script>
<script src="{{ asset('js/vendor/chartjs-plugin-datalabels.js') }}" style="opacity: 1;"></script>
<script src="{{ asset('js/vendor/progressbar.min.js') }}" style="opacity: 1;"></script>
<script src="{{ asset('js/vendor/select2.full.js') }}"></script>
<script src="{{ asset('js/vendor/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/video.min.js') }}"></script>
<script src="{{ asset('js/tinymce/js/tinymce.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.2.0/js/bootstrap-colorpicker.js" integrity="sha256-c3xaRs6Y77ovyNz6pWP6/7CgFW1aSvbyZjff0Ax5CCM=" crossorigin="anonymous"></script>
<script src="{{ asset('js/app/video-edit.js') }}"></script>
@endsection
