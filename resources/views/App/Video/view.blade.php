@extends('Layouts.master')
@section('header')
<title>View Video</title>
@endsection
@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.3/video-js.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/vendor/glide.core.min.css') }}">
    <style>
        .br-widget{
            font-size: 24px;
        }
        .rounded .vjs-poster, .rounded .vjs-tech, .br-0 {
            border-radius: 0px !important;
        }
        #slider{
            width: 50% !important;
        }
        .glide__slide{
            width: 165px !important;
            height: 90px !important;
            flex-wrap: wrap;
        }
        .glide__slides{
            overflow-x:scroll;
            height: 110px !important;
        }
        .img-thumbs{
            width: 160px !important;
            height: 90px !important;
        }
        .thumnail-caption{
            position: relative !important;
            top:-20px !important;
            width: 97% !important;
            background: #000;
            opacity: 0.5;
            color: #FFF;
            height: 20px;
            overflow: hidden;
        }

        .glide__slides::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        .glide__slides::-webkit-scrollbar
        {
            width: 10px;
            background-color: #F5F5F5;
        }

        .glide__slides::-webkit-scrollbar-thumb
        {
            background-color: #343a40;
            border: 2px solid #555555;
        }
        .chat-input-container {
            position: inherit;
        }
        .app-row {
            padding-right: 0px;
        }

        .chat-input-container{
            padding-right: 20px;
            padding-left: 20px !important;
            border-bottom: 2px solid #000 !important;
            border-right: 2px solid #000 !important;
            border-left: 2px solid #000 !important;
        }
        .chat-heading{
            border: 2px solid #000 !important;
        }
        .input-box{
            border-top: 1px solid #000 !important;
            border-bottom: 1px solid #000 !important;
        }
        .chat-d-c{
            border-bottom: 1px solid #000 !important;
        }
    </style>
@endsection

@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                @if(Auth::check() && Auth::id() == $video->user_id)
                    <div class="col-8 mx-auto text-center mb-2">
                        <a href="{{ route('user.videos.edit', $video->uuid)}}" class="btn btn-sm btn-dark default">Edit Video</a>
                    </div>
                @endif
                <div class=" col-8 mx-auto">
                    @csrf
                    <video height="550" controls
                        id="video-player"
                        preload="auto"
                        poster=""
                        data-setup='{}'
                        class="video-js vjs-big-play-centered"
                        style="object-fit: fill; width:100%">
                        <track kind="subtitles" src="{{ $video->vtt_file }}" id="trackSRT" default>

                        {{-- <source :src="" type="video/mp4"> --}}
                            {{-- <track label="English" kind="subtitles" srclang="en" src="/text/test.vtt" default> --}}
                        <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a
                            web browser that
                            <a href="https://videojs.com/html5-video-support/" target="_blank">
                                supports HTML5 video
                            </a>
                            </p>
                    </video>
                    @if($video->videoDetails)
                        @if($video->videoDetails->add_emoji_response_buttons)
                        <div class="br-widget col-12 mx-auto mt-1 text-center bg-dark">
                            <a href="javascript:;" class="mr-2" @click="getReactionClick('😀')">😀</a>
                            <a href="javascript:;"class="mr-2" @click="getReactionClick('💡')">💡</a>
                            <a href="javascript:;"class="mr-2" @click="getReactionClick('👏')">👏</a>
                            <a href="javascript:;"class="mr-2" @click="getReactionClick('🙌')">🙌</a>
                            <a href="javascript:;"class="mr-2" @click="getReactionClick('👍')">👍</a>
                            <a href="javascript:;"class="mr-2" @click="getReactionClick('👎')">👎</a>

                        </div>
                        @endif
                    @endif

                    @if($video->videoDetails)
                    <div class="glide thumbs col-12 mt-2" id="slider">
                        <div class="glide__track" data-glide-el="track">
                            <ul class="glide__slides">
                                <li class="glide__slide mr-2 br-0" @click="selectVideo(video)">
                                    <img alt="thumb" :src="video.video_thumbnail_path" class="responsive border-0 border-radius img-thumbs br-0">
                                    <p class="thumnail-caption">@{{ video.name }}</p>
                                </li>


                                <li class="glide__slide mr-2 br-0" v-for="selectedVideo in playlist" @click="selectVideo(selectedVideo)">
                                    <img alt="thumb" :src="selectedVideo.video_thumbnail_path"
                                    class="responsive border-0 border-radius img-thumbs br-0">
                                    <p class="thumnail-caption">@{{ selectedVideo.name }}</p>
                                </li>

                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-8 mx-auto text-center mb-2 mt-4 row d-flex justify-content-center">
                    <button type="button" id="cta-clicker"
                        data-counter="0"
                        data-toggle="modal" style="display:none"
                        data-target="#cta-modal" class="btn btn-secondary default mb-4">Add CTA</button>

                    @include('Includes.Video.View.ctas')
                </div>
                <div class="col-8 mx-auto text-center mb-2">
                    <div class=" d-flex flex-row mb-4 col-4 mx-auto">
                        <a class="d-flex" href="#"><img alt="Profile" src="{{ getGravatar($video->user->email) }}" class="img-thumbnail border-0 rounded-circle m-4 list-thumbnail align-self-center"></a>
                        <div class="d-flex flex-grow-1 min-width-zero">
                            <div class="card-body pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                <div class="min-width-zero">
                                    <a href="#">
                                        <p class="list-item-heading mb-1 truncate">{{ $video->user->name }}</p>
                                    </a>
                                    <p class="mb-2 text-muted text-small">Head Developer</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($video->videoDetails)
                @if($video->videoDetails->curated_ctas)

                    <div v-for="cta in ctas" class="col-12">
                        <div v-if="cta.type == 'iframe'">
                            <div class="col-8 mx-auto text-center mb-2">
                                <iframe :src="cta.iframe_source"
                                    :scrolling="cta.iframe_scroll"
                                    class="col-12 p-0 border border-secondary"
                                    :height="cta.iframe_height" frameborder="0"></iframe>
                            </div>
                        </div>
                        <div v-if="cta.type == 'web_page'">
                            <div class="col-8 mx-auto text-center mb-2">
                                <iframe :src="cta.web_page_url"
                                    :scrolling="cta.scroll_set"
                                    class="col-12 p-0 border border-secondary"
                                    :height="cta.height_set" frameborder="0"></iframe>
                            </div>
                        </div>
                        <div v-if="cta.type == 'calendar'">
                            <div class="col-8 mx-auto text-center mb-2" v-if="cta.calendar_platform != 'book_me'">
                                <iframe :src="cta.calendar_username"
                                    :scrolling="cta.scroll_set"
                                    class="col-12 p-0 border border-secondary"
                                    :height="cta.height_set" frameborder="0"></iframe>
                            </div>
                        </div>
                        <div v-if="cta.type == 'form'" class="col-8 mx-auto text-center mt-4 mb-2 border border-secondary">
                            <h4>@{{ cta.form.title }}</h4>
                            <div class="form-group" v-for="(formfield, index) in cta.form.fields">
                                <label class="form-group has-top-label" v-if="formfield.is_set">
                                    <input class="form-control"  :type="formfield.type">
                                    <span>@{{formfield.name}}</span>
                                </label>

                            </div>
                            <button type="button" class="btn btn-primary default mb-2">@{{cta.form.form_button_text}}</button>

                        </div>
                        {{-- <div v-if="cta.type == 'calendar'">
                            <div class="col-8 mx-auto text-center mb-2" v-if="cta.calendar_platform == 'book_me'">
                                <script :src="'https://bookme.name/js/booklikeaboss.embed.js?i='+ cta.calendar_id + '&h='+ cta.calendar_hash" async></script>

                            </div>
                        </div> --}}
                    </div>
                @endif


                @if($video->videoDetails->add_messaging)
                <div class="col-8 mx-auto mb-2 mt-4">
                    <div class="row app-row">
                        <div class="col-12 chat-app">
                            <div class="d-flex flex-row justify-content-between mb-0 bg-dark text-white chat-heading-container chat-d-c">
                                <div class="d-flex flex-row chat-heading col-12 mb-0 chat-d-c pt-1 pb-1">
                                    <a class="d-flex" href="#">
                                        <img alt="Profile Picture" src="{{ getGravatar($video->user->email) }}" class="img-thumbnail border-0 rounded-circle ml-0 mr-4 list-thumbnail align-self-center small">
                                    </a>
                                    <div class="d-flex min-width-zero">
                                        <div class="card-body pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                            <div class="min-width-zero">
                                                <a href="#">
                                                    <p class="list-item-heading mb-1 truncate text-white">Chat with {{ $video->user->name }}</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row justify-content-between chat-heading-container">
                                <div class="d-flex flex-row chat-heading">
                                    <div class="scroll">
                                        <div class="scroll-content pl-2 pr-2 pt-2">
                                            <div class="card d-inline-block mb-3 float-left mr-2">
                                                <div class="position-absolute pt-1 pr-2 r-0"><span class="text-extra-small text-muted">09:25</span></div>
                                                <div class="card-body">
                                                    <div class="d-flex flex-row pb-2">
                                                        <a class="d-flex" href="#"><img alt="Profile Picture" src="{{ asset('img/profile-pic-l.jpg') }}" class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall"></a>
                                                        <div class="d-flex flex-grow-1 min-width-zero">
                                                            <div class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                                                <div class="min-width-zero">
                                                                    <p class="mb-0 truncate list-item-heading">Sarah Kortney</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-text-left">
                                                        <p class="mb-0 text-semi-muted">What do you think about our plans for this product launch?</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="card d-inline-block mb-3 float-left mr-2">
                                                <div class="position-absolute pt-1 pr-2 r-0"><span class="text-extra-small text-muted">09:28</span></div>
                                                <div class="card-body">
                                                    <div class="d-flex flex-row pb-2">
                                                        <a class="d-flex" href="#"><img alt="Profile Picture" src="{{ asset('img/profile-pic-l.jpg') }}" class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall"></a>
                                                        <div class="d-flex flex-grow-1 min-width-zero">
                                                            <div class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                                                <div class="min-width-zero">
                                                                    <p class="mb-0 truncate list-item-heading">Sarah Kortney</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-text-left">
                                                        <p class="mb-0 text-semi-muted">It looks to me like you have a lot planned before your deadline. I would suggest you push your deadline back so you have time to run a successful advertising campaign.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="card d-inline-block mb-3 float-left mr-2">
                                                <div class="position-absolute pt-1 pr-2 r-0"><span class="text-extra-small text-muted">09:30</span></div>
                                                <div class="card-body">
                                                    <div class="d-flex flex-row pb-2">
                                                        <a class="d-flex" href="#"><img alt="Profile Picture" src="{{ asset('img/profile-pic-l.jpg') }}" class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall"></a>
                                                        <div class="d-flex flex-grow-1 min-width-zero">
                                                            <div class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                                                <div class="min-width-zero">
                                                                    <p class="mb-0 truncate list-item-heading">Sarah Kortney</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-text-left">
                                                        <p class="mb-0 text-semi-muted">How do you think we should move forward with this project? As you know, we are expected to present it to our clients next week.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="card d-inline-block mb-3 float-right mr-2">
                                                <div class="position-absolute pt-1 pr-2 r-0"><span class="text-extra-small text-muted">09:41</span></div>
                                                <div class="card-body">
                                                    <div class="d-flex flex-row pb-2">
                                                        <a class="d-flex" href="#"><img alt="Profile Picture" src="{{ asset('img/profile-pic-l-4.jpg') }}" class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall"></a>
                                                        <div class="d-flex flex-grow-1 min-width-zero">
                                                            <div class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                                                <div class="min-width-zero">
                                                                    <p class="mb-0 list-item-heading truncate">Mimi Carreira</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-text-left">
                                                        <p class="mb-0 text-semi-muted">I would suggest you discuss this further with the advertising team.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="card d-inline-block mb-3 float-right mr-2">
                                                <div class="position-absolute pt-1 pr-2 r-0"><span class="text-extra-small text-muted">09:41</span></div>
                                                <div class="card-body">
                                                    <div class="d-flex flex-row pb-2">
                                                        <a class="d-flex" href="#"><img alt="Profile Picture" src="{{ asset('img/profile-pic-l-4.jpg') }}" class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall"></a>
                                                        <div class="d-flex flex-grow-1 min-width-zero">
                                                            <div class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                                                <div class="min-width-zero">
                                                                    <p class="mb-0 list-item-heading truncate">Mimi Carreira</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-text-left">
                                                        <p class="mb-0 text-semi-muted">I am very busy at the moment and on top of everything, I forgot my umbrella today.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="card d-inline-block mb-3 float-right mr-2">
                                                <div class="position-absolute pt-1 pr-2 r-0"><span class="text-extra-small text-muted">09:41</span></div>
                                                <div class="card-body">
                                                    <div class="d-flex flex-row pb-2">
                                                        <a class="d-flex" href="#"><img alt="Profile Picture" src="{{ asset('img/profile-pic-l-4.jpg') }}" class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall"></a>
                                                        <div class="d-flex flex-grow-1 min-width-zero">
                                                            <div class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                                                <div class="min-width-zero">
                                                                    <p class="mb-0 list-item-heading truncate">Mimi Carreira</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="chat-text-left">
                                                        <p class="mb-0 text-semi-muted">I am very busy at the moment and on top of everything, I forgot my umbrella today.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-input-container d-flex justify-content-between align-items-center">
                                <input class="form-control flex-grow-1 input-box" type="text" placeholder="Say something...">
                                <div>
                                    <button type="button" class="btn btn-dark icon-button large"><i class="simple-icon-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endif


            </div>
        </div>
        <textarea style="display:none" id="video" cols="30" rows="10">{{ json_encode($video) }}</textarea>
        <textarea style="display:none" id="video-details" cols="30" rows="10">{{ json_encode($video->videoDetails) }}</textarea>
        <textarea style="display:none" id="log-url" cols="30" rows="10">{{ route('user.videos.log.clicks') }}</textarea>

        <div class="modal fade" id="cta-modal" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">Edit CTA</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            @include('Includes.Video.View.ctas')
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="col-8 mx-auto text-center mb-2" id="calenders">

    </div>

@endsection
@section('scripts')
@if($video->videoDetails)
@foreach($video->videoDetails->curated_ctas as $cta)
    @if($cta->type  == 'calendar' && $cta->calendar_platform == 'book_me')
        <script>
            var s=document.createElement('script');
            s.type="text/javascript";
            s.async=true;
            s.src=`{!! 'https://bookme.name/js/booklikeaboss.embed.js?i='.$cta->calendar_id .'&h='. $cta->calendar_hash !!}`;

                document.getElementById('calenders').appendChild(s);
        </script>
    @endif
@endforeach
@endif
<script src="{{ asset('js/video.min.js') }}"></script>
@if($video->videoDetails)
@if($video->videoDetails)
    @if($video->videoDetails->add_exit_intent_modal)
        <script>
            $("body").mouseleave(function(){
                let counter = $("#cta-clicker").data('counter');
                if(counter == 0){
                    $("#cta-clicker").click();
                    $("#cta-clicker").data('counter', 1);
                }
            })

        </script>
    @endif
@endif
@endif

<script src="{{ asset('js/app/video-view.js') }}"></script>
@endsection
