@extends('Layouts.master')
@section('header')
<title>Videos</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
<style>
    .progressbar-text {
        color : #FFF !important;
    }
    .stat-count{
        font-size: 34px;
        color: #FFF;
    }
    .br-0{
        border-radius: 0px !important;
    }
    .caption{
        font-size: 16px;
        font-weight: 800;
    }
    .menu-btn{
        font-size: 20px;
        text-align: center!important;
    }
</style>
    <main>
        <div class="container-fluid" id="app">
            <div class="row">
                <div class="col-12">
                    <h1>Videos</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                        @csrf
                        <div class="col-12 card p-4 br-0">
                            @include('Includes.Video.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="videos">{{ json_encode($videos) }}</textarea>
        <textarea style="display:none" id="tags">{{ json_encode($tags) }}</textarea>

        <textarea style="display:none" id="view-video-url">{{ route('user.videos.view') }}</textarea>
        <textarea style="display:none" id="edit-video-url">{{  route('user.videos.edit') }}</textarea>
        <textarea style="display:none" id="delete-video-url">{{  route('user.videos.delete') }}</textarea>
        <textarea style="display:none" id="replace-video-url">{{  route('user.videos.replace') }}</textarea>
    </main>
@endsection
@section('scripts')
<script src="{{ asset('js/app/video-list.js') }}" style="opacity: 1;"></script>

@endsection
