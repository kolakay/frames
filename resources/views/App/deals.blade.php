@extends('Layouts.master')
@section('header')
<title>Deals</title>
@endsection
@section('styles')
	<style>
		.deal-display {
			margin-top: 10px;
		}
		.deal-container {
			overflow: scroll;
			overflow-y: hidden;
			white-space: nowrap;
		}
	    .deal-container > div {
	        border-radius: 5px;
	        background-color: #F8F8F8;
	        min-height: 500px;
	        min-width: 250px;
		}
		.deal-box {
			height: 90%;
		}
		.deal-content {
			min-height: 100%;
		}
		.add-deal{
			border-radius: 5px;
			cursor: pointer;
			background-color: #FFF;
			box-shadow: 0px 1px #CCC;
		}
		.deal{
			border-radius: 5px;
			padding-left: 4px;
			background-color: #FFF;
			box-shadow: 0px 1px #CCC;
		}
		.deal-title {
			cursor: pointer;
			font-weight: bolder;
		}
		.dealsCount {
			border-radius: 50%;
			background-color: #B5E2E9;
		}
		
		
	</style>
@endsection
@section('sidebar')
	@include('Includes.sidebar')
@endsection

@section('content')
	<main>
		<div class="container-fluid" id="app">
			<div class="col-12">
                <h1>Deals</h1>
                <div class="separator mb-5"></div>
            </div>
			@csrf
			<div class="card">
				<div class="card-body">
					<div class="d-flex justify-content-end">
						
						<div class="btn-group">
							<button type="button" class="btn btn-outline-info default d-flex align-items-center" data-toggle="modal" data-target="#dealCreateModal" v-if="deals_display == 'list'">
								<span class="glyph-icon simple-icon-plus"></span>
								<span>NEW</span>
							</button>
							<button type="button" @click="changeBoard()" class="btn btn-outline-info default">BOARD</button> 
							<button type="button" @click="changeList()" class="btn btn-outline-info default">LIST</button> 
						</div>

					</div>
					<div class="modal fade" id="dealCreateModal" tabindex="-1" role="dialog" style="display: none;" aria-modal="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Create deals</h5>									
								</div>
								<div class="modal-body">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Title<span class="text-danger"> * </span></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="deal.title"/>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Description</label>
										<div class="col-sm-8">
											<textarea type="textarea" class="form-control" v-model="deal.description"></textarea>
										</div>
									</div>
									
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Amount</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="deal.amount"/>
										</div>
									</div>
									
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Stage<span class="text-danger"> * </span></label>
										<div class="col-sm-8">
											<select name="stage" v-model="deal.stage" class="form-control">
												<option v-for="(value, name, index) in stages" :value="name">@{{ value }}</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Owner</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="deal.owner"/>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Closed date</label>
										<div class="col-sm-8">
											<input type="date" v-model="deal.closed_date" placeholder="yyyy-mm-dd" class="form-control" />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated contacts</label>
										<div class="col-sm-8">
											<select id="contacts1" v-model="deal.associated_contacts" class="form-control">
												<option v-for="(value, name, index) in contacts" :value="value.id">@{{ value.full_name_display }}</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated Company</label>
										<div class="col-sm-8">
											<select id="companies"  v-model='deal.associated_company' class="form-control">
												<option v-for="(value, name, index) in companies" :value="value.id" >@{{ value.name }}</option>
											</select>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary default" data-dismiss="modal"><div class="glyph-icon iconsminds-close"> Cancel</div></button>
									<button type="button" class="btn btn-primary default" data-dismiss="modal" @click="store(url.create)" data-toggle="modal" data-target="#tokenModal"><div class="glyph-icon iconsminds-pen-2"> Save</div></button>
								</div>
							</div>
						</div>
					</div>

					<div v-show="deals_display == 'board'">
						@include('Includes.Deals.board')
					</div>
					<div v-show="deals_display == 'list'">
						@include('Includes.Deals.list')
					</div>
					
					<div class="modal fade" id="dealEditModal" tabindex="-1" role="dialog" style="display: none;" aria-modal="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Edit deals</h5>									
								</div>
								<div class="modal-body">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Title<span class="text-danger"> * </span></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="deal.title"/>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Description</label>
										<div class="col-sm-8">
											<textarea type="textarea" class="form-control" v-model="deal.description"></textarea>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Amount</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="deal.amount"/>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Stage<span class="text-danger"> * </span></label>
										<div class="col-sm-8">
											<select name="stage" v-model="deal.stage" class="form-control">
												<option v-for="(value, name, index) in stages" :value="name">@{{ value }}</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Owner</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="deal.owner"/>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Closed date</label>
										<div class="col-sm-8">
											<input type="date" name="closed_date" placeholder="yyyy-mm-dd" v-model="deal.closed_date" class="form-control" />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated contacts</label>
										<div class="col-sm-8">
											{{-- <input list="contacts" name="associated_contacts" v-model="deal.associated_contacts" class="form-control" /> --}}
											<select v-model="deal.associated_contacts" class="form-control">
												<option v-for="(value, name, index) in contacts" :value="value.id">@{{ value.full_name_display }}</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated Company</label>
										<div class="col-sm-8">
											{{-- <input list="companies" name="associated_company" v-model='deal.associated_company' class="form-control" /> --}}
											<select v-model='deal.associated_company' class="form-control">
												<option v-for="(value, name, index) in companies" :value="value.id" >@{{ value.name }}</option>
											</select>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary default" data-dismiss="modal"><div class="glyph-icon iconsminds-close"> Cancel</div></button>
									<button type="button" class="btn btn-primary default" data-dismiss="modal" @click="store(url.edit)" data-toggle="modal" data-target="#tokenModal"><div class="glyph-icon iconsminds-pen-2"> Save</div></button>
								</div>
							</div>
						</div>
					</div>

					<textarea style="display:none" id="createURL">{{ route('user.deals.create') }}</textarea>
					<textarea style="display:none" id="getDealsURL">{{ route('user.deals.get') }}</textarea>
					<textarea style="display:none" id="editURL">{{ route('user.deals.edit') }}</textarea>
					<textarea style="display:none" id="deleteURL">{{ route('user.deals.delete') }}</textarea>

					<textarea id="deals" style="display:none" >{{ json_encode($deals) }}</textarea>
					<textarea id="stages" style="display:none" >{{ json_encode(getDealsStages()) }}</textarea>
					<textarea id="dealContacts" style="display:none" >{{ json_encode($contacts) }}</textarea>
					<textarea id="dealCompanies" style="display:none" >{{ json_encode($companies) }}</textarea>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('scripts')
	<script src="//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
	<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>
	<script src="{{ asset('js/app/deals.js') }}"></script>
@endsection