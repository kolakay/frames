@extends('Layouts.master')
@section('header')
<title>Tasks</title>
@endsection

@section('styles')
	<style>
		.task-title-click {
			cursor: pointer;
		}
	</style>
@endsection

@section('sidebar')
	@include('Includes.sidebar')
@endsection

@section('content')
	<main>
		<div class="container-fluid" id="app">
			<div class="col-12">
                <h1>Tasks</h1>
                <div class="separator mb-5"></div>
            </div>
			@csrf
			<div class="card">
				<div class="card-body">

					<div class="d-flex justify-content-end">
						{{-- <div class="dropdown-menu p-2 input-group">
							<div @click="filterBy = 'pending'">
								<input type="radio" class="input-group-text" id="pendingRadio">
								<label for="pendingRadio">Pending</label>
							</div>
							<div @click="filterBy = 'completed'">
								<input type="radio" class="input-group-text" id="completedRadio"> 
								<label for="completedRadio">Completed</label>
							</div>
							<div @click="filterBy = 'all'">
								<input type="radio" class="input-group-text" id="allRadio"> 
								<label for="allRadio">All</label>
							</div>
						</div> --}}


						<div class="btn-group">
							<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								STATUS
							</button>
							<div class="dropdown-menu">
							    <div class="form-check"  @click="filterBy = 'pending'">
							    	<input type="radio" name="filterRadioOptions" id="taskFilter1" class="form-check-input" aria-label="radio">
							    	<label for="taskFilter1">Pending</label>
							    </div>
								<div class="form-check" @click="filterBy = 'completed'">
							    	<input type="radio" name="filterRadioOptions" id="taskFilter2" class="form-check-label" aria-label="radio">
							    	<label for="taskFilter2">Completed</label>
							    </div>
							    <div class="form-check" @click="filterBy = 'all'">
							    	<input type="radio" name="filterRadioOptions" id="taskFilter3" class="form-check-label" aria-label="radio">
							    	<label for="taskFilter3">All</label>
							    </div>
							</div>
						</div>

						<button type="button" class="btn btn-outline-info btn-sm mb-2 ml-2 default" data-toggle="modal" data-target="#taskCreateModal">
							<span>ADD TASK</span>
						</button>
					</div>
					
					<div class="modal fade shadow p-3 mb-5" id="taskCreateModal" tabindex="-1" role="dialog" style="display: none;" aria-modal="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Create task</h5>								
								</div>
								<div class="modal-body">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Title<span class="text-danger"> * </span></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="task.title"/>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Description</label>
										<div class="col-sm-8">
											<textarea type="textarea" class="form-control" v-model="task.description"></textarea>
										</div>
									</div>
									
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated Contact</label>
										<div class="col-sm-8">
											<select name="associated_contacts" v-model="task.associated_contact_id" class="form-control">
												<option v-for="(value, id, index) in contactsList" :value="value.id">@{{ value.full_name_display }}</option>
											</select>
										</div>
									</div>
									
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated Video</label>
										<div class="col-sm-8">
											<select name="associated_video" v-model="task.associated_video_id" class="form-control">
												<option v-for="(value, id, index) in videosList" :value="value.id">@{{ value.file }}</option>
											</select>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated Deal</label>
										<div class="col-sm-8">
											<select name="associated_deal" v-model="task.associated_deal_id" class="form-control">
												<option v-for="(value, id, index) in dealsList" :value="value.id">@{{ value.title }}</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Assignee</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="task.assignee_id"/>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Due Date</label>
										<div class="col-sm-8">
											<input type="date" v-model="task.due_after" placeholder="yyyy-mm-dd" class="form-control" />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Status<span class="text-danger"> * </span></label>
										<div class="col-sm-8">
											<select  v-model='task.status' class="form-control">
												<option value="pending">Pending</option>
												<option value="completed">Completed</option>
											</select>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary default" data-dismiss="modal"><div class="glyph-icon iconsminds-close"> Cancel</div></button>
									<button type="button" class="btn btn-primary default" data-dismiss="modal" @click="store(url.create)">
										<div class="glyph-icon iconsminds-pen-2"> Save</div>
									</button>
								</div>
							</div>
						</div>
					</div>
					<table class="table table-bordered table-hover">
						<thead class="thead-light">
							<tr>
								<th scope="col"></th>
								<th scope="col">Title</th>
								<th scope="col">Creator</th>
								<th scope="col">Assignee</th>
								<th scope="col">Due Date</th>
								<th scope="col">Actions</th>
							</tr>
						</thead>
						<tbody v-if="tasks.length > 0">
							<tr v-for="(value, name, index) in filterTasks" :key="index" :value="value">
								<td>
									<input v-model="value.selected" type="checkbox" class="custom control">
								</td>
								<td><span class="task-title-click" data-toggle="modal" data-target="#taskEditModal">@{{ value.title }}</span></td>
								<td>@{{ value.creator }}</td>
								<td>@{{ value.assignee_id }}</td>
								<td>@{{ value.due_after }}</td>
								<td>
									<button type="button" @click="findTask(value.id)" data-toggle="modal" data-target="#taskEditModal" class="btn btn-primary mb-1 default">
										<div class="glyph-icon simple-icon-pencil"></div>
									</button>
									<button type="button" class="btn btn-danger mb-1 default">
										<div class="glyph-icon simple-icon-trash"></div>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="modal fade" id="taskEditModal" tabindex="-1" role="dialog" style="display: none;" aria-modal="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Edit Tasks</h5>									
								</div>
								<div class="modal-body">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Title<span class="text-danger"> * </span></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="task.title"/>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Description</label>
										<div class="col-sm-8">
											<textarea type="textarea" class="form-control" v-model="task.description"></textarea>
										</div>
									</div>
									
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated Contact</label>
										<div class="col-sm-8">
											<select name="associated_contacts" v-model="task.associated_contact_id" class="form-control">
												<option v-for="(value, id, index) in contactsList" :value="value.id">@{{ value.full_name_display }}</option>
											</select>
										</div>
									</div>
									
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated Video</label>
										<div class="col-sm-8">
											<select name="associated_video" v-model="task.associated_video_id" class="form-control">
												<option v-for="(value, id, index) in videosList" :value="value.id">@{{ value.file }}</option>
											</select>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Associated Deal</label>
										<div class="col-sm-8">
											<select name="associated_deal" v-model="task.associated_deal_id" class="form-control">
												<option v-for="(value, id, index) in dealsList" :value="value.id">@{{ value.title }}</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Assignee</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" v-model="task.assignee_id"/>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Due Date</label>
										<div class="col-sm-8">
											<input type="date" v-model="task.due_after" placeholder="yyyy-mm-dd" class="form-control" />
										</div>
									</div>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Status<span class="text-danger"> * </span></label>
										<div class="col-sm-8">
											<select  v-model='task.status' class="form-control">
												<option value="pending">Pending</option>
												<option value="completed">Completed</option>
											</select>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary default" data-dismiss="modal"><div class="glyph-icon iconsminds-close"> Cancel</div></button>
									<button type="button" class="btn btn-primary default" data-dismiss="modal" @click="store(url.edit)">
										<div class="glyph-icon iconsminds-pen-2"> Save</div>
									</button>
								</div>
							</div>
						</div>
					</div>

					<textarea style="display:none" id="tasksCreateURL">{{ route('user.tasks.create') }}</textarea>
					<textarea style="display:none" id="getTasksURL">{{ route('user.tasks.get') }}</textarea>
					<textarea style="display:none" id="tasksEditURL">{{ route('user.tasks.edit') }}</textarea>
					<textarea style="display:none" id="taskDeleteURL">{{ route('user.tasks.delete') }}</textarea>

					<textarea id="tasks" style="display:none" >{{ json_encode($tasks) }}</textarea>
					<textarea id="tasksContacts" style="display:none" >{{ json_encode($contacts) }}</textarea>
					<textarea id="tasksDeals" style="display:none" >{{ json_encode($deals) }}</textarea>
					<textarea id="tasksVideos" style="display:none" >{{ json_encode($videos) }}</textarea>
				</div>
			</div>
		</div>
	</main>
@endsection
@section('scripts')
	<script src="{{ asset('js/app/tasks.js') }}"></script>
@endsection