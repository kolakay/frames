@extends('Layouts.master')
@section('header')
<title>Deals</title>
@endsection

@section('sidebar')
	@include('Includes.sidebar')
@endsection

@section('content')
	<main>
		<div class="container-fluid">
			<div class="row d-flex justify-content-center ">
				@csrf
				<div class="col-12">
                    <h1>Page not found</h1>
                    <div class="separator mb-5"></div>
                </div>
				<div class="lead text-center p-3">
					<div>The page you are trying to look for does not exist</div>
					<div>Make sure your URL is correct</div>
				</div>
			</div>
		</div>
	</main>
@endsection