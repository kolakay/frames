@extends('Layouts.master')
@section('header')
<title>Contacts</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Contacts</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                            <div class="col-12 mx-auto mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <button type="button" data-toggle="modal"
                                            data-target="#add-contact" class="btn btn-secondary default mb-4">Add Contact</button>
                                        <table class="data-table data-table-custom table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th>Email</th>
                                                    <th>Mobile</th>
                                                    <th>Phone</th>
                                                    <th>Title</th>
                                                    <th>Company</th>
                                                    {{-- <th>Owner</th> --}}
                                                    {{-- <th>Tags</th> --}}
                                                    <th>Source</th>
                                                    <th>Date Added</th>
                                                    <th>Date Edited</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr v-for="(contact, index) in contacts">
                                                        <td>@{{ contact.first_name }}</td>
                                                        <td>@{{ contact.last_name }}</td>
                                                        <td>@{{ contact.email }}</td>
                                                        <td>@{{ contact.mobile }}</td>
                                                        <td>@{{ contact.phone }}</td>
                                                        <td>@{{ contact.title }}</td>
                                                        <td>@{{ contact.company }}</td>
                                                        {{-- <td>@{{ contact.user_id }}</td> --}}
                                                        {{-- <td>@{{ contact.tags }}</td> --}}
                                                        <td>@{{ contact.source }}</td>
                                                        <td>@{{ contact.created_at }}</td>
                                                        <td>@{{ contact.updated_at }}</td>
                                                        <td>
                                                            <button type="button"
                                                                @click="selectContact(index)"
                                                                data-toggle="modal"
                                                                data-target="#edit-contact"
                                                                class="btn btn-primary btn-sm default">
                                                                    <i class="simple-icon-pencil"></i>
                                                            </button>
                                                            <button type="button"
                                                            @click="deleteContact(index)"
                                                                class="btn btn-danger btn-sm default">
                                                                    <i class="simple-icon-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="contacts" cols="30" rows="10">{{ json_encode($contacts) }}</textarea>
        <textarea style="display:none" id="tags" cols="30" rows="10">{{ json_encode($tags) }}</textarea>
        <textarea style="display:none" id="companies" cols="30" rows="10">{{ json_encode($companies) }}</textarea>

        <textarea style="display:none" id="create-url" cols="30" rows="10">{{ route('user.connections.contacts.create') }}</textarea>
        <textarea style="display:none" id="update-url" cols="30" rows="10">{{ route('user.connections.contacts.update') }}</textarea>
        <textarea style="display:none" id="delete-url" cols="30" rows="10">{{ route('user.connections.contacts.delete') }}</textarea>

        <div class="modal fade" id="add-contact" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">New Contact</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">First Name:</label>
                                    <input type="text" class="form-control" v-model="contact.first_name">
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Last Name:</label>
                                    <input type="text" class="form-control" v-model="contact.last_name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="recipient-name" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" v-model="contact.email">
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Mobile Phone:</label>
                                    <input type="text" class="form-control" v-model="contact.mobile">
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Phone:</label>
                                    <input type="text" class="form-control" v-model="contact.phone">
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Company:</label>
                                    <select class="form-control" v-model="contact.company">
                                        <option :value="company.id" v-for="company in companies">@{{ company.name }}</option>
                                    </select>
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Title:</label>
                                    <input type="text" class="form-control" v-model="contact.title">
                                </div>
                                <div class="form-group col-12">
                                    <label for="recipient-name" class="col-form-label">Tags:</label>
                                    <select class="form-control select2-multiple" multiple="multiple" data-width="100%">
                                        <option :value="tag.id" v-for="tag in tags">@{{ tag.name }}</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label for="recipient-name" class="col-form-label">Notes:</label>
                                    <textarea class="form-control"cols="20" rows="3" v-model="contact.notes"></textarea>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="addContact">Add contact</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="edit-contact" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">Edit Contact</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">First Name:</label>
                                    <input type="text" class="form-control" v-model="contactEdit.first_name">
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Last Name:</label>
                                    <input type="text" class="form-control" v-model="contactEdit.last_name">
                                </div>
                                <div class="form-group col-12">
                                    <label for="recipient-name" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" v-model="contactEdit.email">
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Mobile Phone:</label>
                                    <input type="text" class="form-control" v-model="contactEdit.mobile">
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Phone:</label>
                                    <input type="text" class="form-control" v-model="contactEdit.phone">
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Company:</label>
                                    <select class="form-control" id="" v-model="contactEdit.company">
                                        <option :value="company.id" v-for="company in companies">@{{ company.name }}</option>
                                    </select>
                                </div>
                                <div class="form-group col-6">
                                    <label for="recipient-name" class="col-form-label">Title:</label>
                                    <input type="text" class="form-control" v-model="contactEdit.title">
                                </div>
                                <div class="form-group col-12">
                                    <label for="recipient-name" class="col-form-label">Tags:</label>
                                    <select class="form-control select2-multiple" multiple="multiple" data-width="100%">
                                        <option :value="tag.id" v-for="tag in tags">@{{ tag.name }}</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <label for="recipient-name" class="col-form-label">Notes:</label>
                                    <textarea class="form-control"cols="20" rows="3" v-model="contactEdit.notes"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="updateContact">Update Contact</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script src="{{ asset('js/vendor/select2.full.js') }}"></script>

{{-- <script>
    $(".data-table-custom").DataTable({
        sDom: '<"row view-filter"<"col-sm-12"<"float-right"l><"float-left"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        columns: [{
            data: "Name"
        }, {
            data: "Action"
        }],
        drawCallback: function() {
            $($(".dataTables_wrapper .pagination li:first-of-type")).find("a").addClass("prev"),
            $($(".dataTables_wrapper .pagination li:last-of-type")).find("a").addClass("next"),
            $(".dataTables_wrapper .pagination").addClass("pagination-sm")
        },
        language: {
            paginate: {
                previous: "<i class='simple-icon-arrow-left'></i>",
                next: "<i class='simple-icon-arrow-right'></i>"
            },
            search: "_INPUT_",
            searchPlaceholder: "Search...",
            lengthMenu: "Items Per Page _MENU_"
        }
    });

</script> --}}
<script src="{{ asset('js/app/contacts.js') }}"></script>

@endsection
