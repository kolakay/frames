@extends('Layouts.master') @section('header')
<title>Workflow</title>
@endsection @section('sidebar') @include('Includes.sidebar') @endsection @section('styles')

<style>
    #canvas {
        min-height: 500px;
    }
    #canvas svg {
        width: 100% !important;
    }
    .video-container:hover,
    .active-video-container {
        border: 1px solid blue;
    }
    .video-container {
        cursor: pointer;
    }
    .datepicker.datepicker-dropdown {
        z-index: 999 !important;
    }
    .general-progress-bar {
        margin: auto;
    }
    .general-progress-bar {
        width: 54px;
        height: 54px;
    }

    .icon-title {
        cursor: pointer;
        columns: red;
    }
    .imageActive {
        display: flex;
        border: 2px solid red;
    }
    .clickable_link {
        cursor: pointer !important;
    }
    .flatpickr-calendar {
        top: 104px !important;
        left: 565.5px !important;
        z-index: 999 !important;
        display: block !important;
    }
</style>

<link rel="stylesheet" href="{{ asset('js/tinymce/css/skin.min.css') }}" />
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.3/video-js.min.css" rel="stylesheet" /> --}} {{--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.2.0/css/bootstrap-colorpicker.css" integrity="sha256-VPUdnjgz+19KRUsqdLlc/0gH8ePQ1dY66XajIAlVRqk=" crossorigin="anonymous" /> --}}
<link rel="stylesheet" href="{{ asset('css/vendor/smart_wizard.min.css') }}" />
<link rel="stylesheet" href="{{ asset('flatpickr/flatpickr.css') }}" />

@endsection 
@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Create Workflow</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>

            <div class="col-10 mx-auto" id="app">
                @csrf
                <div class="col-12 mb-4">
                    <div class="card mb-4">
                        <div id="smartWizardDot" class="sw-main sw-theme-dots">
                            <ul class="card-header nav nav-tabs step-anchor">
                                <li class="nav-item" :class="{active: step == 1, done: step > 1 }">
                                    <a href="#" class="nav-link" @click="setStep($event, 1)">
                                        Step 1<br />
                                        <small>Setup</small>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: step == 2, done: step > 2 }">
                                    <a href="#" class="nav-link" @click="setStep($event, 2)">
                                        Step 2<br />
                                        <small>Build</small>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: step == 3, done: step > 3 }">
                                    <a href="#" class="nav-link" @click="setStep($event, 3)">
                                        Step 3<br />
                                        <small>Launch</small>
                                    </a>
                                </li>
                            </ul>
                            
                        </div>
                        <div class="card-body tab-content p-4">
                            <div id="setup" class="tab-pane step-content" style="display: block;" v-show="step == 1">
                                <div>
                                    @include('Includes.Automations.Workflows.setup')
                                </div>
                            </div>
                            <div id="build" class="tab-pane step-content" style="display: block;" v-show="step == 2">
                                <div>
                                    @include('Includes.Automations.Workflows.build')
                                </div>
                            </div>
                            <div id="launch" class="tab-pane step-content" style="display: block;" v-show="step == 3">
                                <div>
                                    @include('Includes.Automations.Workflows.launch')
                                </div>
                            </div>
                        </div>
                        <div class="btn-toolbar sw-toolbar sw-toolbar-bottom justify-content-end">
                            <div class="btn-group mr-2 sw-btn-group" role="group">

                                <a :href="url.campaigns" class="btn btn-danger default mr-2" type="button">Cancel</a>

                                <button class="btn btn-secondary default mr-2" v-if="step < 3" type="button" @click="saveWorkFlowDetails(true)">Save For Later</button>
                                <button class="btn btn-success default mr-2" v-if="step < 3" type="button" @click="nextStep()">Save & Continue</button>
                                <button class="btn btn-success default mr-2" v-if="step == 3" type="button" @click="saveWorkFlowDetails(true)">Save</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <textarea style="display: none;" id="dbworkflow" cols="30" rows="10">{!! json_encode($workflow) !!}</textarea>
    <textarea style="display: none;" id="dbctas" cols="30" rows="10">{!! json_encode($ctas) !!}</textarea>
    <textarea style="display: none;" id="e-campaigns" cols="30" rows="10">{{ json_encode($campaigns) }}</textarea>
    <textarea style="display: none;" id="dbtags" cols="30" rows="10">{!! json_encode($tags) !!}</textarea>
    <textarea style="display: none;" id="dbvideos" cols="30" rows="10">{!! json_encode($videos) !!}</textarea>
    <textarea style="display: none;" id="contacts" cols="30" rows="10">{!! json_encode($contacts) !!}</textarea>
    <textarea style="display: none;" id="nodes" cols="30" rows="10">{!! json_encode($nodes) !!}</textarea>

    {{-- <textarea style="display: none;" id="emailTemplates" cols="30" rows="10">{!! json_encode($emailTemplates) !!}</textarea> --}} {{--
    <textarea style="display: none;" id="emailIntegrations" cols="30" rows="10">{!! json_encode($emailIntegrations) !!}</textarea> --}}
    <textarea style="display: none;" id="list-url" cols="30" rows="10">{{ route('user.automations.workflows') }}</textarea>
    <textarea style="display: none;" id="save-workflow-url" cols="30" rows="10">{{ route('user.automations.workflows.save') }}</textarea>
    <textarea style="display: none;" id="save-node-url" cols="30" rows="10">{{ route('user.automations.workflows.node.save') }}</textarea>
    <textarea style="display: none;" id="update-node-url" cols="30" rows="10">{{ route('user.automations.workflows.node.update') }}</textarea>
    <textarea style="display: none;" id="delete-workflow-url" cols="30" rows="10">{{ route('user.automations.workflows.delete') }}</textarea>

</main>
@endsection @section('scripts')
    {{-- <script src="{{ asset('js/vendor/select2.full.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/vendor/bootstrap-tagsinput.min.js') }}"></script> --}}
    {{--
    <script src="{{ asset('js/video.min.js') }}"></script>
    --}}
    <script src="{{ asset('js/tinymce/js/tinymce.min.js') }}"></script>
    {{--
    <script src="{{ asset('js/vendor/datetimepicker.js') }}"></script>
    --}}
    <script src="{{ asset('flatpickr/flatpickr.js') }}"></script>
    {{--
    <script src="{{ asset('js/flow/svg.js') }}"></script>
    <script src="{{ asset('js/flow/flowsvg.js') }}"></script>
    --}}
    <script src="{{ asset('js/flow/raphael.min.js') }}"></script>
    <script src="{{ asset('js/flow/flowchart-latest.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.js" integrity="sha256-p95IL1RESG1u1ynqmQcAY3f8iXyCWEBX/xtCjdUluG8=" crossorigin="anonymous"></script>
    {{--
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.2.0/js/bootstrap-colorpicker.js" integrity="sha256-c3xaRs6Y77ovyNz6pWP6/7CgFW1aSvbyZjff0Ax5CCM=" crossorigin="anonymous"></script>
    --}}

    <script src="{{ asset('js/app/workflows-edit.js') }}"></script>
    <script src="{{ asset('js/flow/workflow.js') }}"></script>
@endsection
