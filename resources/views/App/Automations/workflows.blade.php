@extends('Layouts.master')
@section('header')
<title>Workflows</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Workflows</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                            <div class="col-12 mx-auto mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <a href="{{ route('user.automations.workflows.create')}}" class="btn btn-secondary default mb-4">Add Workflow</a>
                                        <table class="data-table data-table-custom table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr v-for="(workflow, index) in workflows">
                                                        <td>@{{ workflow.name }}</td>
                                                        <td>
                                                            <a :href="url.edit + workflow.id"
                                                                class="btn btn-primary btn-sm default">
                                                                <i class="simple-icon-pencil"></i>
                                                            </a>
                                                            <button type="button"
                                                                @click="deleteWorkflow(index)"
                                                                class="btn btn-danger btn-sm default">
                                                                    <i class="simple-icon-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <textarea style="display:none" id="workflows" cols="30" rows="10">{{ json_encode($workflows) }}</textarea>

                            <textarea style="display:none" id="create-url" cols="30" rows="10">{{ route('user.automations.workflows.create') }}</textarea>
                            <textarea style="display:none" id="edit-url" cols="30" rows="10">{{ route('user.automations.workflows.edit') }}</textarea>
                            <textarea style="display:none" id="delete-url" cols="30" rows="10">{{ route('user.automations.workflows.delete') }}</textarea>

        
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script src="{{ asset('js/app/workflows.js') }}"></script>

@endsection
