@extends('Layouts.master')
@section('header')
<title>Videos</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Emails</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                            <div class="col-12 mx-auto mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <a href="{{ route('user.automations.emails.create')}}" class="btn btn-secondary default mb-4">Add Email</a>
                                        <table class="data-table data-table-custom table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($emails as $email)
                                                    <tr>
                                                        <td>{{ $email->title }}</td>
                                                        <td>
                                                            <a href="{{ route('user.automations.emails.edit', $email->id)}}"
                                                                class="btn btn-primary btn-sm default">
                                                                <i class="simple-icon-pencil"></i>
                                                            </a>
                                                            <button type="button"
                                                                @click="cloneCampaign(index)"
                                                                class="btn btn-primary btn-sm default">
                                                                    <i class="simple-icon-layers"></i>
                                                            </button>
                                                            <button type="button"
                                                                @click="deleteCompany(index)"
                                                                class="btn btn-danger btn-sm default">
                                                                    <i class="simple-icon-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')

@endsection
