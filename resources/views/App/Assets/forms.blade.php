@extends('Layouts.master')
@section('header')
<title>Forms</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Forms</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                            <div class="col-10 mx-auto mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <button type="button" data-toggle="modal"
                                            data-target="#add-form" class="btn btn-secondary default mb-4">Add Form</button>
                                        <table class="data-table data-table-custom table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr v-for="(form, index) in forms">
                                                        <td>@{{ form.name }}</td>
                                                        <td>@{{ form.description }}</td>
                                                        <td>
                                                            <button type="button"
                                                                @click="selectForm(index)"
                                                                data-toggle="modal"
                                                                data-target="#edit-form"
                                                                class="btn btn-primary btn-sm default">
                                                                    <i class="simple-icon-pencil"></i>
                                                            </button>
                                                            <button type="button"
                                                            @click="deleteForm(index)"
                                                                class="btn btn-danger btn-sm default">
                                                                    <i class="simple-icon-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="forms" cols="30" rows="10">{{ json_encode($forms) }}</textarea>
        <textarea style="display:none" id="tags" cols="30" rows="10">{{ json_encode($tags) }}</textarea>

        <textarea style="display:none" id="create-url" cols="30" rows="10">{{ route('user.assets.forms.create') }}</textarea>
        <textarea style="display:none" id="update-url" cols="30" rows="10">{{ route('user.assets.forms.update') }}</textarea>
        <textarea style="display:none" id="delete-url" cols="30" rows="10">{{ route('user.assets.forms.delete') }}</textarea>
        @include('Includes.Assets.Form.create-modal')
        @include('Includes.Assets.Form.edit-modal')

    </main>
@endsection
@section('scripts')

<script src="{{ asset('js/app/forms.js') }}"></script>

@endsection
