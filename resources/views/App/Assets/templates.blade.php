@extends('Layouts.master')
@section('header')
<title>Videos</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Email Templates</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                            <div class="col-10 mx-auto mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <button type="button" data-toggle="modal"
                                            data-target="#add-template" class="btn btn-secondary default mb-4">Add Template</button>
                                        <table class="data-table data-table-custom table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr v-for="(activeTemplate, index) in templates">
                                                        <td>@{{ activeTemplate.name }} &nbsp;&nbsp;&nbsp;
                                                            <a href="#" data-toggle="modal" @click="selectTemplate(index)" data-target="#edit-template">
                                                                <i class="simple-icon-pencil"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a :href="url.edit + activeTemplate.id"
                                                                target="_blank"
                                                                class="btn btn-info btn-sm default">
                                                                    <i class="simple-icon-pencil"></i>
                                                            </a>
                                                            <a :href="url.preview + activeTemplate.id"
                                                                target="_blank"
                                                                class="btn btn-success btn-sm default">
                                                                    <i class="simple-icon-eye"></i>
                                                            </a>
                                                            <button type="button"
                                                                @click="cloneTemplate(index)"
                                                                class="btn btn-primary btn-sm default">
                                                                    <i class="iconsminds-file-copy"></i>
                                                            </button>
                                                            <button type="button"
                                                            @click="deleteTemplate(index)"
                                                                class="btn btn-danger btn-sm default">
                                                                    <i class="simple-icon-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="templates" cols="30" rows="10">{{ json_encode($templates) }}</textarea>
        <textarea style="display:none" id="defaultTemplates" cols="30" rows="10">{{ json_encode($defaultTemplates) }}</textarea>
        <textarea style="display:none" id="create-url" cols="30" rows="10">{{ route('user.assets.templates.create') }}</textarea>
        <textarea style="display:none" id="update-url" cols="30" rows="10">{{ route('user.assets.templates.update') }}</textarea>
        <textarea style="display:none" id="clone-url" cols="30" rows="10">{{ route('user.assets.templates.clone') }}</textarea>
        <textarea style="display:none" id="delete-url" cols="30" rows="10">{{ route('user.assets.templates.delete') }}</textarea>
        <textarea style="display:none" id="edit-url" cols="30" rows="10">{{ route('user.builder') }}</textarea>
        <textarea style="display:none" id="preview-url" cols="30" rows="10">{{ route('user.builder.preview') }}</textarea>

        <div class="modal fade" id="add-template" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">Add Template Title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Title:</label>
                                <input type="text" class="form-control" id="edit-template-name" v-model="template.name">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="addTemplate">Add Template</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="edit-template" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">Edit Template Title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Title:</label>
                                <input type="text" class="form-control" id="edit-template-name" v-model="templateEdit.name">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="updateTemplate">Update Template</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')

<script src="{{ asset('js/app/templates.js') }}"></script>

@endsection
