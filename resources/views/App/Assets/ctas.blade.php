@extends('Layouts.master')
@section('header')
<title>Videos</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Call To Actions</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    <div class="row">
                            <div class="col-10 mx-auto mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <button type="button" data-toggle="modal"
                                            data-target="#add-cta" class="btn btn-secondary default mb-4">Add CTA</button>
                                        <table class="data-table data-table-custom table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Type</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr v-for="(cta, index) in ctas">
                                                        <td>@{{ cta.name }}</td>
                                                        <td>@{{ cta.type }}</td>
                                                        <td>
                                                            <button type="button"
                                                                @click="selectCTA(index)"
                                                                data-toggle="modal"
                                                                data-target="#edit-cta"
                                                                class="btn btn-primary btn-sm default">
                                                                    <i class="simple-icon-pencil"></i>
                                                            </button>
                                                            <button type="button"
                                                            @click="deleteCTA(index)"
                                                                class="btn btn-danger btn-sm default">
                                                                    <i class="simple-icon-trash"></i>
                                                            </button>
                                                        </td>
                                                    </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="ctas" cols="30" rows="10">{{ json_encode($ctas) }}</textarea>
        <textarea style="display:none" id="forms" cols="30" rows="10">{{ json_encode($forms) }}</textarea>
        <textarea style="display:none" id="create-url" cols="30" rows="10">{{ route('user.assets.ctas.create') }}</textarea>
        <textarea style="display:none" id="update-url" cols="30" rows="10">{{ route('user.assets.ctas.update') }}</textarea>
        <textarea style="display:none" id="delete-url" cols="30" rows="10">{{ route('user.assets.ctas.delete') }}</textarea>
        <textarea style="display:none" id="upload-url" cols="30" rows="10">{{ route('user.assets.ctas.document.upload') }}</textarea>

        @include('Includes.Assets.CTA.create-modal')
        @include('Includes.Assets.CTA.edit-modal')
        {{-- <div class="modal fade" id="edit-cta" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">Edit CTA</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" id="edit-cta-name" v-model="CTAEdit.name">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="updateCTA">Update CTA</button>
                    </div>
                </div>
            </div>
        </div> --}}
    </main>
@endsection
@section('scripts')

<script src="{{ asset('js/app/ctas.js') }}"></script>

@endsection
