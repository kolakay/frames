@extends('Layouts.master')
@section('header')
    <title>Broadcast Settings</title>
@endsection
@section('sidebar')
    @include('Includes.settings-sidebar')
@endsection
@section('content')
    <main id="app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Campaign Configurations</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                <div class="col-12">
                    @include('Includes.messages')
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-10 mx-auto mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <button type="button" data-toggle="modal"
                                        data-target="#add-configuration" class="btn btn-secondary default mb-4">Add Configuration</button>
                                    <table class="data-table data-table-custom table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Provider</th>
                                                <th>From</th>
                                                <th>Limit Per Day</th>
                                                <th>Enabled</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <tr v-for="(configuration, index) in configurations">
                                                    <td>@{{ configuration.type }}</td>
                                                    <td>@{{ configuration.service }}</td>
                                                    <td>@{{ configuration.email }}</td>
                                                    <td>@{{ configuration.limit_per_day }}</td>
                                                    <td>@{{ configuration.is_active }}</td>
                                                    <td>
                                                        <button type="button"
                                                            @click="selectConfiguration(index)"
                                                            data-toggle="modal"
                                                            data-target="#edit-configuration"
                                                            class="btn btn-primary btn-sm default">
                                                                <i class="simple-icon-pencil"></i>
                                                        </button>
                                                        <button type="button"
                                                        @click="deleteConfiguration(index)"
                                                            class="btn btn-danger btn-sm default">
                                                                <i class="simple-icon-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <textarea style="display:none" id="configurations" cols="30" rows="10">{{ json_encode($configurations) }}</textarea>

        <textarea style="display:none" id="create-url" cols="30" rows="10">{{ route('user.settings.broadcast.create') }}</textarea>
        <textarea style="display:none" id="update-url" cols="30" rows="10">{{ route('user.settings.broadcast.update') }}</textarea>
        <textarea style="display:none" id="delete-url" cols="30" rows="10">{{ route('user.settings.broadcast.delete') }}</textarea>

        <div class="modal fade" id="add-configuration" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">New Configurations</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <label class="form-group has-top-label">
                                <select class="form-control" v-model="configuration.type" data-width="100%">
                                    <option value="email">Email</option>
                                    <option value="sms">SMS</option>
                                </select>
                                <span>Type</span>
                            </label>
                            <label class="form-group has-top-label"   v-if="configuration.type == 'email'">
                                <select class="form-control" data-width="100%" v-model="configuration.service">
                                    <option value="">Select</option>
                                    <option value="gmail_api">Gmail API</option>
                                    <option value="mandrill_smtp">Mandrill SMTP</option>
                                    <option value="send_grid_smtp">SendGrid SMTP</option>
                                    <option value="aws_ses">AWS SES</option>
                                    <option value="smtp">SMTP</option>
                                </select>
                                <span>Provider</span>
                            </label>
                            <label class="form-group has-top-label"   v-if="configuration.type != 'email'">
                                <select class="form-control" data-width="100%" v-model="configuration.service">
                                    <option value="">Select</option>
                                    <option value="sns">AWS SNS Message</option>
                                    <option value="twilio">Twilio</option>
                                </select>
                                <span>Provider</span>
                            </label>
                            <div v-show="configuration.type == 'email'">
                                @include('Includes.Settings.mail-modal')
                                <div v-show="configuration.service != '' && configuration.type == 'email'">
                                    <label class="form-group has-top-label" >
                                        <input class="form-control" v-model="configuration.cc">
                                        <span>CC</span>
                                    </label>
                                    <label class="form-group has-top-label" >
                                        <input class="form-control" v-model="configuration.bcc">
                                        <span>BCC</span>
                                    </label>
                                    <label class="form-group has-top-label" >
                                        <input class="form-control" v-model="configuration.limit_per_day">
                                        <span>Send limit per day</span>
                                    </label>
                                </div>
                            </div>
                            <label class="form-group has-top-label" >
                                <select class="form-control" data-width="100%" v-model="configuration.is_active">
                                    <option value="">Select</option>
                                    <option value="true">Active</option>
                                    <option value="false">In Active</option>
                                </select>
                                <span>Enabled</span>
                            </label>
                            <div v-show="configuration.type != 'email'">
                                @include('Includes.Settings.sms-modal')
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="addConfiguration">Add Connection</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="edit-configuration" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">Edit Configurations</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div v-show="configurationEdit.type == 'email'">
                                @include('Includes.Settings.edit-mail-modal')
                                <div v-show="configurationEdit.provider != '' && configurationEdit.type == 'email'">
                                    <label class="form-group has-top-label" >
                                        <input class="form-control" v-model="configurationEdit.cc">
                                        <span>CC</span>
                                    </label>
                                    <label class="form-group has-top-label" >
                                        <input class="form-control" v-model="configurationEdit.bcc">
                                        <span>BCC</span>
                                    </label>
                                    <label class="form-group has-top-label" >
                                        <input class="form-control" v-model="configurationEdit.limit_per_day">
                                        <span>Send limit per day</span>
                                    </label>
                                </div>
                            </div>
                            <label class="form-group has-top-label" >
                                <select class="form-control" data-width="100%" v-model="configurationEdit.is_active">
                                    <option value="">Select</option>
                                    <option value="true">Active</option>
                                    <option value="false">In Active</option>
                                </select>
                                <span>Enabled</span>
                            </label>
                            <div v-show="configuration.type != 'email'">
                                @include('Includes.Settings.edit-sms-modal')
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="updateConfiguration">Update Connection</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
    <script src="{{ asset('js/app/broadcast_settings.js') }}"></script>
@endsection
