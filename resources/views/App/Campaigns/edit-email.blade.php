@extends('Layouts.master')
@section('header')
<title>Edit Video</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('styles')

    <style>
        .video-container:hover,  .active-video-container{
            border:1px solid blue;
        }
        .video-container{
            cursor:pointer
        }
        .datepicker.datepicker-dropdown{
            z-index: 999 !important;
        }
        .general-progress-bar{
            margin: auto;
        }
        .general-progress-bar{
            width:54px;height:54px
        }

        .icon-title{
            cursor: pointer;
            columns: red;
        }
        .imageActive{
            display: flex;
            border: 2px solid red;
        }
        .clickable_link{
            cursor: pointer !important;
        }
        .flatpickr-calendar {
            top: 104px !important;
            left: 565.5px !important;
            z-index: 999 !important;
            display: block !important;
        }
    </style>

    <link rel="stylesheet" href="{{ asset('js/tinymce/css/skin.min.css') }}" />
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.3/video-js.min.css" rel="stylesheet"> --}}
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.2.0/css/bootstrap-colorpicker.css" integrity="sha256-VPUdnjgz+19KRUsqdLlc/0gH8ePQ1dY66XajIAlVRqk=" crossorigin="anonymous" /> --}}
    <link rel="stylesheet" href="{{ asset('css/vendor/smart_wizard.min.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpickr/flatpickr.css') }}">

@endsection
@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Create Email Campaign</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>

            <div class="col-10 mx-auto" id="app">
                @csrf
                <div class="col-12 mb-4">
                    <div class="card mb-4">
                        <div id="smartWizardDot" class="sw-main sw-theme-dots">
                            <ul class="card-header nav nav-tabs step-anchor">
                                <li class="nav-item" :class="{active: step == 1, done: step > 1 }">
                                    <a href="#" class="nav-link" @click="setStep($event, 1)">
                                        Step 1<br />
                                        <small>Email</small>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: step == 2, done: step > 2 }">
                                    <a href="#" class="nav-link" @click="setStep($event, 2)">
                                        Step 2<br />
                                        <small>Body</small>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: step == 3, done: step > 3 }">
                                    <a href="#recipients" class="nav-link" @click="setStep($event, 3)">
                                        Step 3<br />
                                        <small>Recipients</small>
                                    </a>
                                </li>
                                <li class="nav-item" :class="{active: step == 4, done: step > 4 }">
                                    <a href="#send" class="nav-link" @click="setStep($event, 4)">
                                        Step 4<br />
                                        <small>Send</small>
                                    </a>
                                </li>
                            </ul>
                            <div class="card-body tab-content p-4">
                                <div id="email" class="tab-pane step-content" style="display:block" v-show="step == 1">
                                    <h4 class="pb-2">Email</h4>
                                    @include('Includes.Campaigns.Email.email-step')
                                </div>
                                <div id="body" class="tab-pane step-content" style="display:block" v-show="step == 2">
                                    <div>
                                        @include('Includes.Campaigns.Email.body-step')
                                    </div>
                                </div>
                                <div id="recipients" class="tab-pane step-content" style="display:block" v-show="step == 3">
                                    <div>
                                        @include('Includes.Campaigns.Email.recipients-step')
                                    </div>
                                </div>
                                <div id="send" class="tab-pane step-content" style="display:block" v-show="step == 4">
                                    <div>
                                        @include('Includes.Campaigns.Email.send-step')
                                    </div>
                                </div>
                            </div>
                            <div class="btn-toolbar sw-toolbar sw-toolbar-bottom justify-content-end">
                                <div class="btn-group mr-2 sw-btn-group" role="group">
                                    <button class="btn btn-danger default mr-2" type="button">Cancel</button>

                                    <button class="btn btn-secondary default mr-2" type="button">Save Draft</button>
                                    <button class="btn btn-success default mr-2" type="button" @click="nextStep()">Save & Continue</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <textarea style="display:none" id="campaign" cols="30" rows="10">{{ json_encode($campaign) }}</textarea>
    <textarea style="display:none" id="tags" cols="30" rows="10">{{ json_encode($tags) }}</textarea>
    <textarea style="display:none" id="videos" cols="30" rows="10">{{ json_encode($videos) }}</textarea>
    <textarea style="display:none" id="contacts" cols="30" rows="10">{{ json_encode($contacts) }}</textarea>
    <textarea style="display:none" id="emailTemplates" cols="30" rows="10">{{ json_encode($emailTemplates) }}</textarea>
    <textarea style="display:none" id="emailIntegrations" cols="30" rows="10">{{ json_encode($emailIntegrations) }}</textarea>

    <textarea style="display:none" id="save-url" cols="30" rows="10">{{ route('user.campaigns.emails.save') }}</textarea>
    <textarea style="display:none" id="publish-url" cols="30" rows="10">{{ route('user.campaigns.emails.publish') }}</textarea>
    <textarea style="display:none" id="test-email-url" cols="30" rows="10">{{ route('user.campaigns.emails.test') }}</textarea>
    <textarea style="display:none" id="edit-template-url" cols="30" rows="10">{{ route('user.builder') }}</textarea>
    <textarea style="display:none" id="preview-template-url" cols="30" rows="10">{{ route('user.builder.preview') }}</textarea>

</main>
@endsection
@section('scripts')
<script src="{{ asset('js/vendor/select2.full.js') }}"></script>
<script src="{{ asset('js/vendor/bootstrap-tagsinput.min.js') }}"></script>
{{-- <script src="{{ asset('js/video.min.js') }}"></script> --}}
<script src="{{ asset('js/tinymce/js/tinymce.min.js') }}"></script>
{{-- <script src="{{ asset('js/vendor/datetimepicker.js') }}"></script> --}}
<script src="{{ asset('flatpickr/flatpickr.js') }}"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.2.0/js/bootstrap-colorpicker.js" integrity="sha256-c3xaRs6Y77ovyNz6pWP6/7CgFW1aSvbyZjff0Ax5CCM=" crossorigin="anonymous"></script> --}}
<script src="{{ asset('js/app/email-campaign-edit.js') }}"></script>
@endsection
