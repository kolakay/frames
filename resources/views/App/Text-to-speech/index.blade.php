@extends('Layouts.master')
@section('header')
<title>Videos</title>
@endsection
@section('sidebar')
    @include('Includes.sidebar')
@endsection
@section('content')
    <main id="app" v-cloak>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h1>Speeches</h1>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Library</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                    <div class="separator mb-5"></div>
                </div>
                @csrf
                <div class="row col-12">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="">Speech Title</label>
                            <input type="text" name="" id="" class="form-control" v-model="speech.title">
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-6 pl-0">
                                    <div class="form-group">
                                        <label for="">Language</label>
                                        <select name="" id="" class="form-control" v-model="speech.language">
                                            @foreach(googleTextToSpeechLanguages() as $language)
                                                <option value="{{ $language["code"]}}">{{ $language["name"]}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6 pr-0">
                                    <div class="form-group">
                                        <label for="">Gender</label>
                                        <select name="" id="" class="form-control" v-model="speech.gender">
                                            <option value="0">Male</option>
                                            <option value="1">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 p-0">
                            <textarea name="" id="" class="form-control" rows="20" v-model="speech.text"></textarea>
                            <div class="text-center mt-2">
                                <button class="btn btn-primary default mx-auto pt-2" v-if="isLoading == true" disabled>Converting...</button>

                                <button class="btn btn-primary default mx-auto pt-2" v-if="isLoading == false" @click="convertText()">Convert</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <table class="data-table data-table-custom table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr v-for="(speech, index) in speeches">
                                                <td>@{{ speech.title }}</td>
                                                <td>@{{ speech.status }}</td>
                                                <td>
                                                    <a :href="url.download + speech.id"
                                                        v-if="speech.status == 'converted'"
                                                        class="btn btn-primary btn-sm default">
                                                            <i class="simple-icon-arrow-down-circle"></i>
                                                    </a>
                                                    <button type="button"
                                                    @click="deleteSpeech(index)"
                                                        class="btn btn-danger btn-sm default">
                                                            <i class="simple-icon-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <textarea style="display:none" id="speeches" cols="30" rows="10">{{ json_encode($speeches) }}</textarea>

        <textarea style="display:none" id="convert-url" cols="30" rows="10">{{ route('user.text-to-speech.convert') }}</textarea>
        <textarea style="display:none" id="download-url" cols="30" rows="10">{{ route('user.text-to-speech.download') }}</textarea>
        <textarea style="display:none" id="delete-url" cols="30" rows="10">{{ route('user.text-to-speech.delete') }}</textarea>

        </div>
    </main>
@endsection
@section('scripts')
<script src="{{ asset('js/app/speeches.js') }}"></script>

@endsection
