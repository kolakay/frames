<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <iframe src="" frameborder="0"></iframe>
    <!-- jquery-->
    <script src="/js/vendor/builder/js/jquery.min.js"></script>
    <script src="/js/vendor/builder/js/jquery.hotkeys.js"></script>

    <!-- bootstrap-->
    <script src="/js/vendor/builder/js/popper.min.js"></script>
    <script src="/js/vendor/builder/js/bootstrap.min.js"></script>

    <!-- builder code-->
    <script src="/js/vendor/builder/libs/builder/builder.js"></script>
    <!-- undo manager-->
    <script src="/js/vendor/builder/libs/builder/undo.js"></script>
    <!-- inputs-->
    <script src="/js/vendor/builder/libs/builder/inputs.js"></script>
    <!-- components-->
    <script src="/js/vendor/builder/libs/builder/components-bootstrap4.js"></script>
    <script src="/js/vendor/builder/libs/builder/components-widgets.js"></script>

    <script>
        let url = `{{ asset('js/vendor/builder/demo/album/index.html')}}`;
        $(document).ready(function()
        {
            Vvveb.Builder.init(url, function() {
                //load code after page is loaded here
                Vvveb.Gui.init();
            });
        });
    </script>
</body>
</html>
