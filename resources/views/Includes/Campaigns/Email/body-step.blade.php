<div class="form-group">
    <label class="form-group has-top-label">
        <input class="form-control"
        v-model="emailCampaign.video_header"
        type="text">
        <span>Video Header
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This is personalization text that appears as an overlay on the animated gif in the body of an email AND on the video landing page. We recommend adding personalization text with fallback text (in case there is no first name stored in the contact’s record) (e.g. #first-name|Hi#."></i>
        </span>
    </label>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-7">
            <label class="form-group has-top-label">
                <select class="form-control" data-width="100%" id="templateId" v-model="emailCampaign.template_id">
                    <option selected value="null" disabled>Select Template</option>
                    <option value="0">Simple Editor</option>
                    <option :value="emailTemplate.id" v-for="emailTemplate in emailTemplates">@{{ emailTemplate.name }}</option>
                </select>
                <span>Select Template</span>
            </label>
        </div>
        <div class="col-5">
        <a href="{{ route('user.assets.templates') }}" target="_blank" class="btn btn-outline-info mb-1 straight-btn">Create</a>
            <div class="d-inline" v-if="emailCampaign.template_id != '' && emailCampaign.template_id != undefined && emailCampaign.template_id != '0'">
                <a :href="url.edit_template + emailCampaign.template_id"
                    target="_blank" class="btn btn-outline-success mb-1 straight-btn" >Edit</a>
                <a :href="url.preview_template + emailCampaign.template_id"
                    target="_blank" class="btn btn-outline-primary mb-1 straight-btn" >Preview</a>
            </div>
        </div>
    </div>
</div>

<div class="form-group" v-show="emailCampaign.template_id == 0">
    <textarea id="simple-editor"></textarea>
</div>

