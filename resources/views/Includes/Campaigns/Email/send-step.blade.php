<div class="custom-control custom-checkbox mb-4 col-6">
    <input type="checkbox" class="custom-control-input" id="customCheckThis" v-model="emailCampaign.schedule_is_set">
    <label class="custom-control-label" for="customCheckThis">Send Later ?</label>
</div>
<div class="form-group" v-show="emailCampaign.schedule_is_set">
    <input class="form-control user-timepicker" type="text" id="user-timepicker">
</div>
