<div class="form-group">

    <label class="form-group has-top-label">
        <input class="form-control"
        v-model="emailCampaign.title"

        type="text">
        <span>Campaign Title
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This is the subject line that will appear in recipient’s inboxes. To add personalization text include #first-name#, #last-name# or #email#. For a fallback enter text after a | (e.g. #first-name|Hi#."></i>
        </span>
    </label>

</div>
<div class="form-group">
    <label class="form-group has-top-label">
        <select class="form-control select2-single" data-width="100%" id="providerId">
            <option label="&nbsp;">&nbsp;</option>
            <option :value="emailIntegration.id" v-for="emailIntegration in emailIntegrations">@{{ emailIntegration.email }}</option>
        </select>
        <span>Broadcast Provider
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This is the from email that the email will be sent from. To add additional emails here you must visit the Team > Configuration page."></i>
        </span>
    </label>
    {{-- <p >
        Email this video directly from your Gmail account or <a target="_blank" href="{{ route('user.settings.broadcast')}}" class="text-info">configured</a> email server.
    </p> --}}
</div>
<div class="form-group">

    <label class="form-group has-top-label">
        <input class="form-control"
        v-model="emailCampaign.from_name"

        type="text">
        <span>From Name
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This is the subject line that will appear in recipient’s inboxes. To add personalization text include #first-name#, #last-name# or #email#. For a fallback enter text after a | (e.g. #first-name|Hi#."></i>
        </span>
    </label>

</div>
<div class="form-group">

    <label class="form-group has-top-label">
        <input class="form-control"
        v-model="emailCampaign.subject"

        type="text">
        <span>Subject
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This is the subject line that will appear in recipient’s inboxes. To add personalization text include #first-name#, #last-name# or #email#. For a fallback enter text after a | (e.g. #first-name|Hi#."></i>
        </span>
    </label>

</div>
<div class="form-group">
    <label for="">Select Video</label>
    <div class="col-8 mx-auto">
        <div class="row social-image-row gallery" style="max-height: 500px; overflow-y:scroll">
            <div class="col-4 p-2 video-container" v-for="video in videos"
                @click="selectVideo(video.id)"
                :class="{ 'active-video-container': video.id == emailCampaign.video_id }">
                <img class="img-fluid" :src="video.video_thumbnail_path" /></a>
                <p>@{{ video.name}}</p>
            </div>
        </div>
    </div>
</div>
