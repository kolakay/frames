<div class="form-group">
    <label class="form-group has-top-label">
        <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="contactEmails">

            <option :value="contact.id" v-for="contact in contacts">@{{ contact.email }}</option>
        </select>
        <span>Emails</span>
    </label>
</div>
<div class="form-group">
    <label class="form-group has-top-label">
        <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="inclusionList">

            <option :value="tag.id" v-for="tag in tags">@{{ tag.name }}</option>
        </select>
        <span>Inclusion List</span>
    </label>
</div>
<div class="form-group">
    <label class="form-group has-top-label">
        <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="exclusionList">

            <option :value="tag.id" v-for="tag in tags">@{{ tag.name }}</option>
        </select>
        <span>Exclude List</span>
    </label>
</div>
