<div class="menu">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="{{ $page == 'dashboard' ? 'active' : '' }}">
                    <a href="{{ route('user.dashboard') }}"><i class="iconsminds-home"></i> Dashboard</a>
                </li>
                <li class="{{ ($page == 'videos')  ? 'active' : '' }}">
                    <a href="#videos"><i class="iconsminds-camera-4"></i> Videos</a>
                </li>
                <li class="{{ $page == 'assets' ? 'active' : '' }}">
                    <a href="#assets"><i class="iconsminds-diamond"></i> <span>Assets</span></a>
                </li>
                <li class="{{ $page == 'connections' ? 'active' : '' }}">
                    <a href="#connections"><i class="iconsminds-power-cable"></i> Connections</a>
                </li>
                <li class="{{ $page == 'campaigns' ? 'active' : '' }}">
                    <a href="#campaigns"><i class="iconsminds-video"></i> Campaigns</a>
                </li>
                <li class="{{ $page == 'automation' ? 'active' : '' }}">
                    <a href="#automation"><i class="iconsminds-cd-2"></i> Automation</a>
                </li>
                <li class="{{ $page == 'reporting' ? 'active' : '' }}">
                    <a href="{{ route('user.dashboard') }}"><i class="iconsminds-line-chart-1"></i> Reporting</a>
                </li>
                <li class="{{ $page == 'text-to-speech' ? 'active' : '' }}">
                    <a href="{{ route('user.text-to-speech') }}"><i class="iconsminds-megaphone"></i> Text to Speech</a>
                </li>
                <li class="{{ $page == 'tasks' ? 'active' : '' }}">
                    <a href="{{ route('user.tasks') }}"><i class="iconsminds-file-clipboard-file---text"></i> Tasks</a>
                </li>
                <li class="{{ $page == 'deals' ? 'active' : '' }}">
                    <a href="{{ route('user.deals') }}"><i class="iconsminds-line-chart-1"></i> Deals</a>
                </li>
                <li class="{{ $page == 'resources' ? 'active' : '' }}">
                    <a href="#resources"><i class="iconsminds-bookmark"></i> Resources</a>
                </li>
                <li class="{{ $page == 'program' ? 'active' : '' }}">
                    <a href="{{ route('user.dashboard') }}"><i class="iconsminds-affiliate"></i> Affiliate Program</a>
                </li>
                <li class="{{ $page == 'notfound' ? '' : '' }} collapse" >
                    <a href="{{ route('not-found') }}"></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="sub-menu">
        <div class="scroll">
            <ul class="list-unstyled" data-link="videos">
                <li class="{{ ($sub == 'videos')  ? 'active' : '' }}">
                    <a href="{{ route('user.videos.videos')}}">
                        <i class="simple-icon-rocket"></i> <span class="d-inline-block">Videos</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'tags')  ? 'active' : '' }}">
                    <a href="{{ route('user.videos.tags')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Tags</span>
                    </a>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="assets">
                <li class="{{ ($sub == 'ctas')  ? 'active' : '' }}">
                    <a href="{{ route('user.assets.ctas')}}">
                        <i class="simple-icon-rocket"></i>
                        <span class="d-inline-block">Calls To Action</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'templates')  ? 'active' : '' }}">
                    <a href="{{ route('user.assets.templates')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Templates</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'forms')  ? 'active' : '' }}">
                    <a href="{{ route('user.assets.forms')}}">
                        <i class="simple-icon-basket-loaded"></i>
                        <span class="d-inline-block">Forms</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'landing_pages')  ? 'active' : '' }}">
                    <a href="{{ route('user.assets.landing-pages')}}">
                        <i class="simple-icon-doc"></i> 
                        <span class="d-inline-block">Landing Pages</span>
                    </a>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="connections">
                <li class="{{ ($sub == 'contacts')  ? 'active' : '' }}">
                    <a href="{{ route('user.connections.contacts')}}">
                        <i class="simple-icon-rocket"></i>
                        <span class="d-inline-block">Contacts</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'companies')  ? 'active' : '' }}">
                    <a href="{{ route('user.connections.companies')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Companies</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'connection_tags')  ? 'active' : '' }}">
                    <a href="{{ route('user.connections.tags')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Tags</span>
                    </a>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="campaigns">
                <li class="{{ ($sub == 'emails')  ? 'active' : '' }}">
                    <a href="{{ route('user.campaigns.emails') }}"><i class="simple-icon-rocket"></i> <span class="d-inline-block">Email</span></a>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="automation">
                <li class="{{ ($sub == 'workflows')  ? 'active' : '' }}">
                    <a href="{{ route('user.automations.workflows') }}"><i class="simple-icon-rocket"></i>
                        <span class="d-inline-block">Workflows</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'auto-emails')  ? 'active' : '' }}">
                    <a href="{{ route('user.automations.emails') }}"><i class="simple-icon-rocket"></i> <span class="d-inline-block">Email</span></a></li>
            </ul>

            <ul class="list-unstyled" data-link="resources">
                <li><a href="#"><i class="simple-icon-rocket"></i> <span class="d-inline-block">eBooks</span></a></li>
                <li><a href="#"><i class="simple-icon-pie-chart"></i> <span class="d-inline-block">Learning Center</span></a></li>
                <li><a href="#"><i class="simple-icon-basket-loaded"></i> <span class="d-inline-block">Podcasts</span></a></li>
                <li><a href="#"><i class="simple-icon-doc"></i> <span class="d-inline-block">Sample Videos</span></a></li>

                <li><a href="#"><i class="simple-icon-pie-chart"></i> <span class="d-inline-block">Desktop Recorder</span></a></li>
                <li><a href="#"><i class="simple-icon-basket-loaded"></i> <span class="d-inline-block">Video Converter</span></a></li>
                <li><a href="#"><i class="simple-icon-doc"></i> <span class="d-inline-block">Equipment</span></a></li>
            </ul>
        </div>
    </div>
</div>
