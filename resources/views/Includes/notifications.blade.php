@if(Session::has('successMessage'))
    <input type="hidden" id="message" data-type="success" value="{{Session::get('successMessage')}}">
    {{Session::forget('successMessage')}}
@elseif(Session::has('errorMessage'))
    <input type="hidden" id="message" data-type="error" value="{{Session::get('errorMessage')}}">
    {{Session::forget('errorMessage')}}
@else
    <input type="hidden" id="message" data-type="none" value="">
@endif
