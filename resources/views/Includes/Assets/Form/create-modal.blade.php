
        <div class="modal fade" id="add-form" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">New Form</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label class="form-group has-top-label">
                                    <input class="form-control" v-model="form.name"> <span>Form Name</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="form-group has-top-label">
                                    <input class="form-control" v-model="form.title"> <span>Form Title</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="form-group has-top-label">
                                    <textarea class="form-control" v-model="form.description"></textarea> <span>Form Description</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="form-group has-top-label">
                                    <select class="form-control select2-single" data-width="100%" v-model="form.button_text">
                                        <option value="">Select Button Text</option>
                                        <option value="SUBMIT">SUBMIT</option>
                                        <option value="GO">Go</option>
                                        <option value="I'M">I'M IN</option>
                                        <option value="TAKE ME THERE">TAKE ME THERE</option>
                                        <option value="NEXT">NEXT</option>
                                        <option value="GET THE COURSE">GET THE COURSE</option>
                                        <option value="REGISTER">REGISTER</option>
                                        <option value="REGISTER FOR WEBINAR">REGISTER FOR WEBINAR</option>
                                        <option value="WATCH">WATCH</option>
                                        <option value="WATCH VIDEO">WATCH VIDEO</option>
                                        <option value="WATCH MASTERCLASS">WATCH MASTERCLASS</option>
                                        <option value="YES PLEASE">YES PLEASE</option>
                                        <option value="custom">[Custom Text]</option>
                                    </select>
                                    <span>Button Text</span>
                                </label>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-12 col-form-label">Fields</label>
                                <div v-for="(field, index) in form.fields" class="col-12">
                                    <div class="row pl-3">
                                        <div class="custom-control custom-checkbox mb-4 col-6">
                                            <input type="checkbox" class="custom-control-input" :id="'_'+index" v-model="field.is_set">
                                            <label class="custom-control-label" :for="'_'+ index">@{{ field.name }}</label>
                                        </div>

                                        <div class="custom-control custom-checkbox mb-4 col-6">
                                            <input type="checkbox" class="custom-control-input" :id="'_'+ index + 1" v-model="field.is_required">
                                            <label class="custom-control-label" :for="'_'+ index + 1">Required </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-group has-top-label">
                                    <select class="form-control select2-single" data-width="100%" v-model="form.complete_action">
                                        <option value="url">Forward To URL</option>
                                        <option value="message">Display Custom Message</option>
                                    </select>
                                    <span>Complete Action</span>
                                </label>
                            </div>
                            <div class="form-group" v-if = "form.complete_action == 'url'">
                                <label class="form-group has-top-label">
                                    <input class="form-control" v-model="form.url"> <span>URL</span>
                                </label>
                            </div>
                            <div class="form-group" v-if = "form.complete_action == 'message'">
                                <label class="form-group has-top-label">
                                    <input class="form-control" v-model="form.message"> <span>Message</span>
                                </label>
                            </div>
                            <label class="form-group has-top-label">
                                <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="video_tags">
                                    <option :value="tag.id"  v-for="tag in tags">@{{ tag.name }}</option>
                                </select>
                                <span>Tags</span>
                            </label>



                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="addForm">Add Form</button>
                    </div>
                </div>
            </div>
        </div>
