
        <div class="modal fade" id="add-cta" tabindex="-1" role="dialog" aria-modal="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalContentLabel">New CTA</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label class="form-group has-top-label">
                                    <input class="form-control" v-model="cta.name"> <span>Name</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="form-group has-top-label">
                                    <select class="form-control" data-width="100%" v-model="cta.type">
                                        <option value="email">E-mail</option>
                                        <option value="url">URL</option>
                                        <option value="phone">Phone Number</option>
                                        <option value="chat">Chat</option>
                                        <option value="calendar">Calendar</option>
                                        <option value="video_reply">Video Reply</option>
                                        <option value="form">Form</option>
                                        <option value="iframe">External Form</option>
                                        <option value="document">Document</option>
                                        <option value="products">Products</option>
                                        <option value="web_page">Web Page</option>
                                    </select>
                                    <span>Type</span>
                                </label>
                                <div v-if="cta.type == 'email'" class="form-group">
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.email"> <span>E-MAIL</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.email_button_text">
                                                <option value="">Select Button Text</option>
                                                <option value="Respond">Respond</option>
                                                <option value="Reply">Reply</option>
                                                <option value="Email Kolawole">Email Kolawole</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.email_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.email_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'url'" class="form-group">
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.url"> <span>URL</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <div class="row pl-4">
                                            <div class="custom-control custom-radio col-6">
                                                <input type="radio" v-model="cta.url_tab" value="blank" id="customRadio1" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">New Tab</label>
                                            </div>
                                            <div class="custom-control custom-radio col-6">
                                                <input type="radio" id="customRadio2" v-model="cta.url_tab" value="same" name="customRadio" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Same Window</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.url_button_text">
                                                <option value="">Select Button Text</option>
                                                <option value="Apply Now">Apply Now</option>
                                                <option value="Book Now">Book Now</option>
                                                <option value="Contact Us">Contact Us</option>
                                                <option value="Download">Download</option>
                                                <option value="Get Quote">Get Quote</option>
                                                <option value="Learn More">Learn More</option>
                                                <option value="Play Game">Play Game</option>
                                                <option value="Shop Now">Shop Now</option>
                                                <option value="Sign Up">Sign Up</option>
                                                <option value="Subscribe">Subscribe</option>
                                                <option value="Use App">Use App</option>
                                                <option value="Watch Video">Watch Video</option>
                                                <option value="Schedule a Meeting">Schedule a Meeting</option>
                                                <option value="Visit Website">Visit Website</option>
                                                <option value="Download File">Download File</option>
                                                <option value="Leave Review">Leave Review</option>
                                                <option value="Read Reviews">Read Reviews</option>
                                                <option value="Zillow Reviews">Zillow Reviews</option>
                                                <option value="Read Zillow Reviews">Read Zillow Reviews</option>
                                                <option value="Listen Now">Listen Now</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.url_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.url_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'phone'" class="form-group">

                                    <div class="form-group">
                                        <div class="row pl-4">
                                            <div class="custom-control custom-radio col-6">
                                                <input type="radio" v-model="cta.phone_action" value="sms" id="phone-action-sms" name="phone_action" class="custom-control-input">
                                                <label class="custom-control-label" for="phone-action-sms">SMS</label>
                                            </div>
                                            <div class="custom-control custom-radio col-6">
                                                <input type="radio" v-model="cta.phone_action" value="sms" id="phone-action-call" name="phone_action" class="custom-control-input">
                                                <label class="custom-control-label" for="phone-action-call">Phone Call</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.phone"> <span>Phone Number</span>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.phone_button_text">
                                                <option value="">Select Button Text</option>
                                                <option value="Call Now">Call Now</option>
                                                <option value="Schedule a Call">Schedule a Call</option>
                                                <option value="Call Kolawole">Call Kolawole</option>
                                                <option value="Text Kolawole Now">Text Kolawole Now</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.phone_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.phone_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'chat'" class="form-group">

                                    <div class="form-group">
                                        <div class="row pl-4">
                                            <div class="custom-control custom-radio col-4">
                                                <input type="radio" value="messenger" v-model="cta.chat_platform" id="chat-platform-messenger" name="chat_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="chat-platform-messenger">Messenger</label>
                                            </div>
                                            <div class="custom-control custom-radio col-4">
                                                <input type="radio" value="whatsapp" v-model="cta.chat_platform" id="chat-platform-whatsapp" name="chat_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="chat-platform-whatsapp">WhatsApp</label>
                                            </div>
                                            <div class="custom-control custom-radio col-4">
                                                <input type="radio" value="skype" v-model="cta.chat_platform" id="chat-platform-skype" name="chat_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="chat-platform-skype">Skype</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.chat_username">
                                            <span v-if="cta.chat_platform == 'messenger'">Your Messenger Username</span>
                                            <span v-if="cta.chat_platform == 'whatsapp'"> Your WhatsApp Phone Number</span>
                                            <span v-if="cta.chat_platform == 'skype'">Your Skype Username</span>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.chat_button_text">
                                                <option value="">Select Button Text</option>
                                            <option :value="'reply with' + cta.chat_platform">Reply With @{{ cta.chat_platform}}</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.chat_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.chat_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'calendar'" class="form-group">

                                    <div class="form-group">
                                        <div class="row pl-4">
                                            <div class="custom-control custom-radio col-2">
                                                <input type="radio" value="calendly" v-model="cta.calendar_platform" id="calendar-platform-calendly" name="calendar_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="calendar-platform-calendly">Calendly</label>
                                            </div>
                                            <div class="custom-control custom-radio col-2">
                                                <input type="radio" value="acuity_scheduler" v-model="cta.calendar_platform" id="calendar-platform-acuity_scheduler" name="calendar_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="calendar-platform-acuity_scheduler">Acuity Scheduler</label>
                                            </div>
                                            <div class="custom-control custom-radio col-2">
                                                <input type="radio" value="book_me" v-model="cta.calendar_platform" id="chat-platform-book_me" name="calendar_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="chat-platform-book_me">Book Like A Boss</label>
                                            </div>

                                            <div class="custom-control custom-radio col-2">
                                                <input type="radio" value="hubspot" v-model="cta.calendar_platform" id="chat-platform-hubspot" name="calendar_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="chat-platform-hubspot">Hubspot</label>
                                            </div>
                                            <div class="custom-control custom-radio col-2">
                                                <input type="radio" value="schedule_once" v-model="cta.calendar_platform" id="chat-platform-ScheduleOnce" name="calendar_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="chat-platform-ScheduleOnce">ScheduleOnce</label>
                                            </div>
                                            <div class="custom-control custom-radio col-2">
                                                <input type="radio" value="other" v-model="cta.calendar_platform" id="chat-platform-other" name="calendar_platform" class="custom-control-input">
                                                <label class="custom-control-label" for="chat-platform-other">Other</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" v-if="cta.calendar_platform != 'book_me'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.calendar_username">
                                            <span v-if="cta.calendar_platform == 'calendly'">Your Calendly Username</span>
                                            <span v-if="cta.calendar_platform == 'acuity_scheduler'"> Scheduling Link</span>
                                            <span v-if="cta.calendar_platform == 'hubspot'">Your Hubspot Username</span>
                                            <span v-if="cta.calendar_platform == 'schedule_once'"> Your ScheduleOnce Username</span>
                                            <span v-if="cta.calendar_platform == 'other'">Calendar URL</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.calendar_platform == 'book_me'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.calendar_id">
                                            <span>Enter your Book Like a Boss ID here.</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.calendar_platform == 'book_me'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.calendar_hash">
                                            <span>Enter your Book Like a Boss Hash here.</span>
                                        </label>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.calendar_button_text">
                                                <option value="">Select Button Text</option>
                                                <option value="Schedule">Schedule</option>
                                                <option value="Schedule a Consultation">Schedule a Consultation</option>
                                                <option value="Book a Time">Book a Time</option>
                                                <option value="Book a Time with Kolawole">Book a Time with Kolawole</option>
                                                <option value="Make an Appointment">Make an Appointment</option>
                                                <option value="Make an Appointment with Kolawole">Make an Appointment with Kolawole</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.calendar_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.calendar_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'form'" class="form-group">
                                    <label class="form-group has-top-label">
                                        <select class="form-control select2-single" data-width="100%" v-model="cta.form">
                                            <option value="">Select Form</option>
                                            <option :value="form.id" v-for="form in forms">@{{ form.name }}</option>
                                        </select>
                                        <span>Form</span>
                                    </label>

                                    <label class="form-group has-top-label">
                                        <select class="form-control select2-single" data-width="100%" v-model="cta.form_button_text">
                                            <option value="">Select Button Text</option>
                                            <option value="FILL OUT THE FORM">FILL OUT THE FORM</option>
                                            <option value="COMPLETE THE FORM">COMPLETE THE FORM</option>
                                            <option value="TAKE ME TO THE FORM">TAKE ME TO THE FORM</option>
                                            <option value="SEE FORM BELOW">SEE FORM BELOW</option>
                                            <option value="custom">[Custom Text]</option>
                                        </select>
                                        <span>Button Text</span>
                                    </label>
                                    <div class="form-group" v-if="cta.form_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.form_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'iframe'" class="form-group">
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.iframe_title"> <span>Form Title</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.iframe_source"> <span>Form Source</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.iframe_height"> <span>Form Height(px)</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox mb-4 col-6">
                                            <input type="checkbox" v-model="cta.iframe_scroll" class="custom-control-input" id="customCheckSix">
                                            <label class="custom-control-label" for="customCheckSix">Disable Scrolling </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.iframe_button_text">
                                                <option value="">Select Button Text</option>
                                                <option value="Apply Now">Apply Now</option>
                                                <option value="Contact Me">Contact Me</option>
                                                <option value="Contact Now">Contact Now</option>
                                                <option value="Contact Us">Contact Us</option>
                                                <option value="Download Now">Download Now</option>
                                                <option value="Download">Download</option>
                                                <option value="Learn More">Learn More</option>
                                                <option value="Free Consultation ">Free Consultation </option>
                                                <option value="Get Started">Get Started</option>
                                                <option value="Talk to Me">Talk to Me</option>
                                                <option value="Talk to Us">Talk to Us</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.iframe_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.iframe_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'document'" class="form-group">
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.document_title"> <span>Document Title</span>
                                        </label>
                                    </div>
                                    <div class="form-group mb-3">
                                            <label class="form-group has-top-label">
                                                <input type="file" class="form-control" id="fileInput" v-on:change="readFile">
                                                <span>Choose file</span>
                                            </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.document_button_text">
                                                <option value="">Select Button Text</option>
                                                <option value="Review Document">Review Document</option>
                                                <option value="Review Proposal">Review Proposal</option>
                                                <option value="View Document">View Document</option>
                                                <option value="View Proposal">View Proposal</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.document_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.document_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'products'" class="form-group">
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.product_titles"> <span>Product Name</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.product_button_text">
                                                <option value="">Select Button Text</option>
                                                <option value="View Product">View Product</option>
                                                <option value="Buy Product">Buy Product</option>
                                                <option value="Learn More">Learn More</option>
                                                <option value="Buy">Buy</option>
                                                <option value="Purchase">Purchase</option>
                                                <option value="Add to Cart">Add to Cart</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.product_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.product_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                                <div v-if="cta.type == 'web_page'" class="form-group">
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.web_page_title"> <span>Page Title</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.web_page_url"> <span>Web URL</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.web_page_height"> <span>Page Height(px)</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox mb-4 col-6">
                                            <input type="checkbox" v-model="cta.web_page_scroll" class="custom-control-input" id="webPageScrolling">
                                            <label class="custom-control-label" for="webPageScrolling">Disable Page Scrolling </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-group has-top-label">
                                            <select class="form-control select2-single" data-width="100%" v-model="cta.web_page_button_text">
                                                <option value="">Select Button Text</option>
                                                <option value="Apply Now">Apply Now</option>
                                                <option value="Contact Me">Contact Me</option>
                                                <option value="Contact Now">Contact Now</option>
                                                <option value="Contact Us">Contact Us</option>
                                                <option value="Download Now">Download Now</option>
                                                <option value="Download">Download</option>
                                                <option value="Learn More">Learn More</option>
                                                <option value="Free Consultation ">Free Consultation </option>
                                                <option value="Get Started">Get Started</option>
                                                <option value="Talk to Me">Talk to Me</option>
                                                <option value="Talk to Us">Talk to Us</option>
                                                <option value="custom">[Custom Text]</option>
                                            </select>
                                            <span>Button Text</span>
                                        </label>
                                    </div>
                                    <div class="form-group" v-if="cta.web_page_button_text == 'custom'">
                                        <label class="form-group has-top-label">
                                            <input class="form-control" v-model="cta.web_page_custom_button_text"> <span>Enter Custom Button Text</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox mb-4 col-6">
                                    <input type="checkbox" v-model="cta.is_default" class="custom-control-input" id="defaultCTA">
                                    <label class="custom-control-label" for="defaultCTA">Make this my default CTA</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-group has-top-label">
                                    <textarea class="form-control" v-model="cta.description"></textarea> <span>Description</span>
                                </label>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="addCTA">Add CTA</button>
                    </div>
                </div>
            </div>
        </div>
