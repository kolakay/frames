<div v-show="configuration.service == 'gmail_api'">
    <a href="{{ route('user.settings.integrations.connect.gmail') }}" class="btn btn-danger btn-xs mb-1 default"><i class="glyph-icon simple-icon-social-google"></i> <span> CONNECT WITH GMAIL</span></a>
</div>
<div v-show="configuration.service == 'mandrill_smtp'">
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.email">
        <span>From Email</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.mandrill_username">
        <span>Mandrill Username</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.mandrill_password">
        <span>Mandrill Password</span>
    </label>
</div>

<div v-show="configuration.service == 'smtp'">
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.email">
        <span>From Email</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.smtp_host">
        <span>SMTP Host</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.smtp_port">
        <span>SMTP Port</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.smtp_username">
        <span>SMTP Username</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.smtp_password">
        <span>SMTP Password</span>
    </label>
    <label class="form-group has-top-label">
        <select class="form-control" data-width="100%" v-model="configuration.smtp_encryption">
            <option value="none">None</option>
            <option value="ssl">SSL</option>
            <option value="tls">TLS</option>
        </select>
        <span>Encryption</span>
    </label>
</div>

<div v-show="configuration.service == 'send_grid_smtp'">
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.email">
        <span>From Email</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.sendgrid_api_key">
        <span>SendGrid Api Key</span>
    </label>
</div>

<div v-show="configuration.service == 'aws_ses'">
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.email">
        <span>From Email</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.ses_key">
        <span>SES Key</span>
    </label>
    <label class="form-group has-top-label" >
        <input class="form-control" v-model="configuration.ses_secret">
        <span>SES Secret</span>
    </label>
    <label class="form-group has-top-label" >
        <select class="form-control" data-width="100%" v-model="configuration.ses_region">
            <option value="">Select</option>
            <option value="us-east-1">US East (N. Virginia)</option>
            <option value="us-east-2">US East (Ohio)</option>
            <option value="us-west-1">US West (N. California)</option>
            <option value="us-west-2">US West (Oregon)</option>
            <option value="ap-east-1">Asia Pacific (Hong Kong)</option>
            <option value="ap-south-1">Asia Pacific (Mumbai)</option>
            <option value="ap-northeast-2">Asia Pacific (Seoul)</option>
            <option value="ap-southeast-1">Asia Pacific (Singapore)</option>
            <option value="ap-southeast-2">Asia Pacific (Sydney)</option>
            <option value="ap-northeast-1">Asia Pacific (Tokyo)</option>
            <option value="ca-central-1">Canada (Central)</option>
            <option value="eu-central-1">Europe (Frankfurt)</option>
            <option value="eu-west-1">Europe (Ireland)</option>
            <option value="eu-west-2">Europe (London)</option>
            <option value="eu-west-3">Europe (Paris)</option>
            <option value="eu-north-1">Europe (Stockholm)</option>
            <option value="me-south-1">Middle East (Bahrain)</option>
            <option value="sa-east-1">South America (São Paulo)</option>
        </select>
        <span>SES Region</span>
    </label>
</div>
