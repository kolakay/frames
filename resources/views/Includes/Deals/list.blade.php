<table class="table table-bordered table-hover deal-display">
	<thead class="thead-light">
		<tr>
			<th scope="col">Title</th>
			<th scope="col">Owner</th>
			<th scope="col">Amount</th>
			<th scope="col">Stage</th>
			<th scope="col">Closed date</th>
			<th scope="col">Actions</th>
		</tr>
	</thead>
	<tbody v-if="deals.length > 0">
		<tr v-for="(value, name, index) in deals" :key="index" :value="value">
			<td>@{{ value.title }}</td>
			<td>@{{ value.owner }}</td>
			<td>@{{ value.amount }}</td>
			<td>@{{ stages[value.stage] }}</td>
			<td>@{{ value.display_closed_date }}</td>
			<td>
				<button type="button" data-toggle="modal" data-target="#dealEditModal" @click="findDeal(value.id)" class="btn btn-primary mb-1 default">
					<div class="glyph-icon simple-icon-pencil"></div>
				</button>
				<button type="button" class="btn btn-danger mb-1 default" @click="deleteDeal(value.id)">
					<div class="glyph-icon simple-icon-trash"></div>
				</button>
			</td>
		</tr>
	</tbody>
</table>
