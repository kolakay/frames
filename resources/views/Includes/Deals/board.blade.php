

<div class="row mb-4 deal-display">
	<div class="deal-container justify-content-between d-flex flex-row flex-nowrap">
		<div class="mr-2 ml-2 card">
			<div class="deal-box position-relative text-center">
				<span class="d-flex justify-content-between p-2">
					<h5 class="pl-1 pr-1 align-self-center">
						Qualified to buy
					</h5>
					<span class="dealsCount pl-1 pr-1 align-self-center">@{{ listedStages.qualified_to_buy.length }}</span>
				</span>
				<div class='add-deal m-2 p-1' @click='setStage("qualified_to_buy")' data-toggle="modal" data-target="#dealCreateModal">
					<div class="glyph-icon simple-icon-plus ml"></div>
				</div>
				<vuedraggable class="deal-content" :list="listedStages.qualified_to_buy" group="deals" @change="log($event, 'qualified_to_buy')">
					<div class="p-1 deal m-2" v-for='(value, name, index) in listedStages.qualified_to_buy' :key="index" :id="value.id">
					  	<div class="align-self-center d-flex flex-column flex-lg-row justify-content-between">
							<div class="min-width-zero text-left">
								<p class="mb-0 truncate deal-title" data-toggle="modal" data-target="#dealEditModal" @click="findDeal(value.id)">@{{ value.title }}</p>
								<p v-if="value.display_closed_date" class="mb-0">Closed Date: @{{ (value.display_closed_date).substring(0, 10) }}</p>
								<p v-if="value.amount" class="mb-0">Amount: ₦ @{{ value.amount }}</p>
							</div>
						</div>
					</div>
				</vuedraggable>
			</div>
		</div>
	
		<div class="mr-2 ml-2 card">
			<div class="position-relative text-center">
				<span class="d-flex justify-content-between p-2">
					<h5 class="pl-1 pr-1 align-self-center">Presentation scheduled</h5>
					<span class="dealsCount pl-1 pr-1 align-self-center">@{{ listedStages.presentation_scheduled.length }}</span>
				</span>
				<div class='add-deal m-2 p-1' @click='setStage("presentation_scheduled")' data-toggle="modal" data-target="#dealCreateModal" >
					<div class="glyph-icon simple-icon-plus ml"></div>
				</div>
				<vuedraggable class="deal-content" :list="listedStages.presentation_scheduled" group="deals" @change="log($event, 'presentation_scheduled')">
					<div class="p-1 deal m-2" v-for='(value, name, index) in listedStages.presentation_scheduled' :key="index" :id="value.id">
					  	<div class="align-self-center d-flex flex-column flex-lg-row justify-content-between">
							<div class="min-width-zero text-left">
								<p class="mb-0 truncate deal-title" data-toggle="modal" data-target="#dealEditModal" @click="findDeal(value.id)">@{{ value.title }}</p>
								<p v-if="value.display_closed_date" class="mb-0">Closed Date: @{{ (value.display_closed_date).substring(0, 10) }}</p>
								<p v-if="value.amount" class="mb-0">Amount: ₦ @{{ value.amount }}</p>
							</div>
						</div>
					</div>
				</vuedraggable>
			</div>
		</div>
	
		<div class="mr-2 ml-2 card">
			<div class="deal-box position-relative text-center">
				<span class="d-flex justify-content-between p-2">
					<h5 class="pl-1 pr-1 align-self-center">Appointment scheduled</h5>
					<span class="dealsCount pl-1 pr-1 align-self-center">@{{ listedStages.appointment_scheduled.length }}</span>
				</span>
				<div class='add-deal m-2 p-1' data-toggle="modal" data-target="#dealCreateModal" @click='setStage("appointment_scheduled")'>
					<div class="glyph-icon simple-icon-plus ml"></div>
				</div>
				<vuedraggable class="deal-content" :list="listedStages.appointment_scheduled" group="deals" @change="log($event, 'appointment_scheduled')">
					<div class="p-1 deal m-2" v-for='(value, name, index) in listedStages.appointment_scheduled' :key="index" :id="value.id">
					  	<div class="align-self-center d-flex flex-column flex-lg-row justify-content-between">
							<div class="min-width-zero text-left">
								<p class="mb-0 truncate deal-title" data-toggle="modal" data-target="#dealEditModal" @click="findDeal(value.id)">@{{ value.title }}</p>
								<p v-if="value.display_closed_date" class="mb-0">Closed Date: @{{ (value.display_closed_date).substring(0, 10) }}</p>
								<p v-if="value.amount" class="mb-0">Amount: ₦ @{{ value.amount }}</p>
							</div>
						</div>
					</div>
				</vuedraggable>
			</div>
		</div>
	
		<div class="mr-2 ml-2 card">
			<div class="position-relative text-center">
				<span class="d-flex justify-content-between p-2">
					<h5 class="pl-1 pr-1 align-self-center">Decision maker bought in</h5>
				<span class="dealsCount pl-1 pr-1 align-self-center">@{{ listedStages.decision_maker_bought_in.length }}</span>
			</span>
				<div class='add-deal m-2 p-1' data-toggle="modal" data-target="#dealCreateModal" @click='setStage("decision_maker_bought_in")'>
					<div class="glyph-icon simple-icon-plus ml"></div>
				</div>
				<vuedraggable class="deal-content" :list="listedStages.decision_maker_bought_in" group="deals" @change="log($event, 'decision_maker_bought_in')">
					<div class="p-1 deal m-2" v-for='(value, name, index) in listedStages.decision_maker_bought_in' :key="index" :id="value.id">
					  	<div class="align-self-center d-flex flex-column flex-lg-row justify-content-between">
							<div class="min-width-zero text-left">
								<p class="mb-0 truncate deal-title" data-toggle="modal" data-target="#dealEditModal" @click="findDeal(value.id)">@{{ value.title }}</p>
								<p v-if="value.display_closed_date" class="mb-0">Closed Date: @{{ (value.display_closed_date).substring(0, 10) }}</p>
								<p v-if="value.amount" class="mb-0">Amount: ₦ @{{ value.amount }}</p>
							</div>
						</div>
					</div>
				</vuedraggable>
			</div>
		</div>
	
		<div class="mr-2 ml-2 card">
			<div class="position-relative text-center">
				<span class="d-flex justify-content-between p-2">
					<h5 class="pl-1 pr-1 align-self-center">Contract sent</h5>
				<span class="dealsCount pl-1 pr-1 align-self-center">@{{ listedStages.contract_sent.length }}</span>
			</span>
				<div class='add-deal m-2 p-1' data-toggle="modal" data-target="#dealCreateModal" @click='setStage("contract_sent")'>
					<div class="glyph-icon simple-icon-plus ml"></div>
				</div>
				<vuedraggable class="deal-content" :list="listedStages.contract_sent" group="deals" @change="log($event, 'contract_sent')">
					<div class="p-1 deal m-2" v-for='(value, name, index) in listedStages.contract_sent' :key="index" :id="value.id">
					  	<div class="align-self-center d-flex flex-column flex-lg-row justify-content-between">
							<div class="min-width-zero text-left">
								<p class="mb-0 truncate deal-title" data-toggle="modal" data-target="#dealEditModal" @click="findDeal(value.id)">@{{ value.title }}</p>
								<p v-if="value.display_closed_date" class="mb-0">Closed Date: @{{ (value.display_closed_date).substring(0, 10) }}</p>
								<p v-if="value.amount" class="mb-0">Amount: ₦ @{{ value.amount }}</p>
							</div>
						</div>
					</div>
				</vuedraggable>
			</div>
		</div>
	
		<div class="mr-2 ml-2 card">
			<div class="position-relative text-center">
				<span class="d-flex justify-content-between p-2">
					<h5 class="pl-1 pr-1 align-self-center">Closed won</h5>
				<span class="dealsCount pl-1 pr-1 align-self-center">@{{ listedStages.closed_won.length }}</span>
			</span>
				<div class='add-deal m-2 p-1' data-toggle="modal" data-target="#dealCreateModal" @click='setStage("closed_won")'>
					<div class="glyph-icon simple-icon-plus ml"></div>
				</div>
				<vuedraggable class="deal-content" :list="listedStages.closed_won" group="deals" @change="log($event, 'closed_won')">
					<div class="p-1 deal m-2" v-for='(value, name, index) in listedStages.closed_won' :key="index" :id="value.id">
					  	<div class="align-self-center d-flex flex-column flex-lg-row justify-content-between">
							<div class="min-width-zero text-left">
								<p class="mb-0 truncate deal-title" data-toggle="modal" data-target="#dealEditModal" @click="findDeal(value.id)">@{{ value.title }}</p>
								<p v-if="value.display_closed_date" class="mb-0">Closed Date: @{{ (value.display_closed_date).substring(0, 10) }}</p>
								<p v-if="value.amount" class="mb-0">Amount: ₦ @{{ value.amount }}</p>
							</div>
						</div>
					</div>
				</vuedraggable>
			</div>
		</div>
	
		<div class="mr-2 ml-2 card">
			<div class="position-relative text-center">
				<span class="d-flex justify-content-between p-2">
					<h5 class="pl-1 pr-1 align-self-center">Closed lost</h5>
				<span class="dealsCount pl-1 pr-1 align-self-center">@{{ listedStages.closed_lost.length }}</span>
			</span>
				<div class='add-deal m-2 p-1' data-toggle="modal" data-target="#dealCreateModal" @click='setStage("closed_lost")'>
					<div class="glyph-icon simple-icon-plus ml"></div>
				</div>
				<vuedraggable class="deal-content" :list="listedStages.closed_lost" group="deals" @change="log($event, 'closed_lost')">
					<div class="p-1 deal m-2" v-for='(value, name, index) in listedStages.closed_lost' :key="index" :id="value.id">
					  	<div class="align-self-center d-flex flex-column flex-lg-row justify-content-between">
							<div class="min-width-zero text-left">
								<p class="mb-0 truncate deal-title" data-toggle="modal" data-target="#dealEditModal" @click="findDeal(value.id)">@{{ value.title }}</p>
								<p v-if="value.display_closed_date" class="mb-0">Closed Date: @{{ (value.display_closed_date).substring(0, 10) }}</p>
								<p v-if="value.amount" class="mb-0">Amount: ₦ @{{ value.amount }}</p>
							</div>
						</div>
					</div>
				</vuedraggable>							
			</div>
		</div>
	</div>
</div>