<div class="modal fade" id="share" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Share and Send</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs separator-tabs ml-0 mb-5" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="for-email-tab"
                            data-toggle="tab" href="#for-email"
                            role="tab" aria-controls="for-email" aria-selected="false">Copy for Email</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="direct-email-tab"
                            data-toggle="tab" href="#direct-email"
                            role="tab" aria-controls="direct-email" aria-selected="true">Direct Email</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="social-tab"
                            data-toggle="tab" href="#social"
                            role="tab" aria-controls="social" aria-selected="false">Social</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="embed-tab"
                            data-toggle="tab" href="#embed"
                            role="tab" aria-controls="embed" aria-selected="false">Embed</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="widget-tab"
                            data-toggle="tab" href="#widget"
                            role="tab" aria-controls="widget" aria-selected="true">Widget</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="for-email" role="tabpanel" aria-labelledby="for-email-tab">
                        @include('Includes.Video.Share.for-email')
                    </div>
                    <div class="tab-pane fade" id="direct-email" role="tabpanel" aria-labelledby="direct-email-tab">
                        @include('Includes.Video.Share.direct-email')
                    </div>
                    <div class="tab-pane fade" id="social" role="tabpanel" aria-labelledby="social-tab">
                        @include('Includes.Video.Share.social')
                    </div>

                    <div class="tab-pane fade" id="embed" role="tabpanel" aria-labelledby="embed-tab">
                        @include('Includes.Video.Share.embed')
                    </div>
                    <div class="tab-pane fade" id="widget" role="tabpanel" aria-labelledby="widget-tab">
                        @include('Includes.Video.Share.widget')
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" @click="storeVideoShare">Save changes</button>
            </div>
        </div>
    </div>
</div>
