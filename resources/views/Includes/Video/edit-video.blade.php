<div class="col-12 mx-auto">
    <div class="row">
        <div class="col-8">
            <video height="550" controls
                id="video-player"
                preload="auto"
                poster=""
                data-setup='{}'
                class="video-js vjs-big-play-centered"
                style="object-fit: fill; width:100%">
                {{-- <source :src="" type="video/mp4"> --}}
                <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a
                    web browser that
                    <a href="https://videojs.com/html5-video-support/" target="_blank">
                        supports HTML5 video
                    </a>
                    </p>
            </video>
        </div>
        <div class="col-4">
            
            <div class="col-12">
                <div class="row social-image-row gallery" style="max-height: 500px; overflow-y:scroll">
                    <div class="col-6 p-2 video-container" v-for="video in videos"
                        @click="selectVideo(video.id)"
                        :class="{ 'active-video-container': video.id == videoEditOption.selectedEditVideo }">
                        <img class="img-fluid" :src="video.video_thumbnail_path" /></a>
                        <p>@{{ video.name}}</p>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4">
                <select name="" id="" class="form-control mb-2" v-model="videoEditOption.position">
                    <option value="intro">Add Intro</option>
                    <option value="outro">Add Outro</option>
                </select>
                <button type="button" class="btn btn-outline-dark mb-2 straight-btn mr-2" @click="editVideo">Update Video</button>
            </div>
        </div>
    </div>
    

    <div class="form-group row mb-0">
        <div class=" mt-4 pl-2">
            <h2 class="d-inline">Trimming</h2>
            <span class="text-muted text-small d-block">
                Set new start/stop time
                <i class="simple-icon-info icon-title text-info"
                data-toggle="tooltip" data-placement="top" title=""
                data-original-title="Quickly trim the ends of your video."></i>
            </span>
            <div class="col-12 mt-4">
                <button type="button" class="btn btn-outline-dark mb-2 straight-btn mr-2" @click="setTrim('start')">Set current frame as start</button>
                <button type="button" class="btn btn-outline-dark mb-2  straight-btn" @click="setTrim('end')">Set current frame as end</button>
            </div>
            <div class="col-12 mt-2">
                <div class="row pl-3">
                    <input type="text" class="form-control col-4 mr-2" v-model="video_edit.start_trim" placeholder="Start Trim">
                    <input type="text" class="form-control col-4" v-model="video_edit.end_trim" placeholder="End Trim">
                    <div class="col-12 mt-2 pl-0">
                        <button type="button" class="btn btn-outline-success mb-2  straight-btn mx-auto" @click="trimVideo">Apply Changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class=" mt-4 pl-2">
            <h2 class="d-inline">Thumbnail</h2>
            <span class="text-muted text-small d-block">
                Customize thumbnail graphic
                <i class="simple-icon-info icon-title text-info"
                    data-toggle="tooltip" data-placement="top"
                    title=""
                    data-original-title="This thumbnail appears on the dashboard and as a preview on social media.
                        When sending in emails we automatically generate a GIF preview."></i>
            </span>
            <div class="col-12 mt-4" v-if="video.thumbnail == null || video.thumbnail == ''">
                <button type="button" class="btn btn-outline-dark mb-2 straight-btn mr-2" @click="uploadImage('thumbnail')">Upload</button>
                <input id='thumbnail' type='file' @change="readThumbnailImage" hidden/>
                <button type="button" class="btn btn-outline-dark mb-2  straight-btn" @click="setThumbnailTimeFrame">Use Current Frame</button>
            </div>
            <div class="col-12 mt-2">
                <div class="row pl-3" v-if="video.thumbnail == null || video.thumbnail == ''">
                    <input type="text" class="form-control col-4 mr-2" v-model="video_edit.thumbnail_nail" placeholder="Select Time For Thumbnail">
                    <div class="col-6">
                        <button type="button" class="btn btn-outline-success mb-2  straight-btn mx-auto" @click="getVideoThumbnail">Set Thumbnail</button>
                    </div>
                </div>
                <div v-if="video.thumbnail != null && video.thumbnail != ''">
                    <div class="d-flex flex-row mb-3">
                        <a class="d-block position-relative">
                            <img :src="video.video_thumbnail_path" class="list-thumbnail border-0">
                        </a>
                        <div class="pl-3 pt-2 pr-2 pb-2">
                            <button type="button"
                                @click="changeThumbnailImage"
                                class="btn btn-danger btn-sm default">
                                    <i class="simple-icon-trash"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
