<div class="details">
    <div class="form-group row">
        <label for="inputEmail3" class="col-3 col-form-label">Description</label>
        <div class="col-8">
            <label class="form-group has-top-label">
                <textarea class="form-control" v-on:blur="updateVideoDetails"
                     cols="20" rows="3" v-model="video_details.description"></textarea>
                <span>Add text under your video
                    <i class="simple-icon-info icon-title text-info"
                data-toggle="tooltip"
                title=""
                data-original-title="Appears on video page below the video, above the Calls to Action."></i>
                </span>
            </label>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-3 col-form-label">Tags</label>
        <div class="col-8">
            @php
                $videoDetailIsSet = ($video->videoDetails)? true : false
            @endphp
            <label class="form-group has-top-label">
                <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="video_tags" @change="updateVideoDetails">
                    @foreach($tags as $tag)

                        <option value="{{ $tag->id}}"
                            {{ ($videoDetailIsSet)? in_array($tag->id,$video->videoDetails->tags)? 'selected': '': '' }}>{{ $tag->name }}</option>
                    @endforeach
                </select>
                <span>Add tags for internal organization
                     <i class="simple-icon-info icon-title text-info"
                    data-toggle="tooltip"
                    title=""
                    data-original-title="Organize your videos with tags to enable videos to be filtered in the Dubb dashboard."></i>
                    </span>
            </label>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-3 col-form-label">Calls To Action</label>
        <div class="col-8">

            <label class="form-group has-top-label">
                <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="video_ctas" @change="updateVideoDetails">
                    @foreach($ctas as $cta)
                        <option value="{{ $cta->id}}"
                            {{ ($videoDetailIsSet)? in_array($cta->id,$video->videoDetails->ctas)? 'selected': '': '' }}>{{ $cta->name }}</option>
                    @endforeach
                </select>
                <span>Add links and more
                    <i class="simple-icon-info icon-title text-info"
                    data-toggle="tooltip"
                    title=""
                    data-original-title="Make your videos actionable! Achieve your goals with customizable buttons, calendars, forms and more. Calls to Action appear on the video page under your video."></i>
                </span>
            </label>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-3 col-form-label">Playlist</label>
        <div class="col-8">
            <label class="form-group has-top-label">
                <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="video_playlist" @change="updateVideoDetails">
                    @foreach($videos as $userVideo)
                        @if($userVideo->id != $video->id)
                        <option value="{{ $userVideo->id}}"
                            {{ ($videoDetailIsSet)? in_array($userVideo->id,$video->videoDetails->playlist)? 'selected': '' : '' }}>{{ $userVideo->name }}</option>
                        @endif
                    @endforeach
                </select>
                <span>Build a list of videos to play
                    <i class="simple-icon-info icon-title text-info"
                    data-toggle="tooltip"
                    title=""
                    data-original-title="Engage viewers with multiple videos! Videos added here will be featured on the video page and will play after the main video. Tip: Upload an outro video that you can use on several videos."></i>
                </span>
            </label>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-3 col-form-label">Transcriptions</label>
        <div class="col-8">
            <div v-if="transcriptionDetails.status == 'pending'">
                <button class="btn btn-secondary default btn-sm " disabled>
                    <i class="fa fa-circle-o-notch fa-spin"></i> Processing transcription request
                </button>
            </div>
            <div v-if="transcriptionDetails.status != 'pending'">
                <p><span>Add text captions to your video to increase comprehension rates. The cost for auto transcriptions is $.10 per minute.</span></p>
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <button type="button" v-if="!video.vtt_file || video.vtt_file.length < 1" class="btn btn-secondary default" @click="autoTranscribe()">Auto Transcription</button>
                    <a :href="url.srt_download + video.uuid" v-if="video.vtt_file && video.vtt_file.length > 0" class="btn btn-secondary default">Download Caption</a>

                    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a class="dropdown-item" href="#" @click="uploadSrt">Upload SRT File</a>
                        <input id='srtUpload' type='file' @change="readSRTFile()" hidden/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-3 col-form-label">Options</label>
        <div class="col-8">
            <div class="row pl-3">
                <div class="custom-control custom-checkbox mb-4 col-6">
                    <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheckThis" v-model="video_details.auto_play">
                    <label class="custom-control-label" for="customCheckThis">Autoplay Video</label>
                </div>

                <div class="custom-control custom-checkbox mb-4 col-6">
                    <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheckSeven" v-model="video_details.add_exit_intent_modal">
                    <label class="custom-control-label" for="customCheckSeven">Add exit intent modal </label>
                </div>
            </div>
            <div class="row pl-3">
                <div class="custom-control custom-checkbox mb-4 col-6">
                    <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheckSix" v-model="video_details.add_emoji_response_buttons">
                    <label class="custom-control-label" for="customCheckSix">Add emoji response buttons </label>
                </div>

                <div class="custom-control custom-checkbox mb-4 col-6">
                    <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheckOne" v-model="video_details.add_facebook_comment_widget">
                    <label class="custom-control-label" for="customCheckOne">Add Facebook comment widget</label>
                </div>
            </div>
            <div class="row pl-3">
                <div class="custom-control custom-checkbox mb-4 col-6">
                    <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheckTwo" v-model="video_details.add_messaging">
                    <label class="custom-control-label" for="customCheckTwo">Add Chat </label>
                </div>

                <div class="custom-control custom-checkbox mb-4 col-6">
                    <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheckThree" v-model="video_details.share_with_team">
                    <label class="custom-control-label" for="customCheckThree">Share with team and allow collaboration </label>
                </div>
            </div>

            <div class="row pl-3">
                <div class="custom-control custom-checkbox mb-4 col-6">
                    <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheckFour" v-model="video_details.disable_tracking">
                    <label class="custom-control-label" for="customCheckFour">Disable tracking</label>
                </div>

                <div class="custom-control custom-checkbox mb-4 col-6">
                    <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheckFive" v-model="video_details.allow_video_download">
                    <label class="custom-control-label" for="customCheckFive">Allow video download </label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-3 col-form-label">Schedule Auto Delete ?</label>
        <div class="form-group custom-control custom-checkbox mb-4 col-6 mb-3 ml-3">
            <input type="checkbox" class="custom-control-input" @change="updateVideoDetails" id="customCheck22" v-model="video_details.auto_delete">
            <label class="custom-control-label" for="customCheck22"> Yes</label>

        </div>
    </div>
    <div class="form-group row" v-show="video_details.auto_delete">
        <label for="inputEmail3" class="col-3 col-form-label">Schedule Date</label>
        <div class="form-group col-6 mb-3">
            <input class="form-control delete_datepicker"
            data-date-format="yyyy-mm-dd"
                id="autoDeleteDate" placeholder="Date">
        </div>
    </div>
</div>
