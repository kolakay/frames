<div class="card mb-4">
    <div class="card-body">
        <ul class="nav nav-pills justify-content-center">
            <li class="nav-item">
                <a class="nav-link active btn-straight" id="player-tab"
                data-toggle="tab" href="#player"
                style="border-radius:0px;"
                role="tab" aria-controls="player" aria-selected="false">Design Player</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-straight" id="cta-tab"
                data-toggle="tab" href="#cta"
                style="border-radius:0px;"
                role="tab" aria-controls="cta" aria-selected="false">Design CTA</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active show" id="player" role="tabpanel" aria-labelledby="fifth-tab">
                <div class="form-group">
                    <h2 class="">Company Logo</h2>
                    <div class="custom-control custom-checkbox mb-4">
                        <input type="checkbox" class="custom-control-input"
                        @change="updateVideoDesign(null, 'player')"
                        id="customCheckVideoPlayerLogo" v-model="video_design.display_logo">
                        <label class="custom-control-label" for="customCheckVideoPlayerLogo">Display Company Logo on Video Player</label>
                    </div>
                </div>
                <div class="form-group row">
                    <h2 class="col-12">Player Color</h2>
                    <label for="inputEmail3" class="col-2 col-form-label">Set Player Color</label>
                    <div class="col-9">
                        <div id="player-color" class="input-group" title="Using input value">
                            <input type="text" class="form-control input-lg"
                            value="{{ ($video->videoDesign)? $video->videoDesign->player_color: ''  }}"
                                id="vplayer_color"/>
                            <span class="input-group-append">
                            <span class="input-group-text colorpicker-input-addon"><i></i></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        <div class="custom-control custom-checkbox mb-4">
                            <input type="checkbox" class="custom-control-input"
                            @change="updateVideoDesign(null, 'player')"
                                id="customCheckVideoPlayerDefault" v-model="video_design.default_player_color">
                            <label class="custom-control-label" for="customCheckVideoPlayerDefault">Set as default player color for all videos</label>
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <h2 class="d-inline">Player Background Image</h2>
                    <span class="text-muted text-small d-block mb-4">
                        Add a custom background image to your video page.
                    </span>
                    <div v-if="video_design.player_background_image != null && video_design.player_background_image != ''">
                        <div class="d-flex flex-row mb-3">
                            <a class="d-block position-relative">
                                <img :src="video_design.player_background_image_src" class="list-thumbnail border-0">
                            </a>
                            <div class="pl-3 pt-2 pr-2 pb-2">
                                <button type="button"
                                    @click="changeImage('player')"
                                    class="btn btn-danger btn-sm default">
                                        <i class="simple-icon-trash"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div v-if="video_design.player_background_image == null || video_design.player_background_image == ''">

                        <div class="mb-4 col-4 row">
                            <div class="custom-control custom-radio col-6">
                                <input type="radio" id="upload"
                                    v-model="playerUploadType"
                                    value="upload"  name="upload" class="custom-control-input">
                                <label class="custom-control-label" for="upload">Upload</label>
                            </div>
                            <div class="custom-control custom-radio col-6">
                                <input type="radio" id="search"
                                    v-model="playerUploadType"
                                    value="search"  name="search" class="custom-control-input">
                                <label class="custom-control-label" for="search">Search</label>
                            </div>

                        </div>

                        <div class="form-group" v-if="playerUploadType == 'search'">
                            <div class="input-group">
                                <input type="text" class="form-control"
                                    v-model="playerBGQuery"
                                    v-on:keyup.enter="getPixabayImagesForPlayer"
                                    placeholder="Type Query And Press Enter To Search">
                                <div class="input-group-append mb-2">
                                    <button class="btn btn-outline-danger" type="button" @click="clearBackgroundSearch('player')">Clear</button>
                                    <button class="btn btn-outline-secondary dropdown-toggle straight-btn"
                                        style="text-transform: capitalize;" type="button"
                                        data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">@{{ playerBGQueryType }}</button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1198px, 38px, 0px);">
                                        <a class="dropdown-item clickable_link" @click="setPlayerBGQuery('all')">All</a>
                                        <a class="dropdown-item clickable_link" @click="setPlayerBGQuery('photo')">Photos</a>
                                        <a class="dropdown-item clickable_link" @click="setPlayerBGQuery('illustration')">Illustrations</a>
                                    </div>
                                </div>
                                <div v-show="pixabayPlayerImages.length > 0"
                                    class="row social-image-row gallery col-12" style="height:500px; overflow-y:scroll;">
                                    <div class="col-2" v-for="(image, index) in pixabayPlayerImages">
                                        <a @click="setImageAsPlayerBackground(index)" :class="{imageActive:index == currentImageIndex, clickable_link:true}"><img class="img-fluid" :src="image.previewURL"></a>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="button" class="btn btn-outline-dark default" @click="loadMorePlayerBackgroundImages">Load More</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" v-if="playerUploadType == 'upload'">
                            <button type="button" class="btn btn-outline-dark mb-2  straight-btn" @click="uploadImage('player')">Upload Image</button>
                            <input id='player-background' type='file' @change="readImage('player')" hidden/>
                            <span class="text-muted text-small d-block mb-4">
                                Image must be 1280x720.
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox mb-4">
                            <input type="checkbox" class="custom-control-input"
                            @change="updateVideoDesign(null, 'player')"
                                id="customCheckOut" v-model="video_design.default_player_background_image">
                            <label class="custom-control-label" for="customCheckOut">Set as default for all videos</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="cta" role="tabpanel" aria-labelledby="sixth-tab">
                <div class="form-group row">
                    <h2 class="col-12">CTA Color</h2>
                    <label for="head" class="col-2 col-form-label">Set CTA Color</label>
                    <div class="col-9">
                        <div id="cta-color" class="input-group" title="Using input value">
                            <input type="text" class="form-control input-lg"
                                value="{{ ($video->videoDesign)? $video->videoDesign->cta_color: ''  }}" id="vcta_color"/>
                            <span class="input-group-append">
                            <span class="input-group-text colorpicker-input-addon"><i></i></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        <div class="custom-control custom-checkbox mb-4">
                            <input type="checkbox" class="custom-control-input"
                                @change="updateVideoDesign(null, 'cta')"
                                id="customCheckCTAColor"  v-model="video_design.default_cta_color">
                            <label class="custom-control-label" for="customCheckCTAColor">Set as default CTA color for all videos</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <h2 class="d-inline"> Background Image</h2>
                    <span class="text-muted text-small d-block mb-4">
                        Add a custom background image to your video page.
                    </span>
                    <div v-if="video_design.cta_background_image != null && video_design.cta_background_image != ''">
                        <div class="d-flex flex-row mb-3">
                            <a class="d-block position-relative">
                                <img :src="video_design.cta_background_image_src" class="list-thumbnail border-0">
                            </a>
                            <div class="pl-3 pt-2 pr-2 pb-2">
                                <button type="button"
                                    @click="changeImage('cta')"
                                    class="btn btn-danger btn-sm default">
                                        <i class="simple-icon-trash"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div v-if="video_design.cta_background_image == null || video_design.cta_background_image == ''">
                        <div class="mb-4 col-4 row">
                            <div class="custom-control custom-radio col-6">
                                <input type="radio" id="customRadio3"
                                    v-model="CTAUploadType"
                                    value="upload" name="customRadio3" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio3">Upload</label>
                            </div>
                            <div class="custom-control custom-radio col-6">
                                <input type="radio" id="customRadio4"
                                    v-model="CTAUploadType"
                                    value="search" name="customRadio4" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio4">Search</label>
                            </div>
                        </div>

                        <div class="form-group" v-if="CTAUploadType == 'search'">
                            <div class="input-group">
                                <input type="text" class="form-control"
                                v-model="CTABGQuery"
                                v-on:keyup.enter="getPixabayImagesForCTA"
                                    placeholder="Type Query And Press Enter To Search">
                                <div class="input-group-append mb-2">
                                    <button class="btn btn-outline-danger" type="button" @click="clearBackgroundSearch('cta')">Clear</button>
                                    <button class="btn btn-outline-secondary dropdown-toggle straight-btn"
                                        type="button" data-toggle="dropdown"
                                        style="text-transform: capitalize;"
                                        aria-haspopup="true" aria-expanded="false">@{{ CTABGQueryType }}</button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1198px, 38px, 0px);">
                                        <a class="dropdown-item clickable_link" @click="setCTABGQuery('all')">All</a>
                                        <a class="dropdown-item clickable_link" @click="setCTABGQuery('photo')">Photos</a>
                                        <a class="dropdown-item clickable_link" @click="setCTABGQuery('illustration')">Illustrations</a>
                                    </div>
                                </div>
                            </div>
                            <div v-show="pixabayCTAImages.length > 0"
                                class="row social-image-row gallery col-12" style="height:500px; overflow-y:scroll;">
                                <div class="col-2" v-for="(image, index) in pixabayCTAImages">
                                    <a @click="setImageAsCTABackground(index)" :class="{imageActive:index == currentCTAImageIndex, clickable_link:true}"><img class="img-fluid" :src="image.previewURL"></a>
                                </div>
                                <div class="col-12 text-center">
                                    <button type="button" class="btn btn-outline-dark default" @click="loadMoreCTABackgroundImages">Load More</button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" v-if="CTAUploadType == 'upload'">
                            <button type="button" class="btn btn-outline-dark mb-2  straight-btn" @click="uploadImage('cta')">Upload Image</button>
                            <input id='cta-background' type='file' @change="readImage('cta')" hidden/>
                            <span class="text-muted text-small d-block mb-4">
                                Image must be 1280x720.
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox mb-4">
                            <input type="checkbox" class="custom-control-input"
                            @change="updateVideoDesign(null, 'cta')"

                            id="customCheckCTAVideoDefault" v-model="video_design.default_cta_background_image">
                            <label class="custom-control-label" for="customCheckCTAVideoDefault">Set as default for all videos</label>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
