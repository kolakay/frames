<div class="row">

    <div class="col-md-6 col-sm-12 mb-4">
        <div class="card dashboard-filled-line-chart">
            <div class="card-body">
                <div class="float-left float-none-xs">
                    <div class="d-inline-block">
                        <h5 class="d-inline">General Stats</h5>
                    </div>
                </div>
                <div class="btn-group float-right float-none-xs mt-2">
                    <button class="btn btn-outline-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@{{ generalStatsTime.name }}</button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="javascript;;" v-for="(time, index) in times" @click="selectGeneralStatsTime($event, index)">@{{time.name}}</a>
                    </div>
                </div>
            </div>
            <div class=" card-body pt-0">
                <div class="row text-center">
                    <div class="col-4">
                        <div role="progressbar" id="emailClicks" class="general-progress-bar position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="100" aria-valuenow="0" data-show-percent="true"></div>
                        <h6 class="mb-0 mt-2">Email Click Rates</h6>
                    </div>
                    <div class="col-4">
                        <div role="progressbar" id="ctaClicks" class="general-progress-bar position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="100" aria-valuenow="0" data-show-percent="true"></div>
                        <h6 class="mb-0 mt-2">CTA Click Rates</h6>
                    </div>
                    <div class="col-4">
                        <div role="progressbar" id="watchRates" class="general-progress-bar position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="100" aria-valuenow="0" data-show-percent="true"></div>
                        <h6 class="mb-0 mt-2">Watch Rates</h6>
                    </div>


                    <div class="col-4 mt-2 pt-4">
                        <div role="progressbar" id="reactionClicks" class="general-progress-bar position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="100" aria-valuenow="0" data-show-percent="true"></div>
                        <h6 class="mb-0 mt-2">Reaction Rates</h6>
                    </div>
                    <div class="col-4 mt-2 pt-4">
                        <div role="progressbar" id="callRates" class="general-progress-bar position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="100" aria-valuenow="0" data-show-percent="true"></div>
                        <h6 class="mb-0 mt-2">Call Rates</h6>
                    </div>
                    <div class="col-4 mt-2 pt-4">
                        <div role="progressbar" id="emailOpens" class="general-progress-bar position-relative" data-color="#922c88" data-trailcolor="#d7d7d7" aria-valuemax="100" aria-valuenow="0" data-show-percent="true"></div>
                        <h6 class="mb-0 mt-2">Email Rates</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 mb-4">
        <div class="card dashboard-filled-line-chart">
            <div class="card-body">
                <div class="float-left float-none-xs">
                    <div class="d-inline-block">
                        <h5 class="d-inline">Video Views</h5>
                    </div>
                </div>
                <div class="btn-group float-right mt-2 float-none-xs">
                    <button class="btn btn-outline-secondary btn-xs dropdown-toggle"
                        type="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">@{{ videoViewTime.name }}</button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="javascript;;" v-for="(time, index) in times" @click="selectVideoTime($event, index)">@{{time.name}}</a>
                    </div>
                </div>
            </div>
            <div class="chart card-body pt-0">
                <canvas id="AnalyticsChat"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-10 mx-auto mb-4">
        <div class="card">
            <div class="card-body">
                <div class="float-left float-none-xs">
                    <div class="d-inline-block">
                        <h5 class="d-inline">Activity</h5>
                    </div>
                </div>
                <div class="btn-group float-right float-none-xs mt-2">
                    <button class="btn btn-outline-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Filter By</button>
                    <div class="dropdown-menu">
                        <div v-for="(activityType, index) in activityTypes" >
                            <a class="dropdown-item" href="javascript;;" @click="selectActivityType($event, index)">@{{activityType.name}} <i v-if="activityFilterArray.includes(activityType.id)" class="simple-icon-heart ml-4"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                {{-- <h5 class="card-title">Activity</h5> --}}
                <div v-for="log in activityLogs" v-if="activityFilterArray.length < 1 || activityFilterArray.includes(log.type)" class="d-flex flex-row mb-3 pb-3 border-bottom justify-content-between align-items-center">

                    <div class=" flex-fill">
                        <a >
                            <p class="font-weight-medium mb-0">@{{log.parsed_type }}</p>
                        </a>
                    </div>
                    <a  class="pl-3">
                        <p class="text-muted mb-0 text-small">@{{log.parsed_created_at }}</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
