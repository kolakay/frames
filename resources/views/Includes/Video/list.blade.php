<div class="mb-2">
    <div class="collapse dont-collapse-sm" id="displayOptions">

        <div class="d-block d-md-inline-block float-md-left">

            <div class="btn-group float-md-left mr-1 mb-1 mr-2">
                <p class="caption">My Videos</p>
            </div>

            <div class="search-sm d-inline-block float-md-left mr-1 mb-1 align-top br-0">
                <input class="form-control br-0" type="text"  v-model="filter" placeholder="Search">
            </div>
        </div>
        <span class="mr-3 mb-2 d-inline-block float-md-right">
            <div class="btn-group float-md-left mr-2 mb-1">
                <select class="form-control" style="height:26px">
                    <option value="all">All Videos</option>
                </select>
            </div>
            <div class="btn-group float-md-left mr-2 mb-1">
                <select name="" id="" class="form-control" v-model="filterTag" style="height:26px">
                    <option value="all">All Tags</option>
                    <option :value="tag.id" v-for="tag in tags">@{{ tag.name }}</option>
                </select>

                {{-- <button class="btn btn-outline-dark btn-xs dropdown-toggle default"
                    type="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">Tag</button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">All Tags</a>
                <a class="dropdown-item" href="#" v-for="tag in tags">@{{ tag.name }}</a>
                </div> --}}
            </div>
            <a href="{{ route('user.videos.upload') }}" class="mr-2 view-icon menu-btn" id="iconMenuButton">
                <i class="simple-icon-plus"></i>
            </a>
            <a href="#" class="mr-2 view-icon active menu-btn" v-show="videoView == 'list'">
                <i class="simple-icon-list"></i>
            </a>
            <a href="#" class="mr-2 view-icon active menu-btn" v-show="videoView == 'grid'">
                <i class="simple-icon-grid"></i>
            </a>
        </span>

    </div>
</div>
<div class="separator mb-5"></div>
<div class="col-12">
    <div class="row">
        {{-- @foreach($videos as $video) --}}
        <div class="col-md-4 d-flex" v-for="(selectedVideo, index) in getVideos">
            <div class="card br-0">
                <img :src="selectedVideo.video_thumbnail_path" alt="Detail Picture" class="card-img-top br-0">

                <div class="card-body">
                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-10 pl-0">
                                <h6 class="card-title mb-1" style="font-size:12px">@{{ selectedVideo.name }}</h6>
                            </div>
                            <div class="col-2">
                                <div class="btn-group float-right task-list-action">
                                    <div class="dropdown">
                                        <a href="#" class="btn btn-transparent default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="simple-icon-list"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-150px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item" :href="url.edit + selectedVideo.uuid"> <i class="icon-user text-info pr-2"></i> Info</a>
                                            <a class="dropdown-item" target="_blank" :href="url.view + selectedVideo.uuid"><i class="icon-close text-danger pr-2"></i> View</a>
                                            <a class="dropdown-item" :href="url.edit + selectedVideo.uuid"><i class="icon-note text-warning pr-2"></i> Edit</a>

                                            <a class="dropdown-item" :href="url.replace + selectedVideo.uuid"> <i class="icon-user text-info pr-2"></i> Replace Video</a>
                                            <a class="dropdown-item" href="javascript:;" @click="copyVideoURL(selectedVideo, true)"><i class="icon-close text-danger pr-2"></i> Copy Link + Thumbnail</a>
                                            <a class="dropdown-item" href="javascript:;" @click="copyVideoURL(selectedVideo)"><i class="icon-note text-warning pr-2"></i> Copy URL</a>

                                            <a class="dropdown-item" href="#"><i class="icon-close text-danger pr-2"></i> Clear Reporting Data</a>
                                            <a class="dropdown-item" href="javascript:;" @click="deleteVideo(index)"><i class="icon-note text-warning pr-2"></i> Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb p-0">

                        <li class="breadcrumb-item">
                            @{{ selectedVideo.email_opens}}
                            <i class="simple-icon-envelope-open"
                            data-toggle="tooltip"
                            data-original-title="Email Opens"></i>
                        </li>
                        <li class="breadcrumb-item">
                            @{{ selectedVideo.email_clicks}}
                            <i class="simple-icon-mouse"
                            data-toggle="tooltip"
                            data-original-title="Email Clicks"></i>
                        </li>
                        <li class="breadcrumb-item">
                            @{{ selectedVideo.page_views}}
                            <i class="simple-icon-eye"
                                data-toggle="tooltip"
                                data-original-title="Page Views"></i>
                        </li>
                        <li class="breadcrumb-item">
                            @{{ selectedVideo.video_views}}
                            <i class="simple-icon-camrecorder"
                                data-toggle="tooltip"
                                data-original-title="Video Views"></i>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            @{{ selectedVideo.cta_clicks}}
                            <i class="simple-icon-speech"
                                data-toggle="tooltip"
                                data-original-title="CTA Clicks"></i>
                        </li>
                    </ol>
                </nav>
                </div>

                <div class="card-footer">
                    <small class="text-muted">Last updated @{{ selectedVideo.last_updated }}</small>
                </div>
                </div>
        </div>
        {{-- @endforeach --}}
    </div>
</div>
