<p>Email this video with an engaging GIF preview link.
    Copy the trackable thumbnail GIF preview
    link to use in any third-party email or automation system.</p>
<div class="form-group">

    <label class="form-group has-top-label">
        <select class="form-control" id="" v-model="video_share.contact_id">
            <option value="">Select saved contact</option>
            <option v-for="contact in contacts" :value="contact.email">@{{ contact.full_name_display  }}</option>
        </select>
        <span>Add Email For tracking
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="Enter a users email or name (if saved in your contacts)
             to get user-level reporting. Click to learn more."></i>
        </span>
    </label>

</div>
<div class="form-group">
    <label class="form-group has-top-label">
        <input class="form-control" type="text" v-model="video_share.personalization_text">
        <span>Enter personalization text
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="Enter personalized text that can be seen
                as overlay text in an email and landing page.
                This increases email open/click rates and watch rates.
                Click to learn more."></i>
        </span>
    </label>
</div>
<div class="form-group">
    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
        <button type="button" class="btn btn-secondary straight-btn" @click="copyForEmail">Copy For Email</button>
        <button id="btnGroupDrop1" type="button" class="straight-btn btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
            <a  :href="'mailto:?subject=video&body='+ video.thumbnail_video_url" class="dropdown-item">
                <i  class=""></i> Email
            </a>
            <a  href="#" class="dropdown-item" @click="copyGifForEmail">
                <i  class=""></i> Copy Animated GIF &amp; Link
            </a>
            <a  href="#" class="dropdown-item" @click="copyThumbnailForEmail">
                <i  class=""></i> Copy Still Thumbnail &amp; Link
            </a>
            <a  href="#" class="dropdown-item" @click="copyThumbnail">
                <i  class=""></i> Copy Still Thumbnail
            </a>
            <a  href="#" class="dropdown-item" @click="copyGif">
                <i  class=""></i> Copy Animated GIF
            </a>
            <a  href="{{ route('user.videos.download.gif', $video->uuid) }}" class="dropdown-item">
                <i  class=""></i> Download GIF
            </a>
            <a  href="#" class="dropdown-item" @click="copyHtml">
                <i  class=""></i> Copy Html Code
            </a>
            <a  href="#" class="dropdown-item" @click="copyURL">
                <i  class=""></i> Copy URL
            </a>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="custom-control custom-checkbox mb-4">
        <input type="checkbox" class="custom-control-input" id="customDisableGif" v-model="video_share.disable_gif_animation">
        <label class="custom-control-label" for="customDisableGif"> Disable Gif Animation</label>
        <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This feature is intended for Outlook users where animated GIFs do not play and require the clicking of an additional play button."></i>
    </div>

    <div class="custom-control custom-checkbox mb-4">
        <input type="checkbox" class="custom-control-input" id="customDisableTracking" v-model="video_share.disable_email_open_tracking">
        <label class="custom-control-label" for="customDisableTracking"> Disable email open tracking pixel</label>
        <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This disables the email open tracking pixel which in some cases results in an email being blocked from an email network."></i>
    </div>

    <div class="custom-control custom-checkbox mb-4">
        <input type="checkbox" class="custom-control-input" id="customDisableURL" v-model="video_share.disable_url_text_encoding">
        <label class="custom-control-label" for="customDisableURL">Disable URL and personalization text encoding</label>
        <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This option disables HTML encoding of special characters to allow copying and pasting in various email software systems. With this setting, spaces are also encoded. Make sure to test links before sending."></i>
    </div>
</div>
