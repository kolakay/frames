<p class="text-center">Share your video page link on social media.</p>
<div class="col-10 mx-auto">
    <div class="row mind-icons mb-5">
        <div class="glyph col-4">
            <a :href="'https://www.facebook.com/sharer/sharer.php?u=' + url.view" target="_blank"><i class="glyph-icon iconsminds-facebook"></i></a>

        </div>
        <div class="glyph col-4">
            <a :href="'https://twitter.com/intent/tweet?text=' + url.view" target="_blank">
                <i class="glyph-icon iconsminds-twitter"></i>
            </a>
        </div>
        <div class="glyph col-4">
            <a :href="'https://www.linkedin.com/sharing/share-offsite/?url='+ url.view" target="_blank">
                <i class="glyph-icon iconsminds-linkedin-2"></i>
            </a>
        </div>
    </div>
</div>
