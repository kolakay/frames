<div class="form-group">
    <label class="form-group has-top-label" >
        <select class="form-control select2-single" data-width="100%" v-model="video_share.widget_position" id="widget-position">
            <option value="rightBottom">Right Bottom</option>
            <option value="leftBottom">Left Bottom</option>
            <option value="rightTop">Right Top</option>
            <option value="leftTop">Left Top</option>
        </select>
        <span>Position</span>
    </label>
</div>
<div class="form-group">

    <div class="custom-control custom-checkbox mb-4 col-12">
        <input type="checkbox" class="custom-control-input" id="widgetAutoplay" @change="updateWidget" v-model="video_share.widget_autoplay">
        <label class="custom-control-label" for="widgetAutoplay">Autoplay</label>
    </div>
    <div class="custom-control custom-checkbox mb-4 col-12" v-if="video_share.widget_autoplay">
        <input type="checkbox" class="custom-control-input" id="widgetSoundOff" @change="updateWidget" v-model="video_share.widget_sound_off">
        <label class="custom-control-label" for="widgetSoundOff"> Turn sound off on Autoplay</label>
    </div>
    <div class="custom-control custom-checkbox mb-4 col-12">
        <input type="checkbox" class="custom-control-input" id="widgetCTAButtons" @change="updateWidget" v-model="video_share.widget_show_cta">
        <label class="custom-control-label" for="widgetCTAButtons"> Show CTA Buttons</label>
    </div>
    <div class="custom-control custom-checkbox mb-4 col-12">
        <input type="checkbox" class="custom-control-input" id="widgetOverlays" @change="updateWidget" v-model="video_share.widget_hide_in_mobile">
        <label class="custom-control-label" for="widgetOverlays"> Hide in Mobile</label>
    </div>
</div>
<div class="form-group">
    <textarea readonly="readonly" rows="5" class="form-control" v-model="widget_value" @click="copyWidget">
        <div id="fd-widget" style="display: none;" :data-position="video_share.widget_position" :data-mobile="video_share.widget_hide_in_mobile">
            <iframe
                id="fd-widget-iframe"
                url="https://dubb.com/v/tRbPRB/embed?widget=1&no_cta=0&autoplay=1&muted=0"
                frameborder="0" ratio="1.890"
                allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <div id="fd-widget-close">&times;</div>
        </div>
        <script src="{{ asset('embed/widget.js')}}"></script>
    </textarea>
</div>
