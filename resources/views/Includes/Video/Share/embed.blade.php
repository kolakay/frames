<div class="mb-4 form-row pl-4 col-6">
    <div class="custom-control custom-radio col-6">
        <input type="radio" id="customRadio1" @change="updateEmbed" v-model="video_share.embed_type" value="responsive" name="customRadio" class="custom-control-input">
        <label class="custom-control-label" for="customRadio1">Responsive</label>
    </div>
    <div class="custom-control custom-radio col-6">
        <input type="radio" id="customRadio2" @change="updateEmbed" v-model="video_share.embed_type" value="custom" name="customRadio" class="custom-control-input">
        <label class="custom-control-label" for="customRadio2">Custom Size</label>
    </div>
</div>
<div class="form-row" v-if="video_share.embed_type != 'responsive'">
    <div class="form-group col-md-6">
        <label class="form-group has-top-label">
            <input class="form-control" type="text" v-on:keyup="updateEmbed" @change="updateEmbed" v-model="video_share.embed_custom_width">
            <span>Set Width</span>
        </label>
    </div>
    <div class="form-group col-md-6">
        <label class="form-group has-top-label">
            <input class="form-control" type="text" v-on:keyup="updateEmbed" @change="updateEmbed" v-model="video_share.embed_custom_height">
            <span>Set Height</span>
        </label>
    </div>
</div>

<div class="form-group" v-if="video_share.embed_type == 'responsive'">
    <textarea readonly="readonly" rows="5" class="form-control" v-model="embed" @click="copyEmbed">
        <div style="position: relative; height: 0; padding-bottom: 62.50%;">
            <iframe
                style="position: absolute; width: 100%; height: 100%; left: 0;"
                allow="autoplay; encrypted-media"
                src="https://dubb.com/v/OFGw8q/embed?width=auto&height=auto&autoplay=1&no_cta=0&muted=1"
                width="auto" height="auto" frameborder="0" allowfullscreen>
            </iframe>
        </div>
    </textarea>
</div>
<div class="form-group" v-if="video_share.embed_type != 'responsive'">
    <textarea readonly="readonly" rows="5" class="form-control" v-model="custom_embed" @click="copyCustomEmbed">
        <iframe width="2880" height="1800"
        src="https://dubb.com/v/OFGw8q/embed?width=2880&height=1800&autoplay=1&no_cta=0&muted=1"
        frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </textarea>
</div>

<div class="form-group">
    <div class="custom-control custom-checkbox mb-4 col-12">
        <input type="checkbox" class="custom-control-input" id="actionOverlays" @change="updateEmbed" v-model="video_share.embed_disable_cta_overlay">
        <label class="custom-control-label" for="actionOverlays"> Disable Call to Action overlays</label>
    </div>
    <div class="custom-control custom-checkbox mb-4 col-12" >
        <input type="checkbox" class="custom-control-input" id="autoplay" @change="updateEmbed" v-model="video_share.embed_autoplay">
        <label class="custom-control-label" for="autoplay">Autoplay</label>
    </div>
    <div class="custom-control custom-checkbox mb-4 col-12" v-if="video_share.embed_autoplay">
        <input type="checkbox" class="custom-control-input" id="soundOff" @change="updateEmbed" v-model="video_share.embed_sound_off">
        <label class="custom-control-label" for="soundOff"> Turn sound off on Autoplay</label>
    </div>
</div>
