<div class="form-group">
    <p class="text-center">
        Email this video directly from your Gmail account or <a target="_blank" href="{{ route('user.settings.broadcast')}}" class="text-info">configured</a> email server.
    </p>

    <label class="form-group has-top-label">
        <select class="form-control select2-single" data-width="100%" id="providerId">
            <option label="&nbsp;">&nbsp;</option>
            <option :value="emailIntegration.id" v-for="emailIntegration in emailIntegrations">@{{ emailIntegration.email }}</option>
        </select>
        <span>Broadcast Provider
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This is the from email that the email will be sent from. To add additional emails here you must visit the Team > Configuration page."></i>
        </span>
    </label>
</div>

<div class="form-group">
    <label class="form-group has-top-label">
        <select class="form-control select2-single" data-width="100%" id="contactId">
            <option label="&nbsp;">&nbsp;</option>
            <option :value="contact.id" v-for="contact in contacts">@{{ contact.email }}</option>
        </select>
        <span>To</span>
    </label>
</div>

<div class="form-group">

    <label class="form-group has-top-label">
        <input class="form-control"
        v-model="video_share.direct_email_subject"

        type="text">
        <span>Subject
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This is the subject line that will appear in recipient’s inboxes. To add personalization text include #first-name#, #last-name# or #email#. For a fallback enter text after a | (e.g. #first-name|Hi#."></i>
        </span>
    </label>

</div>

<div class="form-group">
    <label class="form-group has-top-label">
        <input class="form-control"
        v-model="video_share.personalization_text"
        type="text">
        <span>Personalization text
            <i class="simple-icon-info icon-title text-info"
            data-toggle="tooltip" data-placement="top"
            title=""
            data-original-title="This is personalization text that appears as an overlay on the animated gif in the body of an email AND on the video landing page. We recommend adding personalization text with fallback text (in case there is no first name stored in the contact’s record) (e.g. #first-name|Hi#."></i>
        </span>
    </label>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-7">
            <label class="form-group has-top-label">
                <select class="form-control" data-width="100%" id="templateId" v-model="video_share.direct_email_template_id">
                    <option selected value="null" disabled>Select Template</option>
                    <option value="0">Simple Editor</option>
                    <option :value="emailTemplate.id" v-for="emailTemplate in emailTemplates">@{{ emailTemplate.name }}</option>
                </select>
                <span>Select Template</span>
            </label>
        </div>
        <div class="col-5">
        <a href="{{ route('user.assets.templates') }}" target="_blank" class="btn btn-outline-info mb-1 straight-btn">Create</a>
            <div class="d-inline" v-if="video_share.direct_email_template_id != '' && video_share.direct_email_template_id != undefined && video_share.direct_email_template_id != '0'">
                <a :href="url.edit_template + video_share.direct_email_template_id"
                    target="_blank" class="btn btn-outline-success mb-1 straight-btn" >Edit</a>
                <a :href="url.preview_template + video_share.direct_email_template_id"
                    target="_blank" class="btn btn-outline-primary mb-1 straight-btn" >Preview</a>
            </div>
        </div>
    </div>
</div>

<div class="form-group" v-show="video_share.direct_email_template_id == 0">
    <textarea id="simple-editor"></textarea>
</div>
<div class="form-group">
    <button type="button" class="btn btn-success mb-1 straight-btn" @click="sendMail">Send Now</button>
</div>
