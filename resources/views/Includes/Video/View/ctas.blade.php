@if($video->videoDetails)
@if($video->videoDetails->curated_ctas)
    <div v-for="(cta, index) in ctas" class="col-2">
        <a :href="'mailto:'+ cta.email"
            v-if ="cta.type == 'email'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default">
            @{{ cta.e_button_text }}
        </a>
        <a :href="cta.url"
            v-if ="cta.type == 'url'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            :target="(cta.url_tab == 'blank')? '_blank': '_self'"
            >
            @{{ cta.u_button_text }}
        </a>
        <a :href="cta.document_url"
            v-if ="cta.type == 'document'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.d_button_text }}
        </a>
        <a :href="cta.product_titles"
            v-if ="cta.type == 'products'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.p_button_text }}
        </a>

        <a :href="'sms:' +cta.phone"
            v-if ="cta.type == 'phone' && cta.phone_action == 'sms'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.ph_button_text }}
        </a>
        <a :href="'tel:' +cta.phone"
            v-if ="cta.type == 'phone' && cta.phone_action == 'call'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.ph_button_text }}
        </a>

        <a :href="'https://m.me/' + cta.chat_username"
            v-if ="cta.type == 'chat' && cta.chat_platform == 'messenger'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.ch_button_text }}
        </a>
        <a :href="'https://wa.me/' + cta.chat_username"
            v-if ="cta.type == 'chat' && cta.chat_platform == 'whatsapp'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.ch_button_text }}
        </a>
        <a :href="'skype:' + cta.chat_username + '?chat'"
            v-if ="cta.type == 'chat' && cta.chat_platform == 'skype'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.ch_button_text }}
        </a>

        {{-- <a :href="cta.product_titles"
            v-if ="cta.type == 'calender'"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.ca_button_text }}
        </a> --}}
        <a :href="cta.product_titles"
            v-if ="cta.type == 'video_reply'"
            @click="getCTAClick(index)"
            class="btn btn-sm btn-dark default"
            target="_blank"
            >
            @{{ cta.vid_button_text }}
        </a>

    </div>
@endif
@endif