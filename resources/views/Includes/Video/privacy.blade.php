<div class="form-group">
    <label>State Single</label>
    <select class="form-control select2-single" data-width="100%" v-model="video_details.visibility">
        <option  value="public">Feature: Publicly visible on your profile page</option>
        <option  value="private">Shareable: Send via link, email or social</option>
        <option  value="draft">Draft: Only visible in my account</option>
    </select>
</div>
<div class="form-group" v-if="video_details.visibility == 'private'">
    <label>Password</label>

    <div class="custom-control custom-checkbox mb-4">

        <input type="checkbox" v-model="video_details.has_password" class="custom-control-input" id="customCheckPrivacyPassword">
        <label class="custom-control-label w-50" for="customCheckPrivacyPassword">
            <input type="password" class="form-control" v-if="video_details.has_password == true"
                v-model="video_details.password" placeholder="Enter Password">
        </label>
    </div>
</div>
<div class="form-group">
    <button type="button" @click="updatePrivacy" class="btn btn-outline-dark default">Update Privacy</button>
</div>
