<div class="menu">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="{{ $page == 'dashboard' ? 'active' : '' }}">
                    <a href="{{ route('user.dashboard') }}"><i class="iconsminds-home"></i> Dashboard</a>
                </li>
                <li class="{{ ($page == 'profile')  ? 'active' : '' }}">
                    <a href="#"><i class="iconsminds-camera-4"></i> Profile</a>
                </li>
                <li class="{{ $page == 'social' ? 'active' : '' }}">
                    <a href="#social"><i class="iconsminds-diamond"></i> <span>Social</span></a>
                </li>
                <li class="{{ $page == 'notifications' ? 'active' : '' }}">
                    <a href="#notifications"><i class="iconsminds-power-cable"></i> Notifications</a>
                </li>
                <li class="{{ $page == 'video_options' ? 'active' : '' }}">
                    <a href="#video_options"><i class="iconsminds-video"></i> Video Options</a>
                </li>
                <li class="{{ $page == 'team' ? 'active' : '' }}">
                    <a href="#team"><i class="iconsminds-cd-2"></i> Team</a>
                </li>
                <li class="{{ $page == 'security' ? 'active' : '' }}">
                    <a href="{{ route('user.dashboard') }}"><i class="iconsminds-line-chart-1"></i> Security</a>
                </li>
                <li class="{{ $page == 'api_token' ? 'active' : '' }}">
                    <a href="#api_token"><i class="iconsminds-bookmark"></i> API Token</a>
                </li>
                <li class="{{ $page == 'integrations' ? 'active' : '' }}">
                    <a href="#integrations"><i class="iconsminds-bookmark"></i> Integrations</a>
                </li>
                <li class="{{ $page == 'code_redemption' ? 'active' : '' }}">
                    <a href="#code_redemption"><i class="iconsminds-bookmark"></i> Code Redemption</a>
                </li>
                <li class="{{ $page == 'referrals' ? 'active' : '' }}">
                    <a href="#referrals"><i class="iconsminds-bookmark"></i> Referrals</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="sub-menu">
        <div class="scroll">
            <ul class="list-unstyled" data-link="team">
                <li class="{{ ($sub == 'my_team')  ? 'active' : '' }}">
                    <a href="{{ route('user.videos.videos')}}">
                        <i class="simple-icon-rocket"></i> <span class="d-inline-block">My Team</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'configuration')  ? 'active' : '' }}">
                    <a href="{{ route('user.videos.tags')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Configuration</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'campaign_settings')  ? 'active' : '' }}">
                    <a href="{{ route('user.settings.broadcast')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Campaign Settings</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'configuration')  ? 'active' : '' }}">
                    <a href="{{ route('user.videos.tags')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Subscription</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'configuration')  ? 'active' : '' }}">
                    <a href="{{ route('user.videos.tags')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Add-ons</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'configuration')  ? 'active' : '' }}">
                    <a href="{{ route('user.videos.tags')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Payment Method</span>
                    </a>
                </li>
                <li class="{{ ($sub == 'configuration')  ? 'active' : '' }}">
                    <a href="{{ route('user.videos.tags')}}">
                        <i class="simple-icon-pie-chart"></i>
                        <span class="d-inline-block">Invoices</span>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</div>
