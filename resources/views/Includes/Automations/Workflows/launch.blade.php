<div>
    <div class="row pl-3">
        <div class="custom-control custom-checkbox mb-4 col-12">
            <input type="checkbox" class="custom-control-input" id="customCheckThis" v-model="workflow.trigger_once">
            <label class="custom-control-label" for="customCheckThis">Only trigger contacts once <i class="simple-icon-info icon-title text-info"
                data-toggle="tooltip"
                title=""
                data-original-title="Switch on to activate contacts only one time per workflow. This will ensure that your contacts don’t receive more than one email."></i></label>
        </div>

        <div class="custom-control custom-checkbox mb-4 col-12">
            <input type="checkbox" class="custom-control-input" id="customCheckSeven" v-model="workflow.is_active">
            <label class="custom-control-label" for="customCheckSeven">Turn this workflow on <i class="simple-icon-info icon-title text-info"
                data-toggle="tooltip"
                title=""
                data-original-title="Switch on to activate this workflow. You can always enable this workflow at a later date from the list menu or by editing the workflow."></i> </label>
        </div>
        <div class="custom-control custom-checkbox mb-4 col-12">
            <input type="checkbox" class="custom-control-input" id="customChecker" v-model="workflow.terms_accepted">
            <label class="custom-control-label" for="customChecker">I agree to the NO SPAM policy and understand my account can be deleted if I violate the terms of service</label>
        </div>
    </div>
</div>