<div class="form-group">
<div id="canvas" class="text-center"></div>

<div class="modal fade" id="add-process" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalContentLabel">Add Action</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="form-group has-top-label">
                        <select class="form-control" data-width="100%" v-model="newNode.action">
                            <option value="">Select Action</option>
                            <option value="add_tag">Add Tag</option>
                            <option value="condition">If/Then Branch</option>
                            <option value="create_task">Create Task</option>
                            <option value="delay">Delay</option>
                            <option value="remove_tag">Remove Tag</option>
                            <option value="send_campaign">Send Campaign</option>
                        </select>
                        <span>Select Action</span>
                    </label>
                </div>
                <div class="form-group" v-show="newNode.action == 'add_tag'">
                    <label class="form-group has-top-label">
                        <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="addTags">
                            <option :value="tag.id"  v-for="tag in tags">@{{ tag.name }}</option>
                        </select>
                        <span>Tags</span>
                    </label>
                </div>
                <div class="form-group" v-show="newNode.action == 'condition'">
                    <label class="form-group has-top-label">
                        <select class="form-control" data-width="100%" id="conditions" v-model="newNode.condition">
                            <option value="video_watched">Video Watched</option>
                            <option value="cta_clicked">CTA Clicked</option>
                        </select>
                        <span>Condition</span>
                    </label>

                    <label class="form-group has-top-label" v-if="newNode.condition == 'video_watched'">
                        <select class="form-control" v-model="newNode.element_id" data-width="100%">
                            <option :value="video.id"  v-for="video in videos">@{{ video.name }}</option>
                        </select>
                        <span>Videos</span>
                    </label>
                    <label class="form-group has-top-label" v-if="newNode.condition == 'cta_clicked'">
                        <select class="form-control" v-model="newNode.element_id" data-width="100%">
                            <option :value="cta.id"  v-for="cta in ctas">@{{ cta.name }}</option>
                        </select>
                        <span>CTAs</span>
                    </label>
                </div>
                <div class="form-group" v-show="newNode.action == 'create_task'">
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <input type="text" name="" class="form-control" v-model="newNode.task_title">
                            <span>Title</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <textarea name="" id="" v-model="newNode.task_description" class="form-control"></textarea>
                            <span>Description</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <select class="form-control" data-width="100%" v-model="newNode.task_assignee">
                                <option value="">Select Assignee</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                            <span>Assignee</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <input type="text" name="" class="form-control" v-model="newNode.task_due_after">
                            <span>Due After</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <select class="form-control" data-width="100%" v-model="newNode.task_unit">
                                <option value="minutes">Minutes</option>
                                <option value="hours">Hours</option>
                                <option value="days">Days</option>
                                <option value="weeks">Weeks</option>
                                <option value="months">Months</option>
                            </select>
                            <span>Select Unit</span>
                        </label>
                    </div>
                </div>
                <div class="form-group" v-show="newNode.action == 'delay'">
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <input type="text" name="" class="form-control" v-model="newNode.delay">
                            <span>Delay</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <select class="form-control" data-width="100%" v-model="newNode.unit">
                                <option value="minutes">Minutes</option>
                                <option value="hours">Hours</option>
                                <option value="days">Days</option>
                                <option value="weeks">Weeks</option>
                                <option value="months">Months</option>
                            </select>
                            <span>Select Unit</span>
                        </label>
                    </div>
                </div>
                <div class="form-group" v-show="newNode.action == 'remove_tag'">
                    <label class="form-group has-top-label">
                        <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="removeTags">
                            <option :value="tag.id"  v-for="tag in tags">@{{ tag.name }}</option>
                        </select>
                        <span>Tags</span>
                    </label>
                </div>
                <div class="form-group" v-show="newNode.action == 'send_campaign'">
                    <label class="form-group has-top-label">
                        <select class="form-control" data-width="100%" id="campaigns" v-model="newNode.campaign_id">
                            <option :value="campaign.id"  v-for="campaign in campaigns">@{{ campaign.title }}</option>
                        </select>
                        <span>Campaigns</span>
                    </label>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" @click="storeNode()">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-process" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalContentLabel">Edit Action</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="form-group has-top-label">
                        <select class="form-control" data-width="100%" v-model="nodeEdit.action" disabled>
                            <option value="">Select Action</option>
                            <option value="add_tag">Add Tag</option>
                            <option value="condition">If/Then Branch</option>
                            <option value="create_task">Create Task</option>
                            <option value="delay">Delay</option>
                            <option value="remove_tag">Remove Tag</option>
                            <option value="send_campaign">Send Campaign</option>
                        </select>
                        <span>Select Action</span>
                    </label>
                </div>
                <div class="form-group" v-show="nodeEdit.action == 'add_tag'">
                    <label class="form-group has-top-label">
                        <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="editAddTags">
                            <option :value="tag.id"  v-for="tag in tags">@{{ tag.name }}</option>
                        </select>
                        <span>Tags</span>
                    </label>
                </div>
                <div class="form-group" v-show="nodeEdit.action == 'condition'">
                    <label class="form-group has-top-label">
                        <select class="form-control" data-width="100%" id="conditions" v-model="nodeEdit.condition">
                            <option value="video_watched">Video Watched</option>
                            <option value="cta_clicked">CTA Clicked</option>
                        </select>
                        <span>Condition</span>
                    </label>

                    <label class="form-group has-top-label" v-if="nodeEdit.condition == 'video_watched'">
                        <select class="form-control" v-model="nodeEdit.element_id" data-width="100%">
                            <option :value="video.id"  v-for="video in videos">@{{ video.name }}</option>
                        </select>
                        <span>Videos</span>
                    </label>
                    <label class="form-group has-top-label" v-if="nodeEdit.condition == 'cta_clicked'">
                        <select class="form-control" v-model="nodeEdit.element_id" data-width="100%">
                            <option :value="cta.id"  v-for="cta in ctas">@{{ cta.name }}</option>
                        </select>
                        <span>CTAs</span>
                    </label>
                </div>
                <div class="form-group" v-show="nodeEdit.action == 'create_task'">
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <input type="text" name="" class="form-control" v-model="nodeEdit.task_title">
                            <span>Title</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <textarea name="" id="" v-model="nodeEdit.task_description" class="form-control"></textarea>
                            <span>Description</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <select class="form-control" data-width="100%" v-model="nodeEdit.task_assignee">
                                <option value="">Select Assignee</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                            <span>Assignee</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <input type="text" name="" class="form-control" v-model="nodeEdit.task_due_after">
                            <span>Due After</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <select class="form-control" data-width="100%" v-model="nodeEdit.task_unit">
                                <option value="minutes">Minutes</option>
                                <option value="hours">Hours</option>
                                <option value="days">Days</option>
                                <option value="weeks">Weeks</option>
                                <option value="months">Months</option>
                            </select>
                            <span>Select Unit</span>
                        </label>
                    </div>
                </div>
                <div class="form-group" v-show="nodeEdit.action == 'delay'">
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <input type="text" name="" class="form-control" v-model="nodeEdit.delay">
                            <span>Delay</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="form-group has-top-label">
                            <select class="form-control" data-width="100%" v-model="nodeEdit.unit">
                                <option value="minutes">Minutes</option>
                                <option value="hours">Hours</option>
                                <option value="days">Days</option>
                                <option value="weeks">Weeks</option>
                                <option value="months">Months</option>
                            </select>
                            <span>Select Unit</span>
                        </label>
                    </div>
                </div>
                <div class="form-group" v-show="nodeEdit.action == 'remove_tag'">
                    <label class="form-group has-top-label">
                        <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="editRemoveTags">
                            <option :value="tag.id"  v-for="tag in tags">@{{ tag.name }}</option>
                        </select>
                        <span>Tags</span>
                    </label>
                </div>
                <div class="form-group" v-show="nodeEdit.action == 'send_campaign'">
                    <label class="form-group has-top-label">
                        <select class="form-control" data-width="100%" id="campaigns" v-model="nodeEdit.campaign_id">
                            <option :value="campaign.id"  v-for="campaign in campaigns">@{{ campaign.title }}</option>
                        </select>
                        <span>Campaigns</span>
                    </label>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" @click="updateNode()">Submit</button>
            </div>
        </div>
    </div>
</div>
</div>