    <div class="form-group">

        <label class="form-group has-top-label">
            <input class="form-control"
            v-model="workflow.name"

            type="text">
            <span>Workflow Name
                <i class="simple-icon-info icon-title text-info"
                data-toggle="tooltip" data-placement="top"
                title=""
                ></i>
            </span>
        </label>

    </div>
    <div class="form-group">
        <label class="form-group has-top-label">
            <select class="form-control" data-width="100%" id="trigger" v-model="workflow.trigger">
                <option value="">Select Trigger</option>
                <option value="cta_clicked">CTA Clicked</option>
                <option value="tag_added">Tag Added</option>
                <option value="video_watched">Video Watched</option>
            </select>
            <span>Trigger
                <i class="simple-icon-info icon-title text-info"
                data-toggle="tooltip" data-placement="top"
                title=""
                ></i>
            </span>
        </label>
    </div>
    <div v-show="workflow.trigger != ''">
        <div class="form-group">
            <label class="form-group has-top-label">
                <select class="form-control" data-width="100%" id="includes" v-model="workflow.includes">
                    <option value="any">Includes any of</option>
                    <option value="all">Includes all of</option>
                </select>
                <span>Includes
                    <i class="simple-icon-info icon-title text-info"
                    data-toggle="tooltip" data-placement="top"
                    title=""></i>
                </span>
            </label>
        </div>

        <div class="form-group" v-show="workflow.trigger == 'tag_added'">
            <label class="form-group has-top-label">
                <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="triggerTags">
                    <option label="&nbsp;">&nbsp;</option>
                    <option :value="tag.id" v-for="tag in tags">@{{ tag.name }}</option>
                </select>
                <span>Select Tags
                    <i class="simple-icon-info icon-title text-info"
                    data-toggle="tooltip" data-placement="top"
                    title=""></i>
                </span>
            </label>
        </div>

        <div class="form-group" v-show="workflow.trigger == 'video_watched'">
            <label class="form-group has-top-label">
                <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="videos">
                    <option value="&nbsp;">&nbsp;</option>
                    <option :value="video.id" v-for="video in videos">@{{ video.name }}</option>
                </select>
                <span>Select Video
                    <i class="simple-icon-info icon-title text-info"
                    data-toggle="tooltip" data-placement="top"
                    title=""></i>
                </span>
            </label>
        </div>

        <div class="form-group" v-show="workflow.trigger == 'cta_clicked'">
            <label class="form-group has-top-label">
                <select class="form-control select2-multiple" multiple="multiple" data-width="100%" id="ctas">
                    <option value="&nbsp;">&nbsp;</option>
                    <option :value="cta.id" v-for="cta in ctas">@{{ cta.name }}</option>
                </select>
                <span>Select CTAs
                    <i class="simple-icon-info icon-title text-info"
                    data-toggle="tooltip" data-placement="top"
                    title=""></i>
                </span>
            </label>
        </div>

    </div>
