<html xmlns="http://www.w3.org/1999/xhtml" style="width: 100%;">

<body class="body" style="-webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; font-family: 'Open Sans', Arial, Sans-serif;">
  <!--header-->
  <table class="full" bgcolor="#F4F4F4" align="center" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
    <tr>
      <td align="center">
        <table class="table-inner" width="600" style="border-spacing: 0; table-layout: auto; margin: 0 auto; max-width: 600px;" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="10"></td>
          </tr>
          <tr>
            <td>
              <!--logo-->
              <table class="table-full" width="150" align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                <tr>
                  <td align="center" style="line-height:0px;"><img style="line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://do.dubbcdn.com/img/dubb-logo-full.png" width="150" alt="logo"></td>
                </tr>

              </table>
              <!-- end logo -->
              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
              <table width="25" align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                <tr>
                  <td height="10"></td>
                </tr>
              </table>
              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
              <!-- slogan -->
              <table class="table-full" width="200" border="0" align="right" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center" style="font-family:Open sans, Arial, Sans-serif; font-size:14px; color:#808E8E;line-height:28px;">
                    <p style="border-spacing: 0; table-layout: auto; margin: 0 auto;">Hello </p></td>
                </tr>
              </table>
              <!--end slogan-->
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <!--end header-->
  <!--1/1 panel-->
  <table class="full" bgcolor="#F4F4F4" align="center" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
    <tr>
      <td align="center">
        <table class="table-inner" width="600" style="border-spacing: 0; table-layout: auto; margin: 0 auto; max-width: 600px;" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td bgcolor="#FFFFFF" style="border:1px solid #EAEAEA; border-radius:6px; border-bottom:2px solid #DDDDDD;" align="center">
              <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                         <!--title-->
			   		    <tr>
            <td height="10"></td>
          </tr>
		  
		    <tr>
            <td align="center"><h1 style="font-family:Open sans, Arial, Sans-serif; font-size:28px; color:#808E8E;line-height:28px;">Here's the Big Idea!</h1></td>
          </tr>
		  
		    <tr>
            <td height="10"></td>
          </tr>

          <!--end title-->

          <!-- image -->
                <tr>
                  <td height="200" width="580" align="center" style="line-height:0px;">
                    <div style=" font-family: Open Sans, Arial, sans-serif;">
  <table style="text-align: center; border: none; padding: 10px; background-color: transparent;">
    <tr>
      <td>
        <a target="_blank" href="https://vid-links.com/v/3qHj1a?from_email=1"><img width="600" height="375" src="https://lightspeeddmlounge.dubb.com/v/3qHj1a/preview.gif" style="max-height: 400px; max-width: 600px;" alt="Setting Up Your Dubb Account" /></a>
      </td>
    </tr>
  </table>
</div>

            </td>
          </tr>
          <!-- end image -->
          <tr>
            <td height="10"></td>
          </tr>



           <tr>
            <td align="center"><p style="font-family:Open sans, Arial, Sans-serif; font-size:14px; color:#808E8E;line-height:28px;">Watch this video to hear about our big idea! We think you'll enjoy it.</p></td>
          </tr>
          <!--end content-->
          <tr>
            <td height="20"></td>
          </tr>
          <!--button-->
          <tr>
            <td>
              <table border="0" align="center" cellpadding="0" cellspacing="0" class="textbutton" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                <tr>
                  <td bgcolor="#1AB99B" class="btn-link" height="40" align="center" style="padding-left:20px;padding-right:20px;border-radius:6px;">
                    <a href="https://corp.dubb.com/v/3qHj1a?from_email=1" style="text-decoration: none; font-family: 'open sans', arial, sans-serif; color: #FFFFFF;">WATCH VIDEO</a></td>
                </tr>
              </table>
            </td>
          </tr>
          <!--end button-->
          <tr>
            <td height="35"></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="20"></td>
    </tr>
  </table>
  </td>
  </tr>
  </table>


  <table class="full" width="100%" align="center" bgcolor="#F4F4F4" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
    <tr>
      <td height="20"></td>
    </tr>
    <tr>
      <td align="center" bgcolor="#FFFFFF">
        <table class="table-inner" width="600" style="border-spacing: 0; table-layout: auto; margin: 0 auto; max-width: 600px;" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td height="15"></td>
          </tr>
          <tr>
            <td>
              <!--social-->
              <table class="table-full" align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                <tr>
                  <td align="center">
                    <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                      <tr>
                        <td align="center" style="line-height:0px;">
                          <a href="" style="color: #1AB99B; text-decoration: none;"><img style="line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://do.dubbcdn.com/email-templates/img/facebook.png" alt="img"></a>
                        </td>
                        <td width="10"></td>
                        <td align="center" style="line-height:0px;">
                          <a href="" style="color: #1AB99B; text-decoration: none;"><img style="line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://do.dubbcdn.com/email-templates/img/twitter.png" alt="img"></a>
                        </td>
                        <td width="10"></td>
                        <td align="center" style="line-height:0px;">
                          <a href="" style="color: #1AB99B; text-decoration: none;"><img style="line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://do.dubbcdn.com/email-templates/img/instagram.png" alt="img"></a>
                        </td>
                        <td width="10"></td>
                        <td align="center" style="line-height:0px;">
                          <a href="" style="color: #1AB99B; text-decoration: none;"><img style="line-height: 0px; font-size: 0px; border: 0px; display: block; overflow: hidden;" src="https://do.dubbcdn.com/email-templates/img/linkedin.png" alt="img"></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <!--end social-->
              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
              <table width="20" align="left" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                <tr>
                  <td height="15"></td>
                </tr>
              </table>
              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
              <!--preference-->
              <table class="table-full" align="right" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">
                <tr>
                  <td align="center">
                    <table border="0" align="center" cellpadding="0" cellspacing="0" style="border-spacing: 0; table-layout: auto; margin: 0 auto;">

                      <tr>
                        <td align="center"><p style="font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; color: #808E8E;"><img src="https://do.dubbcdn.com/img/dubb-icon.png" width="10" height="10" style="overflow: hidden; display: inline;"><span style="font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; color: #808E8E;"> Sent using</span> <a href="https://dubb.com?utm_source=email&amp;utm_medium=email&amp;utm_campaign=email-template" style="color: #808E8E; text-decoration: underline;">Dubb</a></p></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <!--end preference-->
            </td>
          </tr>
          <tr>
            <td height="15"></td>
          </tr>
			
        </table>
      </td>
    </tr>
	                           <tr>
            <td height="15"></td>
          </tr> <tr>
                  <td align="center" class="btn-link"><p style="font-family: 'Open Sans', Arial, sans-serif; font-size:13px; color:#999999;">Lightspeeddmlounge © All Rights Reserved</p></td>
                </tr>          <tr>
            <td height="15"></td>
          </tr>
  </table>
  <!--end footer-->
</body>

</html>