<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Ginger';
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('123456');
        $user->is_active = true;
        $user->user_type = 'admin';
        $user->uuid = (string) \Str::uuid();
        $user->save();

        $user = new User;
        $user->name = 'Vanilla';
        $user->email = 'user@user.com';
        $user->password = bcrypt('123456');
        $user->is_active = true;
        $user->user_type = 'member';
        $user->uuid = (string) \Str::uuid();
        $user->save();
    }
}
