<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->string('associated_contact_id')->nullable();
            $table->string('associated_video_id')->nullable();
            $table->string('associated_deal_id')->nullable();
            $table->string('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn('associated_contact');
            $table->dropColumn('associated_video');
            $table->dropColumn('associated_deal');
            $table->dropColumn('status');
        });
    }
}
