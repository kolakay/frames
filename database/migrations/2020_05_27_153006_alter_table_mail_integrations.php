<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableMailIntegrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mail_integrations', function (Blueprint $table) {
            $table->string('mandrill_username')->nullable();
            $table->string('mandrill_password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mail_integrations', function (Blueprint $table) {
            $table->dropColumn('mandrill_username');
            $table->dropColumn('mandrill_password');
        });
    }
}
