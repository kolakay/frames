<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('uuid');
            $table->string('name')->nullable();
            $table->string('file')->nullable();
            $table->string('thumbnail')->nullable();

            $table->integer('email_opens')->default(0);
            $table->integer('email_clicks')->default(0);
            $table->integer('views')->default(0);
            $table->integer('watches')->default(0);
            $table->integer('cta_clicks')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
