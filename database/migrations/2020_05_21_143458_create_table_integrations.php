<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableIntegrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_integrations', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->text('service');
            $table->string('type');
            $table->text('bcc')->nullable();
            $table->text('cc')->nullable();
            $table->integer('limit_per_day')->default(1000);
            $table->text('access_token')->nullable();
            $table->string('gmail_id')->nullable();
            $table->string('email')->nullable();
            $table->string('picture')->nullable();
            $table->string('verified_email')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_gmail_integrations');
    }
}
