<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deals', function (Blueprint $table) {
			$table->id();
			$table->integer('user_id');
			$table->string('title');
			$table->string('description')->nullable();
			$table->string('owner')->nullable();
			$table->string('amount')->nullable();
			$table->date('closed_date')->nullable();
			$table->enum('stage',['qualified_to_buy', 'presentation_scheduled', 'appointment_scheduled', 'decision_maker_bought_in', 'contract_sent', 'closed_won', 'closed_lost']);
			$table->string('associated_company')->nullable();
			$table->string('associated_contacts')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('deals');
	}
}
