<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCtas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctas', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->boolean('is_default')->default(false);
            $table->text('description')->nullable();


            $table->string('email')->nullable();
            $table->string('email_button_text')->nullable();
            $table->string('email_custom_button_text')->nullable();

            $table->string('url')->nullable();
            $table->string('url_tab')->nullable();
            $table->string('url_button_text')->nullable();
            $table->string('url_custom_button_text')->nullable();

            $table->string('phone')->nullable();
            $table->string('phone_action')->nullable();
            $table->string('phone_button_text')->nullable();
            $table->string('phone_custom_button_text')->nullable();

            $table->string('chat_platform')->nullable();
            $table->string('chat_username')->nullable();
            $table->string('chat_button_text')->nullable();
            $table->string('chat_custom_button_text')->nullable();


            $table->string('calendar_platform')->nullable();
            $table->string('calendar_username')->nullable();
            $table->string('calendar_id')->nullable();
            $table->string('calendar_hash')->nullable();
            $table->string('calendar_button_text')->nullable();
            $table->string('calendar_custom_button_text')->nullable();

            $table->string('form')->nullable();
            $table->string('form_button_text')->nullable();
            $table->string('form_custom_button_text')->nullable();

            $table->string('iframe_title')->nullable();
            $table->string('iframe_source')->nullable();
            $table->string('iframe_height')->nullable();
            $table->boolean('iframe_scroll')->default(false);
            $table->string('iframe_button_text')->nullable();
            $table->string('iframe_custom_button_text')->nullable();

            $table->string('document_title')->nullable();
            $table->string('document_button_text')->nullable();
            $table->string('document_custom_button_text')->nullable();
            $table->string('document_file')->nullable();

            $table->text('product_titles')->nullable();
            $table->string('product_button_text')->nullable();
            $table->string('product_custom_button_text')->nullable();

            $table->string('web_page_title')->nullable();
            $table->string('web_page_url')->nullable();
            $table->string('web_page_height')->nullable();
            $table->boolean('web_page_scroll')->default(false);
            $table->string('web_page_button_text')->nullable();
            $table->string('web_page_custom_button_text')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctas');
    }
}
