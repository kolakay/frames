<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableNodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('workflow_id');
            $table->string('node_id')->nullable();
            $table->string('type')->nullable();
            $table->string('parent')->nullable();
            $table->string('normal_branch')->nullable();
            $table->string('yes_branch')->nullable();
            $table->string('no_branch')->nullable();
            
            $table->string('action')->nullable();
            $table->string('condition')->nullable();
            $table->text('add_tags')->nullable();
            $table->text('remove_tags')->nullable();

            $table->string('element_id')->nullable();
            $table->string('task_title')->nullable();
            $table->string('task_description')->nullable();
            $table->string('task_assignee')->nullable();

            $table->string('task_due_after')->nullable();
            $table->string('task_unit')->nullable();
            $table->string('delay')->nullable();
            $table->string('unit')->nullable();
            $table->string('campaign_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodes');
    }
}
