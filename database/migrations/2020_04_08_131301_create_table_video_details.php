<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableVideoDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_details', function (Blueprint $table) {
            $table->id();
            $table->integer('video_id');
            $table->integer('user_id');
            $table->text('description')->nullable();
            $table->text('tags')->nullable();
            $table->text('ctas')->nullable();
            $table->text('playlist')->nullable();

            $table->boolean('auto_play')->default(false);
            $table->boolean('add_exit_intent_modal')->default(false);
            $table->boolean('add_emoji_response_buttons')->default(false);
            $table->boolean('add_facebook_comment_widget')->default(false);
            $table->boolean('add_messaging')->default(false);
            $table->boolean('share_with_team')->default(false);
            $table->boolean('disable_tracking')->default(false);
            $table->boolean('allow_video_download')->default(false);
            $table->boolean('auto_delete')->default(false);
            $table->text('auto_delete_date')->nullable();
            $table->enum('visibility', ['draft', 'public', 'private'])->default('public');
            $table->boolean('has_password')->default(false);
            $table->string('password')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_details');
    }
}
