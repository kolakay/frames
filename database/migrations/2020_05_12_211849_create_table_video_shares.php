<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableVideoShares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_shares', function (Blueprint $table) {
            $table->id();
            $table->integer('video_id');
            $table->boolean('disable_gif_animation')->default(false);
            $table->boolean('disable_url_text_encoding')->default(false);
            $table->boolean('disable_email_open_tracking')->default(false);
            $table->string('personalization_text')->nullable();
            $table->enum('embed_type', ["responsive", "custom"])->default("responsive");
            $table->integer('embed_custom_width')->default(2880);
            $table->integer('embed_custom_height')->default(1800);
            $table->boolean('embed_disable_cta_overlay')->default(false);
            $table->boolean('embed_autoplay')->default(true);
            $table->boolean('embed_sound_off')->default(true);
            $table->enum('widget_position', ["rightBottom", "leftBottom", "rightTop", "leftTop"])->default("rightBottom");
            $table->boolean('widget_autoplay')->default(false);
            $table->boolean('widget_sound_off')->default(false);
            $table->boolean('widget_show_cta')->default(true);
            $table->boolean('widget_hide_in_mobile')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_shares');
    }
}
