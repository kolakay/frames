<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableEmailCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_campaigns', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('title');
            $table->integer('video_id');
            $table->integer('provider_id');
            $table->string('from_name');
            $table->string('subject');
            $table->text('email_body')->nullable();
            $table->text('emails')->nullable();
            $table->text('inclusion_list')->nullable();
            $table->text('exclusion_list')->nullable();
            $table->boolean('schedule_is_set')->default(false);
            $table->text('schedule_date')->nullable();
            $table->text('date_time')->nullable();

            $table->integer('template_id');
            $table->enum('status', ['draft', 'finnished', 'ongoing'])->default('draft');
            $table->boolean('is_active')->nullable();
            $table->string('video_header')->nullable();
            $table->boolean('is_draft')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_campaigns');
    }
}
