<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableVideoDesigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_designs', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('video_id');
            $table->boolean('display_logo')->default(false);
            $table->string('player_color')->nullable();
            $table->boolean('default_player_color')->default(false);
            $table->string('player_background_image')->nullable();
            $table->boolean('default_player_background_image')->default(false);
            $table->string('cta_color')->nullable();
            $table->boolean('default_cta_color')->default(false);
            $table->string('cta_background_image')->nullable();
            $table->boolean('default_cta_background_image')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_designs');
    }
}
