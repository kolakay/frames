<?php

use App\Models\ContactModel;
use App\Models\NodeModel;
use App\Models\TaskModel;
use App\Models\WorkflowModel;
use App\Models\VideoModel;
use App\Models\EmailCampaignModel;


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function getPixabayAPIKey(){
    return "12163978-f556ac0b9e24dd18e44fe935a";
}


function autoDeleteUserVideos(){
    $videos = VideoModel::where('user_id', Auth::id())->get();
}

function parseWelcomeEmail($firstName, $placeholder, $rawMail){
    $parsedMail = $rawMail;
    $variables = [
        'first_name' => $firstName,
        'video_placeholder' => $placeholder,
    ];

    foreach($variables as $key => $value){
        if (strpos($parsedMail, "{".$key."}") !== false) {
            $parsedMail = str_replace("{".$key."}", $value, $parsedMail);
        }
    }

    return $parsedMail;
}

function setupEmailCopy($contactId, $video, $campaign){
    $contactField = "contact=$contactId";
    $hashField = "image_hash=$contactId";

    $tracker = '<img src="'.route('user.videos.log', $video->uuid).'?'.$hashField.'" style="display: none; width: 0px; height: 0px;">';
    $emailTracker = $tracker;

    $email_copy = '<div style="font-family: Open Sans, Arial, sans-serif;">
        <table style="text-align: center; border: none; padding: 10px; background-color: transparent;">
            <tbody>
                <tr>
                    <td>
                        <div style="font-size: 24px; background-color: rgb(0, 0, 0); color: rgb(255, 255, 255); margin: 0px auto; width: 600px;">${this.video_share.personalization_text}</div>
                        <a target="_blank" href="'.$video->video_url.'?message='.$campaign->video_header.'&'.$contactField.'&from_email=1"><img width="600" height="375" src="'.$video->video_gif_path.'" style="max-height: 400px; max-width: 600px;" alt="Video For Sales"></a>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 20px;">
                        <a target="_blank" href="'.$video->video_url.'?message='.$campaign->video_header.'&'.$contactField.'&from_email=1&campaign='.$campaign->id.'" style="color: rgb(0, 113, 188); background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: middle; padding: 6px 12px; border-radius: 4px; border: 1px solid rgb(0, 113, 188); text-decoration-line: none; margin-left: auto; margin-right: auto;">Watch Video</a>
                    </td>
                </tr>
                <tr>
                    <td style="color: rgb(51, 51, 51); line-height: 24px; font-size: 13px; padding-top: 10px;">
                        <img src="#" style="width: 13px; height: 13px; margin-right: 5px; vertical-align: middle;">
                        Video powered by <a target="_blank" style="color: rgb(51, 51, 51);" href="#">Face Drip</a>
                    </td>
                </tr>
            </tbody>
        </table>'.
        $emailTracker.'
    </div>';

    return $email_copy;
}

function automateWorkflows($logDetails){
    $userId = $logDetails['user_id'];
    $elementId = $logDetails['element_id'];
    $type = $logDetails['type'];
    $contactId = $logDetails['contact_id'];
    $automations = WorkflowModel::where('user_id', $userId)->get();
    foreach($automations as $automation){
        if($automation->trigger == $type){
            if(in_array($elementId, $automation->elements)){
                $node = NodeModel::where('workflow_id', $automation->id)
                    ->where('user_id', $userId)
                    ->where('parent', 'node_0')
                    ->first();
                if($node){
                    activateNodeBranch($logDetails, $automation, $node->node_id);
                }
            }
        }
    }
}

function activateNodeBranch($logDetails, $automation, $currentNodeId){
    $userId = $logDetails['user_id'];
    $elementId = $logDetails['element_id'];
    $type = $logDetails['type'];
    $contactId = $logDetails['contact_id'];
    $currentNode = NodeModel::where('workflow_id', $automation->id)
        ->where('user_id', $userId)
        ->where('node_id', $currentNodeId)
        ->first();
    if($currentNode){
        // if($currentNode->action == 'add_tag'){
        //     addTagToUser($userId,$contactId, $currentNode);
        // }elseif ($currentNode->action == 'create_task') {
        //     createTask($userId,$contactId, $currentNode);
        // }elseif ($currentNode->action == 'delay') {
            
        // }elseif ($currentNode->action == 'remove_tag') {
        //     removeTagFromUser($userId,$contactId, $currentNode);
        // }elseif ($currentNode->action == 'send_campaign') {
        //     acivateUserCampaign($userId,$contactId, $currentNode);
        // }
        if($currentNode->type == 'operation'){
            activateAction($userId,$contactId, $currentNode);
            if($currentNode->normal_branch != ""){
                activateNodeBranch($logDetails, $automation,$currentNodeId);
            }

        }elseif ($currentNode->action != 'operation') {
            if($type == 'cta_clicked' && $currentNode->condition == "cta_clicked") {
                if($currentNode->element_id ==  $elementId){
                    if($currentNode->yes_branch != ""){
                        activateNodeBranch($logDetails, $automation,$currentNodeId);
                    }
                }else{
                    if($currentNode->no_branch != ""){
                        activateNodeBranch($logDetails, $automation,$currentNodeId);
                    }
                }
            }elseif($type == 'video_watched' && $currentNode->condition == "video_watched") {
                if($currentNode->element_id ==  $elementId){
                    if($currentNode->yes_branch != ""){
                        activateNodeBranch($logDetails, $automation,$currentNodeId);
                    }
                }else{
                    if($currentNode->no_branch != ""){
                        activateNodeBranch($logDetails, $automation,$currentNodeId);
                    }
                }
            }         
            

            
            
        }
    }
}

function activateAction($userId,$contactId, $currentNode){
    if($currentNode->action == 'add_tag'){
        addTagToUser($userId,$contactId, $currentNode);
    }elseif ($currentNode->action == 'create_task') {
        createTask($userId,$contactId, $currentNode);
    }elseif ($currentNode->action == 'delay') {
        
    }elseif ($currentNode->action == 'remove_tag') {
        removeTagFromUser($userId,$contactId, $currentNode);
    }elseif ($currentNode->action == 'send_campaign') {
        acivateUserCampaign($userId,$contactId, $currentNode);
    }
}

function addTagToUser($userId,$contactId, $currentNode){
    $contact = ContactModel::where('id', $contactId)->where('user_id', $userId)->first();
    if($contact){
        $userTags = [];
        foreach($currentNode->add_tags as $tag){
            $userTags = $contact->tags;
            array_push($userTags, $tag);
        }
        $contact->tags = $userTags;
        $contact->save();
    }
    
}

function acivateUserCampaign($userId,$contactId, $currentNode){
    $contact = ContactModel::where('user_id', $userId)->where('id', $contactId)->first();
    if($contact){
        $campaign = AutomationEmailModel::where('user_id', $userId)->where('id', $currentNode->campaign_id)->first();
        if($campaign){
            $video = VideoModel::where('user_id', Auth::id())
                    ->where('id', $campaign->video_id)
                    ->first();
            if(!$video){
                return false;
            }
            $emailIntegration = EmailIntegrationModel::where('user_id', Auth::id())
                    ->where('id', $campaign->provider_id)
                    ->first();
            if(!$emailIntegration){
                return false;
            }
            $template = CustomTemplateModel::where('user_id', Auth::id())
                    ->where('id', $campaign->template_id)
                    ->first();
            if(!$template && $campaign->template_id != 0){
                return false;
            }
            
            $email_copy = setupEmailCopy($contactId, $video, $campaign);
            $emailDetails = new \stdClass();
            $emailDetails->subject = $campaign->subject;
            $emailDetails->to = $contact->email;
            $emailDetails->from = $emailIntegration->email;
            $emailDetails->body = setEmailBody($template,$contact, $campaign, $email_copy);
            $emailDetails->integration = $emailIntegration;
            $response = EmailController::sendMail($emailDetails, $campaign);
        }
    }
}

function setEmailBody($template, $contact, $request, $email_copy){
    if($template){
        $body = parseWelcomeEmail($contact->first_name,
            $email_copy, $template->html);
    }else{
        $body = parseWelcomeEmail($contact->first_name,
            $email_copy, $request->email_body);
    }

    return $body;
}

function removeTagFromUser($userId,$contactId, $currentNode){
    $contact = ContactModel::where('id', $contactId)->where('user_id', $userId)->first();
    if($contact){
        $userTags = [];
        $newTags = [];
        foreach($currentNode->add_tags as $tag){
            $userTags = $contact->tags;
            
            foreach($userTags as $userTag){
                if($userTag != $tag){
                    array_push($newTags, $userTag);
                }
            }
            
            
        }
        $contact->tags = $newTags;
        $contact->save();
    }
}

function createTask($userId,$contactId, $currentNode){
    $task = new TaskModel;
    $task->user_id = $userId;
    $task->title = $currentNode->task_title;
    $task->description = $currentNode->task_description;
    $task->assignee_id = $currentNode->task_assignee;
    $task->due_after = $currentNode->task_due_after;
    $task->unit = $currentNode->task_unit;
    $task->save();
}

function googleTextToSpeechLanguages(){
    return [   
        [
            "name" => "Arabic",
            "code" => "ar-XA"
        ],
        [
            "name" => "Arabic",
            "code" => "ar-XA"
        ],
        [
            "name" => "Bengali (India)",
            "code" => "bn-IN"
        ],
        [
            "name" => "Czech (Czech Republic)",
            "code" => "cs-CZ"
        ],
        [
            "name" => "Danish (Denmark)",
            "code" => "da-DK"
        ],
        [
            "name" => "Dutch (Netherlands)",
            "code" => "nl-NL"
        ],
        [
        "name" => "English (Australia)",
        "code" => "en-AU"
        ],
        [
            "name" => "English (India)",
            "code" => "en-IN"
        ],
        [
            "name" => "English (UK)",
            "code" => "en-GB"
        ],
        [
            "name" => "English (US)",
            "code" => "en-US"
        ],
        [
            "name" => "Filipino (Philippines)",
            "code" => "fil-PH"
        ],
        [
            "name" => "Finnish (Finland)",
            "code" => "fi-FI"
        ],
        [
            "name" => "French (Canada)",
            "code" => "fr-FR"
        ],
        [
            "name" => "German (Germany)",
            "code" => "de-DE"
        ],
        [
            "name" => "Greek (Greece)",
            "code" => "el-GR"
        ],
        [
            "name" => "Gujarati (India)",
            "code" => "gu-IN"
        ],
        [
            "name" => "Hindi (India)",
            "code" => "hi-IN"
        ],
        [
            "name" => "Hungarian (Hungary)",
            "code" => "hu-HU"
        ],
        [
            "name" => "Indonesian (Indonesian)",
            "code" => "id-ID"
        ],
        [
            "name" => "Italian (Italy)",
            "code" => "it-IT"
        ],
        [
            "name" => "Japanese (Japan)",
            "code" => "ja-JP"
        ],
        [
            "name" => "Kannada (India)",
            "code" => "kn-IN"
        ],
        [
            "name" => "Korean (South Korea)",
            "code" => "ko-KR"
        ],
        [
            "name" => "Malayalam (India)",
            "code" => "ml-IN"
        ],
        [
            "name" => "Mandarin Chinese",
            "code" => "cmn-CN"
        ],
        [
            "name" => "Norwegian (Norway)",
            "code" => "nb-NO"
        ],
        [
            "name" => "Polish (Poland)",
            "code" => "pl-PL"
        ],
        [
            "name" => "Portuguese (Brazil)",
            "code" => "pt-BR"
        ],
        [
            "name" => "Portuguese (Portugal)",
            "code" => "pt-PT"
        ],
        [
            "name" => "Russian (Russian)",
            "code" => "ru-RU"
        ],
        [
            "name" => "Slovak (Slovakia)",
            "code" => "sk-SK"
        ],
        [
            "name" => "Spanish (Spain)",
            "code" => "es-ES"
        ],
        [
            "name" => "Swedish (Sweden)",
            "code" => "sv-SE"
        ],
        [
            "name" => "Tamil (India)",
            "code" => "ta-IN"
        ],
        [
            "name" => "Telugu (India)",
            "code" => "te-IN"
        ],
        [
            "name" => "Thai (Thailand)",
            "code" => "th-TH"
        ],
        [
            "name" => "Turkish (Turkey)",
            "code" => "tr-TR"
        ],
        [
            "name" => "Ukrainian (Ukraine)",
            "code" => "uk-UA"
        ],
        [
            "name" => "Vietnamese (Vietnam)",
            "code" => "vi-VN"
        ]
        ];
}

function getDealsStages(){
    $stages = [
            'qualified_to_buy' => 'Qualified to buy',
            'presentation_scheduled' => 'Presentation scheduled',
            'appointment_scheduled' => 'Appointment scheduled',
            'decision_maker_bought_in' => 'Decision maker bought in',
            'contract_sent' => 'Contract sent',
            'closed_won' => 'Closed lost',
            'closed_lost' => 'Closed lost'
    ];

    return $stages;
}
