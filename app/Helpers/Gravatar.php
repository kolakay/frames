<?php


function getGravatar($email){
    $size = 150;
    $hash = md5( strtolower( trim( $email ) ) );
    $gravatarUrl = "https://www.gravatar.com/avatar/" . $hash . "?s=" . $size;

    return $gravatarUrl;
}
