<?php
namespace App\Helpers;

class Paths {

    const VIDEO_PATH = 'public/videos/';
    const AUDIO_PATH = 'public/audios/';
    const VIDEO_THUMBNAIL_PATH = 'public/thumbnails/';
    const VIDEO_GIF_PATH = 'public/gifs/';
    const BACKGROUND_IMAGE_PATH = 'public/backgrounds/';

    const DOCUMENT_PATH = 'public/documents/';
    const BUILDER_PATH = 'public/builder/';
    const SRT_FILES_PATH = 'public/srt_files/';

    const REV_ACCESS_TOKEN = "02H6_pSwcjLPQmcox0yOkEQVdrFxXo2ArdZc3PZ18tqQnRlQP_27ViBfKrOg664732sQSd2r1B1tIGqc0XHnlD7SQLrw0";
}
