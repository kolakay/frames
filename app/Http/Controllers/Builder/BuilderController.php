<?php

namespace App\Http\Controllers\Builder;

use App\User;
use App\Models\CustomTemplateModel;
use App\Helpers\Paths;
use Illuminate\Http\Request;
use App\Models\BuilderMediaModel;
use Auth, Session, Exception, Log, Storage, File;
use App\Http\Controllers\Controller;

class BuilderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($templateId = null){
        // try{
            if(!$templateId){
                abort(404);
            }
            $template = CustomTemplateModel::where('user_id', Auth::id())->where('id', $templateId)->first();
            if(!$template){
                abort(404);
            }
            $data = [
                'template' => $template
            ];
            return view('Builder.editor', $data);
        // }catch(Exception $error){
        //     Log::info('BuilderController@callback error message: ' . $error->getMessage());
        //     $message = "Unable to open builder";
        //     Session::put('errorMessage', $message);
        //     return redirect()->back();
        // }
    }

    public function canvas($templateId){
        $template = CustomTemplateModel::where('user_id', Auth::id())->where('id', $templateId)->first();
        $data = [
            'template' => $template
        ];
        return view('Builder.canvas', $data);
    }

    public function save(Request $request){
        try{
            $template = CustomTemplateModel::where('id', $request->template_id)->where('user_id', Auth::id())->first();
            if (!$template) {
                return response()->json([
                    'error' => true,
                    "message" => "template not found",
                ], 404);
            }

            $template->html = $request->html;
            $template->save();
            return "teeeeest";


        }catch(Exception $error){
            Log::info('BuilderController@save error message: ' . $error->getMessage());
            $message = "Unable to save media";
            return response()->json([
                'error' => true,
                "message" => $message,
            ], 500);
        }
    }

    public function upload(Request $request){
        try{
            if($request->hasFile('file')){
                $filename = $request->file('file')->getClientOriginalName();
                $path = Paths::BUILDER_PATH;
                $imagePath = "{$path}{$filename}";
                Storage::put($imagePath, File::get($request->file('file')));

                $media = new BuilderMediaModel();
                $media->user_id = Auth::id();
                $media->name = $filename;
                $media->path = $imagePath;
                $media->save();

                $url = \Request::root().Storage::url($imagePath);
                return response()->json([
                    'error' => false,
                    "path" => $url,
                ], 200);
            }
        }catch(Exception $error){
            Log::info('BuilderController@upload error message: ' . $error->getMessage());
            $message = "Unable to upload template";
            return response()->json([
                'error' => true,
                "message" => $message,
            ], 500);
        }
    }
}
