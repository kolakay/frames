<?php

namespace App\Http\Controllers;

use Auth, Session, Exception, Log;
use App\User;
use App\Models\TagModel;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    public function index(){
        try{

            $tags = TagModel::where('user_id', Auth::id())->get();


            $data = [
                'page' => 'videos',
                'sub' => 'tags',
                'tags' => $tags,
            ];
            return view('App.Video.tags', $data);

        }catch(Exception $error){
            Log::info('TagsController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function create(Request $request){
        try{
            if(!$request->tag_name){
                $message = "Tag Name is required";
                return response()->json(['message' => $message], 400);
            }

            $tag = TagModel::firstOrNew([
                'user_id' => Auth::id(),
                'name' => $request->tag_name
            ]);

            $tag->save();
            return response()->json([
                'message' => "Tag was saved successfully",
                'tag' => $tag

            ]);

        }catch(Exception $error){
            Log::info('TagsController@create error message: ' . $error->getMessage());
            $message = 'Unable to create Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function update(Request $request){
        try{

            if(!$request->tag_name || !$request->tag_id){
                $message = "Tag Details are required";
                return response()->json(['message' => $message], 400);
            }

            $tag = TagModel::where('user_id', Auth::id())->where('id', $request->tag_id)->first();
            if (!$tag) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Tag not found",
                ], 404);
            }

            $tag->name = $request->tag_name;
            $tag->save();

            return response()->json([
                'error' => false,
                'tag' => $tag,
                'message' => "Tag was updated successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('TagsController@update error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function delete(Request $request){
        try{

            $tag = TagModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$tag) {
                $message = "Tag was not found";
                return response()->json(['message' => $message], 404);
            }

            $tag->delete();
            $message = "Tag deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('TagsController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
