<?php

namespace App\Http\Controllers;

use Auth, Session,  Log, Storage, File;
use App\User;
use FFMpeg\FFMpeg;
use App\Helpers\Paths;
use App\Models\ContactModel;
use App\Models\TagModel;
use App\Models\CTAModel;
use App\Models\VideoModel;

use \Done\Subtitles\Subtitles;
use App\Models\VideoViewModel;
use App\Models\CTAClickModel;
use App\Models\EmailOpenModel;
use App\Models\PageViewModel;
use App\Models\EmailClickModel;
use App\Models\ReactionClickModel;
use App\Models\GeneralStatLogModel;
use App\Models\VideoShareModel;
use App\Models\VideoDetailsModel;
use App\Models\VideoDesignModel;
use Illuminate\Http\Request;
use App\Models\TranscriptionModel;
use App\Models\CustomTemplateModel;
use App\Models\EmailIntegrationModel;
use App\Http\Controllers\Integrations\AWSController;

use App\Http\Controllers\Integrations\SMTPController;
use App\Http\Controllers\Integrations\GmailController;
use App\Http\Controllers\Integrations\SendGridController;
use App\Http\Controllers\Integrations\MandrillController;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('viewVideo');
        $this->autoDeleteUserVideos();
    }

    public function videos(){
        try{
            $videos = VideoModel::where('user_id', Auth::id())->get();
            $tags = TagModel::where('user_id', Auth::id())->get();

            $data = [
                'page' => 'videos',
                'sub' => 'videos',
                'tags' => $tags,
                'videos' => $videos
            ];

            return view('App.Video.videos', $data);
        }catch(Exception $error){
            Log::info('VideoController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function downloadGif($videoUUID){
        try{
            $video = VideoModel::where('uuid', $videoUUID)->first();
            if(!$video){
                abort(404);
            }
            return response()->download(storage_path('app/' .Paths::VIDEO_GIF_PATH. $video->video_gif));

        }catch(Exception $error){
            Log::info('VideoController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function uploadPage()
    {
        return view('App.Video.upload');
    }

    public function videoReplacePage($videoUUID)
    {
        $video = VideoModel::where('uuid', $videoUUID)->first();
        if(!$video){
            $message = 'Unable to get Resource. Video not found.';
            return $this->handleError($message);
        }
        $data = [
            'video' => $video
        ];
        return view('App.Video.replace', $data);
    }

    public function replaceVideo($videoUUID, Request $request){
        try {
            if($request->hasFile('video')){
                $video = VideoModel::where('uuid', $videoUUID)->first();
                if(!$video){
                    $message = 'Unable to get Resource. Video not found.';
                    return response()->json([
                        'status' => 'error',
                        'message' => $message
                    ], 404);
                }
                $video->name = request()->file('video')->getClientOriginalName();
                $video->file = $this->storeVideo('video', $video);
                $video->save();

                $creationStatus = $this->createVideoGif($video);
                if($creationStatus['status'] == true){
                    $video->video_gif = $creationStatus['gif'];
                    $video->save();
                }

                return response()->json([
                    'status' => 'success',
                    'url' => route('user.videos.edit', $video->uuid)
                ]);
            }

            return response()->json([
                'status' => 'error',
                'message' => "unable to complete request. please upload a valid video"
            ], 400);

        } catch (Exception $error) {

            Log::info('VideoController@uploadVideo error message: ' . $error->getMessage());
            $message = 'Unable to store Video. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    public function uploadVideo(Request $request){
        try {
            if($request->hasFile('video')){
                $video = new VideoModel;
                $video->user_id = Auth::id();
                $video->uuid = \Str::uuid();
                $video->name = request()->file('video')->getClientOriginalName();
                $video->file = $this->storeVideo('video');
                $video->save();

                $creationStatus = $this->createVideoGif($video);
                if($creationStatus['status'] == true){
                    $video->video_gif = $creationStatus['gif'];
                    $video->save();
                }

                return response()->json([
                    'status' => 'success',
                    'url' => route('user.videos.edit', $video->uuid)
                ]);
            }

            return response()->json([
                'status' => 'error',
                'message' => "unable to complete request. please upload a valid video"
            ], 400);

        } catch (Exception $error) {

            Log::info('VideoController@uploadVideo error message: ' . $error->getMessage());
            $message = 'Unable to store Video. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }

    }

    public function updateVideoName(Request $request){
        try {
            $video = VideoModel::where('user_id', Auth::id())->where('id', $request->id)->first();
            if(!$video){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }
            if($request->name == ''){
                return response()->json([
                    'status' => 'error',
                    'message' => "unable to complete request. please give your video a name"
                ], 400);
            }
            $video->name = $request->name;
            $video->save();

            return response()->json([
                'status' => 'success',
                'message' => "Video name updated"
            ]);



        } catch (Exception $error) {

            Log::info('VideoController@updateVideoName error message: ' . $error->getMessage());
            $message = 'Unable to update Video. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    public function updateDetails(Request $request){
        try {
            $video = VideoModel::where('user_id', Auth::id())->where('id', $request->video_id)->first();
            if(!$video){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }

            $videoDetails = VideoDetailsModel::firstOrNew([
                'user_id' => Auth::id(),
                'video_id' => $request->video_id
            ]);

            $videoDetails->video_id = $request->video_id;
            $videoDetails->description = $request->description;
            $videoDetails->tags = json_decode($request->tags);
            $videoDetails->ctas = json_decode($request->ctas);
            $videoDetails->playlist = json_decode($request->playlist);

            $videoDetails->auto_play = $request->auto_play == 'true'? true : false;
            $videoDetails->add_exit_intent_modal = $request->add_exit_intent_modal == 'true'? true : false;
            $videoDetails->add_emoji_response_buttons = $request->add_emoji_response_buttons == 'true'? true : false;
            $videoDetails->add_facebook_comment_widget = $request->add_facebook_comment_widget == 'true'? true : false;
            $videoDetails->add_messaging = $request->add_messaging == 'true'? true : false;
            $videoDetails->share_with_team = $request->share_with_team == 'true'? true : false;
            $videoDetails->disable_tracking = $request->disable_tracking == 'true'? true : false;
            $videoDetails->allow_video_download = $request->allow_video_download == 'true'? true : false;
            $videoDetails->auto_delete = $request->auto_delete == 'true'? true : false;
            if($videoDetails->auto_delete == true){
                $videoDetails->auto_delete_date = strtotime($request->auto_delete_date);
            }

            $videoDetails->visibility = $request->visibility;
            $videoDetails->has_password = $request->has_password == 'true'? true : false;
            if($videoDetails->has_password && $request->password != ''){
                $videoDetails->password = bcrypt($request->password);
            }

            $videoDetails->save();

            return response()->json([
                'status' => 'success',
                'message' => "Video name updated"
            ]);



        } catch (Exception $error) {

            Log::info('VideoController@updateDetails error message: ' . $error->getMessage());
            $message = 'Unable to update Video. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    public function edit($videoUUID){
        try{

            $video = VideoModel::where('uuid', $videoUUID)->first();
            if(!$video){
                $message = 'Unable to get Resource. Video not found.';
                return $this->handleError($message);
            }

            if($video->video_gif == null || $video->video_gif == ''){
                $creationStatus = $this->createVideoGif($video);
                if($creationStatus['status'] == true){
                    $video->video_gif = $creationStatus['gif'];
                    $video->save();
                }
            }
            $tags = TagModel::where('user_id', Auth::id())->get();
            $ctas = CTAModel::where('user_id', Auth::id())->get();
            $emailTemplates = CustomTemplateModel::where('user_id', Auth::id())->get();
            $emailIntegrations = EmailIntegrationModel::where('user_id', Auth::id())->where('is_active', true)->get();
            $contacts = ContactModel::where('user_id', Auth::id())->get();
            $transcriptionDetails = TranscriptionModel::where('user_id', Auth::id())->where('id', $video->id)->first();
            if(!$transcriptionDetails){
                $transcriptionDetails = [];
            }
            $videos = VideoModel::where('user_id', Auth::id())->where('id', '!=', $video->id)->get();
            $analytics = $this->getAnalytics($video);

            $data = [
                'page' => 'videos',
                'sub' => 'videos',
                'tags' => $tags,
                'ctas' => $ctas,
                'emailTemplates' => $emailTemplates,
                'emailIntegrations' => $emailIntegrations,
                'contacts' => $contacts,
                'videos' => $videos,
                'video' => $video,
                'transcriptionDetails' => $transcriptionDetails
            ];
            $data = array_merge($data, $analytics);
            return view('App.Video.edit', $data);

        }catch(Exception $error){
            Log::info('VideoController@edit error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    private function getAnalytics($video){

        // $emailClicks = EmailClickModel::where('user_id', Auth::id())->where('video_id', $video->id)->count();
        // $CTAClicks = CTAClickModel::where('user_id', Auth::id())->where('video_id', $video->id)->count();
        // $reactionClicks = ReactionClickModel::where('user_id', Auth::id())->where('video_id', $video->id)->count();
        // $videoViews = VideoViewModel::where('user_id', Auth::id())->where('video_id', $video->id)->count();
        // $emailOpens = EmailOpenModel::where('user_id', Auth::id())->where('video_id', $video->id)->count();
        // $pageViews = PageViewModel::where('user_id', Auth::id())->where('video_id', $video->id)->count();
        $graphClass = new AnalyticsController;
        $graph = $graphClass->getStatistics('today', $video);
        $generalStatsClass = new GeneralStatsController;
        $generalStats = $generalStatsClass->getStatistics('today', $video);
        $activityLogsClass = new GeneralStatsLogController;
        $activityLogs = $activityLogsClass->getLogs('all_time', $video);
        return [
            // 'emailClickRates' => $this->getConversionRate($emailClicks, $emailOpens),
            // 'CTAClickRates' => $this->getConversionRate($CTAClicks, $pageViews),
            // 'reactionRates' => $this->getConversionRate($reactionClicks, $pageViews),
            // 'callRates' => 0,
            // 'watchRates' => $this->getConversionRate($videoViews, $pageViews),
            // 'emailRates' => $this->getConversionRate($emailOpens, $pageViews),
            'graph' => $graph,
            'generalStats' => $generalStats,
            'activityLogs' => $activityLogs
        ];
    }

    private function getConversionRate($sumOfLeads, $sumOfVisitors){

        $rate =  ($sumOfLeads && $sumOfVisitors) ? round((($sumOfLeads * 100) / $sumOfVisitors), 2) : 0;

        return $rate;
    }

    public function viewVideo($videoUUID){
        try{
            $video = VideoModel::where('uuid', $videoUUID)->first();
            if(!$video){
                $message = 'Unable to get Resource. Video not found.';
                if(Auth::check()){
                    return $this->handleError($message);
                }
                abort(404);
            }

            $this->storeVideoAnalytic($video);

            $data = [
                'video' => $video,
            ];
            return view('App.Video.view', $data);

        }catch(Exception $error){
            Log::info('VideoController@edit error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            if(Auth::check()){
                return $this->handleError($message);
            }
            abort(500);
        }
    }

    private function storeVideoAnalytic($video){
        if(request()->from_email == 1){
            $emailClick = new EmailClickModel;
            $emailClick->user_id = $video->user_id;
            $emailClick->video_id = $video->id;
            $emailClick->contact_id = request()->contact;
            $emailClick->save();
            $this->logActivity($video, request()->contact, 'email_click');
        }

        $pageView = new PageViewModel;
        $pageView->user_id = $video->user_id;
        $pageView->video_id = $video->id;
        $pageView->contact_id = request()->contact;
        $pageView->save();
        $this->logActivity($video, request()->contact, 'page_view');

    }

    private function logActivity($video, $contact, $type){
        $log = new GeneralStatLogModel;
        $log->user_id = $video->user_id;
        $log->type = $type;
        $log->contact_id = $contact;
        $log->video_id = $video->id;
        $log->save();
    }

    public function deleteSelectedVideo(Request $request){
        try{

            $video = VideoModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$video) {
                $message = "Video was not found";
                return response()->json(['message' => $message], 404);
            }

            if($video->videoDesign){
                $this->deleteBGImage($video->videoDesign->player_background_image);
                $this->deleteBGImage($video->videoDesign->cta_background_image);
                VideoDesignModel::where('video_id', $video->id)->delete();
            }
            if($video->videoDetails){
                VideoDetailsModel::where('video_id', $video->id)->delete();
            }

            $this->deleteThumbnailImage($video->thumbnail);
            $this->deleteVideo($video->file);
            $video->delete();
            $message = "Video deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('VideoController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function storeVideo($key, $video = null)
    {
        $filename = '';

        if (request()->hasFile($key)) {
            $name = request()->file($key)->getClientOriginalName();
            $subject = $name;
            $filename = str_replace(' ', '_',  $subject);
            $path = Paths::VIDEO_PATH;
            $imagePath = "{$path}{$filename}";

            if($video){
                $this->deleteVideo($video->file);
            }
            Storage::put($imagePath, File::get(request()->file($key)));

        }
        return $filename;
    }

    public function sendTranscriptionRequest(Request $request){
        try{
            $storedVideo =  VideoModel::where('id', $request->video_id)->first();
            if(!$storedVideo){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }
            
            $transcriptionResponse = $this->getTranscriptionRequest($storedVideo);
            
            if($transcriptionResponse['status'] == true){
                $contents = $transcriptionResponse['contents'];
                $videoTranscription = $this->storeVideoTranscriptionDetails($storedVideo, $contents);
                $message = "Transcription request added to queue.";
                return response()->json([
                    'message' => $message,
                    'videoTranscription' => $videoTranscription
                ]);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Encountered an error"
                ], 503);
            }
            

        }catch(Exception $error){
            Log::info('VideoController@sendTranscriptionRequest error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }   

    private function getTranscriptionRequest($storedVideo){
        try{
            $videoPath = storage_path('app/'). Paths::VIDEO_PATH .$storedVideo->file;
            $audioName = str_replace('.mp4', '.mp3', $storedVideo->file);
            $audioPath = storage_path('app/'). Paths::AUDIO_PATH .$audioName;

            $audio = Paths::AUDIO_PATH .$audioName;
            exec("ffmpeg -i $videoPath $audioPath");
            exec("chmod 777 $audioPath");
            $mediaURL = \Request::root().Storage::disk('local')->url($audio);

            $client = new \GuzzleHttp\Client();
            $url = "https://api.rev.ai/speechtotext/v1/jobs";

            $request = $client->request('POST', $url, [
                // 'content-type' => 'application/json',
                "headers" => [
                    "Authorization" => "Bearer " . Paths::REV_ACCESS_TOKEN,
                    'Accept'        => 'application/json',
                ],
    
                'json' => [
                    "media_url" => $mediaURL,
                    "callback_url" => route("user.videos.transcribe.callback"),
                    "metadata" => "Just a regular description"
                ]
            ]);
    
            $contents = $request->getBody()->getContents();
    
            $contents = json_decode($contents);
    
            return [
                'status' => true,
                'contents' => $contents
            ];
        }catch(Exception $error){
            Log::info('VideoController@getTranscriptionRequest error message: ' . $error->getMessage());
            return [
                'status' => false,
                'contents' => []
            ];
        }
        

        
    }

    private function storeVideoTranscriptionDetails($storedVideo, $contents){
        $transcription = TranscriptionModel::firstOrNew([
            'video_id' => $storedVideo->id,
            'user_id' => Auth::id()
        ]);
        $transcription->job_id = $contents->id;
        $transcription->save();

        return $transcription;
    }

    public function storeSRT(Request $request){
        try{
            $storedVideo =  VideoModel::where('id', $request->video_id)->first();
            if(!$storedVideo){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }
            if(!$request->hasFile('srt_file')){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. SRT File not found"
                ], 400);
            }

            $creationStatus = $this->storeSRTFile($request);

            if($creationStatus['status'] == true){
                $storedVideo->srt_file = $creationStatus['srt_file'];
                $storedVideo->save();
                return response()->json([
                    'status' => 'success',
                    'srt_file_path' => $storedVideo->srt_file,
                    'message' => "Video subtitle updated"
                ]);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request."
                ], 500);
            }

        }catch(Exception $error){
            Log::info('VideoController@storeSRT error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    private function storeSRTFile($request){
        if($request->hasfile('srt_file')){
            $srtFile = $request->file('srt_file');
            $fileName = str_replace(' ', '_',$srtFile->getClientOriginalName());
            $documentPath = Paths::SRT_FILES_PATH.$fileName;
            $vttFile = str_replace('.srt', '.vtt',$fileName);
            // dd([$documentPath, Paths::SRT_FILES_PATH.$vttFile]);
            Storage::put($documentPath, File::get($srtFile));
            $srtFullPath = storage_path('app/' .$documentPath);
            $vttFullPath = storage_path('app/' .Paths::SRT_FILES_PATH.$vttFile);
            Subtitles::convert($srtFullPath, $vttFullPath);
            return [
                'status' => true,
                'srt_file' => $fileName,
            ];

        }

        return [
            'status' => false,
            'srt_file' => '',
        ];
    }

    public function downloadCaption($videoUUID){
        try{
            $video = VideoModel::where('uuid', $videoUUID)->first();
            if(!$video){
                abort(404);
            }
            return response()->download(storage_path('app/' .Paths::SRT_FILES_PATH. $video->srt_file));

        }catch(Exception $error){
            Log::info('VideoController@downloadCaption error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function createThumbnail(Request $request){
        try{
            $storedVideo =  VideoModel::where('id', $request->video_id)->first();
            if(!$storedVideo){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }

            if($request->hasFile('image')){
                $creationStatus = $this->storeVideoThumbnail($storedVideo, $request);
            }else{
                $frameTime = $request->frame_time;
                $creationStatus = $this->createVideoThumbnail($storedVideo, $frameTime);
            }
            if($creationStatus['status'] == true){
                $storedVideo->thumbnail = $creationStatus['image'];
                $storedVideo->save();
                return response()->json([
                    'status' => 'success',
                    'thumbnail' => $storedVideo->video_thumbnail,
                    'thumbnail_path' => $storedVideo->video_thumbnail_path,
                    'message' => "Video thumbnail updated"
                ]);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request."
                ], 500);
            }

        }catch(Exception $error){
            Log::info('VideoController@createThumbnail error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    private function createVideoThumbnail($storedVideo, $frameTime){
        try{
            $ffmpeg = \FFMpeg\FFMpeg::create();
            $storedVideoPath = Storage::path(Paths::VIDEO_PATH .$storedVideo->file);
            $imageName = str_replace(' ', '_',$storedVideo->name). '.jpg';
            $thumbnailPath = Storage::path(Paths::VIDEO_THUMBNAIL_PATH .$imageName);
            $savePath = public_path().'/'.$imageName;

            $video = $ffmpeg->open($storedVideoPath);
            $file = $video
                ->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds($frameTime))
                ->save($imageName);

            rename($savePath, $thumbnailPath);
            return [
                'status' => true,
                'image' => $imageName,
            ];
        }catch(Exception $error){
            Log::info('VideoController@createVideoThumbnail error message: ' . $error->getMessage());
            return [
                'status' => false,
                'image' => '',
            ];
        }
    }

    private function createVideoGif($storedVideo){
        try{
            $ffmpeg = \FFMpeg\FFMpeg::create();
            $storedVideoPath = Storage::path(Paths::VIDEO_PATH .$storedVideo->file);
            $gifName = str_replace(' ', '_',$storedVideo->name). '.gif';
            $gifPath = Storage::path(Paths::VIDEO_GIF_PATH .$gifName);
            $savePath = public_path().'/'.$gifName;

            $video = $ffmpeg->open($storedVideoPath);
            $file = $video
                ->gif(\FFMpeg\Coordinate\TimeCode::fromSeconds(1), new \FFMpeg\Coordinate\Dimension(640, 480), 5)
                ->save($gifName);

            rename($savePath, $gifPath);
            return [
                'status' => true,
                'gif' => $gifName,
            ];
        }catch(Exception $error){
            Log::info('VideoController@createVideoGif error message: ' . $error->getMessage());
            return [
                'status' => false,
                'gif' => '',
            ];
        }
    }

    private function storeVideoThumbnail($storedVideo, $request){
        if($request->hasfile('image')){
            $image = $request->file('image');
            $fileName = str_replace(' ', '_',$image->getClientOriginalName());
            $documentPath = Paths::VIDEO_THUMBNAIL_PATH.$fileName;
            Storage::put($documentPath, File::get($image));
            $this->deleteThumbnailImage($storedVideo->thumbnail);

            return [
                'status' => true,
                'image' => $fileName,
            ];

        }

        return [
            'status' => false,
            'image' => '',
        ];
    }

    public function trimVideo(Request $request){
        try{
            $storedVideo =  VideoModel::where('id', $request->video_id)->first();
            if(!$storedVideo){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }

            $startTime = $request->start_time;
            $endTime = $request->end_time;
            $creationStatus = $this->getTrimmedVideo($storedVideo, $startTime, $endTime);
            if($creationStatus['status'] == true){
                $storedVideo->file = $creationStatus['video'];
                $storedVideo->save();
                return response()->json([
                    'status' => 'success',
                    'thumbnail' => $storedVideo->video_path,
                    'message' => "Video trimmed"
                ]);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request."
                ], 500);
            }

        }catch(Exception $error){
            Log::info('VideoController@cropVideo error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    private function getTrimmedVideo($storedVideo, $startTime, $endTime){
        try{
            $ffmpeg = \FFMpeg\FFMpeg::create();
            $storedVideoPath = Storage::path(Paths::VIDEO_PATH .$storedVideo->file);
            $videoName = $storedVideo->file;
            $videoPath = Storage::path(Paths::VIDEO_PATH .$videoName);
            $savePath = public_path().'/'.$videoName;
            $video = $ffmpeg->open($storedVideoPath);
            $video->filters()->clip(
                \FFMpeg\Coordinate\TimeCode::fromSeconds($startTime),
                \FFMpeg\Coordinate\TimeCode::fromSeconds($endTime)
            );

            $video->save(new \FFMpeg\Format\Video\X264(), $videoName);


            rename($savePath, $videoPath);
            return [
                'status' => true,
                'video' => $videoName,
            ];

        }catch(Exception $error){
            Log::info('VideoController@getTrimmedVideo error message: ' . $error->getMessage());
            return [
                'status' => false,
                'video' => '',
            ];
        }
    }

    public function autoDeleteUserVideos(){
        try{
            $videos = VideoModel::where('user_id', Auth::id())->get();
            foreach($videos as $video){
                if($video->videoDetails){
                    $now = strtotime("now");
                    $timeElasped = ($video->videoDetails->auto_delete_date <= $now)? true: false;
                    if($video->videoDetails->auto_delete == true && $timeElasped){
                        if($video->videoDesign){
                            $this->deleteBGImage($video->videoDesign->player_background_image);
                            $this->deleteBGImage($video->videoDesign->cta_background_image);
                            VideoDesignModel::where('video_id', $video->id)->delete();
                        }
                        VideoDetailsModel::where('video_id', $video->id)->delete();
                        $this->deleteThumbnailImage($video->thumbnail);
                        $this->deleteVideo($video->file);
                        $video->delete();
                    }
                }
            }

        }catch(Exception $error){
            Log::info('VideoController@autoDeleteUserVideos error message: ' . $error->getMessage());

        }
    }

    public function storeVideoShare (Request $request){
        try{
            $video = VideoModel::where('user_id', Auth::id())->where('id', $request->video_id)->first();
            if(!$video){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }

            $videoShare = VideoShareModel::firstOrNew([
                'video_id' => $request->video_id
            ]);
            $shareDetails = json_decode($request->video_share);

            $videoShare->disable_gif_animation = ($shareDetails->disable_gif_animation == 'true')? true : false;
            $videoShare->disable_url_text_encoding = ($shareDetails->disable_url_text_encoding == 'true')? true : false;
            $videoShare->disable_email_open_tracking = ($shareDetails->disable_email_open_tracking == 'true')? true : false;
            $videoShare->personalization_text = $shareDetails->personalization_text;
            $videoShare->embed_type = $shareDetails->embed_type;
            $videoShare->embed_custom_width = $shareDetails->embed_custom_width;
            $videoShare->embed_custom_height = $shareDetails->embed_custom_height;
            $videoShare->embed_disable_cta_overlay = ($shareDetails->embed_disable_cta_overlay == 'true')? true : false;
            $videoShare->embed_autoplay = ($shareDetails->embed_autoplay == 'true')? true : false;
            $videoShare->embed_sound_off = ($shareDetails->embed_sound_off == 'true')? true : false;
            $videoShare->widget_position = $shareDetails->widget_position;
            $videoShare->widget_autoplay = ($shareDetails->widget_autoplay == 'true')? true : false;
            $videoShare->widget_sound_off = ($shareDetails->widget_sound_off == 'true')? true : false;
            $videoShare->widget_show_cta = ($shareDetails->widget_show_cta == 'true')? true : false;
            $videoShare->widget_hide_in_mobile = ($shareDetails->widget_hide_in_mobile == 'true')? true : false;
            $videoShare->save();

            return response()->json([
                'status' => 'success',
                'videoShare' => $videoShare,
                'message' => "Video Sharing options updated"
            ]);

        }catch(Exception $error){
            Log::info('VideoController@storeVideoShare error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    public function concatenateVideos(Request $request){
        try{
            $mainVideo =  VideoModel::where('id', $request->video_id)->first();
            if(!$mainVideo){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }
            $videoAddition =  VideoModel::where('id', $request->input_video)->first();
            if(!$videoAddition){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }

            $response = $this->processVideoConcatenation($request);
            if($response != true){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Service unavailable"
                ], 503);
            }
            $updatedVideo =  VideoModel::where('id', $request->video_id)->first();

            return response()->json([
                'status' => 'success',
                'video' => $updatedVideo,
                'message' => "Video updated"
            ]);
            
        }catch(Exception $error){
            Log::info('VideoController@concatenateVideos error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    private function processVideoConcatenation($request){
        try{
            $mainVideo = VideoModel::where('id', $request->video_id)->first();
            $videoAddition = VideoModel::where('id', $request->input_video)->first();
            $storedMainVideoPath = Storage::path(Paths::VIDEO_PATH .$mainVideo->file);
            $videoOutput = time()."-".$mainVideo->file;
            $videoOutputPath = storage_path('app/' .Paths::VIDEO_PATH.$videoOutput);
            $storedVideoAdditionPath = Storage::path(Paths::VIDEO_PATH .$videoAddition->file);
            if($request->position == 'intro'){
                $videos = [$storedVideoAdditionPath, $storedMainVideoPath];
            }else{
                $videos = [$storedMainVideoPath, $storedVideoAdditionPath];
            }
            file_put_contents('videolist.txt', implode("\n", array_map(function ($path) {
                return 'file ' . addslashes($path);
            }, $videos)));
            
            exec("chmod 777 videolist.txt");
            exec("ffmpeg -f concat -safe 0 -i videolist.txt -vcodec copy -acodec copy $videoOutputPath");
            exec("chmod 777 $videoOutputPath");
            
            $mainVideo->file = $videoOutput;
            $mainVideo->save();
            
            exec("rm $storedMainVideoPath");

            return true;

        }catch(Exception $error){
            Log::info('VideoController@processVideoConcatenation error message: ' . $error->getMessage());
            return false;

        }
    }

    public function sendVideoMail(Request $request){
        try{
            $video = VideoModel::where('user_id', Auth::id())->where('id', $request->video_id)->first();
            if(!$video){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }

            $emailIntegration = EmailIntegrationModel::where('user_id', Auth::id())->where('id', $request->provider_id)->first();
            if(!$emailIntegration){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Email account not found"
                ], 404);
            }

            $contact = ContactModel::where('user_id', Auth::id())->where('id', $request->contact_id)->first();
            if(!$contact){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Contact not found"
                ], 404);
            }

            $template = CustomTemplateModel::where('user_id', Auth::id())->where('id', $request->template_id)->first();
            if(!$template && $request->template_id != 0){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Template not found"
                ], 404);
            }
            $emailDetails = new \stdClass();
            $emailDetails->subject = $request->subject;
            $emailDetails->to = $contact->email;
            $emailDetails->from = $emailIntegration->email;
            $emailDetails->body = $this->setEmailBody($template,$contact, $request);
            $emailDetails->integration = $emailIntegration;
            $response = $this->sendMail($emailDetails, $request);
            if($response['status'] == true){
                return response()->json([
                    'status' => 'success',
                    'message' => $response['message']
                ]);
            }

            return response()->json([
                'status' => 'failed',
                'message' => $response['message']
            ], 400);

        }catch(Exception $error){
            Log::info('VideoController@sendVideoMail error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    private function setEmailBody($template, $contact, $request){
        if($template){
            $body = parseWelcomeEmail($contact->first_name,
                $request->email_copy, $template->html);
        }else{
            $body = parseWelcomeEmail($contact->first_name,
                $request->email_copy, $request->email_body);
        }

        return $body;
    }

    private function sendMail($emailDetails, $request){
        switch($emailDetails->integration->service){
            case 'gmail_api':
                $gmailController = new GmailController;
                $response = $gmailController->sendMail($emailDetails);
                return $response;
            case 'mandrill_smtp':
                $madrillController = new MandrillController;
                $response = $madrillController->sendMail($emailDetails);
                return $response;
            case 'smtp':
                $smtpController = new SMTPController;
                $response = $smtpController->sendMail($emailDetails);
                return $response;
            case 'send_grid_smtp':
                $sendGridController = new SendGridController;
                $response = $sendGridController->sendMail($emailDetails);
                return $response;
            case 'aws_ses':
                $awsController = new AWSController;
                $response = $awsController->sendMail($emailDetails);
                return $response;
            default :
                return [
                    'status' =>false,
                    "message" => "SMPT setup not found"
                ];
        }


    }

    public function storeVideoDesign(Request $request){
        try{
            $video = VideoModel::where('user_id', Auth::id())->where('id', $request->video_id)->first();
            if(!$video){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Video not found"
                ], 404);
            }

            $videoDesign = VideoDesignModel::firstOrNew([
                'user_id' => Auth::id(),
                'video_id' => $request->video_id
            ]);

            $videoDesign->display_logo = $request->display_logo == 'true'? true : false;
            $videoDesign->player_color = $request->player_color;
            $videoDesign->default_player_color = $request->default_player_color == 'true'? true : false;
            if($request->type != 'cta' && $request->hasFile('image')){
                $videoDesign->player_background_image = $this->getBackgroundImageName($videoDesign, $request);
            }elseif ($request->player_bg_url != '') {
                $videoDesign->player_background_image = $this->getBackgroundImageNameFromURL($videoDesign, $request);
            }
            $videoDesign->default_player_background_image = $request->default_player_background_image == 'true'? true : false;
            $videoDesign->cta_color = $request->cta_color;
            $videoDesign->default_cta_color = $request->default_cta_color == 'true'? true : false;
            if($request->type == 'cta' && $request->hasFile('image')){
                $videoDesign->cta_background_image = $this->getBackgroundImageName($videoDesign, $request);
            }elseif ($request->cta_bg_url != '') {
                $videoDesign->cta_background_image = $this->getBackgroundImageNameFromURL($videoDesign, $request);
            }
            $videoDesign->default_cta_background_image = $request->default_cta_background_image == 'true'? true : false;
            $videoDesign->save();

            return response()->json([
                'status' => 'success',
                'design' => $videoDesign,
                'message' => "Video design updated"
            ]);


        }catch(Exception $error){
            Log::info('VideoController@storeVideoDesign error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    private function getBackgroundImageName($videoDesign, $request){
        if($request->hasfile('image')){
            $image = $request->file('image');
            $fileName = str_replace(' ', '_',$image->getClientOriginalName());
            $documentPath = Paths::BACKGROUND_IMAGE_PATH.$fileName;
            Storage::put($documentPath, File::get($image));

            if($request->type == 'cta'){
                $this->deleteBGImage($videoDesign->cta_background_image);
            }else{
                $this->deleteBGImage($videoDesign->player_background_image);
            }

            return $fileName;
        }

        return '';
    }

    private function getBackgroundImageNameFromURL($videoDesign, $request){
        if($request->type == 'cta'){
            $url = $request->cta_bg_url;
        }else{
            $url = $request->player_bg_url;
        }
        $contents = file_get_contents($url);
        $fileName = substr($url, strrpos($url, '/') + 1);
        $documentPath = Paths::BACKGROUND_IMAGE_PATH.$fileName;
        Storage::put($documentPath, $contents);

        if($request->type == 'cta'){
            $this->deleteBGImage($videoDesign->cta_background_image);
        }else{
            $this->deleteBGImage($videoDesign->player_background_image);
        }

        return $fileName;

    }


    private function deleteVideo($videoName)
    {
        $path = Paths::VIDEO_PATH;
        $filename = $path . $videoName;

        if (Storage::has($filename)) {
            Storage::delete($filename);
        }
    }

    private function deleteBGImage($imageName)
    {
        if($imageName){
            $path = Paths::BACKGROUND_IMAGE_PATH;
            $filename = $path . $imageName;

            if (Storage::has($filename)) {
                Storage::delete($filename);
            }
        }

    }

    private function deleteThumbnailImage($imageName)
    {
        if($imageName){
            $path = Paths::VIDEO_THUMBNAIL_PATH;
            $filename = $path . $imageName;

            if (Storage::has($filename)) {
                Storage::delete($filename);
            }
        }

    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
