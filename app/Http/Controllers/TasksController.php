<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaskModel;
use App\Models\DealsModel;
use App\Models\VideoModel;
use App\Helpers\Paths;
use App\Models\ContactModel;
use Illuminate\Support\Str;
use Auth;
use Exception;
use Validator;
use Log, Storage;

class TasksController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
		$data = [
			'page' => 'tasks',
			'sub' => '',
			'tasks' => '',
			'deals' => '',
			'contacts' => '',
			'videos' => ''
		];

		$contacts = ContactModel::select('id', 'first_name', 'last_name', 'email')->where('user_id', Auth::id())->get();
		$deals = DealsModel::select('id', 'title')->where('user_id', Auth::id())->get();
		$videos = VideoModel::select('id', 'file')->where('user_id', Auth::id())->get();
		$tasks = $this->getTasks(true);

		if ($tasks) {
			$data['tasks'] = $tasks;
		}

		$data['deals'] = $deals;
		$data['videos'] = $videos;
		$data['contacts'] = $contacts;

		return view('App.tasks', $data);
	}

	public function getTasks($index = false){
		try {
			$tasks = TaskModel::where('user_id', Auth::id())->get();
			if($index == true){
				return $tasks;
			}
			return response()->json([
				'status_code' => 200,
				"tasks" => $tasks,
			], 200);
		} catch (Exception $error) {
			Log::info('TasksController@index error message: ' . $error->getMessage());
		}
	}


	public function create(Request $request){
		try {
			$task = new TaskModel();
			
			$validator = $this->validator($request->all());

			if ($validator->fails()) {
				return response()->json([
					'error' => true,
					'errors' => $validator->errors(),
					'status_code' => 400,
					"message" => "incomplete or invalid form values",
				], 400);
			}
			$task->user_id = Auth::id();
			$task->title = $request->title;
			$task->description = $request->description;
			$task->associated_contact_id = $request->associated_contact_id;
			$task->associated_video_id = $request->associated_video_id;
			$task->associated_deal_id = $request->associated_deal_id;

			if (isset($request->assignee_id)) {
				$task->assignee_id = $task->assignee_id;
			} else {
				$task->assignee_id = "";
			}
			
			$task->due_after = $request->due_after;
			$task->status = $request->status;

			$task->save();

			$message = 'Request completed';
			return response()->json([
				'status_code' => 200,
				"message" => $message,
			], 200);
		} catch (Exception $error) {
			Log::info('TasksController@create error message: ' . $error->getMessage());
			$message = 'Unable to create Resource. Encountered an error.';
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
		}
	}

	public function edit(Request $request) {
		try {
			$task = TaskModel::where('id', $request->id)->first();
			
			$validator = $this->validator($request->all());

			if ($validator->fails()) {
				return response()->json([
					'error' => true,
					'errors' => $validator->errors(),
					'status_code' => 400,
					"message" => "incomplete or invalid form values",
				], 400);
			}
			$task->title = $request->title;
			$task->description = $request->description;
			$task->associated_contact_id = $request->associated_contact_id;
			$task->associated_video_id = $request->associated_video_id;
			$task->associated_deal_id = $request->associated_deal_id;
			if (isset($request->assignee_id)) {
				$task->assignee_id = $task->assignee_id;
			} else {
				$task->assignee_id = "";
			}
			$task->due_after = $request->due_after;
			$task->status = $request->status;

			$task->save();

			$message = 'Request completed';

			return response()->json([
				'status_code' => 200,
				"message" => $message,
			], 200);
		} catch (Exception $error) {
			Log::info('TasksController@edit error message: ' . $error->getMessage());
			$message = 'Unable to edit Resource. Encountered an error.';
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
		}
	}

	public function delete(Request $request){
		try {
			$task = TaskModel::where('id', $request->id)->first();
			$task->delete();
			$message = 'Request completed';
			return response()->json([
				'status_code' => 200,
				"message" => $message,
			], 200);
		}catch (Exception $error) {
			Log::info('TasksController@delete error message: ' . $error->getMessage());
			$message = 'Unable to delete Resource. Encountered an error.';
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
		}
		
	}

	protected function validator(array $data) {
		return Validator::make($data, [
			'title' => 'required|string|max:255',
			'creator' => 'string|nullable',
			'assignee_id' => 'numeric|nullable',
			'due_after' => 'nullable',
			'status' => 'required|string'
		]);
	}

	private function handleError($message) {
		Session::put('errorMessage', $message);
		return redirect()->back();
	}
}
