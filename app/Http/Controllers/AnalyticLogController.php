<?php

namespace App\Http\Controllers;

use App\Models\VideoModel;
use App\Models\CTAClickModel;
use App\Models\EmailOpenModel;
use App\Models\EmailClickModel;
use App\Models\VideoViewModel;
use App\Models\ReactionClickModel;
use App\Models\GeneralStatLogModel;
use Illuminate\Http\Request;
use Auth, Session, Exception, Log, Storage, File;


class AnalyticLogController extends Controller
{
    public function logAnalytic(Request $request){
        try{
            $storedVideo =  VideoModel::where('id', $request->video_id)->first();
            if($storedVideo){
                if($request->type == 'cta'){
                    $log = new CTAClickModel;
                    $log->user_id = $storedVideo->user_id;
                    $log->cta_id = $request->cta_id;
                    $log->contact_id = $request->contact_id;
                    $log->video_id = $storedVideo->id;
                    $log->save();
                    $logDetails = [
                        'contact_id' => $request->contact_id,
                        'element_id' => $request->cta_id,
                        'user_id' => $storedVideo->user_id,
                        'type' => 'cta_clicked'
                    ];
                    automateWorkflows($logDetails);

                    $this->logActivity($storedVideo, $request);
                }else if ($request->type == 'reaction') {
                    $log = new ReactionClickModel;
                    $log->user_id = $storedVideo->user_id;
                    $log->reaction = $request->reaction;
                    $log->contact_id = $request->contact_id;
                    $log->video_id = $storedVideo->id;
                    $log->save();
                    $this->logActivity($storedVideo, $request);
                } else if ($request->type == 'video_view') {
                    $log = new VideoViewModel;
                    $log->user_id = $storedVideo->user_id;
                    $log->contact_id = $request->contact_id;
                    $log->video_id = $storedVideo->id;
                    $log->save();

                    $logDetails = [
                        'contact_id' => $request->contact_id,
                        'element_id' => $storedVideo->id,
                        'user_id' => $storedVideo->user_id,
                        'type' => 'video_watched'
                    ];

                    automateWorkflows($logDetails);
                    $this->logActivity($storedVideo, $request);
                }
            }



        }catch(Exception $error){
            Log::info('AnalyticLogController@logAnalytic error message: ' . $error->getMessage());

        }
    }

    private function logActivity($storedVideo, $request){
        $log = new GeneralStatLogModel;
        $log->user_id = $storedVideo->user_id;
        $log->type = $request->type;
        $log->contact_id = $request->contact_id;
        $log->video_id = $storedVideo->id;
        $log->save();
    }

    public function getGraphData(Request $request){
        try{
            $video = VideoModel::where('id', $request->video_id)->first();
            if(!$video){
                $message = 'Unable to get Resource. Video not found.';
                 return response()->json([
                    'status' => 'error',
                    'message' => $message
                ], 404);
            }
            $filter = $request->filter;
            $type = $request->type;
            if(!$filter || !$type){
                $message = 'Unable to get Resource. Incorrect filter.';
                 return response()->json([
                    'status' => 'error',
                    'message' => $message
                ], 400);
            }
            if($type == 'videoViews'){
                $graphClass = new AnalyticsController;
                $graph = $graphClass->getStatistics($filter, $video);
                return response()->json([
                    'status' => 'success',
                    'graph' => $graph
                ]);

            }else{
                $generalStatsClass = new GeneralStatsController;
                $generalStats = $generalStatsClass->getStatistics($filter, $video);
                return response()->json([
                    'status' => 'success',
                    'generalStats' => $generalStats
                ]);
            }



        }catch(Exception $error){
            Log::info('AnalyticLogController@getGraphData error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return response()->json([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    public function logEmailOpen($videoUUID){
        try{
            $video = VideoModel::where('uuid', $videoUUID)->first();
            if(!$video){
                $message = 'Unable to get Resource. Video not found.';
                abort(404);
            }
            if(request()->image_hash){
                $this->storeEmailOpenAnalytic($video);
            }

            $pathToFile = public_path('img/track-image.png');
            return response()->file($pathToFile);


        }catch(Exception $error){
            Log::info('VideoController@edit error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            abort(500);
        }
    }

    private function storeEmailOpenAnalytic($video){
        $emailClick = new EmailOpenModel;
        $emailClick->user_id = $video->user_id;
        $emailClick->video_id = $video->id;
        $emailClick->contact_id = request()->image_hash;
        $emailClick->save();

        $log = new GeneralStatLogModel;
        $log->user_id = $video->user_id;
        $log->type = 'email_open';
        $log->contact_id = request()->image_hash;
        $log->video_id = $video->id;
        $log->save();
    }
}
