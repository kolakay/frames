<?php

namespace App\Http\Controllers;

use Auth, Session, Exception, Log;

use App\Models\VideoModel;
use Illuminate\Http\Request;

class ShareController extends Controller
{
    public function index($videoUUID){
        try{
            $video = VideoModel::where('uuid', $videoUUID)->first();
            if(!$video){
                $message = 'Unable to get Resource. Video not found.';
                if(Auth::check()){
                    return $this->handleError($message);
                }
                abort(404);
            }

            $data = [
                'video' => $video,
            ];
            return view('App.Video.share', $data);

        }catch(Exception $error){
            Log::info('VideoController@edit error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            if(Auth::check()){
                return $this->handleError($message);
            }
            abort(500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
