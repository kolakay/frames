<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Auth, Session, Exception, Log;
use App\User;
use App\Models\EmailIntegrationModel;

use Illuminate\Http\Request;

class BroadcastSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        try{

            $configurations = EmailIntegrationModel::where('user_id', Auth::id())->get();


            $data = [
                'page' => 'team',
                'sub' => 'campaign_settings',
                'configurations' => $configurations,
            ];
            return view('App.Settings.broadcast_settings', $data);

        }catch(Exception $error){
            Log::info('BroadcastSettingsController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }
    public function create(Request $request){
        try{
            $configuration = new EmailIntegrationModel;
            $configuration->user_id = Auth::id();
            $configuration->service = $request->service;
            $configuration->type = $request->type;
            $configuration->email = $request->email;
            $configuration->sendgrid_api_key = $request->sendgrid_api_key;
            $configuration->mandrill_username = $request->mandrill_username;
            $configuration->mandrill_password = $request->mandrill_password;

            $configuration->smtp_host = $request->smtp_host;
            $configuration->smtp_port = $request->smtp_port;
            $configuration->smtp_username = $request->smtp_username;
            $configuration->smtp_password = $request->smtp_password;
            $configuration->smtp_encryption = $request->smtp_encryption;

            $configuration->ses_key = $request->ses_key;
            $configuration->ses_secret = $request->ses_secret;
            $configuration->ses_region = $request->ses_region;

            $configuration->cc = $request->cc;
            $configuration->bcc = $request->bcc;
            $configuration->limit_per_day = $request->limit_per_day;
            $configuration->is_active = $request->is_active == "true";
            $configuration->save();

            return response()->json([
                'message' => "Configuration was saved successfully",
                'configuration' => $configuration

            ]);

        }catch(Exception $error){
            Log::info('ConnectionTagsController@create error message: ' . $error->getMessage());
            $message = 'Unable to store Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function update(Request $request){
        try{

            $configuration = EmailIntegrationModel::where('user_id', Auth::id())->where('id', $request->id)->first();
            if (!$configuration) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Configuration not found",
                ], 404);
            }

            $configuration->email = $request->email;
            $configuration->sendgrid_api_key = $request->sendgrid_api_key;
            $configuration->mandrill_username = $request->mandrill_username;
            $configuration->mandrill_password = $request->mandrill_password;
            $configuration->smtp_host = $request->smtp_host;
            $configuration->smtp_port = $request->smtp_port;
            $configuration->smtp_username = $request->smtp_username;
            $configuration->smtp_password = $request->smtp_password;
            $configuration->smtp_encryption = $request->smtp_encryption;
            $configuration->ses_key = $request->ses_key;
            $configuration->ses_secret = $request->ses_secret;
            $configuration->ses_region = $request->ses_region;
            $configuration->cc = $request->cc;
            $configuration->bcc = $request->bcc;
            $configuration->limit_per_day = $request->limit_per_day;
            $configuration->is_active = $request->is_active == "true";
            $configuration->save();


            return response()->json([
                'error' => false,
                'configuration' => $configuration,
                'message' => "Configuration was updated successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('ConnectionTagsController@update error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
