<?php

namespace App\Http\Controllers\Assets;

use Auth, Session, Exception, Log, Storage, File, Response;
use App\User;
use App\Helpers\Paths;
use App\Models\CTAModel;
use App\Models\FormModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CTAController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('download');
    }

    public function index(){
        try{

            $ctas = CTAModel::where('user_id', Auth::id())->get();
            $forms = FormModel::where('user_id', Auth::id())->get();

            $data = [
                'page' => 'assets',
                'sub' => 'ctas',
                'forms' => $forms,
                'ctas' => $ctas,
            ];
            return view('App.Assets.ctas', $data);

        }catch(Exception $error){
            Log::info('CTAController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function create(Request $request){
        try{

            $cta = new CTAModel;
            $cta->user_id = Auth::id();
            $cta->name = $request->name;
            $cta->type = $request->type;
            $this->updateDefaultCTA($request);
            $cta->is_default = $request->is_default == 'true' ? true : false;
            $cta->description = $request->description;

            $cta->email = $request->email;
            $cta->email_button_text = $request->email_button_text;
            $cta->email_custom_button_text = $request->email_custom_button_text;

            $cta->url = $request->url;
            $cta->url_tab = $request->url_tab;
            $cta->url_button_text = $request->url_button_text;
            $cta->url_custom_button_text = $request->url_custom_button_text;

            $cta->phone_action = $request->phone_action;
            $cta->phone = $request->phone;
            $cta->phone_button_text = $request->phone_button_text;
            $cta->phone_custom_button_text = $request->phone_custom_button_text;

            $cta->chat_platform = $request->chat_platform;
            $cta->chat_username = $request->chat_username;
            $cta->chat_button_text = $request->chat_button_text;
            $cta->chat_custom_button_text = $request->chat_custom_button_text;

            $cta->calendar_platform = $request->calendar_platform;
            $cta->calendar_username = $request->calendar_username;
            $cta->calendar_id = $request->calendar_id;
            $cta->calendar_hash = $request->calendar_hash;
            $cta->calendar_button_text = $request->calendar_button_text;
            $cta->calendar_custom_button_text = $request->calendar_custom_button_text;

            $cta->form = $request->form;
            $cta->form_button_text = $request->form_button_text;
            $cta->form_custom_button_text = $request->form_custom_button_text;

            $cta->iframe_title = $request->iframe_title;
            $cta->iframe_source = $request->iframe_source;
            $cta->iframe_height = $request->iframe_height;
            $cta->iframe_scroll = $request->iframe_scroll == 'true' ? true : false;
            $cta->iframe_button_text = $request->iframe_button_text;
            $cta->iframe_custom_button_text = $request->iframe_custom_button_text;

            $cta->document_title = $request->document_title;
            $cta->document_button_text = $request->document_button_text;
            $cta->document_custom_button_text = $request->document_custom_button_text;
            $cta->document_file = $request->document_file;

            $cta->product_titles = $request->product_titles;
            $cta->product_button_text = $request->product_button_text;
            $cta->product_custom_button_text = $request->product_custom_button_text;

            $cta->web_page_title = $request->web_page_title;
            $cta->web_page_url = $request->web_page_url;
            $cta->web_page_height = $request->web_page_height;
            $cta->web_page_scroll = $request->web_page_scroll == 'true' ? true : false;
            $cta->web_page_button_text = $request->web_page_button_text;
            $cta->web_page_custom_button_text = $request->web_page_custom_button_text;

            $cta->save();
            return response()->json([
                'message' => "CTA was saved successfully",
                'cta' => $cta

            ]);

        }catch(Exception $error){
            Log::info('CTAController@create error message: ' . $error->getMessage());
            $message = 'Unable to create Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function update(Request $request){
        try{

            $cta = CTAModel::where('user_id', Auth::id())->where('id', $request->id)->first();

            if (!$cta) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "CTA not found",
                ], 404);
            }
            // $cta->user_id = Auth::id();
            $cta->name = $request->name;
            $cta->type = $request->type;

            $this->updateDefaultCTA($request);

            $cta->is_default = $request->is_default == 'true' ? true : false;
            $cta->description = $request->description;

            $cta->email = $request->email;
            $cta->email_button_text = $request->email_button_text;
            $cta->email_custom_button_text = $request->email_custom_button_text;

            $cta->url = $request->url;
            $cta->url_tab = $request->url_tab;
            $cta->url_button_text = $request->url_button_text;
            $cta->url_custom_button_text = $request->url_custom_button_text;

            $cta->phone_action = $request->phone_action;
            $cta->phone = $request->phone;
            $cta->phone_button_text = $request->phone_button_text;
            $cta->phone_custom_button_text = $request->phone_custom_button_text;

            $cta->chat_platform = $request->chat_platform;
            $cta->chat_username = $request->chat_username;
            $cta->chat_button_text = $request->chat_button_text;
            $cta->chat_custom_button_text = $request->chat_custom_button_text;

            $cta->calendar_platform = $request->calendar_platform;
            $cta->calendar_username = $request->calendar_username;
            $cta->calendar_id = $request->calendar_id;
            $cta->calendar_hash = $request->calendar_hash;
            $cta->calendar_button_text = $request->calendar_button_text;
            $cta->calendar_custom_button_text = $request->calendar_custom_button_text;

            $cta->form = $request->form;
            $cta->form_button_text = $request->form_button_text;
            $cta->form_custom_button_text = $request->form_custom_button_text;

            $cta->iframe_title = $request->iframe_title;
            $cta->iframe_source = $request->iframe_source;
            $cta->iframe_height = $request->iframe_height;
            $cta->iframe_scroll = $request->iframe_scroll == 'true' ? true : false;
            $cta->iframe_button_text = $request->iframe_button_text;
            $cta->iframe_custom_button_text = $request->iframe_custom_button_text;

            $cta->document_title = $request->document_title;
            $cta->document_button_text = $request->document_button_text;
            $cta->document_custom_button_text = $request->document_custom_button_text;
            $cta->document_file = $request->document_file;

            $cta->product_titles = $request->product_titles;
            $cta->product_button_text = $request->product_button_text;
            $cta->product_custom_button_text = $request->product_custom_button_text;

            $cta->web_page_title = $request->web_page_title;
            $cta->web_page_url = $request->web_page_url;
            $cta->web_page_height = $request->web_page_height;
            $cta->web_page_scroll = $request->web_page_scroll == 'true' ? true : false;
            $cta->web_page_button_text = $request->web_page_button_text;
            $cta->web_page_custom_button_text = $request->web_page_custom_button_text;

            $cta->save();
            return response()->json([
                'message' => "CTA was update successfully",
                'cta' => $cta

            ]);

        }catch(Exception $error){
            Log::info('CTAController@create error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function delete(Request $request){
        try{

            $cta = CTAModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$cta) {
                $message = "CTA was not found";
                return response()->json(['message' => $message], 404);
            }

            $cta->delete();
            $message = "CTA deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('CTAController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function updateDefaultCTA($request){
        $isDefault = $request->is_default == 'true' ? true : false;
        if($isDefault){
            $defaultCTA = CTAModel::where('user_id', Auth::id())
            ->where('id', '!=', $request->id)->where('is_default', true)->first();
            if($defaultCTA){
                $defaultCTA->is_default = false;
                $defaultCTA->save();
            }
        }
    }



    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }

    public function storeDocument(Request $request){
        try{
            $prevDocumentName = $request->prev_document;
            $filename = '';
            if(!$request->hasFile('document')){
                return $prevDocumentName;
            }
            // list($name, $ext) = [request()->file('document')->getClientOriginalName(),  request()->file('document')->extension()];
            $fullName = request()->file('document')->getClientOriginalName();
            $filename = str_replace(' ', '_',  $fullName);
            $path = Paths::DOCUMENT_PATH;
            $imagePath = "{$path}{$filename}";
            $this->deleteFile($prevDocumentName);
            Storage::put($imagePath, File::get(request()->file('document')));

            return response()->json([
                'status' => 'success',
                'message' => 'File Uploaded',
                'document_file'=> $filename
            ]);
        }catch(Exception $error){
            Log::info('CTAController@create error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }

    }

    private function deleteFile($prevDocumentName){
        if($prevDocumentName != ''){
            if(Storage::has(Paths::DOCUMENT_PATH .$prevDocumentName)){
                Storage::delete(Paths::DOCUMENT_PATH.$prevDocumentName);
            }
        }
    }

    public function download($documentName){
        try{
            if(Storage::has(Paths::DOCUMENT_PATH .$documentName)){
                return Response::download(storage_path('app/' . Paths::DOCUMENT_PATH . $documentName));
            }
            abort(404);
        }catch(Exception $error){
            Log::info('CTAController@download error message: ' . $error->getMessage());
            abort(500);
        }
    }
}
