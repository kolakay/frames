<?php

namespace App\Http\Controllers\Assets;

use Auth, Session, Exception, Log;
use App\User;
use App\Models\FormModel;
use App\Models\TagModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        try{

            $forms = FormModel::where('user_id', Auth::id())->get();
            $tags = TagModel::where('user_id', Auth::id())->get();

            $data = [
                'page' => 'assets',
                'sub' => 'forms',
                'tags' => $tags,
                'forms' => $forms,
            ];
            return view('App.Assets.forms', $data);

        }catch(Exception $error){
            Log::info('FormController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function create(Request $request){
        try{

            $form = new FormModel;
            $form->user_id = Auth::id();
            $form->name = $request->name;
            $form->title = $request->title;
            $form->description = $request->description;

            $form->button_text = $request->button_text;
            $form->custom_button_text = $request->custom_button_text;
            $form->fields = json_decode($request->fields);
            $form->complete_action = $request->complete_action;
            $form->tags = json_decode($request->tags);
            $form->url = $request->url;
            $form->message = $request->message;

            $form->save();
            return response()->json([
                'message' => "Form was saved successfully",
                'form' => $form

            ]);

        }catch(Exception $error){
            Log::info('FormController@create error message: ' . $error->getMessage());
            $message = 'Unable to create Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function update(Request $request){
        try{

            $form = FormModel::where('user_id', Auth::id())->where('id', $request->id)->first();

            if (!$form) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Form not found",
                ], 404);
            }
            $form->user_id = Auth::id();
            $form->name = $request->name;
            $form->title = $request->title;
            $form->description = $request->description;

            $form->button_text = $request->button_text;
            $form->custom_button_text = $request->custom_button_text;
            $form->fields = json_decode($request->fields);
            $form->tags = json_decode($request->tags);
            $form->complete_action = $request->complete_action;
            $form->url = $request->url;
            $form->message = $request->message;

            $form->save();

            return response()->json([
                'message' => "Form was updated successfully",
                'form' => $form

            ]);

        }catch(Exception $error){
            Log::info('FormController@create error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function delete(Request $request){
        try{

            $form = FormModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$form) {
                $message = "Form was not found";
                return response()->json(['message' => $message], 404);
            }

            $form->delete();
            $message = "Form deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('FormController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
