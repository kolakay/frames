<?php

namespace App\Http\Controllers\Assets;

use Auth, Session, Exception, Log;
use App\User;
use App\Models\CustomTemplateModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmailTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        try{

            $templates = CustomTemplateModel::select('name','id')->where('user_id', Auth::id())->get();
            $defaultTemplates = [];

            $data = [
                'page' => 'assets',
                'sub' => 'templates',
                'defaultTemplates' => $defaultTemplates,
                'templates' => $templates,
            ];
            return view('App.Assets.templates', $data);

        }catch(Exception $error){
            Log::info('FormController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function create(Request $request){
        try{

            if(!$request->name){
                $message = "Template Details are required";
                return response()->json(['message' => $message], 400);
            }

            $template = new CustomTemplateModel;
            $template->user_id = Auth::id();
            $template->name = $request->name;
            $template->save();

            $url = route('user.builder', $template->id);

            return response()->json([
                'error' => false,
                'template' => $template,
                'url' => $url,
                'message' => "Template was created successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('EmailTemplateController@create error message: ' . $error->getMessage());
            $message = 'Unable to create resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function update(Request $request){
        try{

            if(!$request->name || !$request->id){
                $message = "Template Details are required";
                return response()->json(['message' => $message], 400);
            }

            $template = CustomTemplateModel::where('user_id', Auth::id())->where('id', $request->id)->first();
            if (!$template) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Template not found",
                ], 404);
            }

            $template->name = $request->name;
            $template->save();

            return response()->json([
                'error' => false,
                'template' => $template,
                'message' => "Template title was updated successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('EmailTemplateController@update error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function clone(Request $request){
        try{

            if(!$request->name || !$request->id){
                $message = "Template Details are required";
                return response()->json(['message' => $message], 400);
            }

            $template = CustomTemplateModel::where('user_id', Auth::id())->where('id', $request->id)->first();
            if (!$template) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Template not found",
                ], 404);
            }

            $newTemplate = $template->replicate();
            $newTemplate->save();

            return response()->json([
                'error' => false,
                'template' => $newTemplate,
                'message' => "Template was cloned successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('EmailTemplateController@update error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function delete(Request $request){
        try{

            $template = CustomTemplateModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$template) {
                $message = "Template was not found";
                return response()->json(['message' => $message], 404);
            }

            $template->delete();
            $message = "Template deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('EmailTemplateController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                "message" => $message,
            ], 500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
