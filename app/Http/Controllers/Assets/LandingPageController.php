<?php

namespace App\Http\Controllers\Assets;

use App\Models\LandingPageModel;
use App\Models\CustomTemplateModel;
use App\Http\Controllers\Controller;
use App\Models\VideoModel;
use Illuminate\Http\Request;
use Auth, Session, Exception, Log;

class LandingPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        try{

            $landingPages = LandingPageModel::where('user_id', Auth::id())->get();
            $videos = VideoModel::where('user_id', Auth::id())->get();
            $templates = CustomTemplateModel::select('name','id')->where('user_id', Auth::id())->get();

            $data = [
                'page' => 'assets',
                'videos' => $videos,
                'sub' => 'landing_pages',
                'templates' => $templates,
                'landingPages' => $landingPages,
            ];
            return view('App.Assets.landing-pages', $data);

        }catch(Exception $error){
            Log::info('LandingPageController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function create(Request $request){
        try{
            if(!$request->name || !$request->video_id || !$request->template_id){
                $message = "Landing page name and video and template are required";
                return response()->json(['message' => $message], 400);
            }

            $prevPage = LandingPageModel::where('user_id', Auth::id())
                ->where('name', $request->name)->first();
            if($prevPage){
                $message = "Landing page name already taken";
                return response()->json(['message' => $message], 400);
            }

            $landingPage = new LandingPageModel;
            $landingPage->user_id = Auth::id();
            $landingPage->video_id = $request->video_id;
            $landingPage->slug = str_replace(" ","-",$request->name);
            $landingPage->name = $request->name;
            $landingPage->template_id = $request->template_id;
            $landingPage->is_active = $request->is_active == 'true'? true : false;
            $landingPage->save();
            return response()->json([
                'message' => "Landing page was saved successfully",
                'landingPage' => $landingPage

            ]);

        }catch(Exception $error){
            Log::info('LandingPageController@create error message: ' . $error->getMessage());
            $message = 'Unable to create Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function update(Request $request){
        try{

            if(!$request->name || !$request->id){
                $message = "Landing Page Details are required";
                return response()->json(['message' => $message], 400);
            }

            $prevPage = LandingPageModel::where('user_id', Auth::id())
                ->where('name', $request->name)
                ->where('id', '!=', $request->id)->first();
            if($prevPage){
                $message = "Landing page name already taken";
                return response()->json(['message' => $message], 400);
            }

            $landingPage = LandingPageModel::where('user_id', Auth::id())->where('id', $request->id)->first();
            if (!$landingPage) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Landing Page not found",
                ], 404);
            }

            $landingPage->slug = str_replace(" ","-",$request->name);
            $landingPage->name = $request->name;
            $landingPage->is_active = $request->is_active == 'true'? true : false;
            $landingPage->save();

            return response()->json([
                'error' => false,
                'landingPage' => $landingPage,
                'message' => "Landing page was updated successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('LandingPageController@update error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function delete(Request $request){
        try{

            $landingPage = LandingPageModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$landingPage) {
                $message = "Landing Page was not found";
                return response()->json(['message' => $message], 404);
            }

            $landingPage->delete();
            $message = "Landing Page deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('LandingPageController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function page($pageSlug){

    }

    // public function edit($pageId){
    //     try{

    //         $landingPage = LandingPageModel::where('user_id', Auth::id())->where('id', $pageId)->first();
    //         $videos = VideoModel::where('user_id', Auth::id())->get();
            
    //         $data = [
    //             'page' => 'assets',
    //             'videos' => $videos,
    //             'sub' => 'landing_pages',
    //             'landingPages' => $landingPages,
    //         ];
    //         return view('App.Assets.landing-pages', $data);

    //     }catch(Exception $error){
    //         Log::info('LandingPageController@index error message: ' . $error->getMessage());
    //         $message = 'Unable to get Resource. Encountered an error.';
    //         return $this->handleError($message);
    //     }
    // }

    public function clone(Request $request){
        try{

            if(!$request->name || !$request->id){
                $message = "Landing page Details are required";
                return response()->json(['message' => $message], 400);
            }

            $page = LandingPageModel::where('user_id', Auth::id())->where('id', $request->id)->first();
            if (!$page) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Landing page not found",
                ], 404);
            }

            $newPage = $page->replicate();
            $newPage->name = $page->name . " copy";
            $newPage->slug = $page->slug . "-copy";

            $newPage->save();

            return response()->json([
                'error' => false,
                'landingPage' => $newPage,
                'message' => "Landing page was cloned successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('LandingPageController@update error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
