<?php

namespace App\Http\Controllers;

use Auth, Session, Exception, Log;

use App\Http\Controllers\Integrations\AWSController;
use App\Http\Controllers\Integrations\SMTPController;
use App\Http\Controllers\Integrations\GmailController;
use App\Http\Controllers\Integrations\SendGridController;
use App\Http\Controllers\Integrations\MandrillController;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public static function sendMail($emailDetails, $request){
        switch($emailDetails->integration->service){
            case 'gmail_api':
                $gmailController = new GmailController;
                $response = $gmailController->sendMail($emailDetails);
                return $response;
            case 'mandrill_smtp':
                $madrillController = new MandrillController;
                $response = $madrillController->sendMail($emailDetails);
                return $response;
            case 'smtp':
                $smtpController = new SMTPController;
                $response = $smtpController->sendMail($emailDetails);
                return $response;
            case 'send_grid_smtp':
                $sendGridController = new SendGridController;
                $response = $sendGridController->sendMail($emailDetails);
                return $response;
            case 'aws_ses':
                $awsController = new AWSController;
                $response = $awsController->sendMail($emailDetails);
                return $response;
            default :
                return [
                    'status' =>false,
                    "message" => "SMPT setup not found"
                ];
        }


    }
}
