<?php

namespace App\Http\Controllers;

use App\Models\CampaignLogModel;
use Auth, Session, Exception, Log;
use App\User;
use App\Models\ContactModel;
use App\Models\VideoModel;
use App\Models\ConnectionTagModel;
use App\Models\EmailCampaignModel;
use Illuminate\Http\Request;
use App\Models\CustomTemplateModel;
use App\Models\EmailIntegrationModel;

class EmailCampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        try{

            $campaigns = EmailCampaignModel::where('user_id', Auth::id())->get();

            $data = [
                'page' => 'campaigns',
                'sub' => 'emails',
                'campaigns' => $campaigns
            ];
            return view('App.Campaigns.emails', $data);

        }catch(Exception $error){
            Log::info('EmailCampaignController@emailCampaigns error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function createEmailCampaign(){
        try{

            $campaign = [];
            $videos = VideoModel::where('user_id', Auth::id())->get();
            $tags = ConnectionTagModel::where('user_id', Auth::id())->get();
            $campaigns = EmailCampaignModel::where('user_id', Auth::id())->get();
            $contacts = ContactModel::where('user_id', Auth::id())->get();
            $emailTemplates = CustomTemplateModel::where('user_id', Auth::id())->get();
            $emailIntegrations = EmailIntegrationModel::where('user_id', Auth::id())->where('is_active', true)->get();
            $data = [
                'page' => 'campaigns',
                'sub' => 'emails',
                'tags' => $tags,
                'campaign' => $campaign,
                'videos' => $videos,
                'contacts' => $contacts,
                'campaigns' => $campaigns,
                'emailTemplates' => $emailTemplates,
                'emailIntegrations' => $emailIntegrations,
            ];
            return view('App.Campaigns.create-email', $data);

        }catch(Exception $error){
            Log::info('EmailCampaignController@createEmailCampaign error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function editEmailCampaign($campaignId){
        try{

            $campaign = EmailCampaignModel::where('user_id', Auth::id())
                ->where('id', $campaignId)
                ->first();
            $videos = VideoModel::where('user_id', Auth::id())->get();
            $tags = ConnectionTagModel::where('user_id', Auth::id())->get();
            $campaigns = EmailCampaignModel::where('user_id', Auth::id())->get();
            $contacts = ContactModel::where('user_id', Auth::id())->get();
            $emailTemplates = CustomTemplateModel::where('user_id', Auth::id())->get();
            $emailIntegrations = EmailIntegrationModel::where('user_id', Auth::id())->where('is_active', true)->get();
            $data = [
                'page' => 'campaigns',
                'sub' => 'emails',
                'tags' => $tags,
                'campaign' => $campaign,
                'videos' => $videos,
                'contacts' => $contacts,
                'campaigns' => $campaigns,
                'emailTemplates' => $emailTemplates,
                'emailIntegrations' => $emailIntegrations,
            ];
            return view('App.Campaigns.create-email', $data);

        }catch(Exception $error){
            Log::info('EmailCampaignController@createEmailCampaign error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function saveEmailCampaign(Request $request){
        try{
            if($request->id != ''){
                $campaign = EmailCampaignModel::where('user_id', Auth::id())
                    ->where('id', $request->id)->first();
                if(!$campaign){
                    return response()->json([
                        'status' => 'error',
                        'message' => "Unable to complete request. Campaign not found"
                    ], 404);
                }
            }else{
                $campaign = new EmailCampaignModel;
            }
            $campaign->user_id = Auth::id();
            $campaign->title = $request->title;
            $campaign->provider_id = $request->provider_id;
            $campaign->video_id = $request->video_id;
            $campaign->from_name = $request->from_name;
            $campaign->subject = $request->subject;
            $campaign->video_header = $request->video_header;
            $campaign->template_id = $request->template_id;
            $campaign->email_body = $request->email_body;
            $campaign->schedule_is_set = ($request->schedule_is_set == 'true') ? true : false;
            if($request->schedule_is_set == 'true'){
                $campaign->schedule_date = date('Y-m-d h:m A', strtotime($request->schedule_date));
                $campaign->date_time = strtotime($request->schedule_date);
            }else{
                $campaign->schedule_date = date('Y-m-d h:m A', strtotime("now"));
                $campaign->date_time = strtotime("now");

            }
            $campaign->emails = json_decode($request->emails);
            $campaign->inclusion_list = json_decode($request->inclusion_list);
            $campaign->exclusion_list = json_decode($request->exclusion_list);
            $campaign->is_draft = ($request->is_save_and_publish == 'true') ? false : true;
            $campaign->save();

            if(
                $campaign->schedule_is_set == false &&
                $campaign->is_draft == false
            ){
                $campaign->status = 'ongoing';
                $campaign->save();
                $this->publishCampaign($campaign);
                $campaign->status = 'finnished';
                $campaign->save();
            }

            return response()->json([
                'status' => 'success',
                'message' => "Draft Saved",
                'campaign' => $campaign
            ]);

        }catch(Exception $error){
            Log::info('EmailCampaignController@saveEmailCampaign error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'error' => true,
                "message" => $message,
            ], 500);
        }
    }

    private function publishCampaign($campaign){
        foreach($campaign->emails as $contactId){
            $contact = ContactModel::where('user_id', $campaign->user_id)
            ->where('id', $contactId)->first();
            if($contact){
                $video = VideoModel::where('user_id', Auth::id())
                    ->where('id', $campaign->video_id)
                    ->first();
                if(!$video){
                    $this->logCampaignReport($campaign, 'skipped', $contactId);
                }
                $emailIntegration = EmailIntegrationModel::where('user_id', Auth::id())
                    ->where('id', $campaign->provider_id)
                    ->first();
                if(!$emailIntegration){
                    $this->logCampaignReport($campaign, 'skipped', $contactId);
                }
                $template = CustomTemplateModel::where('user_id', Auth::id())
                    ->where('id', $campaign->template_id)
                    ->first();
                if(!$template && $campaign->template_id != 0){
                    $this->logCampaignReport($campaign, 'skipped', $contactId);
                }

                $email_copy = setupEmailCopy($contactId, $video, $campaign);
                $emailDetails = new \stdClass();
                $emailDetails->subject = $campaign->subject;
                $emailDetails->to = $contact->email;
                $emailDetails->from = $emailIntegration->email;
                $emailDetails->body = $this->setEmailBody($template,$contact, $campaign, $email_copy);
                $emailDetails->integration = $emailIntegration;
                $response = EmailController::sendMail($emailDetails, $campaign);
                if($response['status'] == true){
                    $this->logCampaignReport($campaign, 'success', $contactId);
                }else{
                    $this->logCampaignReport($campaign, 'failed', $contactId);
                }


            }else{
                $this->logCampaignReport($campaign, 'skipped', $contactId);
            }
        }
    }

    private function setEmailBody($template, $contact, $request, $email_copy){
        if($template){
            $body = parseWelcomeEmail($contact->first_name,
                $request->email_copy, $template->html);
        }else{
            $body = parseWelcomeEmail($contact->first_name,
                $request->email_copy, $request->email_body);
        }

        return $body;
    }

    private function sendMail($emailDetails, $request){
        switch($emailDetails->integration->service){
            case 'gmail_api':
                $gmailController = new GmailController;
                $response = $gmailController->sendMail($emailDetails);
                return $response;
            case 'mandrill_smtp':
                $madrillController = new MandrillController;
                $response = $madrillController->sendMail($emailDetails);
                return $response;
            case 'smtp':
                $smtpController = new SMTPController;
                $response = $smtpController->sendMail($emailDetails);
                return $response;
            case 'send_grid_smtp':
                $sendGridController = new SendGridController;
                $response = $sendGridController->sendMail($emailDetails);
                return $response;
            case 'aws_ses':
                $awsController = new AWSController;
                $response = $awsController->sendMail($emailDetails);
                return $response;
            default :
                return [
                    'status' =>false,
                    "message" => "SMPT setup not found"
                ];
        }


    }

    private function logCampaignReport($campaign, $status, $contactId){
        $log = new CampaignLogModel;
        $log->user_id = $campaign->user_id;
        $log->campaign_id = $campaign->id;
        $log->email = $contactId;
        $log->status = $status;
        $log->save();
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
