<?php

namespace App\Http\Controllers;

use DB, Log, Auth;
use Carbon\Carbon;
use App\Models\VideoViewModel;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    public function getStatistics($filter, $video){
        $response = [];
        switch ($filter) {
            case "last_7_days":
                $response = $this->statisticsForDays(7, $video);
                break;
            case "last_30_days":
                $response = $this->statisticsForDays(30, $video);
                break;
            case "yesterday":
                $response = $this->statisticsForYesterday($video);
                break;
            case "today":
                $response = $this->statisticsForToday($video);
                break;
            case "this_week":
                $response = $this->statisticsForThisWeek($video);
                break;
            case "last_week":
                $response = $this->statisticsForLastWeek($video);
                break;
            case "this_month":
                $response = $this->statisticsForThisMonth($video);
                break;
            case "last_month":
                $response = $this->statisticsForLastMonth($video);
                break;
            default:
                $response = $this->statisticsForAllTime($video);
    			break;

        }
       return $response;
    }

    private function statisticsForDays($amountOfDays, $video){
        try{
            $statistics = $this->getOverAllStatsForDays($amountOfDays, $video->id);
            $videoViews = VideoViewModel::where('video_id', $video->id)->get();
            for ($i=0; $i<=$amountOfDays; $i++){

                $sumOfViews = ($videoViews)? $this->getNumberOfViewsForDays($i, $videoViews):0;
			    $statistics["videoViews"][] = array(
                    "time"=>date("M d", strtotime($i." days ago")),
                    "sumOfViews"=>$sumOfViews,
                    "index" => $i);
            }
            $statistics["videoViews"] = collect($statistics["videoViews"])->sortByDesc('index')->values()->all();
            return $statistics;

        }catch(\Exception $error){
            Log::error($error);

            return [];
        }
    }

    private function getOverAllStatsForDays($amountOfDays, $videoId){

        $statistics["overall"]["videoViews"] = VideoViewModel::where('video_id', $videoId)
            ->whereDate('created_at','>=', date("Y-m-d", strtotime($amountOfDays." days ago") ) )
            ->count();
        return $statistics;
    }

    private function getNumberOfViewsForDays($numberOfDays, $videoViews){
        $sumOfViews = 0;
        foreach($videoViews as $videoView){
            if(date("Y-m-d", strtotime($numberOfDays." days ago")) == date("Y-m-d", strtotime($videoView->created_at))){
                $sumOfViews++;
            }
        }
        return $sumOfViews;
    }

    private function statisticsForYesterday($video){
        try{
            $statistics = $this->getOverAllStatsForYesterDay($video->id);
            $videoViews = VideoViewModel::where('video_id',$video->id)->get();
            $hoursInAday = 24;

            for($i = 0; $i <= $hoursInAday; $i++){

                $sumOfViews = ($videoViews)? $this->getNumberOfViewsForYesterDay($i, $videoViews): 0;
                $statistics["videoViews"][] = array(
                    "time"=>date("H A", strtotime(Carbon::now()->startOfDay()->subHours($i) )),
                    "sumOfViews"=>$sumOfViews,
                    "index" => $i);
            }
            return $statistics;

        }catch(\Exception $error){
            Log::error($error->getMessage());
            return [];
        }
    }

    private function getNumberOfViewsForYesterDay($numberOfHours, $videoViews){
        $sumOfViews = 0;
        foreach($videoViews as $videoView){
            $videoViewsCreatedAt = date("Y-m-d H", strtotime($videoView->created_at));
            $requestedDate = date("Y-m-d H", strtotime(Carbon::now()->startOfDay()->subHours($numberOfHours) ));

            if($requestedDate == $videoViewsCreatedAt){
                $sumOfViews++;
            }
        }
        return $sumOfViews;
    }

    private function getOverAllStatsForYesterDay($videoId){

        $statistics["overall"]["videoViews"] = VideoViewModel::where('video_id', $videoId)
                ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::yesterday() )).'%' )
                ->count();
        return $statistics;
    }

    private function statisticsForToday($video){
        try{
            $videoId = $video->id;
            $now = Carbon::createFromFormat('Y-m-d H:s:i', Carbon::now());
            $from = Carbon::createFromFormat('Y-m-d H:s:i', Carbon::now()->startOfDay());
            $diffInHours = $now->diffInHours($from);
            $videoViews = VideoViewModel::where('video_id', $videoId)->get();
            $statistics = $this->getOverAllStatsForToday($videoId);
            for($i = 0; $i <= $diffInHours; $i++){
                $sumOfViews = ($videoViews)? $this->getNumberOfViewsForToday($i, $videoViews) : 0;
                $statistics["videoViews"][] = array(
                    "time"=>date("H A", strtotime(Carbon::now()->subHours($i) )),
                    "sumOfViews"=>$sumOfViews,
                    "index" => $i);

            }
            $statistics["videoViews"] = collect($statistics["videoViews"])->sortBy('time')->values()->all();
            return $statistics;
        }catch(\Exception $error){
            Log::error($error->getMessage());
            return [];
        }
    }

    private function getOverAllStatsForToday($videoId){

        $statistics["overall"]["videoViews"] = VideoViewModel::where('video_id', $videoId)
                ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::now() )).'%' )
                ->count();
        return $statistics;
    }

    private function getNumberOfViewsForToday($numberOfHours, $videoViews){
        $sumOfViews = 0;
        foreach($videoViews as $videoView){
            $videoViewCreatedAt = date("Y-m-d H", strtotime($videoView->created_at));
            $requestedDate = date("Y-m-d H", strtotime(Carbon::now()->subHours($numberOfHours) ));

            if($requestedDate == $videoViewCreatedAt){
                $sumOfViews++;
            }
        }
        return $sumOfViews;
    }

    private function statisticsForAllTime($video){
        try{
            $statistics = $this->getOverAllStatsForAllTime($video->id);
            $statistics["videoViews"] = $this->getViewStatsForAllTime($video->id);

            return $statistics;

        }catch(\Exception $error){
            Log::error($error);
            return [];
        }


    }

    private function getOverAllStatsForAllTime($videoId){

        $statistics["overall"]["videoViews"] = VideoViewModel::where('video_id', $videoId)->count();

        return $statistics;
    }

    private function getViewStatsForAllTime($videoId){
        $firstViewRecord = VideoViewModel::where('video_id',$videoId)->first();
        $diffInMonthsForFirstViewRecord = $this->differenceInMonths($firstViewRecord->created_at);
        $statistics = [];

        $videoViews = VideoViewModel::where('video_id',$videoId)->get();
        for($i=0; $i<= $diffInMonthsForFirstViewRecord; $i++){

            $sumOfViews = 0;
            foreach($videoViews as $videoView){
                $videoViewCreatedAt = date("Y-m", strtotime($videoView->created_at));
                $requestedDate = date("Y-m", strtotime("first day of -$i month") );

                if($requestedDate == $videoViewCreatedAt){
                    $sumOfViews++;
                }
            }

            $statistics["videoViews"][] = array(
                "time"=>date("M Y", strtotime("first day of -$i month")),
                "sumOfViews"=>$sumOfViews,
                "index" => $i
            );
        }
        $statistics["videoViews"] = collect($statistics["videoViews"])->sortByDesc('index')->values()->all();
        return $statistics["videoViews"];
    }

    private function differenceInMonths($date){
        $now = date("m");
        $from = date("m", strtotime($date) );
        $diffInMonths = $now - $from;

        return $diffInMonths;
    }

    private function statisticsForLastWeek($video){
        try{
            $dateForToday = new \DateTime();
            $year = $dateForToday->format("Y");
            $thisWeek = $dateForToday->format("W");
            $lastWeek = $thisWeek - 1;
            $videoId = $video->id;
            $date = Carbon::now();
            $date->setISODate($year,$lastWeek);
            $weekStart = $date->startOfWeek();
            $weekEnd = $date->endOfWeek();
            // dd(date("M d", strtotime($weekStart->startOfWeek()->addDays(1)->format("Y-m-d"))));
            $videoViews = VideoViewModel::where('video_id', $videoId)->get();
            $statistics = $this->getOverAllStatsForLastWeek($videoId, $weekStart->startOfWeek()->format("Y-m-d"), $weekEnd->endOfWeek()->format("Y-m-d"));
            for($i = 0; $i < 7; $i++){
                $weekDate = $weekStart->startOfWeek()->addDays($i)->format("Y-m-d");
                $sumOfViews = ($videoViews)? $this->getNumberOfViewsForLastWeek($videoViews, $weekDate) : 0;
                $statistics["videoViews"][] = array(
                    "time"=>date("M d", strtotime($weekStart->startOfWeek()->addDays($i)->format("Y-m-d"))),
                    "sumOfViews"=>$sumOfViews,
                    "index" => $i);

            }
            $statistics["videoViews"] = collect($statistics["videoViews"])->sortBy('time')->values()->all();
            return $statistics;
        }catch(\Exception $error){
            Log::error($error);
            return [];
        }
    }

    private function getOverAllStatsForLastWeek($videoId, $weekStart, $weekEnd){

        $statistics["overall"]["videoViews"] = VideoViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$weekEnd)
            ->count();
        return $statistics;
    }

    private function getNumberOfViewsForLastWeek($videoViews, $weekStart){
        $sumOfViews = 0;
        foreach($videoViews as $videoView){
            $videoViewCreatedAt = date("Y-m-d", strtotime($videoView->created_at));
            $requestedDate = date("Y-m-d", strtotime($weekStart));

            if($requestedDate == $videoViewCreatedAt){
                $sumOfViews++;
            }
        }
        return $sumOfViews;
    }

    private function statisticsForThisWeek($video){
        try{
            $dateForToday = new \DateTime();
            $year = $dateForToday->format("Y");
            $thisWeek = $dateForToday->format("W");
            $videoId = $video->id;
            $date = Carbon::now();
            $date->setISODate($year,$thisWeek);
            $weekStart = $date->startOfWeek();
            $now = Carbon::now();
            $end = Carbon::parse($weekStart->startOfWeek()->format("Y-m-d"));
            $length = $end->diffInDays($now);
            // dd(date("M d", strtotime($weekStart->startOfWeek()->addDays(1)->format("Y-m-d"))));
            $videoViews = VideoViewModel::where('video_id', $videoId)->get();
            $statistics = $this->getOverAllStatsForThisWeek($videoId, $weekStart->startOfWeek()->format("Y-m-d"), $now->format("Y-m-d"));
            for($i = 0; $i <= $length; $i++){
                $weekDate = $weekStart->startOfWeek()->addDays($i)->format("Y-m-d");
                $sumOfViews = ($videoViews)? $this->getNumberOfViewsForThisWeek($videoViews, $weekDate) : 0;
                $statistics["videoViews"][] = array(
                    "time"=>date("M d", strtotime($weekStart->startOfWeek()->addDays($i)->format("Y-m-d"))),
                    "sumOfViews"=>$sumOfViews,
                    "index" => $i);

            }
            $statistics["videoViews"] = collect($statistics["videoViews"])->sortBy('time')->values()->all();
            return $statistics;
        }catch(\Exception $error){
            Log::error($error);
            return [];
        }
    }

    private function getOverAllStatsForThisWeek($videoId, $weekStart, $now){

        $statistics["overall"]["videoViews"] = VideoViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$now)
            ->count();
        return $statistics;
    }

    private function getNumberOfViewsForThisWeek($videoViews, $weekStart){
        $sumOfViews = 0;
        foreach($videoViews as $videoView){
            $videoViewCreatedAt = date("Y-m-d", strtotime($videoView->created_at));
            $requestedDate = date("Y-m-d", strtotime($weekStart));

            if($requestedDate == $videoViewCreatedAt){
                $sumOfViews++;
            }
        }
        return $sumOfViews;
    }

    private function statisticsForThisMonth($video){
        try{
            $videoId = $video->id;
            $date = Carbon::now();
            $firstDay = $date->firstOfMonth();
            $now = Carbon::now();
            $end = Carbon::parse($firstDay->firstOfMonth()->format("Y-m-d"));
            $length = $end->diffInDays($now);
            $videoViews = VideoViewModel::where('video_id', $videoId)->get();
            $statistics = $this->getOverAllStatsForThisMonth($videoId, $firstDay, $now->format("Y-m-d"));
            for($i = 0; $i <= $length; $i++){
                $weekDate = $firstDay->firstOfMonth()->addDays($i)->format("Y-m-d");
                $sumOfViews = ($videoViews)? $this->getNumberOfViewsForThisWeek($videoViews, $weekDate) : 0;
                $statistics["videoViews"][] = array(
                    "time"=>date("M d", strtotime($firstDay->firstOfMonth()->addDays($i)->format("Y-m-d"))),
                    "sumOfViews"=>$sumOfViews,
                    "index" => $i);

            }
            $statistics["videoViews"] = collect($statistics["videoViews"])->sortBy('time')->values()->all();
            return $statistics;


        }catch(\Exception $error){
            Log::error($error);
            return [];
        }
    }

    private function getOverAllStatsForThisMonth($videoId, $monthStart, $today){

        $statistics["overall"]["videoViews"] = VideoViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$monthStart)
            ->where('created_at','<=',$today)
                ->count();
        return $statistics;
    }

    private function statisticsForLastMonth($video){
        try{
            $videoId = $video->id;
            $startDate = Carbon::now();
            $now = Carbon::now();
            $videoViews = VideoViewModel::where('video_id', $videoId)->get();
            $statistics = $this->getOverAllStatsForLastWeek($videoId,
                $startDate->startOfMonth()->subMonth()->format('Y-m-d'),
                $now->endOfMonth()->subMonth()->format('Y-m-d')
            );
            $length = Carbon::now()->startOfMonth()->subMonth()->daysInMonth;
            // dd($length);
            for($i = 0; $i < $length; $i++){
                $starter = Carbon::now();
                $weekDate = $starter->startOfMonth()->subMonth()->addDays($i)->format('Y-m-d');
                $sumOfViews = ($videoViews)? $this->getNumberOfViewsForLastWeek($videoViews, $weekDate) : 0;
                $statistics["videoViews"][] = array(
                    "time"=>date("Y M d", strtotime($weekDate)),
                    "sumOfViews"=>$sumOfViews,
                    "index" => $i);

            }
            $statistics["videoViews"] = collect($statistics["videoViews"])->sortBy('time')->values()->all();
            return $statistics;


        }catch(\Exception $error){
            Log::error($error);
            return [];
        }
    }

}
