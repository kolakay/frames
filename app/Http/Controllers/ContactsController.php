<?php

namespace App\Http\Controllers;

use Auth, Session, Exception, Log;
use App\User;
use App\Models\CompanyModel;
use App\Models\ContactModel;
use App\Models\ConnectionTagModel;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        try{

            $tags = ConnectionTagModel::where('user_id', Auth::id())->get();
            $companies = CompanyModel::where('user_id', Auth::id())->get();
            $contacts = ContactModel::where('user_id', Auth::id())->get();
            $data = [
                'page' => 'connections',
                'sub' => 'connection_tags',
                'tags' => $tags,
                'companies' => $companies,
                'contacts' => $contacts
            ];
            return view('App.Connections.contacts', $data);

        }catch(Exception $error){
            Log::info('ContactsController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function create(Request $request){
        try{
            if(!$request->email){
                $message = "Contact Emails is required";
                return response()->json(['message' => $message], 400);
            }

            $contact = new ContactModel;
            $contact->user_id = Auth::id();
            $contact->email = $request->email;
            $contact->first_name = $request->first_name;
            $contact->last_name = $request->last_name;
            $contact->company = $request->company;
            $contact->title = $request->title;

            $contact->phone = $request->phone;
            $contact->mobile = $request->mobile;
            $contact->tags = $request->tags;
            $contact->notes = $request->notes;
            $contact->source = "Manually";


            $contact->save();
            return response()->json([
                'message' => "Contact was saved successfully",
                'contact' => $contact

            ]);

        }catch(Exception $error){
            Log::info('ContactsController@create error message: ' . $error->getMessage());
            $message = 'Unable to create Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function update(Request $request){
        try{

            if(!$request->email || !$request->id){
                $message = "Contact Details are required";
                return response()->json(['message' => $message], 400);
            }

            $contact = ContactModel::where('user_id', Auth::id())->where('id', $request->id)->first();
            if (!$contact) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Contact not found",
                ], 404);
            }

            $contact->email = $request->email;
            $contact->first_name = $request->first_name;
            $contact->last_name = $request->last_name;
            $contact->company = $request->company;
            $contact->title = $request->title;

            $contact->phone = $request->phone;
            $contact->mobile = $request->mobile;
            $contact->tags = $request->tags;
            $contact->notes = $request->notes;


            $contact->save();

            return response()->json([
                'error' => false,
                'contact' => $contact,
                'message' => "Contact was updated successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('ContactsController@update error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function delete(Request $request){
        try{

            $contact = ContactModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$contact) {
                $message = "Contact was not found";
                return response()->json(['message' => $message], 404);
            }

            $contact->delete();
            $message = "Contact deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('ContactsController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
