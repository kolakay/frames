<?php

namespace App\Http\Controllers;

use Auth, Session, Exception, Log;
use App\User;
use App\Models\CompanyModel;
use App\Models\ContactModel;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        try{

            $companies = CompanyModel::where('user_id', Auth::id())->get();


            $data = [
                'page' => 'connections',
                'sub' => 'companies',
                'companies' => $companies,
            ];
            return view('App.Connections.companies', $data);

        }catch(Exception $error){
            Log::info('CompaniesController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function create(Request $request){
        try{
            if(!$request->company_name){
                $message = "Company Name is required";
                return response()->json(['message' => $message], 400);
            }

            $company = CompanyModel::firstOrNew([
                'user_id' => Auth::id(),
                'name' => $request->company_name
            ]);

            $company->save();
            return response()->json([
                'message' => "Company was saved successfully",
                'company' => $company

            ]);

        }catch(Exception $error){
            Log::info('CompaniesController@create error message: ' . $error->getMessage());
            $message = 'Unable to create Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 500,
                "message" => $message,
            ], 500);
        }
    }

    public function update(Request $request){
        try{

            if(!$request->company_name || !$request->company_id){
                $message = "Company Details are required";
                return response()->json(['message' => $message], 400);
            }

            $company = CompanyModel::where('user_id', Auth::id())->where('id', $request->company_id)->first();
            if (!$company) {
                return response()->json([
                    'error' => true,
                    'status_code' => 404,
                    "message" => "Company not found",
                ], 404);
            }

            $company->name = $request->company_name;
            $company->save();

            return response()->json([
                'error' => false,
                'company' => $company,
                'message' => "Company was updated successfully"
            ], 200);
        }catch(Exception $error){
            Log::info('CompaniesController@update error message: ' . $error->getMessage());
            $message = 'Unable to update Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    public function delete(Request $request){
        try{

            $company = CompanyModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$company) {
                $message = "Company was not found";
                return response()->json(['message' => $message], 404);
            }

            $company->delete();
            $message = "Company deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('CompaniesController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
