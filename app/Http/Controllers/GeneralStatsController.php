<?php

namespace App\Http\Controllers;

use DB, Log, Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\CTAClickModel;
use App\Models\PageViewModel;
use App\Models\VideoViewModel;
use App\Models\EmailOpenModel;
use App\Models\EmailClickModel;
use App\Models\ReactionClickModel;



class GeneralStatsController extends Controller
{
    public function getStatistics($filter, $video){
        $response = [];
        switch ($filter) {
            case "last_7_days":
                $response = $this->statisticsForDays(7, $video);
                break;
            case "last_30_days":
                $response = $this->statisticsForDays(30, $video);
                break;
            case "yesterday":
                $response = $this->statisticsForYesterday($video);
                break;
            case "today":
                $response = $this->statisticsForToday($video);
                break;
            case "this_week":
                $response = $this->statisticsForThisWeek($video);
                break;
            case "last_week":
                $response = $this->statisticsForLastWeek($video);
                break;
            case "this_month":
                $response = $this->statisticsForThisMonth($video);
                break;
            case "last_month":
                $response = $this->statisticsForLastMonth($video);
                break;
            default:
                $response = $this->statisticsForAllTime($video);
    			break;

        }
       return $response;
    }

    private function statisticsForDays($amountOfDays, $video){
        try{
            $sumOfViews = 0;
            $statistics = $this->getOverAllStatsForDays($amountOfDays, $video->id);

            return $statistics;

        }catch(\Exception $error){
            Log::error($error);

            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }
    }

    private function getOverAllStatsForDays($amountOfDays, $videoId){

        $ctaClicks = CTAClickModel::where('video_id', $videoId)
            ->whereDate('created_at','>=', date("Y-m-d", strtotime($amountOfDays." days ago") ) )
            ->count();
        $pageViews = PageViewModel::where('video_id', $videoId)
            ->whereDate('created_at','>=', date("Y-m-d", strtotime($amountOfDays." days ago") ) )
            ->count();

        $watchRates = VideoViewModel::where('video_id', $videoId)
            ->whereDate('created_at','>=', date("Y-m-d", strtotime($amountOfDays." days ago") ) )
            ->count();

        $emailOpens = EmailOpenModel::where('video_id', $videoId)
            ->whereDate('created_at','>=', date("Y-m-d", strtotime($amountOfDays." days ago") ) )
            ->count();

        $emailClicks = EmailClickModel::where('video_id', $videoId)
            ->whereDate('created_at','>=', date("Y-m-d", strtotime($amountOfDays." days ago") ) )
            ->count();

        $reactionClicks = ReactionClickModel::where('video_id', $videoId)
            ->whereDate('created_at','>=', date("Y-m-d", strtotime($amountOfDays." days ago") ) )
            ->count();

        $statistics["ctaClicks"] = $this->getConversionRate($ctaClicks, $pageViews);
        $statistics["watchRates"] = $this->getConversionRate($watchRates, $pageViews);
        $statistics["emailOpens"] = $this->getConversionRate($emailOpens, $pageViews);
        $statistics["emailClicks"] = $this->getConversionRate($emailClicks, $emailOpens);
        $statistics["reactionClicks"] = $this->getConversionRate($reactionClicks, $pageViews);
        $statistics["callRates"] = 0;

        return $statistics;
    }

    private function statisticsForYesterday($video){
        try{
            $statistics = $this->getOverAllStatsForYesterDay($video->id);
            return $statistics;

        }catch(\Exception $error){
            Log::error($error->getMessage());
            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }
    }

    private function getOverAllStatsForYesterDay($videoId){
        $ctaClicks = CTAClickModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::yesterday() )).'%' )
            ->count();
        $pageViews = PageViewModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::yesterday() )).'%' )
            ->count();

        $watchRates = VideoViewModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::yesterday() )).'%' )
            ->count();

        $emailOpens = EmailOpenModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::yesterday() )).'%' )
            ->count();

        $emailClicks = EmailClickModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::yesterday() )).'%' )
            ->count();

        $reactionClicks = ReactionClickModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::yesterday() )).'%' )
            ->count();

        $statistics["ctaClicks"] = $this->getConversionRate($ctaClicks, $pageViews);
        $statistics["watchRates"] = $this->getConversionRate($watchRates, $pageViews);
        $statistics["emailOpens"] = $this->getConversionRate($emailOpens, $pageViews);
        $statistics["emailClicks"] = $this->getConversionRate($emailClicks, $emailOpens);
        $statistics["reactionClicks"] = $this->getConversionRate($reactionClicks, $pageViews);
        $statistics["callRates"] = 0;

        return $statistics;
    }

    private function statisticsForToday($video){
        try{
            $videoId = $video->id;
            $statistics = $this->getOverAllStatsForToday($videoId);
            return $statistics;
        }catch(\Exception $error){
            Log::error($error->getMessage());
            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }
    }

    private function getOverAllStatsForToday($videoId){

        $ctaClicks = CTAClickModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::now() )).'%' )
            ->count();
        $pageViews = PageViewModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::now() )).'%' )
            ->count();

        $watchRates = VideoViewModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::now() )).'%' )
            ->count();

        $emailOpens = EmailOpenModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::now() )).'%' )
            ->count();

        $emailClicks = EmailClickModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::now() )).'%' )
            ->count();

        $reactionClicks = ReactionClickModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::now() )).'%' )
            ->count();

        $statistics["ctaClicks"] = $this->getConversionRate($ctaClicks, $pageViews);
        $statistics["watchRates"] = $this->getConversionRate($watchRates, $pageViews);
        $statistics["emailOpens"] = $this->getConversionRate($emailOpens, $pageViews);
        $statistics["emailClicks"] = $this->getConversionRate($emailClicks, $emailOpens);
        $statistics["reactionClicks"] = $this->getConversionRate($reactionClicks, $pageViews);
        $statistics["callRates"] = 0;

        return $statistics;
    }

    private function statisticsForLastWeek($video){
        try{
            $videoId = $video->id;
            $weekDates = $this->getLastWeekDates();
            $weekStart = $weekDates["weekStart"];
            $weekEnd = $weekDates["weekEnd"];
            $statistics = $this->getOverAllStatsForLastWeek($videoId, $weekStart, $weekEnd);
            return $statistics;
        }catch(\Exception $error){
            Log::error($error);
            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }
    }

    private function getLastWeekDates(){
        $dateForToday = new \DateTime();
        $year = $dateForToday->format("Y");
        $thisWeek = $dateForToday->format("W");
        $lastWeek = $thisWeek - 1;

        $date = Carbon::now();
        $date->setISODate($year,$lastWeek);
        $weekStart = $date->startOfWeek();
        $weekEnd = $date->endOfWeek();

        return [
            'weekStart' => $weekStart->startOfWeek()->format("Y-m-d"),
            'weekEnd' => $weekEnd->endOfWeek()->format("Y-m-d")
        ];
    }

    private function getOverAllStatsForLastWeek($videoId, $weekStart, $weekEnd){

        $ctaClicks = CTAClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$weekEnd)
            ->count();
        $pageViews = PageViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$weekEnd)
            ->count();

        $watchRates = VideoViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$weekEnd)
            ->count();

        $emailOpens = EmailOpenModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$weekEnd)
            ->count();

        $emailClicks = EmailClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$weekEnd)
            ->count();

        $reactionClicks = ReactionClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$weekEnd)
            ->count();

        $statistics["ctaClicks"] = $this->getConversionRate($ctaClicks, $pageViews);
        $statistics["watchRates"] = $this->getConversionRate($watchRates, $pageViews);
        $statistics["emailOpens"] = $this->getConversionRate($emailOpens, $pageViews);
        $statistics["emailClicks"] = $this->getConversionRate($emailClicks, $emailOpens);
        $statistics["reactionClicks"] = $this->getConversionRate($reactionClicks, $pageViews);
        $statistics["callRates"] = 0;

        return $statistics;
    }

    private function statisticsForThisWeek($video){
        try{
            $videoId = $video->id;
            $weekDates = $this->getThisWeekDates();
            $weekStart = $weekDates["weekStart"];
            $now = $weekDates["now"];
            $statistics = $this->getOverAllStatsForThisWeek($videoId, $weekStart, $now);
            return $statistics;
        }catch(\Exception $error){
            Log::error($error);
            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }
    }

    private function getOverAllStatsForThisWeek($videoId, $weekStart, $now){

        $ctaClicks = CTAClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$now)
            ->count();
        $pageViews = PageViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$now)
            ->count();

        $watchRates = VideoViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$now)
            ->count();

        $emailOpens = EmailOpenModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$now)
            ->count();

        $emailClicks = EmailClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$now)
            ->count();

        $reactionClicks = ReactionClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$now)
            ->count();

        $statistics["ctaClicks"] = $this->getConversionRate($ctaClicks, $pageViews);
        $statistics["watchRates"] = $this->getConversionRate($watchRates, $pageViews);
        $statistics["emailOpens"] = $this->getConversionRate($emailOpens, $pageViews);
        $statistics["emailClicks"] = $this->getConversionRate($emailClicks, $emailOpens);
        $statistics["reactionClicks"] = $this->getConversionRate($reactionClicks, $pageViews);
        $statistics["callRates"] = 0;

        return $statistics;
    }

    private function getThisWeekDates(){
        $dateForToday = new \DateTime();
        $year = $dateForToday->format("Y");
        $thisWeek = $dateForToday->format("W");

        $date = Carbon::now();
        $date->setISODate($year,$thisWeek);
        $weekStart = $date->startOfWeek();
        $now = Carbon::now();
        $end = Carbon::parse($weekStart->startOfWeek()->format("Y-m-d"));

        return [
            'weekStart' => $weekStart->startOfWeek()->format("Y-m-d"),
            'now' => $now->format("Y-m-d")
        ];
    }

    private function statisticsForThisMonth($video){
        try{
            $videoId = $video->id;
            $monthDates = $this->getThisMonthDates();
            $firstDay = $monthDates["firstDay"];
            $now = $monthDates["now"];
            $statistics = $this->getOverAllStatsForThisMonth($videoId, $firstDay, $now);
            return $statistics;


        }catch(\Exception $error){
            Log::error($error);
            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }
    }

    private function getOverAllStatsForThisMonth($videoId, $monthStart, $today){

        $ctaClicks = CTAClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$monthStart)
            ->where('created_at','<=',$today)
            ->count();
        $pageViews = PageViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$monthStart)
            ->where('created_at','<=',$today)
            ->count();

        $watchRates = VideoViewModel::where('video_id', $videoId)
            ->where('created_at','>=',$monthStart)
            ->where('created_at','<=',$today)
            ->count();

        $emailOpens = EmailOpenModel::where('video_id', $videoId)
            ->where('created_at','>=',$monthStart)
            ->where('created_at','<=',$today)
            ->count();

        $emailClicks = EmailClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$monthStart)
            ->where('created_at','<=',$today)
            ->count();

        $reactionClicks = ReactionClickModel::where('video_id', $videoId)
            ->where('created_at','>=',$monthStart)
            ->where('created_at','<=',$today)
            ->count();

        $statistics["ctaClicks"] = $this->getConversionRate($ctaClicks, $pageViews);
        $statistics["watchRates"] = $this->getConversionRate($watchRates, $pageViews);
        $statistics["emailOpens"] = $this->getConversionRate($emailOpens, $pageViews);
        $statistics["emailClicks"] = $this->getConversionRate($emailClicks, $emailOpens);
        $statistics["reactionClicks"] = $this->getConversionRate($reactionClicks, $pageViews);
        $statistics["callRates"] = 0;

        return $statistics;
    }

    private function getThisMonthDates(){
        $date = Carbon::now();
        $firstDay = $date->firstOfMonth();
        $now = Carbon::now();

        return [
            'firstDay' => $firstDay,
            'now' => $now->format("Y-m-d")
        ];
    }

    private function statisticsForLastMonth($video){
        try{
            $videoId = $video->id;
            $lastMonthDates = $this->getLastMonthDates();
            $startDate = $lastMonthDates["startDate"];
            $now = $lastMonthDates["now"];
            $statistics = $this->getOverAllStatsForLastWeek($videoId, $startDate, $now);
            return $statistics;


        }catch(\Exception $error){
            Log::error($error);
            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }
    }

    private function getLastMonthDates(){
        $startDate = Carbon::now();
        $now = Carbon::now();

        return [
            'startDate' => $startDate->startOfMonth()->subMonth()->format('Y-m-d'),
            'now' => $now->endOfMonth()->subMonth()->format('Y-m-d')
        ];
    }

    private function statisticsForAllTime($video){
        try{
            $statistics = $this->getOverAllStatsForAllTime($video->id);
            return $statistics;

        }catch(\Exception $error){
            Log::error($error);
            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }


    }

    private function getOverAllStatsForAllTime($videoId){

        $ctaClicks = CTAClickModel::where('video_id', $videoId)->count();

        $pageViews = PageViewModel::where('video_id', $videoId)->count();

        $watchRates = VideoViewModel::where('video_id', $videoId)->count();

        $emailOpens = EmailOpenModel::where('video_id', $videoId)->count();

        $emailClicks = EmailClickModel::where('video_id', $videoId)->count();

        $reactionClicks = ReactionClickModel::where('video_id', $videoId)->count();

        $statistics["ctaClicks"] = $this->getConversionRate($ctaClicks, $pageViews);
        $statistics["watchRates"] = $this->getConversionRate($watchRates, $pageViews);
        $statistics["emailOpens"] = $this->getConversionRate($emailOpens, $pageViews);
        $statistics["emailClicks"] = $this->getConversionRate($emailClicks, $emailOpens);
        $statistics["reactionClicks"] = $this->getConversionRate($reactionClicks, $pageViews);
        $statistics["callRates"] = 0;

        return $statistics;
    }

    private function getConversionRate($sumOfLeads, $sumOfVisitors){

        $rate =  ($sumOfLeads && $sumOfVisitors) ? round((($sumOfLeads * 100) / $sumOfVisitors), 2) : 0;

        return $rate;
    }

    private function getEmptyDataForError(){
        $statistics["ctaClicks"] = 0;
        $statistics["watchRates"] = 0;
        $statistics["emailOpens"] = 0;
        $statistics["emailClicks"] = 0;
        $statistics["reactionClicks"] = 0;
        $statistics["callRates"] = 0;

        return $statistics;
    }
}
