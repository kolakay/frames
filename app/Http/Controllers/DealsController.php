<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DealsModel;
use App\Models\ContactModel;
use App\Models\CompanyModel;
use Illuminate\Support\Str;
use Auth;
use Exception;
use Validator;
use Log;

class DealsController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(){
		$contacts = ContactModel::select('id', 'first_name', 'last_name', 'email')->where('user_id', Auth::id())->get();
		$companies = CompanyModel::select('id', 'name')->where('user_id', Auth::id())->get();

		$data = [
			'page' => 'deals',
			'sub' => '',
			'deals' => '',
			'contacts' => '',
			'companies' => ''
		];

		$deals = $this->getDeals(true);

		if ($deals) {
			$data['deals'] = $deals;
		}

		$data['companies'] = $companies;
		$data['contacts'] = $contacts;

		return view('App.deals', $data);
	}

	public function getDeals($index = false){
		try {
			$deals = DealsModel::where('user_id', Auth::id())->get();

			if($index == true){
				return $deals;
			}
			return response()->json([
				'status_code' => 200,
				"deals" => $deals,
			], 200);
		} catch (Exception $error) {
			Log::info('DealsController@index error message: ' . $error->getMessage());
		}
	}

	public function create(Request $request){
		try {
			$deal = new DealsModel();
			
			$validator = $this->validator($request->all());

			if ($validator->fails()) {
				return response()->json([
					'error' => true,
					'errors' => $validator->errors(),
					'status_code' => 400,
					"message" => "incomplete or invalid form values",
				], 400);
			}

			$deal->user_id = Auth::id();
			$deal->title = $request->title;
			$deal->description = $request->description;
			$deal->owner = $request->owner;
			$deal->amount = $request->amount;
			$deal->stage = $request->stage;
			$deal->closed_date = $request->closed_date;

			$deal->save();

			$message = 'Request completed';
			return response()->json([
				'status_code' => 200,
				"message" => $message,
			], 200);
		} catch (Exception $error) {
			Log::info('DealsController@create error message: ' . $error->getMessage());
			$message = 'Unable to create Resource. Encountered an error.';
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
		}
	}

	public function edit(Request $request) {
		try {
			$deal = DealsModel::where('id', $request->id)->first();
			
			$validator = $this->validator($request->all());

			if ($validator->fails()) {
				return response()->json([
					'error' => true,
					'errors' => $validator->errors(),
					'status_code' => 400,
					"message" => "incomplete or invalid form values",
				], 400);
			}
			$deal->title = $request->title;
			$deal->description = $request->description;
			$deal->owner = $request->owner;
			$deal->amount = $request->amount;
			$deal->stage = $request->stage;
			if ($request->closed_date != "null") {
				$deal->closed_date = $request->closed_date;
			}

			$deal->save();

			$message = 'Request completed';

			return response()->json([
				'status_code' => 200,
				"message" => $message,
			], 200);
		} catch (Exception $error) {
			Log::info('DealsController@edit error message: ' . $error->getMessage());
			$message = 'Unable to edit Resource. Encountered an error.';
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
		}
	}

	public function delete(Request $request){
		try {
			$deal = DealsModel::where('id', $request->id)->first();
			$deal->delete();
			$message = 'Request completed';
			return response()->json([
				'status_code' => 200,
				"message" => $message,
			], 200);
		}catch (Exception $error) {
			Log::info('DealsController@delete error message: ' . $error->getMessage());
			$message = 'Unable to delete Resource. Encountered an error.';
			return response()->json([
				'error' => true,
				'status_code' => 500,
				"message" => $message,
			], 500);
		}
		
	}

	protected function validator(array $data) {
		return Validator::make($data, [
			'title' => 'required|string|max:255',
			'owner' => 'string|nullable',
			'amount' => 'numeric|nullable',
			'stage' => 'required|string',
			'closed_date' => 'nullable'
		]);
	}

	private function handleError($message) {
		Session::put('errorMessage', $message);
		return redirect()->back();
	}
}
