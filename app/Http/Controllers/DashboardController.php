<?php

namespace App\Http\Controllers;

use App\Models\CTAModel;
use App\Models\TagModel;
use App\Models\VideoModel;
use Auth, Session, Exception, Log;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $videos = VideoModel::where('user_id', Auth::id())->get();
        $ctaCount = CTAModel::where('user_id', Auth::id())->count();
        $tags = TagModel::where('user_id', Auth::id())->get();
        $videoCount = count($videos);
        $data = [
            'page' => 'dashboard',
            'sub' => '',
            'tags' => $tags,
            'videos' => $videos,
            'videoCount' => $videoCount,
            'ctaCount' =>$ctaCount
        ];
        return view('App.dashboard', $data);
    }
}
