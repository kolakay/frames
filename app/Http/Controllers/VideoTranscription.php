<?php

namespace App\Http\Controllers;

use Log;
use \Done\Subtitles\Subtitles;
use App\Models\VideoModel;
use App\Helpers\Paths;
use Illuminate\Http\Request;
use App\Models\TranscriptionModel;
use Google\Cloud\Speech\V1\SpeechClient;
use Google\Cloud\Speech\V1\RecognitionAudio;
use Google\Cloud\Speech\V1\RecognitionConfig;
use Google\Cloud\Speech\V1\RecognitionConfig\AudioEncoding;

class VideoTranscription extends Controller
{
    
    public function convertVideoToAudio(){
        putenv('GOOGLE_APPLICATION_CREDENTIALS='. storage_path('app/public/keys/frames.json'));
        $videoPath = storage_path('app/public/videos/'). "tester.mp4";
        $audioPath = storage_path('app/public/audios/'). "tester.mp3";
        $model = 'video';
        // dd([$videoPath, $audioPath]);
        exec("ffmpeg -i $videoPath $audioPath");
        
        // change these variables if necessary
        // $encoding = AudioEncoding::ENCODING_UNSPECIFIED;
        // $sampleRateHertz = 48000;
        // $languageCode = 'en-US';

        // // When true, time offsets for every word will be included in the response.
        // $enableWordTimeOffsets = true;

        // // get contents of a file into a string
        // $content = file_get_contents($audioPath);

        // // set string as audio content
        // $audio = (new RecognitionAudio())
        //     ->setContent($content);

        // // set config
        // $config = (new RecognitionConfig())
        //     ->setEncoding($encoding)
        //     ->setSampleRateHertz($sampleRateHertz)
        //     ->setLanguageCode($languageCode)
        //     ->setModel($model)
        //     ->setEnableWordTimeOffsets($enableWordTimeOffsets);

        // // create the speech client
        // $client = new SpeechClient();

        // // make the API call
        // $response = $client->recognize($config, $audio);
        // $results = $response->getResults();
        // foreach ($results as $result) {
        //     $alternatives = $result->getAlternatives();
        //     $mostLikely = $alternatives[0];
        //     $transcript = $mostLikely->getTranscript();
        //     $confidence = $mostLikely->getConfidence();
        //     // printf('Transcript: %s' . PHP_EOL, $transcript);
        //     // printf('Confidence: %s' . PHP_EOL, $confidence);
        //     foreach ($mostLikely->getWords() as $wordInfo) {
        //         $startTime = $wordInfo->getStartTime();
        //         $endTime = $wordInfo->getEndTime();
        //         Log::info([$wordInfo->getWord(),
        //         $startTime->serializeToJsonString(),
        //         $endTime->serializeToJsonString()]);
        //         printf('  Word: %s (start: %s, end: %s)' . PHP_EOL,
        //             $wordInfo->getWord(),
        //             $startTime->serializeToJsonString(),
        //             $endTime->serializeToJsonString());
        //     }
        // }
        // dd($results);
        // Log::info(json_encode($results));
        // // print results
        // foreach ($results as $result) {
        //     $alternatives = $result->getAlternatives();
        //     $mostLikely = $alternatives[0];
        //     $transcript = $mostLikely->getTranscript();
        //     $confidence = $mostLikely->getConfidence();
        //     Log::info($transcript);
        //     Log::info($confidence);
        // }

        // $client->close();
    }

    public function callback(Request $request){
        $responseData = $request->all();
        if(isset($responseData["job"])){
            if(isset($responseData["job"]["id"]) && isset($responseData["job"]["status"])){
                $jobId = $responseData["job"]["id"];
                $status = $responseData["job"]["status"];
                $transcriptionDetails = TranscriptionModel::where('job_id', $jobId)->first();
                if($transcriptionDetails){
                    if($status == 'transcribed'){
                        $transcriptionDetails->status = 'done';
                        $this->storeSubrip($jobId);
                    }else{
                        $transcriptionDetails->status = 'failed';
                    }
                    $transcriptionDetails->save();
                }

            }
            
        }
        Log::info($request->all());
    }

    public function storeSubrip($id){
        $client = new \GuzzleHttp\Client();
        $url = "https://api.rev.ai/speechtotext/v1/jobs/$id/captions";

        $request = $client->request('GET', $url, [
            // 'content-type' => 'application/json',
            "headers" => [
                "Authorization" => "Bearer " . Paths::REV_ACCESS_TOKEN,
                'Accept'        => 'application/x-subrip',
            ]
        ]);

        $contents = $request->getBody()->getContents();
        $transcriptionDetails = TranscriptionModel::where('job_id', $id)->first();
        
        if($transcriptionDetails){
            $video = VideoModel::where('id', $transcriptionDetails->video_id)->first();
            if($video){
                $srtFileName = $video->name .".srt";
                $vttFileName = $video->name .".vtt";
                $srtFullPath = storage_path('app/' .Paths::SRT_FILES_PATH) . $srtFileName;
                $vttFullPath = storage_path('app/' .Paths::SRT_FILES_PATH) . $vttFileName;

                exec("touch $srtFullPath");
                exec("chmod 777 $srtFullPath");
                $caption = fopen("$srtFullPath", "wb");
                fwrite($caption, $contents);
                fclose($caption);
                Subtitles::convert($srtFullPath, $vttFullPath);

                $video->srt_file = $srtFileName;
                $video->save();
            }

        }
    
    }
}
