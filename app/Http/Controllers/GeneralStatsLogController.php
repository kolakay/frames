<?php

namespace App\Http\Controllers;

use DB, Log, Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\GeneralStatLogModel;

class GeneralStatsLogController extends Controller
{
    public function getLogs($filter, $video){
        $response = [];
        switch ($filter) {
            case "last_7_days":
                $response = $this->logsForDays(7, $video);
                break;
            case "last_30_days":
                $response = $this->logsForDays(30, $video);
                break;
            case "yesterday":
                $response = $this->logsForYesterday($video);
                break;
            case "today":
                $response = $this->logsForToday($video);
                break;
            case "this_week":
                $response = $this->logsForThisWeek($video);
                break;
            case "last_week":
                $response = $this->logsForLastWeek($video);
                break;
            case "this_month":
                $response = $this->logsForThisMonth($video);
                break;
            case "last_month":
                $response = $this->logsForLastMonth($video);
                break;
            default:
                $response = $this->logsForAllTime($video);
    			break;

        }
       return $response;
    }

    private function logsForDays($amountOfDays, $video){
        try{
            $logs = $this->getOverAllLogsForDays($amountOfDays, $video->id);

            return $logs;

        }catch(\Exception $error){
            Log::error($error);

            return [];
        }
    }
    private function getOverAllLogsForDays($amountOfDays, $videoId){

        $logs = GeneralStatLogModel::where('video_id', $videoId)
            ->whereDate('created_at','>=', date("Y-m-d", strtotime($amountOfDays." days ago") ) )
            ->get();

        return $logs;
    }

    private function logsForYesterday($video){
        try{
            $logs = $this->getOverAllLogsForYesterDay($video->id);
            return $logs;

        }catch(\Exception $error){
            Log::error($error->getMessage());
        }
    }

    private function getOverAllLogsForYesterDay($videoId){
        $logs = GeneralStatLogModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::yesterday() )).'%' )
            ->get();

        return $logs;
    }

    private function logsForToday($video){
        try{
            $videoId = $video->id;
            $logs = $this->getOverAllLogsForToday($videoId);
            return $logs;
        }catch(\Exception $error){
            Log::error($error->getMessage());
            return [];
        }
    }

    private function getOverAllLogsForToday($videoId){

        $logs = GeneralStatLogModel::where('video_id', $videoId)
            ->where('created_at','LIKE', '%'.date("Y-m-d", strtotime(Carbon::now() )).'%' )
            ->get();

        return $logs;
    }

    private function logsForThisWeek($video){
        try{
            $videoId = $video->id;
            $weekDates = $this->getThisWeekDates();
            $weekStart = $weekDates["weekStart"];
            $now = $weekDates["now"];
            $logs = $this->getOverAllLogsForThisWeek($videoId, $weekStart, $now);
            return $logs;
        }catch(\Exception $error){
            Log::error($error);
            $statistics = $this->getEmptyDataForError();
            return $statistics;
        }
    }

    private function getThisWeekDates(){
        $dateForToday = new \DateTime();
        $year = $dateForToday->format("Y");
        $thisWeek = $dateForToday->format("W");

        $date = Carbon::now();
        $date->setISODate($year,$thisWeek);
        $weekStart = $date->startOfWeek();
        $now = Carbon::now();
        $end = Carbon::parse($weekStart->startOfWeek()->format("Y-m-d"));

        return [
            'weekStart' => $weekStart->startOfWeek()->format("Y-m-d"),
            'now' => $now->format("Y-m-d")
        ];
    }

    private function getOverAllLogsForThisWeek($videoId, $weekStart, $now){

        $logs = GeneralStatLogModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$now)
            ->get();


        return $logs;
    }

    private function logsForLastWeek($video){
        try{
            $videoId = $video->id;
            $weekDates = $this->getLastWeekDates();
            $weekStart = $weekDates["weekStart"];
            $weekEnd = $weekDates["weekEnd"];
            $statistics = $this->getOverAllLogsForLastWeek($videoId, $weekStart, $weekEnd);
            return $statistics;
        }catch(\Exception $error){
            Log::error($error);
            return [];
        }
    }

    private function getLastWeekDates(){
        $dateForToday = new \DateTime();
        $year = $dateForToday->format("Y");
        $thisWeek = $dateForToday->format("W");
        $lastWeek = $thisWeek - 1;

        $date = Carbon::now();
        $date->setISODate($year,$lastWeek);
        $weekStart = $date->startOfWeek();
        $weekEnd = $date->endOfWeek();

        return [
            'weekStart' => $weekStart->startOfWeek()->format("Y-m-d"),
            'weekEnd' => $weekEnd->endOfWeek()->format("Y-m-d")
        ];
    }

    private function getOverAllLogsForLastWeek($videoId, $weekStart, $weekEnd){

        $logs = GeneralStatLogModel::where('video_id', $videoId)
            ->where('created_at','>=',$weekStart)
            ->where('created_at','<=',$weekEnd)
            ->get();

        return $logs;
    }

    private function logsForThisMonth($video){
        try{
            $videoId = $video->id;
            $monthDates = $this->getThisMonthDates();
            $firstDay = $monthDates["firstDay"];
            $now = $monthDates["now"];
            $logs = $this->getOverAllLogsForThisMonth($videoId, $firstDay, $now);
            return $logs;


        }catch(\Exception $error){
            Log::error($error);
            return [];
        }
    }

    private function getOverAllLogsForThisMonth($videoId, $monthStart, $today){

        $logs = GeneralStatLogModel::where('video_id', $videoId)
            ->where('created_at','>=',$monthStart)
            ->where('created_at','<=',$today)
            ->get();

        return $logs;
    }

    private function getThisMonthDates(){
        $date = Carbon::now();
        $firstDay = $date->firstOfMonth();
        $now = Carbon::now();

        return [
            'firstDay' => $firstDay,
            'now' => $now->format("Y-m-d")
        ];
    }

    private function logsForLastMonth($video){
        try{
            $videoId = $video->id;
            $lastMonthDates = $this->getLastMonthDates();
            $startDate = $lastMonthDates["startDate"];
            $now = $lastMonthDates["now"];
            $logs = $this->getOverAllLogsForLastWeek($videoId, $startDate, $now);
            return $logs;


        }catch(\Exception $error){
            Log::error($error);
            return [];
        }
    }

    private function getLastMonthDates(){
        $startDate = Carbon::now();
        $now = Carbon::now();

        return [
            'startDate' => $startDate->startOfMonth()->subMonth()->format('Y-m-d'),
            'now' => $now->endOfMonth()->subMonth()->format('Y-m-d')
        ];
    }

    private function logsForAllTime($video){
        try{
            $logs = $this->getOverAllLogsForAllTime($video->id);
            return $logs;

        }catch(\Exception $error){
            Log::error($error);
            return [];
        }
    }

    private function getOverAllLogsForAllTime($videoId){

        $logs = GeneralStatLogModel::where('video_id', $videoId)->get();

        return $logs;
    }
}
