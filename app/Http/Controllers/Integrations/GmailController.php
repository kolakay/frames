<?php

namespace App\Http\Controllers\Integrations;

use App\Http\Controllers\Controller;
use Auth, Session, Exception, Log;
use App\User;
use App\Models\EmailIntegrationModel;

class GmailController extends Controller
{
    public $client;
    public $objOAuthService;

    public function __construct()
    {
        $this->middleware('auth');
        $this->setupClient();
    }

    public function connect(){

        $authUrl = $this->client->createAuthUrl();

        return redirect($authUrl);
    }
    public function callback(){
        try{


            $code = request()->get('code');
            $scope = request()->get('scope');
            $this->client->authenticate($code);
            // Exchange authorization code for an access token.
            $accessToken =  $this->client->getAccessToken();
            Log::info([$code, $accessToken]);
            $this->client->setAccessToken($accessToken);

            // Check to see if there was an error.
            if (array_key_exists('error', $accessToken)) {

                $message = "Unable to integrate with your google account";
                Session::put('errorMessage', $message);
                return redirect(route('user.settings.broadcast'));
            }else{
                $this->client->setAccessToken($accessToken);
                $token = $this->client->getAccessToken();
                $userData = $this->objOAuthService->userinfo->get();

                $integration = new EmailIntegrationModel;
                $integration->user_id = Auth::id();
                $integration->access_token = $token;
                $integration->service = 'gmail_api';
                $integration->type = 'email';
                $integration->gmail_id = $userData->id;
                $integration->email = $userData->email;
                $integration->picture = $userData->picture;
                $integration->verified_email = $userData->verifiedEmail;
                $integration->save();

                $message = "Connected account successfully";
                Session::put('successMessage', $message);
                return redirect(route('user.settings.broadcast'));
            }
        }catch(Exception $error){
            Log::info('GmailController@callback error message: ' . $error->getMessage());
            $message = "Unable to connect to integrate account";
            Session::put('errorMessage', $message);
            return redirect(route('user.settings.broadcast'));
        }

    }

    public function setupClient(){
        $this->client = new \Google_Client();
        $this->client->setApplicationName("FaceDrip");
        $this->client->setAccessType('offline');
        $this->client->setClientId('1011993154754-8i1msi3m43vru7vte23b8ebjjlv0q98j.apps.googleusercontent.com');
        $this->client->setClientSecret('bpdu3h3ey_kCl1x6ozibKwsK');
        $this->client->setRedirectUri(route('user.settings.integrations.callback.gmail'));
        // $this->client->setScopes(\Google_Service_Gmail::GMAIL_READONLY);
        $this->client->addScope("https://www.googleapis.com/auth/userinfo.email");
        $this->client->addScope("https://www.googleapis.com/auth/gmail.send");

        $this->objOAuthService = new \Google_Service_Oauth2($this->client);
    }

    public function sendMail($emailDetails){
        try{
            $this->setupClientAccess($emailDetails->integration);
            $service = new \Google_Service_Gmail($this->client);
            $message = $this->createMessage($emailDetails->from, $emailDetails->to, $emailDetails->subject, $emailDetails->body);
            $response = $this->sendMessage($service, $message);

            if($response == true){
                $response = [
                    'status' =>true,
                    "message" => "Message sent"
                ];
            }else{
                $response = [
                    'status' =>false,
                    "message" => "Message not sent"
                ];
            }



        }catch(Exception $error){
            Log::info('GmailController@sendMail error message: ' . $error->getMessage());
            $response = [
                'status' =>false,
                "message" => "Encountered an error"
            ];
            return $response;
        }
    }

    public function createMessage($sender, $to, $subject, $messageText) {
        $message = new \Google_Service_Gmail_Message();

        $rawMessageString = "From: <{$sender}>\r\n";
        $rawMessageString .= "To: <{$to}>\r\n";
        $rawMessageString .= 'Subject: =?utf-8?B?' . base64_encode($subject) . "?=\r\n";
        $rawMessageString .= "MIME-Version: 1.0\r\n";
        $rawMessageString .= "Content-Type: text/html; charset=utf-8\r\n";
        $rawMessageString .= 'Content-Transfer-Encoding: quoted-printable' . "\r\n\r\n";
        $rawMessageString .= "{$messageText}\r\n";

        $rawMessage = strtr(base64_encode($rawMessageString), array('+' => '-', '/' => '_'));
        $message->setRaw($rawMessage);
        return $message;

    }

    public function sendMessage($service, $message) {
        try {
            $message = $service->users_messages->send('me', $message);
            //dd('Message with ID: ' . $message->getId() . ' sent.');
            return true; //$message;
        }catch (Exception $error) {
            Log::info('GmailController@sendMessage error message: ' . $error->getMessage());
            return false;
        }

    }

    public function setupClientAccess($integration){
        $accessToken = $integration->access_token;
        $this->client->setAccessToken($accessToken);
        if ($this->client->isAccessTokenExpired()) {
            if ($this->client->getRefreshToken()) {
                $this->client->fetchAccessTokenWithRefreshToken(
                        $this->client->getRefreshToken()
                    );
            }
        }
    }
}
