<?php

namespace App\Http\Controllers\Integrations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth, Session, Exception, Log;
use App\User;
use App\Models\EmailIntegrationModel;

class SendGridController extends Controller
{
    public function sendMail($emailDetails){
        try{

            $email = new \SendGrid\Mail\Mail();
            $email->setFrom($emailDetails->from);
            $email->setSubject($emailDetails->subject);
            $email->addTo($emailDetails->to);
            $email->addContent("text/html", $emailDetails->body);
            $sendgrid = new \SendGrid($emailDetails->integration->sendgrid_api_key);
            $sendgridResponse = $sendgrid->send($email);
            if($sendgridResponse->statusCode() == 200 || $sendgridResponse->statusCode() == 202
                || $sendgridResponse->statusCode() == 201
            ){
                $response = [
                    'status' =>true,
                    "message" => "Message sent"
                ];
            }else{
                $response = [
                    'status' =>false,
                    "message" => "Message not sent"
                ];
            }

            return $response;

        }catch(Exception $error){
            Log::info('SendGridController@sendMail error message: ' . $error->getMessage());
            $response = [
                'status' =>false,
                "message" => "Encountered an error"
            ];
            return $response;
        }
    }
}
