<?php

namespace App\Http\Controllers\Integrations;

use Exception;
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth, Session, Log;


class AWSController extends Controller
{
    public function sendMail($emailDetails){
        try{
            // Create an SesClient. Change the value of the region parameter if you're
            // using an AWS Region other than US West (Oregon). Change the value of the
            // profile parameter if you want to use a profile in your credentials file
            // other than the default.
            $SesClient = new SesClient([
                'credentials' => [
                    'key'    => $emailDetails->integration->ses_key,
                    'secret' => $emailDetails->integration->ses_secret,
                ],
                'region' => $emailDetails->integration->ses_region,
                'version' => "latest"
            ]);

            // This address must be verified with Amazon SES.
            $sender_email = $emailDetails->from;

            $recipient_emails = [$emailDetails->to];
            $subject = $emailDetails->subject;
            $html_body =  $emailDetails->body;
            $char_set = 'UTF-8';

            $result = $SesClient->sendEmail([
                'Destination' => [
                    'ToAddresses' => $recipient_emails,
                ],
                'ReplyToAddresses' => [$sender_email],
                'Source' => $sender_email,
                'Message' => [
                  'Body' => [
                      'Html' => [
                          'Charset' => $char_set,
                          'Data' => $html_body,
                      ],
                  ],
                  'Subject' => [
                      'Charset' => $char_set,
                      'Data' => $subject,
                  ],
                ],
            ]);

            $response = [
                'status' =>true,
                "message" => "Message sent"
            ];

            return $response;
            // $messageId = $result['MessageId'];
            // echo("Email sent! Message ID: $messageId"."\n");

        }catch (AwsException $error) {
            Log::info('AWSController@sendMail error message: ' . $error->getMessage());
            Log::info('AWSController@sendMail error message: ' . $error->getAwsErrorMessage());
            $response = [
                'status' =>false,
                "message" => "Encountered an error"
            ];
            return $response;
        }catch(Exception $error){
            Log::info('AWSController@sendMail error message: ' . $error->getMessage());
            $response = [
                'status' =>false,
                "message" => "Encountered an error"
            ];
            return $response;
        }
    }
}
