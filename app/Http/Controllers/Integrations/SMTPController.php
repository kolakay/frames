<?php

namespace App\Http\Controllers\Integrations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth, Session, Exception, Log;
use App\User;
use App\Models\EmailIntegrationModel;

class SMTPController extends Controller
{
    public function sendMail($emailDetails){
        try{
            $subject = $emailDetails->subject;
            $from = array($emailDetails->from);
            $to = array(
                $emailDetails->to  //=> 'Recipient1 Name',
            // 'recipient2@example2.com' => 'Recipient2 Name'
            );

            $html = $emailDetails->body;

            $transport = new \Swift_SmtpTransport($emailDetails->integration->smtp_host, $emailDetails->integration->smtp_port);
            $transport->setUsername($emailDetails->integration->smtp_username);
            $transport->setPassword($emailDetails->integration->smtp_password);
            $swift = new \Swift_Mailer($transport);

            $message = new \Swift_Message($subject);
            $message->setFrom($from);
            $message->setBody($html, 'text/html');
            $message->setTo($to);

            if ($recipients = $swift->send($message)){
                $response = [
                    'status' =>true,
                    "message" => "Message sent"
                ];
            } else {
                $response = [
                    'status' =>false,
                    "message" => "Message not sent"
                ];
            }

            return $response;
        }catch(Exception $error){
            Log::info('SMTPController@sendMail error message: ' . $error->getMessage());
            $response = [
                'status' =>false,
                "message" => "Encountered an error"
            ];
            return $response;
        }
    }
}
