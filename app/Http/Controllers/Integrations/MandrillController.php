<?php

namespace App\Http\Controllers\Integrations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth, Session, Exception, Log;
use App\User;
use App\Models\EmailIntegrationModel;

class MandrillController extends Controller
{
    public function sendMail($emailDetails){
        try{
            $subject = $emailDetails->subject;
            $from = array($emailDetails->from);
            $to = array(
                $emailDetails->to  //=> 'Recipient1 Name',
            // 'recipient2@example2.com' => 'Recipient2 Name'
            );

            $html = $emailDetails->body;

            $transport = new \Swift_SmtpTransport('smtp.mandrillapp.com', 587);
            $transport->setUsername($emailDetails->integration->mandrill_username);
            $transport->setPassword($emailDetails->integration->mandrill_password);
            $swift = new \Swift_Mailer($transport);

            $message = new \Swift_Message($subject);
            $message->setFrom($from);
            $message->setBody($html, 'text/html');
            $message->setTo($to);

            if ($recipients = $swift->send($message)){
                $response = [
                    'status' =>true,
                    "message" => "Message sent"
                ];
            } else {
                $response = [
                    'status' =>false,
                    "message" => "Message not sent"
                ];
            }

            return $response;
        }catch(Exception $error){
            Log::info('MandrillController@sendMail error message: ' . $error->getMessage());
            $response = [
                'status' =>false,
                "message" => "Encountered an error"
            ];
            return $response;
        }
    }
}
