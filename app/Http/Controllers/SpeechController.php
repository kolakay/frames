<?php

namespace App\Http\Controllers;

use App\Helpers\Paths;
use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SsmlVoiceGender;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;
use Auth, Session, Exception, Log, Storage;
use App\User;

use Illuminate\Http\Request;
use App\Models\SpeechModel;

class SpeechController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    public function index(){
        try{

            $speeches = SpeechModel::where('user_id', Auth::id())->get();

            $data = [
                'page' => 'text-to-speech',
                'sub' => '',
                'speeches' => $speeches,
            ];
            return view('App.Text-to-speech.index', $data);

        }catch(Exception $error){
            Log::info('SpeechController@index error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function convert(Request $request){
        try{
            if(!$request->title || $request->title == ""){
                $message = "Speech Title is required";
                return response()->json(['message' => $message], 400);
            }

            $speech = new SpeechModel;
            $speech->user_id = Auth::id();
            $speech->title = $request->title;
            $speech->gender = $request->gender;
            $speech->text = $request->text;
            $speech->language = $request->language;
            $speech->save();
            $response = $this->convertTextToSpeech($speech);

            if($response['status'] != true){
                $speech->status = 'failed';
                $speech->save();
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Service unavailable"
                ], 503);
            }
            $speech->audio = $response['audioTitle'];
            $speech->status = 'converted';
            $speech->save();

            return response()->json([
                'message' => "Text was converted successfully",
                'speech' => $speech

            ]);

        }catch(Exception $error){
            Log::info('SpeechController@convert error message: ' . $error->getMessage());
            $message = 'Unable to create Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function convertTextToSpeech($speech){
        try{
            putenv('GOOGLE_APPLICATION_CREDENTIALS='. storage_path('app/public/keys/speech.json'));
            
            // instantiates a client
            $client = new TextToSpeechClient();

                // sets text to be synthesised
            $synthesisInputText = (new SynthesisInput())
            ->setText($speech->text);

            $gender = ($speech->gender == 1)? SsmlVoiceGender::FEMALE : SsmlVoiceGender::MALE;

            // build the voice request, select the language code ("en-US") and the ssml
            // voice gender
            $voice = (new VoiceSelectionParams())
                ->setLanguageCode($speech->language)
                ->setSsmlGender($gender);
            
            // Effects profile
            $effectsProfileId = "telephony-class-application";

            // select the type of audio file you want returned
            $audioConfig = (new AudioConfig())
                ->setAudioEncoding(AudioEncoding::MP3)
                ->setEffectsProfileId(array($effectsProfileId));

            // perform text-to-speech request on the text input with selected voice
            // parameters and audio file type
            $response = $client->synthesizeSpeech($synthesisInputText, $voice, $audioConfig);
            $audioContent = $response->getAudioContent();

            $audioTitle = time() + Auth::id()."-".time().".mp3";
            $audioPath = storage_path('app/' .Paths::AUDIO_PATH. $audioTitle);

            // the response's audioContent is binary
            file_put_contents($audioPath, $audioContent);

            $client->close();
            exec("chmod 777 $audioPath");
            
            return [
                'audioTitle' => $audioTitle,
                'status' => true
            ];


        }catch(Exception $error){
            Log::info('SpeechController@convertTextToSpeech error message: ' . $error->getMessage());
            return [
                'audioTitle' => '',
                'status' => false
            ];
        }
    }

    public function download($speechId){
        $speech = SpeechModel::where('user_id', Auth::id())->where('id', $speechId)->first();
        if(!$speech){
            abort(404);
        }
        return response()->download(storage_path('app/' .Paths::AUDIO_PATH. $speech->audio));
    }

    public function delete(Request $request){
        try{

            $speech = SpeechModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$speech) {
                $message = "Speech was not found";
                return response()->json(['message' => $message], 404);
            }

            if($speech->audio){
                $this->deleteAudio($speech->audio);
            }
            $speech->delete();
            $message = "Speech deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('SpeechController@delete error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }

    private function deleteAudio($audioName)
    {
        $path = Paths::AUDIO_PATH;
        $filename = $path . $audioName;

        if (Storage::has($filename)) {
            Storage::delete($filename);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
