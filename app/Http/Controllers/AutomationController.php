<?php

namespace App\Http\Controllers;

use App\Models\CampaignLogModel;
use Auth, Session, Exception, Log;
use App\User;
use App\Models\CTAModel;
use App\Models\ContactModel;
use App\Models\VideoModel;
use App\Models\ConnectionTagModel;
use App\Models\WorkflowModel;
use Illuminate\Http\Request;
use App\Models\CustomTemplateModel;
use App\Models\EmailIntegrationModel;
use App\Models\AutomationEmailModel;
use App\Models\NodeModel;

class AutomationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function emails(){
        try{

            $emails = AutomationEmailModel::where('user_id', Auth::id())->get();

            $data = [
                'page' => 'automation',
                'sub' => 'auto-emails',
                'emails' => $emails
            ];
            return view('App.Automations.emails', $data);

        }catch(Exception $error){
            Log::info('AutomationController@emails error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function createEmailCampaign(){
        try{

            $campaign = null;
            $videos = VideoModel::where('user_id', Auth::id())->get();
            $tags = ConnectionTagModel::where('user_id', Auth::id())->get();
            $campaigns = AutomationEmailModel::where('user_id', Auth::id())->get();
            $contacts = ContactModel::where('user_id', Auth::id())->get();
            $emailTemplates = CustomTemplateModel::where('user_id', Auth::id())->get();
            $emailIntegrations = EmailIntegrationModel::where('user_id', Auth::id())->where('is_active', true)->get();
            $data = [
                'page' => 'automation',
                'sub' => 'auto-emails',
                'tags' => $tags,
                'campaign' => $campaign,
                'videos' => $videos,
                'contacts' => $contacts,
                'campaigns' => $campaigns,
                'emailTemplates' => $emailTemplates,
                'emailIntegrations' => $emailIntegrations,
            ];
            return view('App.Automations.create-email', $data);

        }catch(Exception $error){
            Log::info('AutomationController@createEmailCampaign error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function editEmailCampaign($campaignId){
        try{

            $campaign = AutomationEmailModel::where('user_id', Auth::id())
                ->where('id', $campaignId)
                ->first();
            $videos = VideoModel::where('user_id', Auth::id())->get();
            $tags = ConnectionTagModel::where('user_id', Auth::id())->get();
            $campaigns = AutomationEmailModel::where('user_id', Auth::id())->get();
            $contacts = ContactModel::where('user_id', Auth::id())->get();
            $emailTemplates = CustomTemplateModel::where('user_id', Auth::id())->get();
            $emailIntegrations = EmailIntegrationModel::where('user_id', Auth::id())->where('is_active', true)->get();
            $data = [
                'page' => 'campaigns',
                'sub' => 'emails',
                'tags' => $tags,
                'campaign' => $campaign,
                'videos' => $videos,
                'contacts' => $contacts,
                'campaigns' => $campaigns,
                'emailTemplates' => $emailTemplates,
                'emailIntegrations' => $emailIntegrations,
            ];
            return view('App.Automations.create-email', $data);

        }catch(Exception $error){
            Log::info('AutomationController@createEmailCampaign error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function saveEmailCampaign(Request $request){
        try{
            if($request->id != ''){
                $campaign = AutomationEmailModel::where('user_id', Auth::id())
                    ->where('id', $request->id)->first();
                if(!$campaign){
                    return response()->json([
                        'status' => 'error',
                        'message' => "Unable to complete request. Email not found"
                    ], 404);
                }
            }else{
                $campaign = new AutomationEmailModel;
            }
            $campaign->user_id = Auth::id();
            $campaign->title = $request->title;
            $campaign->provider_id = $request->provider_id;
            $campaign->video_id = $request->video_id;
            $campaign->from_name = $request->from_name;
            $campaign->subject = $request->subject;
            $campaign->video_header = $request->video_header;
            $campaign->template_id = $request->template_id;
            $campaign->email_body = $request->email_body;

            $campaign->is_draft = ($request->is_save_and_publish == 'true') ? false : true;
            $campaign->save();

            return response()->json([
                'status' => 'success',
                'message' => "Draft Saved",
                'campaign' => $campaign
            ]);

        }catch(Exception $error){
            Log::info('AutomationController@saveEmailCampaign error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'error' => true,
                "message" => $message,
            ], 500);
        }
    }

    public function workflows(){
        try{

            $workflows = WorkflowModel::where('user_id', Auth::id())->get();

            $data = [
                'page' => 'automation',
                'sub' => 'workflows',
                'workflows' => $workflows
            ];
            return view('App.Automations.workflows', $data);

        }catch(Exception $error){
            Log::info('AutomationController@workflows error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function createWorkflowCampaign(){
        try{

            $workflow = null;
            $nodes = null;
            $ctas = CTAModel::where('user_id', Auth::id())->get();

            $videos = VideoModel::where('user_id', Auth::id())->get();
            $tags = ConnectionTagModel::where('user_id', Auth::id())->get();
            $campaigns = AutomationEmailModel::where('user_id', Auth::id())->get();
            $contacts = ContactModel::where('user_id', Auth::id())->get();
            $emailTemplates = CustomTemplateModel::where('user_id', Auth::id())->get();
            $emailIntegrations = EmailIntegrationModel::where('user_id', Auth::id())->where('is_active', true)->get();
            $data = [
                'page' => 'automation',
                'sub' => 'workflows',
                'tags' => $tags,
                'ctas' => $ctas,
                'workflow' => $workflow,
                'nodes' => $nodes,
                'videos' => $videos,
                'contacts' => $contacts,
                'campaigns' => $campaigns,
                'emailTemplates' => $emailTemplates,
                'emailIntegrations' => $emailIntegrations,
            ];
            return view('App.Automations.create-workflow', $data);

        }catch(Exception $error){
            Log::info('AutomationController@createWorkflowCampaign error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function editWorkflowCampaign($workflowId){
        try{

            $workflow = WorkflowModel::where('user_id', Auth::id())
                ->where('id', $workflowId)
                ->first();
            if(!$workflow){
                $message = 'Unable to get Resource. Workflow not found.';
                return $this->handleError($message);
            }
            $nodes = NodeModel::where('user_id', Auth::id())
                ->where('workflow_id', $workflowId)
                ->get();
            $ctas = CTAModel::where('user_id', Auth::id())->get();

            $videos = VideoModel::where('user_id', Auth::id())->get();
            $tags = ConnectionTagModel::where('user_id', Auth::id())->get();
            $campaigns = AutomationEmailModel::where('user_id', Auth::id())->get();
            $contacts = ContactModel::where('user_id', Auth::id())->get();
            $emailTemplates = CustomTemplateModel::where('user_id', Auth::id())->get();
            $emailIntegrations = EmailIntegrationModel::where('user_id', Auth::id())->where('is_active', true)->get();
            $data = [
                'page' => 'automation',
                'sub' => 'workflows',
                'tags' => $tags,
                'ctas' => $ctas,
                'workflow' => $workflow,
                'nodes' => $nodes,
                'videos' => $videos,
                'contacts' => $contacts,
                'campaigns' => $campaigns,
                'emailTemplates' => $emailTemplates,
                'emailIntegrations' => $emailIntegrations,
            ];
            return view('App.Automations.create-workflow', $data);

        }catch(Exception $error){
            Log::info('AutomationController@createWorkflowCampaign error message: ' . $error->getMessage());
            $message = 'Unable to get Resource. Encountered an error.';
            return $this->handleError($message);
        }
    }

    public function saveWorkflow(Request $request){
        try{
            if($request->id == ''){
                $workflow = new WorkflowModel;
            }else{
                $workflow = WorkflowModel::where('id', $request->id)->where('user_id', Auth::id())->first();
                if(!$workflow){
                    return response()->json([
                        'status' => 'error',
                        'message' => "Unable to complete request. Workflow not found"
                    ], 404);
                }
            }
            $workflow->user_id = Auth::id();
            $workflow->name = $request->name;
            $workflow->trigger = $request->trigger;
            $workflow->includes = $request->includes;
            $workflow->elements = json_decode($request->elements);
            $workflow->build = $request->build;
            $workflow->trigger_once = $request->trigger_once == 'true';
            $workflow->is_active = $request->is_active == 'true';
            $workflow->terms_accepted = $request->terms_accepted == 'true';
            $workflow->save();
            return response()->json([
                'status' => 'success',
                'message' => "Draft Saved",
                'workflow' => $workflow
            ]);

        }catch(Exception $error){
            Log::info('AutomationController@saveWorkflow error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'error' => true,
                "message" => $message,
            ], 500);
        }
    }

    public function deleteWorkflow(Request $request){
        try{

            $workflow = WorkflowModel::where('id', $request->id)->where('user_id', Auth::id())->first();
            if (!$workflow) {
                $message = "Workflow was not found";
                return response()->json(['message' => $message], 404);
            }

            $workflow->delete();
            $message = "Workflow deleted successfully";
            return response()->json(['message' => $message]);

        }catch(Exception $error){
            Log::info('AutomationController@deleteWorkflow error message: ' . $error->getMessage());
            $message = 'Unable to delete Resource. Encountered an error.';
            return response()->json([
                'error' => true,
                'status_code' => 404,
                "message" => $message,
            ], 500);
        }
    }


    public function saveNode(Request $request){
        try{
            $node = new NodeModel;
            $node->user_id = Auth::id();
            $node->workflow_id = $request->workflow_id;
            $node->action = $request->action;
            if($request->action != 'condition'){
                $node->type = 'operation';
            }else{
                $node->type = 'condition';
            }
            $node->parent = $request->parent;
            $node->normal_branch = $request->normal_branch;
            $node->yes_branch = $request->yes_branch;
            $node->no_branch = $request->no_branch;

            $node->condition = $request->condition;
            $node->add_tags = json_decode($request->add_tags);
            $node->remove_tags = json_decode($request->no_branch);

            $node->element_id = $request->element_id;
            $node->task_title = $request->task_title;
            $node->task_description = $request->task_description;
            $node->task_assignee = $request->task_assignee;

            $node->task_due_after = $request->task_due_after;
            $node->task_unit = $request->task_unit;
            $node->delay = $request->delay;
            $node->unit = $request->unit;
            $node->campaign_id = $request->campaign_id;
            $node->save();

            $node->node_id = 'node_'. $node->id;
            $node->save();
            $this->updateNodeParent($request, $node);
            $nodes = NodeModel::where('workflow_id', $request->workflow_id)->get();
            return response()->json([
                'status' => 'success',
                'message' => "Node Saved",
                'nodes' => $nodes
            ]);

        }catch(Exception $error){
            Log::info('AutomationController@saveNode error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'error' => true,
                "message" => $message,
            ], 500);
        }
    }

    private function updateNodeParent($request, $node){
        $parentNode = NodeModel::where('node_id', $request->parent)->first();
        if($parentNode){
            if($request->condition_type == 'normal'){
                $parentNode->normal_branch = $node->node_id;
            }elseif($request->condition_type == 'yes'){
                $parentNode->yes_branch = $node->node_id;
            }elseif($request->condition_type == 'no'){
                $parentNode->no_branch = $node->node_id;
            }
            $parentNode->save();
        }
    }

    public function updateNode(Request $request){
        try{
            $workflow = WorkflowModel::where('id', $request->workflow_id)
                ->where('user_id', Auth::id())->first();
            if(!$workflow){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Workflow not found"
                ], 404);
            }

            $node = NodeModel::where('id', $request->id)
                ->where('user_id', Auth::id())->first();

            if(!$node){
                return response()->json([
                    'status' => 'error',
                    'message' => "Unable to complete request. Node not found"
                ], 404);
            }
            
            $node->condition = $request->condition;
            $node->add_tags = json_decode($request->add_tags);
            $node->remove_tags = json_decode($request->no_branch);

            $node->element_id = $request->element_id;
            $node->task_title = $request->task_title;
            $node->task_description = $request->task_description;
            $node->task_assignee = $request->task_assignee;

            $node->task_due_after = $request->task_due_after;
            $node->task_unit = $request->task_unit;
            $node->delay = $request->delay;
            $node->unit = $request->unit;
            $node->campaign_id = $request->campaign_id;
            $node->save();

            $nodes = NodeModel::where('workflow_id', $request->workflow_id)->get();
            return response()->json([
                'status' => 'success',
                'message' => "Node updated",
                'nodes' => $nodes
            ]);

        }catch(Exception $error){
            Log::info('AutomationController@updateNode error message: ' . $error->getMessage());
            $message = 'Unable to complete request. Encountered an error.';
            return response()->json([
                'error' => true,
                "message" => $message,
            ], 500);
        }
    }

    private function handleError($message){
        Session::put('errorMessage', $message);
        return redirect()->back();
    }
}
