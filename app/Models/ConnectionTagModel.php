<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConnectionTagModel extends Model
{
    protected $table = 'connection_tags';

    protected $fillable = [
        'user_id',
        'name'
    ];
}
