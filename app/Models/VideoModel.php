<?php

namespace App\Models;

use Auth;
use Storage;
use App\Models\CTAClickModel;
use App\Models\PageViewModel;
use App\Models\VideoViewModel;
use App\Models\EmailOpenModel;
use App\Models\EmailClickModel;
use App\Models\ReactionClickModel;
use App\Helpers\Paths;
use Illuminate\Database\Eloquent\Model;

class VideoModel extends Model
{
    protected $table = "videos";


    protected $appends = [
        'video_path',
        'video_thumbnail_path',
        'last_updated',
        'tags',
        'video_url',
        'vtt_file',
        'thumbnail_video_url',
        'video_gif_path',

        "cta_clicks",
        "video_views",
        "email_opens",
        "email_clicks",
        "page_views",
        // "call_rates",
    ];

    public function getCtaClicksAttribute(){
        return CTAClickModel::where('video_id', $this->id)->count();
    }

    public function getVideoViewsAttribute(){
        return VideoViewModel::where('video_id', $this->id)->count();
    }

    public function getEmailOpensAttribute(){
        return EmailOpenModel::where('video_id', $this->id)->count();
    }

    public function getEmailClicksAttribute(){
        return EmailClickModel::where('video_id', $this->id)->count();
    }

    public function getPageViewsAttribute(){
        return PageViewModel::where('video_id', $this->id)->count();
    }

    public function getVideoUrlAttribute(){
        return route('user.videos.view', $this->uuid);
    }

    public function getThumbnailVideoUrlAttribute(){
        $message = "Check out this Video Message from ".
            Auth::user()->name." ". route('user.videos.view', $this->uuid);
        return $message;
    }
    public function getVideoPathAttribute(){
        $video = Paths::VIDEO_PATH .$this->file;
        if(Storage::has($video)){
            $path = \Request::root().Storage::disk('local')->url($video);
            return $path;

        }
    }

    public function getVttFileAttribute(){
        if($this->srt_file == null || $this->srt_file == ""){
            return '';
        }
        $vttFileName = str_replace('.srt', '.vtt',$this->srt_file);

        $vttFile = Paths::SRT_FILES_PATH .$vttFileName;
        if(Storage::has($vttFile)){
            $path = \Request::root().Storage::disk('local')->url($vttFile);
            return $path;

        }

        return '';
    }

    public function getVideoThumbnailPathAttribute(){
        $thumbnail = Paths::VIDEO_THUMBNAIL_PATH .$this->thumbnail;
        if(Storage::has($thumbnail) && $this->thumbnail != null){
            $path = \Request::root().Storage::disk('local')->url($thumbnail);
            return $path;

        }else{
            return asset('img/thumbnail.jpeg');
        }
    }

    public function getVideoGifPathAttribute(){
        $gif = Paths::VIDEO_GIF_PATH .$this->video_gif;
        if(Storage::has($gif) && $this->video_gif != null){
            $path = \Request::root().Storage::disk('local')->url($gif);
            return $path;

        }else{
            return '';
        }
    }

    public function videoDetails()
    {
        return $this->hasOne('App\Models\VideoDetailsModel', 'video_id', 'id');
    }

    public function videoDesign()
    {
        return $this->hasOne('App\Models\VideoDesignModel', 'video_id', 'id');
    }

    public function videoShare()
    {
        return $this->hasOne('App\Models\VideoShareModel', 'video_id', 'id');
    }

    public function getLastUpdatedAttribute(){
        return time_elapsed_string($this->updated_at);
    }

    public function user(){
		return $this->belongsTo('App\User','user_id','id');
    }

    public function getTagsAttribute(){
        if($this->videoDetails){
            return $this->videoDetails->tags;
        }

        return [];
    }
}
