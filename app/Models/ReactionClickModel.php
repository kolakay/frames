<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReactionClickModel extends Model
{
    protected $table = "reaction_clicks";
}
