<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutomationEmailModel extends Model
{
    protected $table = "automation_emails";
}
