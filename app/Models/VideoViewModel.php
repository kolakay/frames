<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoViewModel extends Model
{
    protected $table = "video_views";
}
