<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CTAModel extends Model
{
    protected $table = 'ctas';
    protected $with = [
        'form'
    ];
    protected $appends = [
        'e_button_text',
        'u_button_text',
        'p_button_text',
        'd_button_text',
        'ph_button_text',
        'ch_button_text',
        'ca_button_text',
        'vid_button_text',
        'scroll_set',
        'height_set',
        'document_url'
    ];

    public function getEButtonTextAttribute(){
        $text = ($this->email_button_text == 'custom')?
            $this->email_custom_button_text : $this->email_button_text;



        return $text;
    }

    public function getUButtonTextAttribute(){
        $text = ($this->url_button_text == 'custom')?
            $this->url_custom_button_text : $this->url_button_text;



        return $text;
    }

    public function getDButtonTextAttribute(){
        $text = ($this->document_button_text == 'custom')?
            $this->document_custom_button_text : $this->document_button_text;



        return $text;
    }

    public function getPButtonTextAttribute(){
        $text = ($this->product_button_text == 'custom')?
            $this->product_custom_button_text : $this->product_button_text;



        return $text;
    }

    public function getChButtonTextAttribute(){
        $text = ($this->chat_button_text == 'custom')?
            $this->chat_custom_button_text : $this->chat_button_text;



        return $text;
    }

    public function getCaButtonTextAttribute(){
        $text = ($this->calendar_button_text == 'custom')?
            $this->calendar_custom_button_text : $this->calendar_button_text;



        return $text;
    }

    public function getVidButtonTextAttribute(){
        $text = ($this->document_button_text == 'custom')?
            $this->document_custom_button_text : $this->document_button_text;



        return $text;
    }

    public function getPhButtonTextAttribute(){
        $text = ($this->phone_button_text == 'custom')?
            $this->phone_custom_button_text : $this->phone_button_text;



        return $text;
    }

    public function getHeightSetAttribute(){
        $text = ($this->web_page_height != null && $this->web_page_height != '')?
            $this->web_page_height : 'auto';



        return $text;
    }

    public function getScrollSetAttribute(){
        $text = ($this->web_page_scroll)?
            'no' : "yes";



        return $text;
    }

    public function getDocumentUrlAttribute(){
        if($this->document_file != ''){
            return route('user.assets.document.download', $this->document_file);
        }
        return '#';
    }

    public function form()
    {
        return $this->belongsTo('App\Models\FormModel', 'form', 'id');
    }
}
