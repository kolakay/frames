<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class TaskModel extends Model
{
    protected $table = "tasks";

    protected $fillable = [
		'user_id',
	];

	protected $appends = [ 'creator' ];

	public function getCreatorAttribute() {
		$name = Auth::user()->name;
		return $name;
	}
}
