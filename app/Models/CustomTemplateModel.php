<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomTemplateModel extends Model
{
    protected $table = "custom_templates";
}
