<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageViewModel extends Model
{
    protected $table = "page_views";
}
