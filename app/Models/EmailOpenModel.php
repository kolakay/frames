<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailOpenModel extends Model
{
    protected $table = "email_opens";
}
