<?php

namespace App\Models;
use Log;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DealsModel extends Model
{
    protected $table = "deals";

    protected $fillable = [
		'user_id',
	];

    protected $appends = [ 'display_closed_date' ];

    protected $dates = [ 'closed_date' ];

    // public function getClosedDateAttribute($value){
    // 	return Carbon::toDateString($value);
    // }

    public function getAmountAttribute($value) {
    	if (isset($value)) {
    		return $value;
    	}
    	return "";
    }
    public function getOwnerAttribute($value) {
    	if (isset($value)) {
    		return $value;
    	}
    	return "";
    }

    public function getDisplayClosedDateAttribute() {
    	$date = substr($this->closed_date, 0, 10);
    	return $date;
    }
}
