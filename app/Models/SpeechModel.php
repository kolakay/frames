<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpeechModel extends Model
{
    protected $table = "speeches";
}
