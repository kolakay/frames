<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class LandingPageModel extends Model
{
    protected $table = "landing_pages";

    protected $appends = [
        'creator',
        'page_url',
        'page_status',
        'created_time'
    ];

    public function getCreatorAttribute(){
        $user = User::where('id', $this->user_id)->first();
        if($user){
            return $user->name;
        }


        return '';
    }

    public function getPageUrlAttribute(){
        if($this->slug){
            return route('user.assets.landing-pages.page', $this->slug);
        }


        return '';
    }

    public function getCreatedTimeAttribute(){
        return time_elapsed_string($this->created_at);
    }

    public function getPageStatusAttribute(){
        if($this->is_active){
            return "Published";
        }


        return "Draft";
    }
}
