<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailCampaignModel extends Model
{
    protected $table = "email_campaigns";

    protected $appends = [
        'parsed_date'
    ];

    protected $casts = [
        'emails' => 'array',
        'schedule_is_set' => 'boolean',
        'inclusion_list' => 'array',
        'exclusion_list' => 'array'
    ];

    public function getParsedDateAttribute(){
        $parsedDate = date('Y-m-d h:m A', $this->date_time);
        return $parsedDate;
    }
}
