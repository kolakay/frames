<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralStatLogModel extends Model
{
    protected $table = "general_stat_logs";

    protected $with = [
        'video'
    ];

    protected $appends = [
        'parsed_type',
        'parsed_created_at'
    ];

    public function video()
    {
        return $this->belongsTo('App\Models\VideoModel', 'video_id', 'id');
    }

    public function getParsedCreatedAtAttribute(){
        return time_elapsed_string($this->created_at);
    }

    public function getParsedTypeAttribute(){
        $typeIsSet = isset($this->getLogTypeName()[$this->type])? true : false;
        $type = ($typeIsSet)? $this->getLogTypeName()[$this->type] : $this->type;

        return $type;
    }

    private function getLogTypeName(){
        return [
            "cta" => "CTA Clicks",
            "video_view" => "Watch Rates",
            "email_open" => "Email Opens",
            "email_click" => "Email Clicks",
            "reaction" => "Reaction Clicks",
            "call_rate" => "Call Rates",
            "page_view" => "Page View"
        ];
    }
}
