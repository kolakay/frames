<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CTAClickModel extends Model
{
    protected $table = "cta_clicks";
}
