<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoDetailsModel extends Model
{
    protected $table = "video_details";

    protected $casts = [
        'auto_play' => 'boolean',
        'add_exit_intent_modal' => 'boolean',
        'add_emoji_response_buttons' => 'boolean',
        'add_facebook_comment_widget' => 'boolean',
        'add_messaging' => 'boolean',
        'share_with_team' => 'boolean',
        'disable_tracking' => 'boolean',
        'allow_video_download' => 'boolean',
        'auto_delete' => 'boolean',
        'ctas' => 'array',
        'tags' => 'array',
        'playlist' => 'array'
    ];

    protected $fillable = [
        'user_id', 'video_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    protected $appends = [
        'parsed_date',
        'curated_list',
        'curated_ctas',
    ];


    public function getParsedDateAttribute(){
        $parsedDate = date("Y-m-d", $this->auto_delete_date);
        if($parsedDate){
            return $parsedDate;
        }

        return '';
    }

    public function getCuratedListAttribute(){
        $videos = [];
        if(!is_array($this->playlist)){
            return [];
        }
        foreach($this->playlist as $videoId){
            $video = VideoModel::where('id', $videoId)->first();
            if($video){
                $videos[] = $video;
            }
        }

        return $videos;
    }

    public function getCuratedCtasAttribute(){
        $ctas = [];
        if(!is_array($this->ctas)){
            return [];
        }
        // dd($this->ctas);
        foreach($this->ctas as $ctaId){
            $cta = CTAModel::where('id', $ctaId)->first();
            if($cta){
                $ctas[] = $cta;
            }
        }
        return $ctas;
    }
}
