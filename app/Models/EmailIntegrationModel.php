<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailIntegrationModel extends Model
{
    protected $table = "mail_integrations";

    protected $casts = [
        'access_token' => 'json',
        'is_active' => 'boolean'
    ];
}
