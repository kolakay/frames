<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TranscriptionModel extends Model
{
    protected $table = "video_transcriptions";

    protected $fillable = [
        'video_id', 'user_id',
    ];
}
