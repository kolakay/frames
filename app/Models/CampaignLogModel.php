<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignLogModel extends Model
{
    protected $table = "campaign_logs";
}
