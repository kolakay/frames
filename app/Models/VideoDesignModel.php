<?php

namespace App\Models;

use Storage;
use App\Helpers\Paths;
use Illuminate\Database\Eloquent\Model;

class VideoDesignModel extends Model
{
    protected $table = 'video_designs';

    protected $fillable = [
        'user_id', 'video_id'
    ];

    protected $casts = [
        'display_logo' => 'boolean',
        'default_player_color' => 'boolean',
        'default_player_background_image' => 'boolean',
        'default_cta_color' => 'boolean',
        'default_cta_background_image' => 'boolean',
        'has_password' => 'boolean'
    ];

    protected $appends = [
        'player_background_image_src',
        'cta_background_image_src'
    ];

    public function getPlayerBackgroundImageSrcAttribute(){
        if(!$this->player_background_image){
            return '';
        }
        $player_background_image = Paths::BACKGROUND_IMAGE_PATH .$this->player_background_image;
        if(Storage::has($player_background_image)){
            $path = \Request::root().Storage::disk('local')->url($player_background_image);
            return $path;

        }
    }

    public function getctabackgroundimagesrcAttribute(){
        if(!$this->cta_background_image){
            return '';
        }
        $cta_background_image = Paths::BACKGROUND_IMAGE_PATH .$this->cta_background_image;
        if(Storage::has($cta_background_image)){
            $path = \Request::root().Storage::disk('local')->url($cta_background_image);
            return $path;

        }
    }
}
