<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuilderMediaModel extends Model
{
    protected $table = "builder_medias";
}
