<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NodeModel extends Model
{
    protected $table = "nodes";
    
    protected $casts = [
        'add_tags' => 'array',
        'remove_tags' => 'array',
    ];

    protected $appends = [
        'node_text'
    ];

    public function getParentAttribute($value){
        if($value == null){
            return '';
        }else{
            return $value;
        }
    }

    public function getNormalBranchAttribute($value){
        if($value == null){
            return '';
        }else{
            return $value;
        }
    }

    public function getNoBranchAttribute($value){
        if($value == null){
            return '';
        }else{
            return $value;
        }
    }

    public function getYesBranchAttribute($value){
        if($value == null){
            return '';
        }else{
            return $value;
        }
    }
    public function getNodeTextAttribute(){
        return $this->getNodeTexts()[$this->action];
    }

    private function getNodeTexts(){
        return [
            'add_tag' => 'Add Tag',
            'condition' => 'If/Then Branch',
            'create_task' => 'Create Task',
            'delay' => 'Delay',
            'remove_tag' => 'Remove Tag',
            'send_campaign' => 'Send Campaign'
        ];
    }
}
