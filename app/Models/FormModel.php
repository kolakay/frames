<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormModel extends Model
{
    protected $table = "forms";

    protected $casts = [
        'fields' => 'array',
        'tags' => 'array'
    ];

    protected $appends = [
        'form_button_text'
    ];

    public function getFormButtonTextAttribute(){
        $text = ($this->button_text == 'custom')?
            $this->custom_button_text : $this->button_text;



        return $text;
    }

}
