<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactModel extends Model
{
    protected $table = 'contacts';

    protected $casts = [
        'tags' => 'array'
    ];
    protected $appends = [
        'full_name',
        'full_name_display'
    ];

    public function getFullNameAttribute(){
        $text = $this->first_name . $this->last_name;



        return $text;
    }

    public function getFullNameDisplayAttribute(){
        $text = "$this->first_name $this->last_name ($this->email)" ;



        return $text;
    }

    // public function getCreatedAtAttribute() {
    //     $date = $this->created_at->format('Y-m-d H:i:s');
    //     $date = substr($date, 0, 10);
    //     return $date;
    // }
    // public function getUpdatedAtAttribute() {
    //     return substr($this->created_at, 0, 10);
    // }
}
