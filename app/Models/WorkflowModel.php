<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkflowModel extends Model
{
    protected $table = "workflows";
    protected $casts = [
        'elements' => 'array',
        'trigger_once' => 'boolean',
        'is_active' => 'boolean',
    ];
}
