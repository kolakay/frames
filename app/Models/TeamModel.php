<?php

namespace App\Models;
use App\Helpers\Paths;
use Illuminate\Database\Eloquent\Model;
use Storage;

class TeamModel extends Model {
	protected $table = 'team_table';

	protected $fillable = [
		'user_id',
	];
	protected $appends = [
		'team_image_path',
	];

	public function getTeamImagePathAttribute() {
		$team_image = Paths::TEAM_IMAGE_PATH . $this->team_image;
		if (Storage::has($team_image) && $this->team_image != null) {
			$path = \Request::root() . Storage::disk('local')->url($team_image);
			return $path;
		} else {
			return asset('img/unknown.jpg');
		}
	}
}