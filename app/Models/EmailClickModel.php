<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailClickModel extends Model
{
    protected $table = "email_clicks";
}
