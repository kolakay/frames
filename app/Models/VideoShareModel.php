<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoShareModel extends Model
{
    protected $table = "video_shares";

    protected $casts = [
        'disable_gif_animation' => 'boolean',
        'disable_url_text_encoding' => 'boolean',
        'disable_email_open_tracking' => 'boolean',
        'embed_disable_cta_overlay' => 'boolean',
        'embed_autoplay' => 'boolean',
        'embed_sound_off' => 'boolean',
        'widget_autoplay' => 'boolean',
        'widget_sound_off' => 'boolean',
        'widget_show_cta' => 'boolean',
        'widget_hide_in_mobile' => 'boolean'
    ];

    protected $fillable = [
        'video_id'
    ];
}
