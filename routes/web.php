<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
// Route::get('/test/{id}', 'VideoTranscription@storeSubrip');
// Route::get('/builder', function(){
//     return view('Builder.editor');
// })->name('auth.test');
Route::get('/login', 'Auth\LoginController@loginView')->name('auth.login');
Route::get('/logout', 'Auth\LoginController@logout')->name('auth.logout');
Route::post('/login', 'Auth\LoginController@login')->name('auth.login.account');

Route::get('/forgot-password', 'Auth\ForgotPasswordController@forgotPassword')->name('auth.forgot-password');
Route::post('/recover-password', 'Auth\ForgotPasswordController@recoverPassword')->name('auth.recover-password');

Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@resetPassword')->name('auth.reset-password');
Route::post('/reset-password', 'Auth\ResetPasswordController@updatePassword')->name('auth.update-password');

Route::get('/', 'DashboardController@index')->name('user.dashboard');
Route::get('/videos', 'VideoController@videos')->name('user.videos.videos');
Route::get('/video/upload', 'VideoController@uploadPage')->name('user.videos.upload');
Route::post('/video/upload', 'VideoController@uploadVideo')->name('user.videos.submit');
Route::get('/video/edit/{videoUUID?}', 'VideoController@edit')->name('user.videos.edit');
Route::delete('/video/delete', 'VideoController@deleteSelectedVideo')->name('user.videos.delete');

Route::get('/video/replace/{videoUUID?}', 'VideoController@videoReplacePage')->name('user.videos.replace');
Route::post('/video/replace/{videoUUID?}', 'VideoController@replaceVideo')->name('user.videos.replace.upload');

Route::post('/videos/edit/details', 'VideoController@updateDetails')->name('user.videos.details.update');
Route::get('/video/view/{videoUUID?}', 'VideoController@viewVideo')->name('user.videos.view');
Route::post('/video/edit/name', 'VideoController@updateVideoName')->name('user.videos.edit.name');
Route::get('/video/download/gif/{videoUUID?}', 'VideoController@downloadGif')->name('user.videos.download.gif');
Route::get('/video/download/caption/{videoUUID?}', 'VideoController@downloadCaption')->name('user.videos.download.caption');

Route::get('/video/view/{videoUUID}/embed', 'ShareController@index')->name('user.videos.embed.widget');
Route::get('/video/log/{videoUUID?}', 'VideoController@logEmailOpen')->name('user.videos.log');
Route::post('/video/log', 'AnalyticLogController@logAnalytic')->name('user.videos.log.clicks');
Route::post('/video/chart/data', 'AnalyticLogController@getGraphData')->name('user.videos.chart.data');

Route::get('/videos/tags', 'TagsController@index')->name('user.videos.tags');
Route::post('/videos/tags/create', 'TagsController@create')->name('user.videos.tags.create');
Route::post('/videos/tags/update', 'TagsController@update')->name('user.videos.tags.update');
Route::delete('/videos/tags/delete', 'TagsController@delete')->name('user.videos.tags.delete');

Route::get('/assets/ctas', 'Assets\CTAController@index')->name('user.assets.ctas');
Route::post('/assets/ctas/create', 'Assets\CTAController@create')->name('user.assets.ctas.create');
Route::post('/assets/ctas/update', 'Assets\CTAController@update')->name('user.assets.ctas.update');
Route::delete('/assets/ctas/delete', 'Assets\CTAController@delete')->name('user.assets.ctas.delete');
Route::post('/assets/ctas/document/upload', 'Assets\CTAController@storeDocument')->name('user.assets.ctas.document.upload');

Route::get('/assets/ctas/download/{documentName}', 'Assets\CTAController@download')->name('user.assets.document.download');



Route::post('/videos/edit/thumbnail', 'VideoController@createThumbnail')->name('user.videos.edit.thumbnail');
Route::post('/videos/edit/trim', 'VideoController@trimVideo')->name('user.videos.edit.trim');
Route::post('/videos/edit/design', 'VideoController@storeVideoDesign')->name('user.videos.edit.design');
Route::post('/videos/edit/share', 'VideoController@storeVideoShare')->name('user.videos.edit.share');
Route::post('/videos/edit/send/mail', 'VideoController@sendVideoMail')->name('user.videos.edit.send.mail');
Route::post('/videos/edit/srt', 'VideoController@storeSRT')->name('user.videos.edit.srt');
Route::post('/videos/edit/transcribe', 'VideoController@sendTranscriptionRequest')->name('user.videos.edit.transcribe');
Route::post('/videos/edit/concatenate', 'VideoController@concatenateVideos')->name('user.videos.edit.concatenate');

Route::post('/transcribe/callback', 'VideoTranscription@callback')->name('user.videos.transcribe.callback');

Route::get('/videos/edit/send/mail', 'VideoController@sendVideoMail')->name('user.videos.edit.send.mail.get');


Route::get('/assets/forms', 'Assets\FormController@index')->name('user.assets.forms');
Route::post('/assets/forms/create', 'Assets\FormController@create')->name('user.assets.forms.create');
Route::post('/assets/forms/update', 'Assets\FormController@update')->name('user.assets.forms.update');
Route::delete('/assets/forms/delete', 'Assets\FormController@delete')->name('user.assets.forms.delete');

Route::get('/assets/landing-pages', 'Assets\LandingPageController@index')->name('user.assets.landing-pages');
Route::post('/assets/landing-pages/create', 'Assets\LandingPageController@create')->name('user.assets.landing-pages.create');
Route::post('/assets/landing-pages/update', 'Assets\LandingPageController@update')->name('user.assets.landing-pages.update');
Route::delete('/assets/landing-pages/delete', 'Assets\LandingPageController@delete')->name('user.assets.landing-pages.delete');
Route::post('/assets/landing-pages/clone', 'Assets\LandingPageController@clone')->name('user.assets.landing-pages.clone');
Route::post('/assets/landing-pages/edit/{PageId?}', 'Assets\LandingPageController@edit')->name('user.assets.landing-pages.edit');

Route::get('/pages/{pageSlug}', 'LandingPageController@page')->name('user.assets.landing-pages.page');

Route::get('/connections/tags', 'ConnectionTagsController@index')->name('user.connections.tags');
Route::post('/connections/tags/create', 'ConnectionTagsController@create')->name('user.connections.tags.create');
Route::post('/connections/tags/update', 'ConnectionTagsController@update')->name('user.connections.tags.update');
Route::delete('/connections/tags/delete', 'ConnectionTagsController@delete')->name('user.connections.tags.delete');

Route::get('/connections/companies', 'CompaniesController@index')->name('user.connections.companies');
Route::post('/connections/companies/create', 'CompaniesController@create')->name('user.connections.companies.create');
Route::post('/connections/companies/update', 'CompaniesController@update')->name('user.connections.companies.update');
Route::delete('/connections/companies/delete', 'CompaniesController@delete')->name('user.connections.companies.delete');

Route::get('/connections/contacts', 'ContactsController@index')->name('user.connections.contacts');
Route::post('/connections/contacts/create', 'ContactsController@create')->name('user.connections.contacts.create');
Route::post('/connections/contacts/update', 'ContactsController@update')->name('user.connections.contacts.update');
Route::delete('/connections/contacts/delete', 'ContactsController@delete')->name('user.connections.contacts.delete');


Route::get('/settings/broadcast-settings', 'Settings\BroadcastSettingsController@index')->name('user.settings.broadcast');

Route::post('/settings/broadcast-settings/create', 'Settings\BroadcastSettingsController@create')->name('user.settings.broadcast.create');
Route::post('/settings/broadcast-settings/update', 'Settings\BroadcastSettingsController@update')->name('user.settings.broadcast.update');
Route::delete('/settings/broadcast-settings/delete', 'Settings\BroadcastSettingsController@delete')->name('user.settings.broadcast.delete');


Route::get('/settings/integrations/callback/gmail', 'Integrations\GmailController@callback')->name('user.settings.integrations.callback.gmail');
Route::get('/settings/integrations/connect/gmail', 'Integrations\GmailController@connect')->name('user.settings.integrations.connect.gmail');

Route::get('/builder/{templateId?}', 'Builder\BuilderController@index')->name('user.builder');
Route::post('/builder/save', 'Builder\BuilderController@save')->name('user.builder.save');
Route::post('/builder/upload', 'Builder\BuilderController@upload')->name('user.builder.save');
Route::get('/builder/{templateId}/canvas', 'Builder\BuilderController@canvas')->name('user.builder.canvas');
Route::get('/builder/preview/{templateId?}', 'Builder\BuilderController@canvas')->name('user.builder.preview');

Route::get('/assets/email-templates', 'Assets\EmailTemplateController@index')->name('user.assets.templates');
Route::post('/assets/email-templates/create', 'Assets\EmailTemplateController@create')->name('user.assets.templates.create');
Route::post('/assets/email-templates/update', 'Assets\EmailTemplateController@update')->name('user.assets.templates.update');
Route::delete('/assets/email-templates/delete', 'Assets\EmailTemplateController@delete')->name('user.assets.templates.delete');
Route::post('/assets/email-templates/clone', 'Assets\EmailTemplateController@clone')->name('user.assets.templates.clone');

Route::get('/campaigns/emails', 'EmailCampaignController@index')->name('user.campaigns.emails');
Route::get('/campaigns/emails/create', 'EmailCampaignController@createEmailCampaign')->name('user.campaigns.emails.create');
Route::get('/campaigns/emails/edit/{campaignId}', 'EmailCampaignController@editEmailCampaign')->name('user.campaigns.emails.edit');

Route::post('/campaigns/emails/save', 'EmailCampaignController@saveEmailCampaign')->name('user.campaigns.emails.save');
Route::post('/campaigns/emails/publish', 'EmailCampaignController@publishEmailCampaign')->name('user.campaigns.emails.publish');
Route::post('/campaigns/emails/test', 'EmailCampaignController@testEmailCampaign')->name('user.campaigns.emails.test');


Route::get('/automations/emails', 'AutomationController@emails')->name('user.automations.emails');
Route::get('/automations/emails/create', 'AutomationController@createEmailCampaign')->name('user.automations.emails.create');
Route::get('/automations/emails/edit/{campaignId}', 'AutomationController@editEmailCampaign')->name('user.automations.emails.edit');


Route::post('/automations/emails/save', 'AutomationController@saveEmailCampaign')->name('user.automations.emails.save');
Route::post('/automations/emails/publish', 'AutomationController@publishEmailCampaign')->name('user.automations.emails.publish');
Route::post('/automations/emails/test', 'AutomationController@testEmailCampaign')->name('user.automations.emails.test');

Route::get('/automations/workflows', 'AutomationController@workflows')->name('user.automations.workflows');
Route::get('/automations/workflows/create', 'AutomationController@createWorkflowCampaign')->name('user.automations.workflows.create');
Route::get('/automations/workflows/edit/{workflowId?}', 'AutomationController@editWorkflowCampaign')->name('user.automations.workflows.edit');

Route::post('/automations/workflows/save', 'AutomationController@saveWorkflow')->name('user.automations.workflows.save');
Route::post('/automations/workflows/node/save', 'AutomationController@saveNode')->name('user.automations.workflows.node.save');
Route::post('/automations/workflows/node/update', 'AutomationController@updateNode')->name('user.automations.workflows.node.update');

Route::delete('/automations/workflows/delete', 'AutomationController@deleteWorkflow')->name('user.automations.workflows.delete');

Route::get('/text-to-speech', 'SpeechController@index')->name('user.text-to-speech');
Route::post('/text-to-speech/convert', 'SpeechController@convert')->name('user.text-to-speech.convert');
Route::get('/text-to-speech/download/{speechId?}', 'SpeechController@download')->name('user.text-to-speech.download');
Route::delete('/text-to-speech/delete', 'SpeechController@delete')->name('user.text-to-speech.delete');

Route::get('/test/speech', 'TestController@index');

Route::get('/deals', 'DealsController@index')->name('user.deals');
Route::post('/deals/create', 'DealsController@create')->name('user.deals.create');
Route::get('/deals/get', 'DealsController@getDeals')->name('user.deals.get');
Route::post('/deals/edit', 'DealsController@edit')->name('user.deals.edit');
Route::post('/deals/delete', 'DealsController@delete')->name('user.deals.delete');

Route::get('/tasks', 'TasksController@index')->name('user.tasks');
Route::post('/tasks/create', 'TasksController@create')->name('user.tasks.create');
Route::get('/tasks/get', 'TasksController@getTasks')->name('user.tasks.get');
Route::post('/tasks/edit', 'TasksController@edit')->name('user.tasks.edit');
Route::post('/tasks/delete', 'TasksController@delete')->name('user.tasks.delete');

Route::get('page-not-found', function () {
	$data = [
        	'page' => 'notfound',
        	'sub' => ''
        ];
    return view("App.notfound", $data);
})->name('not-found');

